#include <stdio.h>
#include <gcc-plugin.h>
#include <basic-block.h>
#include <cgraph.h>
#include <ggc.h>
#include <output.h>
#include <tree.h>
#include <simple-object.h>
#include "crap.h"
#include "crap_local.h"


/* Type info
 *
 * '<offset_from_parent, size, is_member, is_pointer,
 *   type_info_table_index, n_members, flags, name, symboldname>'
 */
#define CRAP_EXPORT_TYPE_INFO_SECTION "."TAG"_type_info"
#define TYPE_INFO_FORMAT "<%d,%d,%d,%d,%d,%d,%u,%s,%s>"
#define TYPE_INFO_SSCANF_FORMAT "<%d,%d,%d,%d,%d,%d,%u,%[^>]s>"


/* Function info
 *
 *  '<stack_frame_size, n_var_infos, func_name_symbol, [var_infos]>'
 *  var_info is [offset_from_fp, is_pointer, type_info_table_index ]
 */
#define CRAP_EXPORT_FUNC_INFO_SECTION "."TAG"_func_info"
#define FUNC_INFO_FORMAT "<%lu,%d,%s,"
#define FUNC_INFO_SSCANF_FORMAT "<%lu,%d,%[^,]s,"
#define VAR_INFO_FORMAT "[%ld,%d,%d]"


/* Call info
 *
 * '<return_pt_label, func_info_id, [regs]'
 * regs is [regnum, type_info_table_index]'
 */
#define CRAP_EXPORT_CALL_INFO_SECTION "."TAG"_call_info"
#define CALL_INFO_FORMAT "<%s,%d,"
#define REG_INFO_FORMAT "[%d,%d]"


/* Global info
 *
 * '<symbol, is_pointer, type_info_table_index>'
 */
#define CRAP_EXPORT_GLOBAL_INFO_SECTION "."TAG"_global_info"
#define GLOBAL_INFO_FORMAT "<%s,%d,%d>"


static void import_type_info(crap_t *ctx, const char *buf)
{
    int offset_from_parent, size, is_pointer, is_member;
    int type_info_table_index, n_members;
    type_info_t *ti;
    unsigned flags;
    char str[64], sym[64], strbuffer[256];

    /* For each type info "< ... >" */
    while (*buf)
    {
        sscanf(buf, TYPE_INFO_SSCANF_FORMAT, &offset_from_parent, &size,
               &is_member, &is_pointer,
               &type_info_table_index, &n_members, &flags, strbuffer);
        strncpy(str, strtok(strbuffer, ","), sizeof(str));
        strncpy(sym, strtok(NULL, ">"), sizeof(sym));

        if (strchr(str, '>'))
          *strchr(str, '>') = '\0';

        ti = VEC_safe_push(type_info_t, heap, ctx->type_infos, NULL);
        memset(ti, 0, sizeof(type_info_t));
        ti->offset_from_parent = offset_from_parent;
        ti->size = size;
        ti->is_member = is_member;
        ti->is_pointer = is_pointer;
        ti->type_info_table_index = type_info_table_index;
        ti->n_members = n_members;
        ti->flags = flags;
        ti->name = ggc_strdup(str);
        ti->id_symbol = get_identifier(ggc_strdup(sym));

        /* Next entry */
        buf = strchr(buf, '>');
        ++buf;
    }
}


static void export_type_info(const vec_type_info_t types)
{
    unsigned i, len;
    type_info_t *ti;
    section *sec;
    char str[256];

    /* Add analysis data to .rbmm_export section in the resulting .o file */
    sec = get_section(CRAP_EXPORT_TYPE_INFO_SECTION, SECTION_DEBUG, NULL);
    switch_to_section(sec);

    FOR_EACH_VEC_ELT(type_info_t, types, i, ti)
    {
        /* Generate binary string */
        len = snprintf(str, sizeof(str), TYPE_INFO_FORMAT,
                       ti->offset_from_parent, ti->size, 
                       ti->is_member, ti->is_pointer,
                       ti->type_info_table_index, ti->n_members, ti->flags,
                       ti->name,
                       IDENTIFIER_POINTER(ti->id_symbol));

        if (len >= sizeof(str))
          abort();

        assemble_string(str, strlen(str));
    }
}


static void import_func_info(crap_t *ctx, const char *buf)
{
    int i, n_var_infos, is_ptr, ti_idx;
    long offset_from_fp;
    tree id;
    var_info_t *vi;
    func_info_t *fi;
    struct cgraph_node *node;
    unsigned long stack_frame_size;
    char name[128] = {0};

    while (*buf)
    {
        sscanf(buf, FUNC_INFO_SSCANF_FORMAT,
               &stack_frame_size, &n_var_infos, name);
        fi = VEC_safe_push(func_info_t, heap, ctx->func_infos, NULL);
        memset(fi, 0, sizeof(func_info_t));
        fi->stack_frame_size = stack_frame_size;
        fi->name = ggc_strdup(name);
        id = get_identifier(name);

        /* Find decl */
        for (node=cgraph_nodes; node; node=node->next)
        {
            if (DECL_NAME(node->decl) == id)
            {
                fi->func_decl = node->decl;
                break;
            }
        }

        for (i=0; i<n_var_infos; ++i)
        {
            buf = strchr(buf, '[');
            sscanf(buf, VAR_INFO_FORMAT, &offset_from_fp, &is_ptr, &ti_idx);
            buf = strchr(buf, ']') + 1;
            vi = VEC_safe_push(var_info_t, heap, fi->var_info, NULL);
            memset(vi, 0, sizeof(var_info_t));
            vi->u.offset_from_fp = offset_from_fp;
            vi->is_pointer = is_ptr;
            vi->ti = VEC_index(type_info_t, ctx->type_infos, ti_idx);
            vi->name = "IMPORTED";
        }

        /* Next entry */
        buf = strchr(buf, '>') + 1;
    }
}


static void export_func_info(const vec_func_info_t fns)
{
    unsigned i, j, len, tmp_len, n_var_infos;
    var_info_t *vi;
    func_info_t *fi;
    section *sec;
    char str[256], tmp[64];

    /* Add analysis data to ELF section in the resulting .o file */
    sec = get_section(CRAP_EXPORT_FUNC_INFO_SECTION, SECTION_DEBUG, NULL);
    switch_to_section(sec);

    FOR_EACH_VEC_ELT(func_info_t, fns, i, fi)
    {
        /* Count valid var infos */
        n_var_infos = 0;
        FOR_EACH_VEC_ELT(var_info_t, fi->var_info, j, vi)
          if (vi->ti  && vi->u.offset_from_fp != BAD_OFFSET)
            ++n_var_infos;

        /* First part of func info string */
        len = snprintf(str, sizeof(str), FUNC_INFO_FORMAT,
            fi->stack_frame_size, n_var_infos, fi->name);

        /* Now add the var infos */
        FOR_EACH_VEC_ELT(var_info_t, fi->var_info, j, vi)
        {
            if (!vi->ti || vi->u.offset_from_fp == BAD_OFFSET)
              continue;

            tmp_len = snprintf(tmp, sizeof(tmp), VAR_INFO_FORMAT,
                               vi->u.offset_from_fp,
                               vi->is_pointer,
                               vi->ti->type_info_table_index);

            if (tmp_len + len > sizeof(str))
              abort();

            strncat(str, tmp, tmp_len);
            len += tmp_len;
        }

        if (len+1 >= sizeof(str))
          abort();

        strcat(str, ">");
        assemble_string(str, strlen(str));
    }
}


static void import_global_info(crap_t *ctx, const char *buf)
{
    int is_ptr, ti_idx;
    char str[128];

    if (!buf)
      return;

    while (*buf)
    {
        sscanf(buf, GLOBAL_INFO_FORMAT, str, &is_ptr, &ti_idx);

        /* Next entry */
        buf = strchr(buf, '>') + 1;
        //TODO add identifier nodes to global_info
    }
}


static void export_global_info(crap_t *ctx)
{
    unsigned i, len;
    tree global;
    section *sec;
    const type_info_t *ti;
    char str[32];
    
    /* Add analysis data to .rbmm_export section in the resulting .o file */
    sec = get_section(CRAP_EXPORT_TYPE_INFO_SECTION, SECTION_DEBUG, NULL);
    switch_to_section(sec);

    FOR_EACH_VEC_ELT(tree, ctx->global_vars, i, global)
    {
        ti = get_type_info(ctx, global, false);
        len = snprintf(str, sizeof(str), GLOBAL_INFO_FORMAT,
                       get_name(global), POINTER_TYPE_P(global),
                       ti->type_info_table_index);

        if (len >= sizeof(str))
          abort();

        assemble_string(str, strlen(str));
    }
}


static void import_label_layout_info(crap_t *ctx, const char *buf)
{
    int regnum, fi_idx, ti_idx;
    char *label, buffer[256];
    const char *next;
    reg_info_t *ri;
    call_info_t *ci;

    while (*buf)
    {
        strncpy(buffer, buf, (uintptr_t)strchr(buf,'>') - (uintptr_t)buf);
        label = strtok(buffer, ",") + 1;
        fi_idx = atoi(strtok(NULL, ","));

        ci = VEC_safe_push(call_info_t, heap, ctx->call_infos, NULL);
        memset(ci, 0, sizeof(call_info_t));
        ci->caller_func = VEC_index(func_info_t, ctx->func_infos, fi_idx);

        // FIXME
        //lbl_decl = create_artificial_label(UNKNOWN_LOCATION);
        //lbl
        //DECL_NAME(lbl_decl) = get_identifier(label);
        //DECL_EXTERNAL(lbl_decl) = 1;
        //ci->return_callsite_addr = build1(ADDR_EXPR, ptr_type_node, lbl_decl);
        //rtx ignore;
        //ci->return_callsite_addr = crap_create_callsite_label(ci, &ignore);

        next = strchr(buf, '<');
        while (strchr(buf, '[') && (strchr(buf, '[') < next))
        {
            buf = strchr(buf, '[');
            sscanf(buf, REG_INFO_FORMAT, &regnum, &ti_idx);
            buf = strchr(buf, ']') + 1;
            ri = VEC_safe_push(reg_info_t, heap, ci->register_info, NULL);
            memset(ri, 0, sizeof(call_info_t));
            ri->u.reg = regnum;
            ri->ti = VEC_index(type_info_t, ctx->type_infos, ti_idx);
        }

        /* Next entry */
        buf = strchr(buf, '>') + 1;
    }
}


static void export_label_layout_info(const crap_t *ctx)
{
    unsigned i, j, len, tmp_len;
    reg_info_t *ri;
    call_info_t *ci;
    section *sec;
    char str[256], tmp[32];

    /* Add analysis data to ELF section in the resulting .o file */
    sec = get_section(CRAP_EXPORT_CALL_INFO_SECTION, SECTION_DEBUG, NULL);
    switch_to_section(sec);

    FOR_EACH_VEC_ELT(call_info_t, ctx->call_infos, i, ci)
    {
        /* First part of call info string */
        len = snprintf(str, sizeof(str), CALL_INFO_FORMAT, 
            get_name(ci->return_callsite_addr), ci->caller_func->id);

        FOR_EACH_VEC_ELT(reg_info_t, ci->register_info, j, ri)
        {
            tmp_len = snprintf(tmp, sizeof(tmp), REG_INFO_FORMAT,
                               ri->u.reg, ri->ti->type_info_table_index);

            if (tmp_len + len > sizeof(str))
              abort();

            strncat(str, tmp, tmp_len);
            len += tmp_len;
        }

        if (len+1 >= sizeof(str))
          abort();

        strcat(str, ">");
        assemble_string(str, strlen(str));
    }
}


static char *get_section(const char *fname, const char *section_name)
{
    int err, fd;
    off_t xset, len;
    char *buf;
    ssize_t bytes;
    const char *errmsg;
    simple_object_read *sor;
    
    if ((fd = open(fname, O_RDONLY)) == -1)
      return NULL;

    /* Get the data from the module */
    if (!(sor = simple_object_start_read(fd, 0, section_name, &errmsg, &err)))
    {
        close(fd);
        return NULL;
    }
    
    /* Get the section */
    if (!(simple_object_find_section(
          sor, section_name, &xset, &len, &errmsg, &err)))
    {
        close(fd);
        return NULL;
    }
    simple_object_release_read(sor);

    buf = (char *)xcalloc(1, len + 1);    
    lseek(fd, xset, SEEK_SET);
    if ((bytes = read(fd, buf, len)) == -1)
      abort();

    close(fd);
    return buf;
}


/* Adjust the type and func info offsets so that we can affix the types and func
 * info to the end of the original context.
 */
static void update_offsets(const crap_t *orig, crap_t *newctx)
{
    int i, off;
    type_info_t *ti;

    off = VEC_length(type_info_t, orig->type_infos);
    FOR_EACH_VEC_ELT(type_info_t, newctx->type_infos, i, ti)
      ti->type_info_table_index += off;
}


/* Concatenate b onto a */
static void catenate_contexts(crap_t *a, const crap_t *b)
{
    unsigned i;
    tree global;
    type_info_t *ti;
    func_info_t *fi;
    call_info_t *ci;

    FOR_EACH_VEC_ELT(type_info_t, b->type_infos, i, ti)
        VEC_safe_push(type_info_t, heap, a->type_infos, ti);
        
    FOR_EACH_VEC_ELT(func_info_t, b->func_infos, i, fi)
        VEC_safe_push(func_info_t, heap, a->func_infos, fi);

    FOR_EACH_VEC_ELT(call_info_t, b->call_infos, i, ci)
        VEC_safe_push(call_info_t, heap, a->call_infos, ci);

    FOR_EACH_VEC_ELT(tree, b->global_vars, i, global)
        VEC_safe_push(tree, heap, a->global_vars, global);
}


void crap_import_info(crap_t *context, const char *fname)
{
    char *buf;
    crap_t *newctx;

    /* New context to add all the imported data to */
    newctx = crap_new_context();
    crap_plug_set_context(newctx);

    /* Import the type information */
    buf = get_section(fname, CRAP_EXPORT_TYPE_INFO_SECTION);
    import_type_info(newctx, buf);
    free(buf);

    /* Import the function information */
    buf = get_section(fname, CRAP_EXPORT_FUNC_INFO_SECTION);
    import_func_info(newctx, buf);
    free(buf);
    
    /* Import the global information */
    buf = get_section(fname, CRAP_EXPORT_GLOBAL_INFO_SECTION);
    import_global_info(newctx, buf);
    free(buf);

    /* Import the label layout (call) information */
    buf = get_section(fname, CRAP_EXPORT_CALL_INFO_SECTION);
    import_label_layout_info(newctx, buf);
    free(buf);

    /* Update type info table offsets */
    update_offsets(context, newctx);

    /* Affix the vectors */
    catenate_contexts(context, newctx);

    crap_plug_set_context(context);
}


void crap_export_info(crap_t *context)
{
    export_global_info(context); /* Must be first it might create type infos */
    export_type_info(context->type_infos);
    export_func_info(context->func_infos);
    export_label_layout_info(context);
}
