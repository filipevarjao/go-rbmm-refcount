FROM gcc474:latest

RUN apt-get update && \
	apt-get install -y locate libgmp3-dev nano gdb wget

RUN mkdir -p /usr/libmago
WORKDIR /usr/libmago
COPY libmago/ /usr/libmago
RUN make

RUN wget --no-check-certificate https://dl.google.com/go/go1.9.4.linux-amd64.tar.gz
RUN tar -xvf go1.9.4.linux-amd64.tar.gz
RUN cp -r go /usr/local
ENV GOROOT=/usr/local/go
ENV GOROOT_BOOTSTRAP=/usr/libmago/go

ENV PATH="/usr/local/go/bin:${PATH}"
RUN mkdir /usr/tmp

CMD ["/bin/bash"]
