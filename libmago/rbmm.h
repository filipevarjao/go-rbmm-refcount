#ifndef _RBMM_GCC_PLUGIN_H
#define _RBMM_GCC_PLUGIN_H

#include <function.h>
#include <gimple.h>
#include <vec.h>
#include "libcrap/crap.h"

#undef TAG
#define TAG "rbmm"


/* Suffix appended to function names for functions we transform */
#define RBMM_FN_SUFFIX "__rbmm"


/* 
 * Debuging macros
 */
#ifdef DEBUG
#define DEBUG_ALL     1  /* All leveles of debugging accept this         */
#define DEBUG_ANAL    2  /* Output analysis information (very annoying)  */
#define DEBUG_OBJ     4  /* Output analysis data being put into the .o   */
#define DEBUG_GIMPLE  8  /* Output gimple related data                   */
#define DEBUG_IMPORT  16 /* Output import related data                   */
#define DEBUG_PARAMS  32 /* Output the function params and region params */
#define DEBUG_MERGE   64 /* Output the merging data                      */
#define DEBUG_FNPTR  128 /* Output when the address of a func is taken   */
#define DEBUG_GC     256 /* Output type and garbage collection info      */


#define RBMM_DEBUG_NL() putc('\n', stdout)
#define RBMM_DEBUG_HDR "[" TAG "] "


#define RBMM_DEBUG_BLOCK(lvl, _x)  \
{                                  \
    if (lvl & (DEBUG | DEBUG_ALL)) \
      {_x}                         \
}


#define _RBMM_DEBUG(lvl, nl, tag, ...)         \
{                                              \
    if (lvl & (DEBUG | DEBUG_ALL))             \
    {                                          \
        if (tag)                               \
          printf(RBMM_DEBUG_HDR  __VA_ARGS__); \
        else                                   \
          printf(__VA_ARGS__);                 \
        if (nl)                                \
          RBMM_DEBUG_NL();                     \
        fflush(NULL);                          \
    }                                          \
}


#define RBMM_DEBUG(lvl, ...)       _RBMM_DEBUG(lvl, 1, 1, __VA_ARGS__) 
#define RBMM_DEBUG_NO_NL(lvl, ...) _RBMM_DEBUG(lvl, 0, 1, __VA_ARGS__)
#define RBMM_DEBUG_PLAIN(lvl, ...) _RBMM_DEBUG(lvl, 0, 0, __VA_ARGS__)

#else /* Debuging not enabled */

#define RBMM_DEBUG_NL()
#define RBMM_DEBUG(...)
#define RBMM_DEBUG_NO_NL(...)
#define RBMM_DEBUG_PLAIN(...)
#define RBMM_DEBUG_LOC(ignored)
#define RBMM_DEBUG_BLOCK(fredflintstone, ignored)

#endif /* DEBUG */


/* The region type used in the executable (the region variable) (rbmm_region.c) */
extern tree __rbmm_region_type;


/* Used for our analysis allowing us to reason about what regions will be used
 * per function
 */
typedef struct _ssa_name_info_d *ssa_name_info_t;
DEF_VEC_P(ssa_name_info_t);
DEF_VEC_ALLOC_P(ssa_name_info_t, heap);
DEF_VEC_ALLOC_P(ssa_name_info_t, gc);
struct  _ssa_name_info_d
{
    const_tree ssaname;

    /* Identifier node for ssaname */
    const_tree identifier;

   /* Only for parent, or base structures not fields of a struct
    * If we want to consider the base/parent node separately such as:
    * foo = bar;
    * 'foo' would be considered in our anaylsis and this should be set to '1'
    * This is only for parent nodes.  Likewise, if we only have something like:
    * foo->bar = blah;
    * We only know about foo->bar and not foo, so set this flag to '0'
    */
    ssa_name_info_t base;

    /* What region this name belongs to */
    int region_uid;

    /* This represents all unifications/assignments of the same data.
     * When we get "a = b;" both 'a' and 'b' would have the same 'unified-id'
     * But, a = b.x; both 'a' and 'x' would have the same unified-id but 'b'
     * would be different.
     *
     * The master that all of these temporaries refer to is 'unified_to'
     */
    int unified_id;
    ssa_name_info_t unified_to;
#define UNIFIED_TO(_x) ((_x)->unified_to ? (_x)->unified_to : (_x))

    /* Child nodes/members */
    VEC(ssa_name_info_t,heap) *members;

    /* Bitmasked: see macros below */
    unsigned aliased_state;

    /* Param that might alias this name and its index in the funcs arg list */
    const_tree aliased_by_decl;
    int param_decl_idx;

    /* If this name is dereferenced this is the dereferenced info
     * *xyz = this_info
     * This name below represents 'xyz'
     */
    tree non_void_type;

    /* Vector of basic blocks this puppy is alive in */
    VEC(basic_block,heap) *live_bbs;

    /* Forms the points-to graph */
    VEC(ssa_name_info_t,heap) *points_to;

    /* 'points_to' can have cycles, use this member to avoid processing cycles
     * and blowing up the stack see 'clear_marks' in rbmm.c to clear all marked
     * names in a function.
     */
    bool marked;
}; 


/* Bitmask values for the ssa_name_info_t 'aliased_state' field
 * PARAM     -- If the formal params of the function alias the ssaname
 * CALLPARAM -- If we use the ssaname in a function call within the function
 * being analyized.
 */
#define ALIASED_BY_NONE       0 /* Set if the ssaname is aliased by nothing  */
#define ALIASED_BY_GLOBAL     1 /* Set if the ssaname is aliased by a global */
#define ALIASED_BY_PARAM      2 /* Set if the ssaname is aliased by a param  */
#define ALIASED_BY_RETURN     4 /* Set if the ssaname is aliased by a return */
#define ALIASED_BY_ALLOC      8 /* Set if the ssaname is allocated to        */
#define ALIASED_BY_CALLPARAM 16 /* Set if the ssaname is passed as a param   */
#define ALIASED_BY_SLICE     32 /* Set if the ssaname is allocd from a slice */


/* Predicates for the masks above: Input must be region_t or ssa_name_info_t */
#define ALIASED_BY_NONE_P(_x)      ((_x)->aliased_state == ALIASED_BY_NONE)
#define ALIASED_BY_GLOBAL_P(_x)    ((_x)->aliased_state & ALIASED_BY_GLOBAL)
#define ALIASED_BY_PARAM_P(_x)     ((_x)->aliased_state & ALIASED_BY_PARAM)
#define ALIASED_BY_RETURN_P(_x)    ((_x)->aliased_state & ALIASED_BY_RETURN)
#define ALIASED_BY_ALLOC_P(_x)     ((_x)->aliased_state & ALIASED_BY_ALLOC)
#define ALIASED_BY_CALLPARAM_P(_x) ((_x)->aliased_state & ALIASED_BY_CALLPARAM)
#define ALIASED_BY_SLICE_P(_x)     ((_x)->aliased_state & ALIASED_BY_SLICE)


/* For debug output */
static const struct {int value; const char *name;} aliases[] = 
{
    {ALIASED_BY_GLOBAL,    "global"},
    {ALIASED_BY_PARAM,     "input"},
    {ALIASED_BY_RETURN,    "returned"},
    {ALIASED_BY_ALLOC,     "alloc"},
    {ALIASED_BY_CALLPARAM, "call"},
    {ALIASED_BY_SLICE,     "slice"},
};
static const int N_ALIASES = sizeof(aliases) / sizeof(aliases[0]);


/* Types that could be placed into regions */
#define REGION_CANDIDATE_TYPE_P(_x) (POINTER_TYPE_P(_x)||AGGREGATE_TYPE_P(_x))


/* If a region has an alias to a global variable, this will be true.  The code
 * generated for a global region (only one global region) will always have the
 * same region_var tree node.  So while many regions (in our internal analysis)
 * might exist and be aliased by a global.  They all share the same region_var
 * tree node, so anytime we instrument code for that region, we use that node.
 * Only ever one global region, but our analysis might have numerous regions
 * (all sharing the same tree node).  We could fold these into one internal
 * representation, but thats not necessary right now.
 */
#define GLOBAL_REGION_P ALIASED_BY_GLOBAL_P


/* We identify temproary regions at compile time as having a negative id */
#define TEMP_REGION_P(_x) ((_x)->id < 0)


/* Region information */
struct GTY(()) _region_d
{
    int id;
    
    /* Region variable and its index in funcs arg list.  There are cases when
     * data is allocated to the global region and that object is returned.  An
     * object returning from a function that is aliased to a global.  We still
     * need a non -1 value since it is a valid region, in this special case, it
     * has a variable index into the function of -666.  Now, we never modify a
     * function prototype/param-list to take as input a global region.  That's
     * pointless, but we need  a way of returning -1 not-an-error value.  So use
     * negative number of the beast.
     */
#define GLOBAL_RETURN_REGION_VAR_IDX -666 
    tree region_var; 
    int region_var_idx;

    /* Index of the first parameter in the function decl that the region
     * variable is associated with.  A region might be for multiple params.
     */
    int param_idx;

    /* Bitmask reflecting the OR of all aliased states of the instances in
     * 'names'
     */
    unsigned aliased_state;

    /* Objects in this region */
    VEC(ssa_name_info_t,heap) *names;
    
    /* Vector of basic blocks this mofo is alive in */
    VEC(basic_block,heap) *live_bbs;

    /* Stmt this region was created in */
    gimple created_stmt;

    /* Counters for the number of times this fn is incremented/decremented in
     * the function.
     */
    int n_protection_increments;
    int n_protection_decrements;

    /* kill_regions() must add a call to remove region on this instance */
    bool force_region_removal;

    /* True if this region is passed to a fn as an argument to that fn:
     * This is only for kill_regions()
     */
    bool is_passed_as_argument;

	/* Unique data types that can be produced from this region */
    VEC(tree,gc) *unique_types;
};
typedef struct _region_d *region_t;


/* Map of region to region merge.
 * The child merges into the parent (scary concept...)
 */
struct _merge_d
{
    region_t parent, child;
    gimple stmt;
    gimple_stmt_iterator gsi; /* Where merge insertion should go before */
    bool use_stmt;            /* Use the stmt instead of the gsi above  */
};
typedef struct _merge_d merge_t;
DEF_VEC_O(merge_t);
DEF_VEC_ALLOC_O(merge_t, gc);


/* SSA name to region map (for convience) */
struct _ssa_name_to_reg_d
{
    ssa_name_info_t info;
    region_t region;

    /* Type hash for the type the region is used for (gcc/tree.h: TYPE_HASH)
     * NOTE: This is now an index into the parent type.  Field '1' means the
     * first field of the parent type
     */
    int member_index;

    /* This should almost always be true.  This flag means that this 'region' is
     * used as a formal paramter in the function's prototoype.  We have this
     * flag, since there are cases where two different variables will use the
     * same region, to avoid haveing the region reprsented twice as a formal
     * parameter, we only need to pass it once.  No need to be redundant.
     */
    bool use_as_formal_param;
};
typedef struct _ssa_name_to_reg_d ssa_name_to_reg_t;
DEF_VEC_O(ssa_name_to_reg_t);
DEF_VEC_ALLOC_O(ssa_name_to_reg_t, gc);


/* Maps function formal parameters to region variables and to which index into
 * the function prototype these badboys live.
 */
struct _param_to_region_d
{
    const_tree                 param_decl; /* Param decl node         */
    ssa_name_info_t            param_info; /* Analysis data for param */
    VEC(ssa_name_to_reg_t,gc) *regions;    /* Regions for this param  */
};
typedef struct _param_to_region_d param_to_reg_t;
DEF_VEC_O(param_to_reg_t);
DEF_VEC_ALLOC_O(param_to_reg_t, gc);


/* Represents data we know for each function */
DEF_VEC_P(region_t);
DEF_VEC_ALLOC_P(region_t, heap);
DEF_VEC_ALLOC_P(region_t, gc);
struct _rbmm_fn_data_d 
{
    tree func_decl; /* Function decl */

    /* Function name identifier node */
    const_tree func_name;

    /* We clone the function we want to transform (has __rbmm suffix) */
    bool is_clone;

    /* If this function information is from another rbmm-compiled library */
    bool is_imported;

    /* This value is set in the original, non-clone node.  'clone_decl' is the
     * cloned instance.
     */
    tree clone_decl;

    /* Once this function has been analyized intraprocedurally (pass 1) */
    bool intraprocedurally_analyized;

    /* Number of original formal parameters before transformation */
    int n_orig_params;
    int n_region_var_params;

    /* True if we have added the necessary temp variable for the global region
     * to be accessed through.  This is only necessary if this function makes
     * use of a global region.  This is false otherwise.
     */
    bool global_temp_var;

    /* True if this function calls itself */
    bool recursive;

    /* True if this function should not be transformed */
    bool do_not_region_enable;
    
    /* True if this function is used by a function pointer */
    bool used_by_fnptr;

    /* True if this is the specialized version of the function */
    bool is_specialized_version;

    /* Analysis for the types used in this function */
    VEC(ssa_name_info_t,heap) *names;

    /* Regions in this function */
    VEC(region_t,heap) *regions;

    /* Data about merging: for the transformation pass */
    VEC(merge_t,gc) *merge_data;

    /* Map formal parameters to regions */
    VEC(param_to_reg_t,gc) *parammap;
    param_to_reg_t *returned_parammap;

    /* Vector of fndecl nodes who are external to this function, yet their
     * address is taken.  Declearation node for an extern function whose address
     * is taken/made-use-of in this function.
     */
    VEC(tree,gc) *needed_extern_fnptrs;
};
typedef struct _rbmm_fn_data_d *rbmm_fn_data_t;


/* Global region initalized in rbmm.c */
extern region_t global_region;


/* Global vector of all analyized functions in the program */
DEF_VEC_P(rbmm_fn_data_t);
DEF_VEC_ALLOC_P(rbmm_fn_data_t, heap);
typedef VEC(rbmm_fn_data_t,heap) *analyized_fn_vec_t;


/* Create and add to regions (rbmm_region.c) */
extern region_t new_region_info(rbmm_fn_data_t fn, unsigned region_id);
extern region_t new_temp_region_info(rbmm_fn_data_t fn, tree type);
extern void add_to_region(
    const rbmm_fn_data_t  fn,
    region_t              reg,
    const ssa_name_info_t info);


/* Add basic block of the name to the live set of bb in reg (rbmm_region.c) */
extern void add_live_bbs(
    const rbmm_fn_data_t  fn,
    region_t              reg,
    const ssa_name_info_t info);


/* Uniquely add a region to a vector of regions (rbmm_region.c) */
extern void unique_add_to_region_vec(VEC(region_t,gc) **regions, region_t addme);


/* Builtins that get inserted into the source of a file being compiled
 * (rbmm_region.c)
 */
extern void init_rbmm_builtins(void);
extern void init_global_region_data(bool is_extern);
extern tree get_alloc_from_region_fndecl(void);
extern void create_global_region_bn(gimple stmt);
extern void create_region_bn(gimple stmt, region_t reg);
extern void create_map_bn(region_t reg, gimple stmt);
extern void map_index_bn(region_t reg, gimple stmt);
extern void slice1_make_bn(rbmm_fn_data_t fn, region_t reg, gimple stmt);
extern void slice2_make_bn(rbmm_fn_data_t fn, region_t reg, gimple stmt);
extern void append_bn(region_t reg, gimple stmt);
extern void construct_map_bn(region_t reg, gimple stmt);
extern void alloc_from_region_bn(
    const rbmm_fn_data_t fn,
    gimple               stmt,
    const region_t       reg,
    tree                 lhs,
    tree                 size);
#ifdef RBMM_GC
extern void alloc_slice_values_bn(rbmm_fn_data_t fn, region_t reg,gimple stmt);
#endif
extern void remove_region_bn(region_t reg, basic_block bb);
extern void incdec_ref_count_bn(region_t reg, gimple stmt, bool decrement);
extern void print_stats_bn(gimple stmt);
extern void trampoline_bn(rbmm_fn_data_t fn, gimple stmt);
#ifdef ENABLE_RUNTIME_MERGING
extern void merge_region_bn(region_t parent, region_t child, gimple stmt);
#endif


/* Finding region information (rbmm_region.c) */
extern region_t find_region_by_id(const rbmm_fn_data_t fn, unsigned region_id);
extern region_t find_region_by_decl(const rbmm_fn_data_t fn, const_tree decl);
extern region_t find_global_region_reference(const rbmm_fn_data_t fn);
extern region_t find_region_by_fncall_idx(
    const rbmm_fn_data_t fn,
    int                  region_idx);
extern VEC(region_t,gc) *find_regions_by_param_idx(
    const rbmm_fn_data_t fn,
    int                  param_index);


/* Init the global region var (tree) used at compile time (rbmm_region.c) */
extern void init_global_region();


/* Modify the passed function by adding a parameter (rbmm_region.c) */
extern void add_region_param(rbmm_fn_data_t fn, tree fn_decl, region_t reg);


/* Move the creation point for region 'reg' to basic block 'bb' */
extern void move_region_creation(region_t reg, basic_block bb);


/* Modify an existing func call adding the region as an arg (rbmm_region.c)
 * The retuned value is the new version of the function call statement.
 */
extern gimple add_region_arg_to_fncall(
    region_t reg,
    int      reg_idx,
    gimple   call,
    tree     param);


/* Returns true of 'bb' is marked as being one of the last live blocks in reg */
extern bool region_live_after_p(const region_t reg, gimple stmt);


/* Returs true if there is an edge between src and dest (rbmm_region.c) */
extern bool can_reach(const_basic_block src, const_basic_block dest);


/* Debugging */
extern void print_region_info(VEC(region_t,heap) *regions);


/* Get the rbmm or name data for a specific function (rbmm.c) */
extern rbmm_fn_data_t find_fn_data(const_tree fndecl);


/* Return the proper tree node associated to the ssaname (rbmm.c) */
extern const_tree get_ssaname_decl(const ssa_name_info_t info);
extern const char *get_ssaname(const ssa_name_info_t info);


/* Return the tree node associated to a node such as 'base->member' (rbmm.c) */
extern tree get_base(tree cmpnt);
extern tree get_member(const_tree cmpnt);


/* Return the PARAM_DECL node for the function decl (rbmm_region.c) */
extern tree get_param(tree fndecl, int param_idx);


/* Find the ssaname given a tree node (rbmm.c) */
extern ssa_name_info_t find_ssa_name_info(
    rbmm_fn_data_t fn,
    const_tree     base,
    const_tree     member);
extern ssa_name_info_t find_ssa_name_in_region(
    const region_t reg,
    const_tree     decl);
extern VEC(ssa_name_info_t,gc) *find_names_by_unified_id(
    const rbmm_fn_data_t fn,
    int                  unified_id);


/* Find a non-void ptr type associated to the info (points-to assoc) (rbmm.c) */
extern tree find_non_void_ptr(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t info);


/* Return a vector of all of the nodes that have been assigned to or points-to
 * the one passed in.  Free the vec when done. (rbmm.c)
 */
extern VEC(ssa_name_info_t,gc) *get_associated_members(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t info);


/* Create a temp variable before 'stmt', and assign the global region to it */
extern tree assign_global_to_temp(gimple stmt);


/* Count the number of parameters in a func decl (rbmm_region.c) */
extern int count_fnparams(const_tree fndecl);


/* Return true if the fndecl passed as input is for a builtin we have added */
extern bool is_rbmm_builtin(const_tree fndecl);


/* Return true if the type is a slice "go frontend specific" */
extern bool is_slice_type_p(const_tree node);


/* Returns what the '__values' member is (what the slice contains) */
extern const_tree get_slice_type(const_tree type);


/* Returns a vector of any names in the function that point to the passed
 * (rbmm.c)
 */
VEC(ssa_name_info_t,gc) *get_who_points_to(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t info);


/* Calculate each basic block in fn, what other blocks they can access
 * (rbmm_region.c)
 */
extern void compute_reachability(const struct function *fn);
extern void print_reachability();
extern const_bitmap get_reaches(
    const struct function *fn,
    const_basic_block      bb);


/* Load a module used in the function call decl 'decl' (rbmm_import_export.c) */
extern bool import_module_used_in_decl(const_tree decl);


/* Export analysis data to object file (rbmm_import_export.c) */
extern void rbmm_emit_analysis(VEC(rbmm_fn_data_t,heap) *analyized_fns);


/* Return the analysis data we have stashed away from imported modules.
 * 'fn_name' is the function name of the function whose analysis data is to be
 * retrieved (rbmm_import.c).
 */
extern rbmm_fn_data_t find_imported_fn_data(const char *fn_name);


/* Merge regions where a ssaname is in both regions.  This case occurs from the
 * interprocedural analysis when function calls are analyzed in each function.
 * (rbmm_region.c)
 */
extern void fixup_duplicate_names(rbmm_fn_data_t fn);


/* Clear the "has been visited marks on all names */
extern void clear_marks(rbmm_fn_data_t fn);


/* Remove a specific merge data element from the functions merge data vec
 * (rbmm.c)
 */
extern void remove_merge_data(rbmm_fn_data_t fn, merge_t *md);


/* Return 'true' if the tree node is a fnptr (rbmm.c) */
extern bool is_fnptr(const_tree node);


/* Given the tramp type find the original fndecl (rbmm_region.c) */
extern tree find_tramp_orig_fndecl(const_tree type);


/* Given the fndecl find the tramp fn declaration type (rbmm_region.c) */
extern tree find_tramp_fndecl(const_tree fndecl);


/* Create a function type (prototype) for a trampoline.  This returns a 
 * FUNCTION_TYPE  node, and not a pointer to a function type.
 */
extern tree create_trampoline_type(tree fndecl);


/* Dump the GIMPLE output for all RBMM trampolines created (rbmm_region.c) */
extern void print_trampolines(void);


/* Create function rbmm analysis data for a trampoline function.  We never
 * really analyze the functions we add ourself.  But we do need just a bit of
 * information for argument/formal-parameter matching and this function works
 * that. (rbmm.c)
 */
extern void create_tramp_fndata(tree tramp_fndecl);


/* Given a caller and callee determine if any of the callers region arguments
 * need to be merged at runtime. (rbmm.c)
 */
extern void detect_merges(
    rbmm_fn_data_t       caller, /* Set the merge data in this guy */
    const rbmm_fn_data_t callee,
    gimple               call,
    bool                 do_runtime_merging,
    bool                 for_tramp); /* True if the merge is in a trampoline */


/* Merge region food into region mouth (compile time merge) (rbmm_region.c) */
extern void merge_regions(rbmm_fn_data_t fn, region_t mouth, region_t food);

#ifdef RBMM_GC
/* Initalize for GC/Type information creation.  Must be called before any *gc*
 * routines (rbmm_gc.c)
 */
extern void rbmm_gc_init(void);


/* Import garbage collection info from a module (object) file (rbmm_gc.c) */
extern void rbmm_gc_import_info(const char *package_name);


/* Generate a list of type information data useable by the GC and for
 * sub-regions.  This visits each region in the function 'fn' (rbmm_gc.c)
 */
extern void rbmm_gc_create_type_info(rbmm_fn_data_t fn);


/* Add a type to the list of types the region contains.
 * This routine checks uniqueness (rbmm_gc.c)
 */
extern void rbmm_gc_add_unique_type(region_t reg, tree type);


/* Locate the maximum index of a type in the region.  The index refers to the
 * index in the global type information table (rbmm_gc.c)
 */
extern int rbmm_gc_find_max_type_index(const region_t reg);


/* Return the type information declaration for the type argument (rbmm_gc.c) */
extern const_tree rbmm_gc_find_subregion_type(const_tree type);


/* After 'rbmm_gc_create_type_info()' has generated all the type data.  We now
 * need to insert that information as a table into the program... this routine
 * accomplishes that. (rbmm_gc.c)
 */
extern void rbmm_gc_insert_type_info_table(void);


/* Number of elements in the type information table */
extern int rbmm_gc_n_type_infos(void);


/* Generate a decl that represents a series of type_info objects (from the
 * garbage collector).  This array can be used to initalize a region creation at
 * runtime.
 */
extern tree rbmm_gc_create_type_info_array(const region_t reg, gimple stmt);


/* Insert the type info and GC handling data into the binary. (rbmm_gc.c) */
extern void rbmm_gc_finalize(const analyized_fn_vec_t fns);


/* Debugging awesomeness (rbmm_gc.c) */
extern void rbmm_gc_debug(const rbmm_fn_data_t fn);

#else  /* Else RBMM_GC is not enabled */

#define rbmm_gc_init()
#define rbmm_gc_create_type_info(_fn) rbmm_gc_create_type_info_nogc((_fn))
#define rbmm_gc_create_type_info_array(_reg, _stmt) null_pointer_node
#define rbmm_gc_insert_type_info_table()
#define rbmm_gc_add_unique_type(_ignore0, _ignore1)
#define rbmm_gc_n_type_infos() 0
#define rbmm_gc_import_info(_ignore)
#define rbmm_gc_find_max_type_index(_ignore) 0
#define rbmm_gc_find_subregion_type(_ignore) ptr_type_node
#define rbmm_gc_finalize(_ignore)
#define rbmm_gc_debug(_ignore)

/* Add unique types to all regions does not use garbage collection type info */
extern void rbmm_gc_create_type_info_nogc(rbmm_fn_data_t fn);
#endif /* RBMM_GC */


#endif /* _RBMM_GCC_PLUGIN_H */
