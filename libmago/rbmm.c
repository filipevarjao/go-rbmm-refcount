#include <stdio.h>
#include <gcc-plugin.h>
#include <coretypes.h>
#include <tm.h>
#include <cfgloop.h>
#include <basic-block.h>
#include <ggc.h>
#include <gimple.h>
#include <tree.h>
#include <tree-dump.h>
#include <tree-pass.h>
#include <tree-flow.h>
#include <tree-flow-inline.h>
#include <ipa-prop.h>
#include "rbmm.h"


int plugin_is_GPL_compatible = 1;


/* Global region, one per program */
region_t global_region = NULL;


/* Vector of function analysis information */
analyized_fn_vec_t rbmm_analyized_fns;


/* Vector of functions which we also need to specialize.  This vector is built
 * from demand files (.demand) that other translation units need.
 * If another object file takes the address of a function in the object file
 * being processed, the function whose address is taken is a demand and in in
 * this list of (tree identifier nodes).
 */
VEC(tree,gc) *hospecialize;


/* Like asprintf(): Allocate and set the string, the return is a gcc garbage
 * collected item and does not need to be free'd
 */
#define string_copy(_str, _fmt, ...)      \
do                                        \
{                                         \
    char *_tmp;                           \
    asprintf(&_tmp, (_fmt), __VA_ARGS__); \
    const char *_s = ggc_strdup(_tmp);    \
    free(_tmp);                           \
    (_str) = _s;                          \
} while (0);


/* Find the function data for the given function */
rbmm_fn_data_t find_fn_data(const_tree fndecl)
{
    unsigned i;
    rbmm_fn_data_t fn;

    /* Avoid potential function pointers to functions only known at runtime */
    if (!fndecl || TREE_CODE(fndecl) == SSA_NAME || 
        TREE_CODE(fndecl) == VAR_DECL || TREE_CODE(fndecl) == INTEGER_CST)
      return NULL;

    FOR_EACH_VEC_ELT(rbmm_fn_data_t, rbmm_analyized_fns, i, fn)
      if (fndecl == fn->func_decl)
        return fn;

    /* Shit... we couldn't find anything, probably imported, check them */
    return find_imported_fn_data(get_name((tree)fndecl));
}


rbmm_fn_data_t find_fn_data_name(const char *fn_name)
{
    unsigned i;
    rbmm_fn_data_t fn;
    const_tree name = get_identifier(fn_name), orig_name, clone_name;

    FOR_EACH_VEC_ELT(rbmm_fn_data_t, rbmm_analyized_fns, i, fn)
    {
        orig_name = get_identifier(get_name(fn->func_decl));
        clone_name = (fn->clone_decl) ? 
            get_identifier(get_name(fn->clone_decl)) : NULL_TREE;

        if (name == orig_name || name == clone_name)
          return fn;
     }

    return find_imported_fn_data(fn_name);
}


const_tree get_ssaname_decl(const ssa_name_info_t info)
{
    if (DECL_P(info->ssaname) || !SSA_VAR_P(info->ssaname))
      return info->ssaname;
    else
      return SSA_NAME_VAR(info->ssaname);
}


/* Allocate data for our type analysis, which allows us to reason about region
 * creation, etc.
 */
static ssa_name_info_t create_ssa_name_info(const_tree name)
{
    static int unique_region_id_pool, unique_unified_id_pool;
    ssa_name_info_t new_info;
    const_tree id;

    /* Sanity check */
    if (!REGION_CANDIDATE_TYPE_P((TREE_TYPE(name))))
      abort();

    new_info = XCNEW(struct _ssa_name_info_d);
    new_info->ssaname = get_base((tree)name); /* Strip off SSA name instance */
    new_info->region_uid = ++unique_region_id_pool;
    new_info->unified_id = ++unique_unified_id_pool;
    new_info->param_decl_idx = -1;

    /* Use the identifier node (easy to id  members of a complex type) */
    if (TREE_CODE(name) == SSA_NAME)
      id = SSA_NAME_VAR(name);
    else
      id = name;

    if (get_name((tree)id))
      new_info->identifier = get_identifier(get_name((tree)id));
    
    return new_info;
}


/* Return 'true' if the node is a slice type.  There is no defined slice in
 * golang front end so we need to check all fields.
 */
bool is_slice_type_p(const_tree node)
{
    tree fields;

    if (!node)
      return false;

    if (DECL_P(node))
      node = TREE_TYPE(node);

    /* OK: I do not think the front end sets the artificial flag.  AFAIK it
     * should be, so we know that the type is compiler generated and not
     * actually a data type in the code we are parsing.
    if (TYPE_ARTIFICIAL(node) == 0)
      return false;
     */

    if (TREE_CODE(node) != RECORD_TYPE)
      return false;

    if (!(fields = TYPE_FIELDS(node)))
      return false;

    if (strcmp(IDENTIFIER_POINTER(DECL_NAME(fields)), "__values"))
      return false;

    fields = DECL_CHAIN(fields);
    if (strcmp(IDENTIFIER_POINTER(DECL_NAME(fields)), "__count"))
      return false;

    fields = DECL_CHAIN(fields);
    if (strcmp(IDENTIFIER_POINTER(DECL_NAME(fields)), "__capacity"))
      return false;

    return true;
}


static bool is_map_type_p(const_tree node)
{
    tree fields;

    if (!node)
      return false;

    if (DECL_P(node))
      node = TREE_TYPE(node);

    if (POINTER_TYPE_P(node))
      node = TREE_TYPE(node);

    if (TREE_CODE(node) != RECORD_TYPE)
      return false;

    fields = TYPE_FIELDS(node);
    if (strcmp(IDENTIFIER_POINTER(DECL_NAME(fields)), "__descriptor"))
      return false;

    fields = DECL_CHAIN(fields);
    if (strcmp(IDENTIFIER_POINTER(DECL_NAME(fields)), "__element_count"))
      return false;

    fields = DECL_CHAIN(fields);
    if (strcmp(IDENTIFIER_POINTER(DECL_NAME(fields)), "__bucket_count"))
      return false;
    
    fields = DECL_CHAIN(fields);
    if (strcmp(IDENTIFIER_POINTER(DECL_NAME(fields)), "__buckets"))
      return false;

    return true;
}


tree find_non_void_ptr(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t info)
{
    unsigned i;
    ssa_name_info_t pts_to;
    VEC(ssa_name_info_t,gc) *assoc;

    if (ptr_type_node != TREE_TYPE(info->ssaname))
      return TREE_TYPE(info->ssaname);

    if (info->non_void_type && info->non_void_type != ptr_type_node)
      return info->non_void_type;

    assoc = find_names_by_unified_id(fn, info->unified_id);
    
    FOR_EACH_VEC_ELT(ssa_name_info_t, assoc, i, pts_to)
    {
        if (ptr_type_node != TREE_TYPE(pts_to->ssaname))
          return TREE_TYPE(pts_to->ssaname);
    }

    return TREE_TYPE(info->ssaname);
}


/* Add the ssa_name_info to to function's set of information
 * If a 'base.member' (composite) is passed.  The member is added to the
 * functions analysis data.  If just member is passed and its 'parent' pointer,
 * called 'base' is not set.  Member will be treated as a base.  This is bad.
 * So be smart when calling this.
 */
static void add_ssa_name_info(rbmm_fn_data_t fn, ssa_name_info_t info)
{
    const_tree base, member;

    RBMM_DEBUG(DEBUG_ANAL, "Adding ssaname: %s to Function: %s",
               get_ssaname(info), get_name(fn->func_decl));

    /* Handle all type based ssaname flags here */
    if (is_slice_type_p(info->ssaname) || is_map_type_p(info->ssaname))
      info->aliased_state |= ALIASED_BY_SLICE;

    /* If this has a parent, then it must be a member of that parent */
    if (info->base)
    {
        base = info->base->ssaname;
        member = info->ssaname;
    }
    else
    {
        base = info->ssaname;
        member = NULL_TREE;
    }

    if (!find_ssa_name_info(fn, base, member))
      VEC_safe_push(ssa_name_info_t, heap, fn->names, info);
}


/* Uniquely add a name to a vector of names */
static void unique_add_to_name_vec_gc(
    VEC(ssa_name_info_t,gc) **names,
    ssa_name_info_t           addme)
{
    unsigned i;
    ssa_name_info_t name;

    FOR_EACH_VEC_ELT(ssa_name_info_t, *names, i, name)
      if ((name == addme) && (name->base == addme->base))
        return;

    VEC_safe_push(ssa_name_info_t, gc, *names, addme);
}


/* Uniquely add a name to a vector of names
 * Returns 'TRUE' if the addition is unique and not already in 'names'
 */
static bool unique_add_to_name_vec_heap(
    VEC(ssa_name_info_t,heap) **names,
    ssa_name_info_t             addme)
{
    unsigned i;
    ssa_name_info_t name;

    FOR_EACH_VEC_ELT(ssa_name_info_t, *names, i, name)
      if ((name == addme) && (name->base == addme->base))
        return false;

    VEC_safe_push(ssa_name_info_t, heap, *names, addme);
    return true;
}


/* Uniquely Add 'pointstome' to parent, suggesting that 'parent' can point to
 * 'pointstome'
 */
static void add_to_points_to(ssa_name_info_t parent, ssa_name_info_t pointstome)
{
    ssa_name_info_t unified_to;

    if (!pointstome || !parent || (parent == pointstome))
      return;

    unique_add_to_name_vec_heap(&parent->points_to, pointstome);

    unified_to = UNIFIED_TO(parent);
    unique_add_to_name_vec_heap(&unified_to->points_to, pointstome);

    RBMM_DEBUG(DEBUG_ANAL, "%s points to %s",
               get_ssaname(parent), get_ssaname(pointstome));
}


/* Returns true if the node is a compiler generated (temporary var) node */
static bool is_artificial_p(const_tree node)
{
    if (TREE_CODE(node) == SSA_NAME)
      return DECL_ARTIFICIAL(SSA_NAME_VAR(node));
    else
      return DECL_ARTIFICIAL(node);
}


/* Easy case, this just assumes you have already created the member info and
 * want to add it to the parent.  This routine uniquely adds the member to the
 * parent.
 */
static void add_member(
    rbmm_fn_data_t  fn,
    ssa_name_info_t parent,
    ssa_name_info_t member)
{
    ssa_name_info_t unified_to;

    /* Search to see if we have already added this member. */
    if (unique_add_to_name_vec_heap(&parent->members, member))
    {
        member->base = parent;
        add_ssa_name_info(fn, member);
        unique_add_to_name_vec_heap(&parent->members, member);
    }

    /* Add to unified to's member list */
    unified_to = UNIFIED_TO(parent);
    unique_add_to_name_vec_heap(&unified_to->members, member);
}


/* Return the ssa_name_info for the base in func's analysis data.
 * If the type is a composite: e.g. foo.bar
 *  'foo' is the base and 'bar' is the member. The node for the member is
 *  returned ('bar').
 * For a convenience if 'base' is passed as a composite: foo.bar, then we
 * automatically break it down in this function and return the analysis data
 * associated with 'bar.'
 * 
 * 'member' is optional.
 */
static ssa_name_info_t _find_info(
    VEC(ssa_name_info_t,heap) *names,
    const_tree                 base,
    const_tree                 member)
{
    unsigned i;
    const_tree b, m;
    ssa_name_info_t info, unified_to;

    if (!base)
      return NULL;

    b = get_base((tree)base);
    m = (member) ? member : get_member(base);

    /* Find the base info */
    FOR_EACH_VEC_ELT(ssa_name_info_t, names, i, info)
      if (info->ssaname == b || 
          ((TREE_CODE(b) == PARM_DECL) && (SSA_NAME_VAR(info->ssaname) == b)))
        break;

    if (!info)
      return NULL;
    else if (!m)
      return info;

    /* Find the member info */
    unified_to = UNIFIED_TO(info);
    FOR_EACH_VEC_ELT(ssa_name_info_t, unified_to->members, i, info)
      if (info->ssaname == member)
        return info;

    return NULL;
}


ssa_name_info_t find_ssa_name_info(
    const rbmm_fn_data_t fn,
    const_tree           base,
    const_tree           member)
{
    return _find_info(fn->names, base, member);
}


ssa_name_info_t find_ssa_name_in_region(
    const region_t reg,
    const_tree     decl)
{
    return _find_info(reg->names, get_base((tree)decl), get_member(decl));
}


VEC(ssa_name_info_t,gc) *find_names_by_unified_id(
    const rbmm_fn_data_t fn,
    int                  unified_id)
{
    unsigned i;
    ssa_name_info_t info;
    VEC(ssa_name_info_t,gc) *names = NULL;

    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
      if (info->unified_id == unified_id)
        VEC_safe_push(ssa_name_info_t, gc, names, info);

    return names;
}


const char *get_ssaname(const ssa_name_info_t info)
{
    char buf[32];
    const char *name;

    if (!info)
      return NULL;

    if (!(name = get_name((tree)info->ssaname)) &&
        (TREE_CODE(info->ssaname) == SSA_NAME))
      name = get_name(SSA_NAME_VAR(info->ssaname));
          
    if (!name)
    {
        name = buf;
        snprintf(buf, 31, "%u", (TREE_CODE(info->ssaname) == SSA_NAME) ?
                                DECL_UID(SSA_NAME_VAR(info->ssaname)) :
                                DECL_UID(info->ssaname));
    }

    return ggc_strdup(name);
}


/* Returns the base (parent tree node) */
tree get_base(tree cmpnt)
{
    if (!cmpnt)
      return NULL_TREE;

    /* If array get the base of that */
    while (!SSA_VAR_P(cmpnt) && TREE_OPERAND_LENGTH(cmpnt)>0 &&
           TREE_OPERAND(cmpnt, 0) != NULL_TREE)
      cmpnt = TREE_OPERAND(cmpnt, 0);

    if (TREE_CODE(cmpnt) == SSA_NAME)
      cmpnt = SSA_NAME_VAR(cmpnt);

    return cmpnt;
}


/* Returns the the 'member' field in a node that looks like 'parent->member' */
tree get_member(const_tree cmpnt)
{
    if (!cmpnt)
      return NULL_TREE;

    /* Not a component ref... NO MEMBER FOR YOU! */
    if (TREE_CODE(cmpnt) != COMPONENT_REF)
      return NULL_TREE;
    
    if (TREE_CODE(cmpnt) == SSA_NAME)
      cmpnt = SSA_NAME_VAR(cmpnt);

    return TREE_OPERAND(cmpnt, 1);
}


#ifdef DEBUG
static void print_analyized_ssa_names(const VEC(ssa_name_info_t,heap) *names)
{
    unsigned ii, jj, k;
    ssa_name_info_t parent, member;

    for (ii=0; VEC_iterate(ssa_name_info_t, names, ii, parent); ++ii)
    {
        RBMM_DEBUG(DEBUG_ANAL,
                   "Name: %s RID: %d Aliased State: %u",
                   get_ssaname(parent),
                   parent->region_uid,
                   parent->aliased_state);

        FOR_EACH_VEC_ELT(ssa_name_info_t, parent->members, jj, member)
          RBMM_DEBUG(DEBUG_ANAL,
                     "    Field: %s RID: %d",
                     get_ssaname(member), member->region_uid);

        FOR_EACH_VEC_ELT(ssa_name_info_t, parent->points_to, k, member)
          RBMM_DEBUG(DEBUG_ANAL,
                     "    Points-to: %s in region %d",
                     get_ssaname(member),
                     member->region_uid);
    }
}
#endif /* DEBUG */


/* Get the name of the node and see if it matches any alloction builtin */
static bool is_allocation_call(gimple gimp)
{
    int i;
    tree id, fn;
    const char *name;
    static tree ids[8] = {NULL_TREE};

    if (gimple_call_builtin_p(gimp, BUILT_IN_MALLOC))
      return true;

    if (!ids[0])
    {
        ids[0] = get_identifier("__go_new_nopointers");
        ids[1] = get_identifier("__go_new");
        ids[2] = get_identifier("__go_new_map");
        ids[3] = get_identifier("__go_map_index"); /* Yes it causes an alloc */
        ids[4] = get_identifier("__go_make_slice1");
        ids[5] = get_identifier("__go_make_slice2");
        ids[6] = get_identifier("__go_append");
        ids[7] = get_identifier("__go_construct_map");
    }

    fn = gimple_call_fn(gimp);

    /* Break out early if this is a function pointer */
    if (TREE_CODE(fn) == SSA_NAME &&
        POINTER_TYPE_P(TREE_TYPE(fn)) &&
        TREE_CODE((TREE_TYPE(TREE_TYPE(fn)))) == FUNCTION_TYPE)
      return false;

    name = get_name(gimple_call_fn(gimp));
    gcc_assert(name);

    if (!(id = get_identifier(name)))
      return false;

    /* Check to see if the id node matches a builtin */
    for  (i=0; i<sizeof(ids)/sizeof(ids[0]); ++i)
      if (id == ids[i])
        return true;
    
    return false;
}


static inline bool is_function_p(const_tree node)
{    
    return (TREE_CODE(node) == ADDR_EXPR &&
        POINTER_TYPE_P(TREE_TYPE(node))  &&
        TREE_CODE(TREE_TYPE(TREE_TYPE(node))) == FUNCTION_TYPE);
}


bool is_fnptr(const_tree node)
{
#ifndef DISABLE_HIGHER_ORDER_ANALYSIS

    /* Simple case: fn(); */
    if (is_function_p(node))
      return false;

    /* Simple case: fnp(); 
     * or 'x' in:   x = fnp;
     */
    else if (TREE_CODE(node) == SSA_NAME     &&
             POINTER_TYPE_P(TREE_TYPE(node)) &&
             TREE_CODE(TREE_TYPE(TREE_TYPE(node))) == FUNCTION_TYPE)
      return true;

    /* These are more for handling function arguments */
    if (TREE_CODE(node) == COMPONENT_REF)
      node = get_member(node);
    if (DECL_P(node))
      node = TREE_TYPE(node);
    if (TREE_CODE(node) == ADDR_EXPR)
      node = TREE_TYPE(node);
    if (TREE_CODE(node) == MEM_REF)
      node = TREE_TYPE(node);
    if (DECL_P(node) || SSA_VAR_P(node))
      node = TREE_TYPE(node);
    return POINTER_TYPE_P(node) && (TREE_CODE(TREE_TYPE(node))==FUNCTION_TYPE);
#else
    return false;
#endif
}


/* For use in GDB */
static void print_regions_in_fn(const rbmm_fn_data_t fn)
{
    unsigned i;
    region_t reg;

    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
      printf("[%u] Region: %d\n", i+1, reg->id);
}


/* For use in GDB */
static void print_names_in_region(const region_t reg)
{
    unsigned i;
    ssa_name_info_t info;

    printf("Region %d:\n", reg->id);
    FOR_EACH_VEC_ELT(ssa_name_info_t, reg->names, i, info)
      printf("\t[%u] Name: %s for value %p\n",
             i, get_ssaname(info), info->ssaname);
}


/* For use in GDB */
static void print_members(const ssa_name_info_t info)
{
    unsigned i;
    ssa_name_info_t mem;
    
    printf("Members:\n");
    FOR_EACH_VEC_ELT(ssa_name_info_t, info->members, i, mem)
      printf("\t[%u] Name: %s (%u) %p\n", 
             i+1, get_ssaname(mem), mem->aliased_state, mem->ssaname);
}


/* For use in GDB */
static void print_points_to(const ssa_name_info_t info)
{
    unsigned i;
    ssa_name_info_t pts_to;

    printf("Points-To:\n");
    FOR_EACH_VEC_ELT(ssa_name_info_t, info->points_to, i, pts_to)
    {
        if (pts_to->base)
          printf("\t[%u] Name: %s.%s (%u)\n",
                 i+1, get_ssaname(pts_to->base), get_ssaname(pts_to),
                 pts_to->aliased_state);
        else
          printf("\t[%u] Name: %s (%u)\n",
                 i+1, get_ssaname(pts_to), pts_to->aliased_state);
    }
}
    

/* For use in GDB */
static void print_associated_names(const ssa_name_info_t info)
{
    unsigned i;
    ssa_name_info_t match;
    rbmm_fn_data_t fn;

    if (!cfun || !(fn = find_fn_data(cfun->decl)))
      return;

    printf("Unified With Names:\n");
    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, match)
      if ((match != info) && (match->unified_id == info->unified_id))
        printf("\tAssociated Name: %s.%s (%u)\n", 
               get_ssaname(match->base), get_ssaname(match),
               match->aliased_state);
}


/* For use in GDB */
static void print_name(const ssa_name_info_t info)
{
    if (info->base)
      printf("Name: %s.%s ID(%d) Aliased(%u) UnifiedTo(%s) %p\n",
             get_ssaname(info->base), get_ssaname(info), 
             info->unified_id, info->aliased_state,
             get_ssaname(info->unified_to), info->ssaname);
    else
      printf("Name: %s ID(%d) Aliased(%u) UnifiedTo(%s) %p\n",
             get_ssaname(info), info->unified_id, info->aliased_state,
             get_ssaname(info->unified_to), info->ssaname);

    print_members(info);
    print_points_to(info);
    print_associated_names(info);
    putc('\n', stdout);
}


/* For use in GDB */
static void print_names_in_function(const rbmm_fn_data_t fn)
{
    unsigned i;
    ssa_name_info_t info;

    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
      print_name(info);
}


inline static void shutupwarnings(void)
{
    print_regions_in_fn(NULL);
    print_names_in_region(NULL);
    print_members(NULL);
    print_points_to(NULL);
    print_associated_names(NULL);
    print_name(NULL);
    print_names_in_function(NULL);
}


#ifdef DEBUG
static void print_rbmm_info(void)
{
    unsigned ii;
    rbmm_fn_data_t fn;
    
    for (ii=0; VEC_iterate(rbmm_fn_data_t, rbmm_analyized_fns, ii, fn); ++ii)
    {
        RBMM_DEBUG(DEBUG_ANAL, "[ Function: %s ]", get_name(fn->func_decl));
        print_analyized_ssa_names(fn->names);
        print_region_info(fn->regions);
    }
}


static void print_param_to_region_mapping(const rbmm_fn_data_t fn)
{
    unsigned i, j;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *n_to_reg;

    RBMM_DEBUG(DEBUG_PARAMS, "Function: %s", get_name((tree)fn->func_decl));
    FOR_EACH_VEC_ELT(param_to_reg_t, fn->parammap, i, pr)
    {
        RBMM_DEBUG(DEBUG_PARAMS, "\tParameter at index %d maps to regions:",i);
        FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, j, n_to_reg)
          RBMM_DEBUG(DEBUG_PARAMS,
                     "\t\tRegion %d: at index %d (%d) %s",
                     n_to_reg->region->id,
                     n_to_reg->region->region_var_idx,
                     n_to_reg->region->aliased_state,
                     n_to_reg->use_as_formal_param ? 
                         "" : "(Already used as formal param)");
    }

    if (!VEC_length(ssa_name_to_reg_t, fn->returned_parammap->regions))
    {
        RBMM_DEBUG(DEBUG_PARAMS, "\tDoes not need a region for the return.");
    }
    else
    {
        RBMM_DEBUG(DEBUG_PARAMS, "\tReturned variable maps to regions:", i);
        FOR_EACH_VEC_ELT(ssa_name_to_reg_t, fn->returned_parammap->regions,
                         i, n_to_reg)
          RBMM_DEBUG(DEBUG_PARAMS,
                     "\t\tRegion %d: at index %d (%d) %s",
                     n_to_reg->region->id,
                     n_to_reg->region->region_var_idx,
                     n_to_reg->region->aliased_state,
                     n_to_reg->use_as_formal_param ?
                         "" : "(Already used as formal param)");
    }

}
#endif /* DEBUG */


/* Check to see if a global variable aliases this the SSA_NAME in 'info'...
 * there has got to be a better way of checking this.  Returns the bitmask value
 * for 'aliased_flags'
 */
static unsigned is_aliased_by_global(rbmm_fn_data_t fn, ssa_name_info_t info)
{
    const_tree var, test;
    ssa_name_info_t global_info;
    referenced_var_iterator rvi;

    /* If this is a global... well hell, it must be aliased to itself */
    test = info->ssaname;

    if (TREE_CODE(test) == ADDR_EXPR)
      test = TREE_OPERAND(test, 0);
    else if (TREE_CODE(test) == SSA_NAME)
      test = SSA_NAME_VAR(test);

    /* Is this ssaname a global? */
    if ((TREE_CODE(test) == VAR_DECL   ||
         TREE_CODE(test) == PARM_DECL) && 
        is_global_var(test))
    {
        RBMM_DEBUG(DEBUG_ANAL, "%s is aliased by a global", get_ssaname(info));
        return ALIASED_BY_GLOBAL;
    }

    /* Locate all globals this function references */
    FOR_EACH_REFERENCED_VAR(cfun, var, rvi)
    {
        if (!is_global_var(var))
          continue;

        /* Have we unified 'info' with 'global' */
        global_info = find_ssa_name_info(fn, var, NULL);
        if (global_info && (global_info->region_uid == info->region_uid))
        {
            RBMM_DEBUG(DEBUG_ANAL, "%s aliased by a global",get_ssaname(info));
            return ALIASED_BY_GLOBAL;
        }
    }

    return ALIASED_BY_NONE;
}


static unsigned is_aliased_by_param(rbmm_fn_data_t fn, ssa_name_info_t info)
{
    int idx;
    const_tree param, decl;
              
    decl = get_ssaname_decl(info);

    /* Check all args to see if they point-to var */
    for (idx=0, param=DECL_ARGUMENTS(cfun->decl);
         param;
         ++idx, param=DECL_CHAIN(param))
      if (param == decl)
      {
          info->aliased_by_decl = param;
          info->param_decl_idx = idx;
          return ALIASED_BY_PARAM;
      }
      
    return ALIASED_BY_NONE;
}


/* Given a name, find all of the other names unified to it.  And get the members
 * of thoes names.
 *
 * A single parent might have been unified with tons of other temporary names.
 * So a single parent node might effectively be the unification of all of the
 * assignments to it and its temporary names.  Similar for the members of the
 * parent.
 */
VEC(ssa_name_info_t,gc) *get_associated_members(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t info)
{
    unsigned i, j;
    ssa_name_info_t name, parent, member;
    VEC(ssa_name_info_t,gc) *members, *parents, *all_members;

    /* Get the nodes representing the parent (info) */
    parents = VEC_alloc(ssa_name_info_t, gc, 1);
    VEC_quick_push(ssa_name_info_t, parents, info);
    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, parent)
      if (parent != info && parent->unified_id == info->unified_id)
        unique_add_to_name_vec_gc(&parents, parent);

    /* Get the members that make up the parent (info) */
    members = VEC_alloc(ssa_name_info_t, gc, 0);
    FOR_EACH_VEC_ELT(ssa_name_info_t, parents, i, parent)
      FOR_EACH_VEC_ELT(ssa_name_info_t, parent->members, j, member)
        unique_add_to_name_vec_gc(&members, member);

    /* Get all the names that make up each member of the parent (info) */
    all_members = VEC_alloc(ssa_name_info_t, gc, 0);
    FOR_EACH_VEC_ELT(ssa_name_info_t, members, i, member)
      FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, j, name)
        if (member->unified_id == name->unified_id)
          unique_add_to_name_vec_gc(&all_members, name);

    VEC_free(ssa_name_info_t, gc, parents);
    VEC_free(ssa_name_info_t, gc, members);
    ggc_collect();

    return all_members;
}


void clear_marks(rbmm_fn_data_t fn)
{
    unsigned i;
    ssa_name_info_t info;

    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
      info->marked = false;
}


/* Visit all nodes info can point to.  Include the nodes which the points to
 * points to and points to points to points to...  Call "clear_marks()" before
 * and after this call to prevent infinite loops.
 */
static void propagate_alias_to_pointed_to(
    ssa_name_info_t info,
    unsigned        aliased_state)
{
    unsigned i;
    ssa_name_info_t pt;

    if (info->marked)
      return;
    else
      info->marked = true;

    FOR_EACH_VEC_ELT(ssa_name_info_t, info->points_to, i, pt)
    {
        pt->aliased_state |= aliased_state;
        propagate_alias_to_pointed_to(pt, aliased_state);
    }
}


/* Make op1 and op2 have the same alias info.  It as if op1 is becoming op2,
 * such as the case for assignment: op1 = op2;
 */
static inline void unify_info(
    rbmm_fn_data_t  fn,
    ssa_name_info_t op1,
    ssa_name_info_t op2)
{
    unsigned i;
    int match_id, new_id;
    ssa_name_info_t info, master;

    if (!op1 || !op2)
      return;

    RBMM_DEBUG(DEBUG_ANAL,
               "Unifying: %s(%p | %d | %d) and %s(%p | %d | %d)",
               get_ssaname(op1), op1->ssaname, op1->region_uid,
               op1->aliased_state,
               get_ssaname(op2), op2->ssaname, op2->region_uid,
               op2->aliased_state);

    /* Try to fix up void* temp nodes */
    if (TREE_TYPE(op1->ssaname) == ptr_type_node)
      op1->non_void_type = TREE_TYPE(op2->ssaname);
    if (TREE_TYPE(op2->ssaname) == ptr_type_node)
      op2->non_void_type = TREE_TYPE(op1->ssaname);

    /* Update unified_to (smallest id wins) */
    master = (op1->unified_id < op2->unified_id) ? op1 : op2;
    op1->unified_to = UNIFIED_TO(master);
    op2->unified_to = UNIFIED_TO(master);

    /* Update unified_ids (smallest id wins) */
    match_id = MAX(op1->unified_id, op2->unified_id);
    new_id = MIN(op1->unified_id, op2->unified_id);
    if (match_id != new_id)
      FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
        if (info->unified_id == match_id)
        {
            info->unified_id = new_id;
            info->unified_to = UNIFIED_TO(master);
        }
    
    /* Add to points-to graph */
    if ((op2->base != op1) && (op1->base != op2))
      add_to_points_to(op1, op2);

    /* Update region_ids (smallest id wins) */
    match_id = MAX(op1->region_uid, op2->region_uid);
    new_id = MIN(op1->region_uid, op2->region_uid);
    if (match_id != new_id)
      FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
        if (info->region_uid == match_id)
          info->region_uid = new_id;
}


/* For each formal parameter, get the associated ssa_name, and if it has
 * members, unify them with the parent.
 * This is a special case and should only be performd on functions used/pointed
 * to by function pointer varaibles.
 */
static void unify_parents_and_member_params(rbmm_fn_data_t fn)
{
    unsigned i, j;
    tree param;
    ssa_name_info_t info, master, member;

    /* Unify return regions
     * Find a name that is a aliased by return.  Then unify all return aliases
     * and their members with that node.
     */
    master = NULL;
    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, master)
      if (ALIASED_BY_RETURN_P(master))
          break;

    if (master)
    {
        /* Unify all members of 'master' with 'master' */
        FOR_EACH_VEC_ELT(ssa_name_info_t, master->members, i, member)
          unify_info(fn, master, member);

        /* Unify all nodes and their members with master if they are aliased by
         * a return variable.
         */
        FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
        {
            if (!ALIASED_BY_RETURN_P(info))
              break;
            FOR_EACH_VEC_ELT(ssa_name_info_t, info->members, j, member)
              unify_info(fn, master, member);
        }
    }

    /* Unify formal parameters to 'fn' */
    for (param=DECL_ARGUMENTS(fn->func_decl); param; param=DECL_CHAIN(param))
    {
        if (!(info = find_ssa_name_info(fn, param, NULL)))
          continue;
        FOR_EACH_VEC_ELT(ssa_name_info_t, info->members, i, member)
          unify_info(fn, info, member);
    }
}


/* Make all nodes with 'id' have the same alias information.
 * This should be used only by unify_finalize(), as it requires a fixed point.
 * Fixed point is required because info->base might not have been calculated
 * yet.
 */
static unsigned _unify_id(rbmm_fn_data_t fn, int id)
{
    unsigned i, alias = 0;
    ssa_name_info_t info;

    /* Get the cumulative alias information for all nodes of 'id' */
    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
    {
        if (info->region_uid != id)
          continue;

        alias |= info->aliased_state;

        /* If info is a member (base is the parent) OR in the alias information
         * but ignoreing the allocation flag.  We only want to OR allocation if
         * info actually is set from an allocation directly, and not if it's
         * parent is set from an allocation.
         */
        if (info->base)
          alias |= (info->base->aliased_state) & ~ALIASED_BY_ALLOC;
    }

    /* Set cumulative alias information to all nodes of 'id' */
    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
    {
        if (info->region_uid != id)
          continue;
        info->aliased_state = alias;
    }

    return alias;
}


/* For each name, get it's associated members.  And make sure the parent of
 * thoes members (aka 'base' field) points to parent.
 */
static void _unify_update_members_and_parent_pointers(
    rbmm_fn_data_t        fn,
    const ssa_name_info_t parent)
{
    unsigned i, j, k;
    ssa_name_info_t assoc_parent, assoc_member, member;

    /* Look for the same parent just under a different temporary variable name.
     * E.g. this same 'parent'/temp we call an associated parent.  Just look for
     * the unified_id.  If the two id's match, its the same guy!
     */
    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, assoc_parent)
    {
        /* Don't update self or non-associated parent */
        if (assoc_parent == parent ||
            assoc_parent->unified_id != parent->unified_id)
          continue;

        /* Now look at all members of the parent (associated) */
        FOR_EACH_VEC_ELT(ssa_name_info_t, assoc_parent->members, j, member)
          FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, k, assoc_member)
            if (assoc_member->unified_id == member->unified_id &&
                assoc_member->unified_id != parent->unified_id)
              assoc_member->base = assoc_parent;
    }
}


/* Post-intraprocedural unification:
 * Visit each ssaname in our analysis and produce a set of information telling
 * us if that name is asliased by an input argument, if that name is aliased by
 * a global variable, if the name is returned, and if any functions the
 * analyized function calls might need that ssaname as an argument.
 */
#define UGLY 1//(ALIASED_BY_GLOBAL | ALIASED_BY_PARAM | ALIASED_BY_RETURN)
static void unify_finalize(rbmm_fn_data_t fn)
{
    unsigned i, before, changed;
    bool fixed_point;
    ssa_name_info_t info;

    before = 0;
    fixed_point = false;
    while (!fixed_point)
    {
        changed = 0;

        FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
        {
            /* Return value aliasing is calculated in process_retval() */
            before = info->aliased_state;
            info->aliased_state |= is_aliased_by_global(fn, info) |
                                   is_aliased_by_param(fn, info);

            /* Make sure the members know that they have a parent */
            _unify_update_members_and_parent_pointers(fn, info);

            /* TODO: (Implement #1 below) [NOTE-FNPTR]
             * If this function is used as a fnptr, we can either:
             * 1 -- Specialize 
             * 2 -- Or, we can unify parents and members
             * ----- We perfom #2 here now, but in the future we should profile
             * and see which is best
             */
            if (fn->used_by_fnptr)
              unify_parents_and_member_params(fn);

            /* Pass global alias info to all nodes this info can reach */
            clear_marks(fn);
            propagate_alias_to_pointed_to(info, UGLY & info->aliased_state);
            clear_marks(fn);

            _unify_id(fn, info->region_uid);
            changed += (before != info->aliased_state);
        }

        if (changed == 0)
          fixed_point = true;
    }
}


/* Return true if the types being pointed to differ */
static bool is_different_base_type_p(const_tree t1, const_tree t2)
{
    if (!t1 || !t2)
      return false; /* Say they are not different */

    t1 = TREE_TYPE(t1);
    t2 = TREE_TYPE(t2);

    /* Peel off the pointer and decl nodes */
    while (POINTER_TYPE_P(t1))
      t1 = TREE_TYPE(t1);
    while (POINTER_TYPE_P(t2))
      t2 = TREE_TYPE(t2);

    return t1 != t2;
}


/* If this is a derefrence: *node.  Get the ssa name for what node points at.
 * Info will be the ssa_name for 'node'
 */
static tree get_dereferenced_data(
    const rbmm_fn_data_t  fn,
    const_tree            node,
    ssa_name_info_t      *info)
{
    ssa_name_info_t nfo;
    
    *info = find_ssa_name_info(fn, get_base((tree)node), NULL);
    nfo = *info;

    /* Not a dereference, just return what we were given (regurgitate) */
    if (TREE_CODE(node) != MEM_REF)
      return get_base((tree)node);

    /* Not enough info yet: vomit things up */
    if (nfo &&
        (!nfo->points_to || VEC_length(ssa_name_info_t, nfo->points_to) == 0))
      return get_base((tree)node);

    /* Return the first thing info points to */
    if (nfo)
      return (tree)VEC_index(ssa_name_info_t, nfo->points_to, 0)->ssaname;
    else
      return get_base((tree)node);
}


/* Find the ssa data for the node in our graph of ssanames we unify.  
 * If it does not exist... create and return it.
 * TODO: USE THIS UNIVERSALLY
 */
static ssa_name_info_t find_or_add_ssa_name_info(
    rbmm_fn_data_t fn,
    tree           node)
{
    tree base, member;
    ssa_name_info_t base_info, member_info;

    base = get_base(node);
    member = get_member(node);

    /* Handle: *base OR *(base->member) */
    base_info = member_info = NULL;
    if (TREE_CODE(node) == MEM_REF) /* if dereference */
    {
        if (!member)
          base = get_dereferenced_data(fn, node, &base_info);
        else  /* TODO: TEST */
          member = get_dereferenced_data(fn, member, &member_info);
    }

    /* Find or add base */
    if (base && !(base_info = find_ssa_name_info(fn, base, NULL)))
    {
        base_info = create_ssa_name_info(base);
        add_ssa_name_info(fn, base_info);
    }

    /* Find or add member */
    if (member && !(member_info = find_ssa_name_info(fn, base, member)))
    {
        member_info = create_ssa_name_info(member);
        member_info->base = base_info;
        add_ssa_name_info(fn, member_info);
    }

    /* Return the data being read/write to (member if it was base.member) */
    if (member)
    {
        add_member(fn, base_info, member_info);
        return member_info;
    }
    else
      return base_info;
}


/* Unify our ssa name graph nodes (and/or create them)
 * Returns 'true' if we add data to the analysis
 */
static bool process_assignment(rbmm_fn_data_t fn, gimple stmt)
{
    unsigned i;
    bool same_region, has_been_modified;
    tree lhs, lhs_base, lhs_member, rhs, rhs_base, rhs_member, lhs_tmp;
    ssa_name_info_t lhs_info, rhs_info, lhs_mem_info, rhs_mem_info;

    has_been_modified = false;
    lhs_mem_info = rhs_mem_info = NULL;

    /* Add LHS to our analysis */
    lhs = gimple_assign_lhs(stmt);
    lhs_base = get_base(lhs);
    lhs_member = get_member(lhs);

    /* If lhs is a dereference: get what lhs points to */
    if (TREE_CODE(lhs) == MEM_REF)
      lhs_base = get_dereferenced_data(fn, lhs, &lhs_info);

    /* We need to see if the base and member are of the same type.  If they are,
     * or there is no member.  Than the rhs will be in the same region as the
     * lhs.  If the types differ, we put the base in its own region and child (a
     * different type) into a separate region
     */
    if (TREE_CODE(lhs) == COMPONENT_REF &&
        TREE_CODE(TREE_OPERAND(lhs, 0)) == MEM_REF)
      lhs_tmp = TREE_OPERAND(lhs, 0);
    else
      lhs_tmp = lhs_base;

    /* This is key to handling single variable multi-region functionality */
    same_region = true;
    if (lhs_base && lhs_member && is_different_base_type_p(lhs_tmp,lhs_member))
      same_region = false;
    
    /* If we are not assigning to a pointer or agg ... OUT! GET OUT! NOW... */
    if (!POINTER_TYPE_P(TREE_TYPE(lhs_base)) &&
        !AGGREGATE_TYPE_P(TREE_TYPE(lhs_base)))
      return has_been_modified;

    if (lhs_member &&
        !(RECORD_OR_UNION_TYPE_P(TREE_TYPE(lhs_member)) ||
          POINTER_TYPE_P(TREE_TYPE(lhs_member))))
      return has_been_modified;

    /* Add or update the LHS in our type analysis graph */
    if (!(lhs_info = find_ssa_name_info(fn, lhs_base, NULL)))
    {
        lhs_info = find_or_add_ssa_name_info(fn, lhs_base);
        has_been_modified = true;
    }

    /* Slices and all of their members (count, values, capacity) are all from
     * one contigious allocation.  At least as far as we are concerned.
     * TODO: In the future, do not call "unify_finalize here" instead, it is
     * slow!.  In the future just check points-to and members if they belong to
     * data allocated by a slice.
     */
    unify_finalize(fn);
    if (ALIASED_BY_SLICE_P(lhs_info))
      same_region = true;

    /* Get the info and add the lhs */
    if (lhs_member)
    {
        if (!(lhs_mem_info = find_ssa_name_info(fn, lhs_base, lhs_member)))
        {
            lhs_mem_info = find_or_add_ssa_name_info(fn, lhs);
            has_been_modified = true;
        }

        if (same_region)
          unify_info(fn, lhs_info, lhs_mem_info);
    }

    /* Now get the other operands and add to analysis if not in graph */
    for (i=1; i<gimple_num_ops(stmt); ++i)
    {
        rhs = gimple_op(stmt, i);
        rhs_base = get_base(rhs);
        rhs_member = get_member(rhs);

        if (!REGION_CANDIDATE_TYPE_P(TREE_TYPE(rhs_base)))
          continue;

        /* If the rhs is dereferenced get what rhs points at */
        if (TREE_CODE(rhs) == MEM_REF)
        {
            rhs_info = find_ssa_name_info(fn, rhs_base, NULL);
            if (rhs_info && rhs_info->points_to &&
                VEC_length(ssa_name_info_t, rhs_info->points_to) > 0)
              rhs_base = (tree)VEC_index(ssa_name_info_t,
                                         rhs_info->points_to, 0)->ssaname;
        }

        /* Ignore fnptrs (bail from this for loop) */
        if (TREE_CODE(rhs_base) == FUNCTION_DECL)
          break;

        /* Ignore (void *)0, or integer constants */
        if (TREE_CODE(rhs) == INTEGER_CST     ||
            TREE_CODE(rhs_base) == STRING_CST ||
            TREE_CODE(rhs_base) == CONSTRUCTOR)
        {
            rhs = rhs_base = rhs_member = NULL_TREE;
            rhs_info = NULL;
        }

        if (rhs_base && !(rhs_info = find_ssa_name_info(fn, rhs_base, NULL)))
        {
            rhs_info = find_or_add_ssa_name_info(fn, rhs_base);
            has_been_modified = true;
        }
        
        if (rhs_member)
        {
            if (!(rhs_mem_info = find_ssa_name_info(fn,rhs_base,rhs_member)))
            {
                rhs_mem_info = find_or_add_ssa_name_info(fn, rhs);
                has_been_modified = true;
            }

            /* If base and member are same types OR if the base was allocated
             * memory (which means the member comes from that same junk).  If
             * the base was alloacted memory and we never did for the member,
             * then the member must be allocated from the data from base.
             * Example of this is:
             *   monster_chunk = alloc(sizeof(Type);
             *   *monster_chunk = Type{TypeA *a, TypeB *b}; 
             *
             * Both the 'a' and 'b' member are different types but allocated in
             * one monster chunk.
             */
            if (ALIASED_BY_SLICE_P(rhs_info) ||
                !is_different_base_type_p(rhs_base, rhs_member))
              unify_info(fn, rhs_info, rhs_mem_info);
        }

        /* If lhs is ptr, set lhs to whatever rhs or rhs member points to
         * 4 possibilities: 
         * lhs.mem = rhs.mem OR lhs.mem = rhs OR lhs = rhs.mem OR lhs = rhs
         */
        if (lhs_member && rhs_member)
          unify_info(fn, lhs_mem_info, rhs_mem_info); /* lhs.mem = rhs.mem */
        else if (lhs_member && !rhs_member)     
          unify_info(fn, lhs_mem_info, rhs_info);     /* lhs.mem = rhs     */
        else if (!lhs_member && rhs_member)
          unify_info(fn, lhs_info, rhs_mem_info);     /* lhs     = rhs.mem */
        else
          unify_info(fn, lhs_info, rhs_info);         /* lhs     = rhs     */
    }

    return has_been_modified;
}


/* Unify our ssa name graph nodes (and/or create them) */
static void process_allocation(rbmm_fn_data_t fn, gimple stmt)
{
    tree lhs;
    ssa_name_info_t info, info_for_map, arg1, arg2;

    /* Get the variable that will point to the allocated memory */
    lhs = gimple_call_lhs(stmt);
    
    /* Check to see if 'lhs' is in our analysis graph, if not add it */
    if (lhs)
       info = find_or_add_ssa_name_info(fn, lhs);

    if (info)
      info->aliased_state |= ALIASED_BY_ALLOC;

    /* Special case */
    if (get_identifier(get_name(gimple_call_fn(stmt))) ==
        get_identifier("__go_map_index"))
    {
        lhs = gimple_call_arg(stmt, 0);
        if (!(info_for_map = find_ssa_name_info(fn, lhs, NULL)))
        {
            info_for_map = create_ssa_name_info(lhs);
            add_ssa_name_info(fn, info_for_map);
        }
        info_for_map->aliased_state |= ALIASED_BY_ALLOC;
        unify_info(fn, info, info_for_map);
    }

    /* Other special case */
    else if (get_identifier(get_name(gimple_call_fn(stmt))) ==
             get_identifier("__go_append"))
    {
        /* Unify the lhs, arg1, and arg2 regions of the append call */
        arg1 = find_or_add_ssa_name_info(fn, gimple_call_arg(stmt, 0));
        arg2 = find_or_add_ssa_name_info(fn, gimple_call_arg(stmt, 1));

        if (info) info->aliased_state |= ALIASED_BY_ALLOC;
        //if (arg1) arg1->aliased_state |= ALIASED_BY_ALLOC;
        //if (arg2) arg2->aliased_state |= ALIASED_BY_ALLOC;

        //unify_info(fn, info, arg1);
        unify_info(fn, arg1, arg2);
    }
    
    /* Other other special case */
    else if ((get_identifier(get_name(gimple_call_fn(stmt))) ==
              get_identifier("__go_make_slice1")) ||
             (get_identifier(get_name(gimple_call_fn(stmt))) ==
              get_identifier("__go_make_slice2")))
    {
        info->aliased_state |= ALIASED_BY_SLICE;
    }
}


/* If a return value is processed (which, given SSA GIMPLE there should only be
 * one return value even with languages like Go where multiple return values
 * are allowed), set the aliased_state flag.
 */
static void process_retval(rbmm_fn_data_t fn, gimple stmt)
{
    tree retval;
    ssa_name_info_t info;

    if ((retval = gimple_return_retval(stmt)) &&
        (info = find_ssa_name_info(fn, retval, NULL)))
      info->aliased_state |= ALIASED_BY_RETURN;
}


/* Predicate returning the fndecl of a gimple function call statement.
 * NULL is returned for functions we cannot process
 */
static tree can_handle_fncall(const_gimple stmt)
{
    tree decl;

    if (!is_gimple_call(stmt))
       return NULL;

    decl = gimple_call_fn(stmt);

    /* Function pointer (so coax the decl out) */
    if (TREE_CODE(decl) == SSA_NAME)
      decl = gimple_op(SSA_NAME_DEF_STMT(decl), 1);

    if (!decl)
       return NULL;

    if (TREE_OPERAND_LENGTH(decl) == 0)
      return NULL;
    
    decl = TREE_OPERAND(decl, 0);

    if (is_rbmm_builtin(decl))
      return NULL;

    /* Try to process other modules */
    if (DECL_EXTERNAL(decl) && !import_module_used_in_decl(decl))
      return NULL;

    return decl;
}


static bool is_in_branch(basic_block bb)
{
    edge e;
    edge_iterator ei;

    FOR_EACH_EDGE(e, ei, bb->preds)
      if (VEC_length(edge, e->src->succs) > 1)
        return true;
      else
        is_in_branch(e->src);

    return false;
}


/* Return 'true' if we are to merge the regions associated to the names at
 * runtime as opposed to now at compile time.
 */
#if 0
#ifdef ENABLE_RUNTIME_MERGING
static bool merge_at_runtime(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t n1,
    const ssa_name_info_t n2)
{
    int pid, cid;
    unsigned i;
    merge_t *md;

    FOR_EACH_VEC_ELT(merge_t, fn->merge_data, i, md)
    {
        pid = md->parent->id;
        cid = md->child->id;

        if (((pid==n1->region_uid) && (cid==n2->region_uid)) ||
            ((pid==n2->region_uid) && (cid==n1->region_uid)))
          return true;
    }

    return false;
}
#endif /* ENABLE_RUNTIME_MERGING */
#endif


static void add_merge_data(
    rbmm_fn_data_t fn,
    region_t       parent,
    region_t       child,
    gimple         stmt,
    bool           for_trampoline)
{
    unsigned i;
    merge_t *md;

    /* Don't merge global regions */
    if (GLOBAL_REGION_P(parent) || GLOBAL_REGION_P(child) ||
        parent->id == -666 || child->id == -666)
      return;

    /* If we have already added this bail early (not efficient) */
    FOR_EACH_VEC_ELT(merge_t, fn->merge_data, i, md)
      if ((md->parent == parent && md->child == child) ||
          (md->parent == child && md->child == parent))
        return;

    /* Never added before, so add now */
    md = VEC_safe_push(merge_t, gc, fn->merge_data, NULL);
    md->parent = parent;
    md->child = child;
    md->use_stmt = for_trampoline;
    if (md->use_stmt)
      md->stmt = stmt;
    else
      md->gsi = gsi_for_stmt(stmt);
}


void remove_merge_data(rbmm_fn_data_t fn, merge_t *md)
{
    unsigned i;
    merge_t *data;

    FOR_EACH_VEC_ELT(merge_t, fn->merge_data, i, data)
      if (data == md)
      {
          VEC_ordered_remove(merge_t, fn->merge_data, i);
          return;
      }
}


/* Return a bitmap where a bit for each param_index into fncall 'fn' the region
 * 'reg' is used for.
 */
#if 0
static bitmap get_param_indices(const rbmm_fn_data_t fn, const region_t reg)
{
    unsigned i, j;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *n_to_reg;
    bitmap idxes;
   
    idxes = BITMAP_GGC_ALLOC();
    FOR_EACH_VEC_ELT(param_to_reg_t, fn->parammap, i, pr)
      FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, j, n_to_reg)
        if (reg == n_to_reg->region)
          bitmap_set_bit(idxes, i);

    return idxes;
}
#endif /* if 0 */


/* Given two formal parameter indexes into function 'fn' return 'true' if they
 * both have regions in common.
 */
static bool share_regions_p(
    const rbmm_fn_data_t fn,
    int                  param_idx1,
    int                  param_idx2)
{
    unsigned i, j;
    region_t reg1, reg2;
    param_to_reg_t *pr1, *pr2;
    ssa_name_to_reg_t *n_to_r1, *n_to_r2;
    VEC(region_t,gc) *idx1_regs, *idx2_regs;

    idx1_regs = find_regions_by_param_idx(fn, param_idx1);
    idx2_regs = find_regions_by_param_idx(fn, param_idx2);

    FOR_EACH_VEC_ELT(region_t, idx1_regs, i, reg1)
      FOR_EACH_VEC_ELT(region_t, idx2_regs, j, reg2)
        if (reg1 == reg2)
          return true;

    /* More appropriate */
    if ((param_idx1 != -1 || param_idx2 != -1) && !fn->parammap)
      return false;

    pr1 = (param_idx1 == -1) ? 
        fn->returned_parammap:VEC_index(param_to_reg_t,fn->parammap,param_idx1);
    pr2 = (param_idx2 == -1) ?
        fn->returned_parammap:VEC_index(param_to_reg_t,fn->parammap,param_idx2);

    FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr1->regions, i, n_to_r1)
      FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr2->regions, j, n_to_r2)
        if (n_to_r1->region == n_to_r2->region)
          return true;

    return false;
}


/* Generate merge data and return that */
void detect_merges(
    rbmm_fn_data_t       caller, /* Set the merge data in this guy */
    const rbmm_fn_data_t callee,
    gimple               call,
    bool                 do_runtime_merging,
    bool                 for_tramp)
{
    int i, j;
    tree arg, decl;
    region_t caller_arg_reg, child;

    /* For each argument in the function call (-1 is for the return val) */
    for (i=-1; i<(int)gimple_call_num_args(call); ++i)
    {
        /* Get the arg we are using to call callee with */
        if (i == -1)
          arg = gimple_get_lhs(call);
        else 
          arg = gimple_call_arg(call, i);

        if (!arg || !REGION_CANDIDATE_TYPE_P(TREE_TYPE(arg)))
          continue;
          
        /* Get the region where 'arg' lives */  
        if (!(caller_arg_reg = find_region_by_decl(caller, get_base(arg))))
          continue;

        /* For each return and formal param of callee */
        for (j=-1; j<(int)gimple_call_num_args(call); ++j)
        {
            if (j == i)
              continue;

            /* Find regions which parameters i and j share */
            if (!share_regions_p(callee, i, j))
              continue;

            /* Get the child arg in the caller */
            decl = (j<0) ? get_base(gimple_get_lhs(call)) :
                           get_base(gimple_call_arg(call, j));
            if (!decl)
              continue;
            if (!(child = find_region_by_decl(caller, decl)))
              continue;

            RBMM_DEBUG(DEBUG_MERGE,
                       "Merge regions for %s and %s (arg %d and arg %d) "
                       "to call %s in %s",
                       get_ssaname(find_ssa_name_info(caller, arg, NULL)),
                       get_ssaname(find_ssa_name_info(caller, decl, NULL)),
                       i, 
                       j,
                       get_name(gimple_call_fn(call)),
                       get_name(cfun->decl));

            /* Got the urge.... to merge */
            if (do_runtime_merging)
              add_merge_data(caller,caller_arg_reg,child,call,for_tramp);
            else
              merge_regions(caller, caller_arg_reg, child);
        }
    }
}


/* Clone the function 'fndecl' or returned the already cloned version */
static tree clone_function(tree fndecl, const char *suffix)
{
    const char *name;
    unsigned i;
    tree dolly;
    struct cgraph_node *node;
    static VEC(tree,heap) *clones;
    
    /* What we call the cloned function */
    string_copy(name, "%s%s", get_name(fndecl), suffix);

    /* If we have already created the pristine version, don't do it again... */
    FOR_EACH_VEC_ELT(tree, clones, i, dolly)
      if (get_identifier(get_name(dolly)) == get_identifier(name))
        return dolly;

    node = cgraph_function_versioning(cgraph_get_node(fndecl), 
        NULL, NULL, NULL, NULL, NULL, "rename_me");
    dolly = node->decl;
    DECL_ARTIFICIAL(dolly) = 1;
    DECL_NAME(dolly) = get_identifier(name);
    SET_DECL_ASSEMBLER_NAME(dolly, DECL_NAME(dolly));
    VEC_safe_push(tree, heap, clones, dolly);
    return dolly;
}


/* If we are making a function call to an external (non-region-enabled
 * library/module) we cannot pass that function region-enabled stuff.
 * Therefore, pass it stuff only allocated from the global region.  So what we
 * do is mark things we would pass to it as being for the global region.
 */
static bool process_fncall_extern(rbmm_fn_data_t fn, gimple stmt)
{
    int i;
    tree arg;
    bool has_been_modified;
    ssa_name_info_t info;

    /* Get all ssanames and mark them global */
    has_been_modified = false;
    for (i=-1; i<(int)gimple_call_num_args(stmt); ++i)
    {
        arg = (i == -1) ? arg = gimple_get_lhs(stmt) : gimple_call_arg(stmt, i);
        if (!arg)
          continue;

        if (TREE_CODE(arg) == ADDR_EXPR)
          arg = TREE_OPERAND(arg, 0);

        if ((info = find_ssa_name_info(fn, arg, NULL)) &&
            !ALIASED_BY_GLOBAL_P(info))
        {
            info->aliased_state = ALIASED_BY_GLOBAL;
            has_been_modified = true;
        }
    }
    return has_been_modified;
}


/* Return true if we modify the aliased state of the ssa name */
static bool aliased_to_global_params(rbmm_fn_data_t fn, const_gimple call)
{
    int i, j, orig_state;
    bool has_been_modified;
    tree arg;
    region_t reg;
    ssa_name_info_t info;
    rbmm_fn_data_t callee;
    VEC(region_t,gc) *regs;

    has_been_modified = false;

    if (!(callee = find_fn_data(gimple_call_fndecl(call))))
      return has_been_modified;

    for (i=-1; i<gimple_call_num_args(call); ++i)
    {
        if (i == -1)
          arg = gimple_get_lhs(call);
        else
          arg = gimple_call_arg(call, i);

        info = find_ssa_name_info(fn, arg, NULL);
        orig_state = info->aliased_state;
        regs = find_regions_by_param_idx(callee, i);
        FOR_EACH_VEC_ELT(region_t, regs, j, reg)
          if (ALIASED_BY_GLOBAL_P(reg))
          {
              info->aliased_state |= ALIASED_BY_GLOBAL;
              if (info->aliased_state != orig_state)
                has_been_modified = true;
          }
    }

    return has_been_modified;
}


/* Given a function declaration, return a name to represent the transformed
 * version */
static const char *create_transformation_name(const_tree fndecl)
{
    const char *name;
    string_copy(name, "%s"RBMM_FN_SUFFIX, get_name((tree)fndecl));
    return name;
}


/* Given a function call statement, transform it to use the region-enabled
 * version of the function call.  This is often stuffed into the analysis
 * information's "clone_decl" member.
 * This is the first transformation.  I hate it.  This pass should not
 * transform at all, but we need a way of accessing the analysis data we
 * generate for functions.  And since we clone functions we need to access
 * their data too.  So we transform all functions we know MUST use the
 * transformed version (clone).
 * TODO: Clean up conditional
 */
static void transform_fncall_stmt(
    gimple         stmt,
    tree           orig_fndecl, /* Source code has non-transformed name */
    rbmm_fn_data_t call_data)
{
    tree dolly;

    if ((call_data->clone_decl || call_data->is_imported) &&
        !is_fnptr(gimple_call_fn(stmt))                   &&
        (get_identifier(get_name(orig_fndecl)) != call_data->clone_decl))
    {
        /* Create a clone now if the call is imported */
        if (call_data->is_imported && !call_data->clone_decl)
        {
            dolly = copy_node(orig_fndecl);
            DECL_NAME(dolly) = get_identifier(create_transformation_name(orig_fndecl));
            SET_DECL_ASSEMBLER_NAME(dolly, DECL_NAME(dolly));
            call_data->clone_decl = dolly;
            call_data->is_clone = true;
            RBMM_DEBUG(DEBUG_ANAL, 
                       "Transforming function call %s to use clone function %s",
                       get_name(orig_fndecl), get_name(dolly));
        }

        gimple_call_set_fndecl(stmt, call_data->clone_decl); /* Transform */
    }
}


/* Unify the lhs portion of a function call with arguments: 
 * lhs = call(a,b,c); 
 */
static void unify_fncall_lhs_to_args(
    rbmm_fn_data_t       fn,
    ssa_name_info_t      lhs,
    gimple               call,
    const rbmm_fn_data_t call_data)
{
    unsigned i, j;
    ssa_name_info_t arg;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *n_to_r;

    /* Look at all regions for the arguments passed to call_data */
    if (!call_data->parammap)
      return;

    FOR_EACH_VEC_ELT(param_to_reg_t, call_data->parammap, i, pr)
      FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, j, n_to_r)
        if (ALIASED_BY_RETURN_P(n_to_r->region))
        {
            arg = find_ssa_name_info(fn, gimple_call_arg(call, i), NULL);
            unify_info(fn, lhs, arg);
        }
}


/* Forward declaration */
static void set_alias_for_fncall(
    const_tree, rbmm_fn_data_t,  const rbmm_fn_data_t, int);


/* Analyize stmt and see if we need to create a region to pass into it: This
 * must occur after we have analyized all function (after pass 1) since
 * it requires knowledge of functions that are being called.
 * Returns TRUE if we add (modify) analysis data
 */
static bool process_fncall(rbmm_fn_data_t fn, gimple stmt)
{
    int i, j;
    unsigned before_alias, after_alias;
    bool has_been_modded;
    tree arg, lhs, fn_decl, base, member;
    region_t reg;
    ssa_name_info_t info;
    VEC(region_t,gc) *regs;
    rbmm_fn_data_t call_data;

    /* Check the args and if they are function pointers mark em to preserve */
    for (i=0; i<gimple_call_num_args(stmt); ++i)
      if (is_function_p(gimple_call_arg(stmt, i)))
      {
          fn_decl = gimple_call_arg(stmt, i);
          fn_decl = TREE_OPERAND(fn_decl, 0);
          if (fn_decl && (call_data = find_fn_data(fn_decl)))
            call_data->used_by_fnptr = true;
      }

    /* Bounce early if this is a function we cannot handle (get new fndecl) */
    if (!(fn_decl = can_handle_fncall(stmt)))
    {
        fn_decl = gimple_call_fn(stmt);
        if (TREE_CODE(fn_decl) == ADDR_EXPR)
          fn_decl = TREE_OPERAND(fn_decl, 0);
        if (!SSA_VAR_P(fn_decl) &&
            (DECL_EXTERNAL(fn_decl) && !is_rbmm_builtin(fn_decl)))
          return process_fncall_extern(fn, stmt);
        return false;
    }

    /* Check if this is recursive */
    if (cfun->decl == fn_decl)
      fn->recursive = true;

    /* Get data about the function we are calling */
    if (!(call_data = find_fn_data(fn_decl)))
      /* If the callee fn is from an extern lib compiled with rbmm */
      if (!(call_data=find_fn_data_name(create_transformation_name(fn_decl))))
        return false;

    transform_fncall_stmt(stmt, fn_decl, call_data);

    /* For fixed point... if we add more to our analysis, other functions that
     * rely on this puppy needa know about it too...
     */
    has_been_modded = false;

    /* Check the args being passed to the call are aliased to globals */
    has_been_modded |= aliased_to_global_params(fn, stmt);
    
    /* If call() is in a branch and we want to merge it
     * Add in note to __rbmm_runtime_merge().  This is all done via
     * "needs_merge"
     * This is disabled.  If we can compiletime merge we do that.
     */
/*    if (is_in_branch(gimple_bb(stmt))) */
/*      detect_merges(fn, call_data, stmt, true, false);*//* runtime merge     */
/*    else */
        detect_merges(fn, call_data, stmt, false, false); /* compiletime merge */

    /* Check to see if the LHS (returned value) needs a region for 'stmt' */
    info = NULL;
    if ((lhs = gimple_call_lhs(stmt)) && TREE_CODE(lhs) == ADDR_EXPR)
      lhs = TREE_OPERAND(lhs, 0);
    else if (lhs && (TREE_CODE(lhs) == MEM_REF))
      lhs = get_dereferenced_data(fn, lhs, &info);

    if (lhs && !REGION_CANDIDATE_TYPE_P(TREE_TYPE(lhs)))
      lhs = NULL_TREE;

    if (lhs && !info && !(info = find_ssa_name_info(fn, lhs, NULL)))
    {
        info = create_ssa_name_info(lhs);
        add_ssa_name_info(fn, info);
        has_been_modded = true;
    }

    /* Set allocation alias info from the returned region */
    if (lhs)
    {
        regs = find_regions_by_param_idx(call_data, -1);
        FOR_EACH_VEC_ELT(region_t, regs, i, reg)
          info->aliased_state |=
              reg->aliased_state & (ALIASED_BY_GLOBAL | ALIASED_BY_ALLOC);

        set_alias_for_fncall(lhs, fn, call_data, -1);
        unify_fncall_lhs_to_args(fn, info, stmt, call_data);
    }

    /* Check args to 'stmt' to see if we need regions */
    for (i=0; i<gimple_call_num_args(stmt); ++i)
    {
        arg = gimple_call_arg(stmt, i);

        if (TREE_CODE(arg) == ADDR_EXPR)
          arg = TREE_OPERAND(arg, 0);

        if (!REGION_CANDIDATE_TYPE_P(TREE_TYPE(arg)))
          continue;

        if (!(info = find_ssa_name_info(fn, get_base(arg), get_member(arg))))
        {
            base = get_base(arg);
            member = get_member(arg);

            if (!find_ssa_name_info(fn, base, NULL))
            {
                info = create_ssa_name_info(base);
                add_ssa_name_info(fn, info);
                has_been_modded = true;
            }

            if (member && !find_ssa_name_info(fn, base, member))
            {
                info = create_ssa_name_info(member);
                add_ssa_name_info(fn, info);
                add_member(fn, find_ssa_name_info(fn, base, NULL), info);
                has_been_modded = true;
            }
        }

        if (info && (cfun->decl != fn_decl)) /* Not recursive */
        {
            before_alias = info->aliased_state;
            info->aliased_state |= ALIASED_BY_CALLPARAM;
            
            /* OR in data from the callee's arg */
            regs = find_regions_by_param_idx(call_data, i);
            FOR_EACH_VEC_ELT(region_t, regs, j, reg)
              info->aliased_state |= (reg->aliased_state & 
                  ~(ALIASED_BY_RETURN|ALIASED_BY_CALLPARAM|ALIASED_BY_PARAM));
            after_alias = info->aliased_state;
            has_been_modded = (before_alias != after_alias);
        }

        set_alias_for_fncall(arg, fn, call_data, i);
    }

    return has_been_modded;
}


/* TODO: This is overly conservative.  Any arguments to the function pointer
 * belong to the global region.  The LHS which might have a region passes as an
 * argument to the function pointer must also be from the global region
 */
static bool process_fnptr_call(rbmm_fn_data_t fn, gimple stmt)
{
    tree arg;
    bool has_been_modified;
    unsigned i, before;
    ssa_name_info_t info;

    has_been_modified = false;

    /* LHS */
    if ((arg = gimple_get_lhs(stmt)) && 
        (info = find_ssa_name_info(fn, arg, NULL)))
    {
        before = info->aliased_state;
        info->aliased_state |= ALIASED_BY_GLOBAL;
        has_been_modified |= (before != info->aliased_state);
    }

    /* Arguments to the function pointer being called */
    for (i=0; i<gimple_call_num_args(stmt); ++i)
    {
        arg = gimple_call_arg(stmt, i);
        if ((info = find_ssa_name_info(fn, arg, NULL)))
        {
            before = info->aliased_state;
            info->aliased_state |= ALIASED_BY_GLOBAL;
            has_been_modified |= (before != info->aliased_state);
        }
    }

    return has_been_modified;
}


/* Uniquely add a fndecl whose address is taken in this module, and is extern to
 * this module.
 */
static void add_needed_extern_fnptr(rbmm_fn_data_t fn, tree fndecl)
{
    unsigned i;
    tree fnptr;

    FOR_EACH_VEC_ELT(tree, fn->needed_extern_fnptrs, i, fnptr)
      if (fndecl == fnptr)
        return;

    RBMM_DEBUG(DEBUG_OBJ, "Adding needed function: %s", get_name(fndecl));
    VEC_safe_push(tree, gc, fn->needed_extern_fnptrs, fndecl);
}


/* This is overly coservative.  All regions associated with return and arguments
 * to the fnptr will belong to the global region
 */
static bool process_fnptr_assign(rbmm_fn_data_t fn, gimple stmt)
{
    rbmm_fn_data_t call_data;
    tree node;

    node  = get_base(gimple_assign_rhs1(stmt));
   
    if ((call_data = find_fn_data(node)))
      call_data->used_by_fnptr = true;

    if (DECL_EXTERNAL(node) && TREE_CODE(node) == FUNCTION_DECL)
      add_needed_extern_fnptr(fn, node);

    return false;
}


/* Trace through points_to list for all points_to to points_to... (recursive)
 * for info.  If 'same_region_id' is set to 'true' then we only get alias data
 * from nodes that are in the same region.
 */
static unsigned get_points_to_alias(ssa_name_info_t info, bool same_region_id)
{
    unsigned i, alias;
    ssa_name_info_t pts_to;

    if (!info)
      return ALIASED_BY_NONE;

    alias = info->aliased_state;

    FOR_EACH_VEC_ELT(ssa_name_info_t, info->points_to, i, pts_to)
    {
        if (same_region_id && (pts_to->region_uid != info->region_uid))
          continue;
        alias |= pts_to->aliased_state;
        alias |= get_points_to_alias(pts_to, same_region_id);
    }

    return alias;
}


/* Returns 'true' if the region or name is aliased in such a way that we need to pass in
 * a region to the current function for reg.
 * If we returned allocated data OR if we pass a object who needs a region
 * to a function this function calls (a callee) OR if we take a parameter
 * and allocate data to it
 */
#define ORIG 0
#if ORIG == 1
#define needs_formal_param_p(_x) \
  (!GLOBAL_REGION_P(_x)                                     && \
    (ALIASED_BY_RETURN_P(_x) && ALIASED_BY_CALLPARAM_P(_x)) || \
    (ALIASED_BY_RETURN_P(_x) && ALIASED_BY_ALLOC_P(_x))     || \
    (ALIASED_BY_PARAM_P(_x) && ALIASED_BY_CALLPARAM_P(_x))  || \
    (ALIASED_BY_PARAM_P(_x) && ALIASED_BY_ALLOC_P(_x)))
#else
#define needs_formal_param_p(_x) (!GLOBAL_REGION_P(_x)&&ALIASED_BY_ALLOC_P(_x))
#endif


VEC(ssa_name_info_t,gc) *get_who_points_to(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t info)
{
    unsigned i, j;
    ssa_name_info_t name, pt;
    VEC(ssa_name_info_t,gc) *pts_to;

    pts_to = VEC_alloc(ssa_name_info_t, gc, 0);

    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, name)
      FOR_EACH_VEC_ELT(ssa_name_info_t, name->points_to, j, pt)
        if (pt == info)
          unique_add_to_name_vec_gc(&pts_to, name);

    return pts_to;
}


static ssa_name_info_t find_member_from_id_type(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t parent,
    const_tree            node)
{
    unsigned i;
    const char *name;
    const_tree id, type;
    ssa_name_info_t child;
    VEC(ssa_name_info_t,gc) *pts_to;

    if (!parent || !node) // TODO binary-tree-free list produces a null node
      return NULL;

    id = NULL_TREE;
    if ((name = get_name((tree)node)))
      id = get_identifier(name);
    type = TREE_TYPE(node);

    FOR_EACH_VEC_ELT(ssa_name_info_t, parent->members, i, child)
      if ((child->identifier == id) && (type == TREE_TYPE(child->ssaname)))
        return child;

    /* Ok, so maybe this is a temp variable... find who points to this and
     * search them.
     */
    pts_to = get_who_points_to(fn, parent);
    FOR_EACH_VEC_ELT(ssa_name_info_t, pts_to, i, child)
      if ((child->identifier == id) && (type == TREE_TYPE(child->ssaname)))
        return child;

    return NULL;
}


/* Set the alias data for the 'arg' that the 'caller' is passing to the
 * 'callee'.  The callee might assign memory allocation information to the arg
 * being based.
 */
static void set_alias_for_fncall(
    const_tree           arg, 
    rbmm_fn_data_t       caller,
    const rbmm_fn_data_t callee,
    int                  call_idx) /* -1 for return variable */
{
    unsigned i;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *n_to_reg;
    ssa_name_info_t info, mem;

    /* Get the param to region map from the callee */
    if (call_idx == -1)
      pr = callee->returned_parammap;
    else if (VEC_length(param_to_reg_t, callee->parammap) > call_idx)
      pr = VEC_index(param_to_reg_t, callee->parammap, call_idx);
    else
      return;

    /* For each region that is returned, alias it to the proper name/member that
     * is being passed (arg)
     */
    mem = NULL;
    info = find_ssa_name_info(caller, arg, NULL);
    FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, i, n_to_reg)
    {
        /* Avoid aliasing properties unique to the callee.  We only care about
         * if the type being passed as an argument to the function is being
         * assigned ALLOCATION data or to a GLOBAL
         */
        if (n_to_reg->info) // TODO: fnptr have NULL
          mem = find_member_from_id_type(caller, info, n_to_reg->info->ssaname);
        if (!mem)           // TODO: If no mem... maybe we just return early
          mem = info;
        mem->aliased_state |= 
            n_to_reg->region->aliased_state & 
            (ALIASED_BY_GLOBAL | ALIASED_BY_ALLOC | ALIASED_BY_SLICE);
    }
}


/* Given an index into the TYPE_FIELD list for a given data type:
 * Get the associated members of 'parent' and figure out which one has a
 * matching identifier.
 */
static ssa_name_info_t find_member_at_index(
    const rbmm_fn_data_t     fn,
    const ssa_name_info_t    parent,
    const ssa_name_to_reg_t *callee)
{
    int field_idx, index;
    unsigned i;
    const char *field_name;
    tree field, field_id, type;
    ssa_name_info_t info;

    if (!parent)
      return NULL;

    index = callee->member_index;

    /* Get the type of parent */
    type = (tree)parent->ssaname;
    while (type && !RECORD_OR_UNION_TYPE_P(type))
      type = TREE_TYPE(type);

    if (!type)
      return NULL;

    /* Locate the field type information */
    field_idx = 0;
    for (field=TYPE_FIELDS(type);
         field && field_idx!=index;
         field=DECL_CHAIN(field))
      ++field_idx;

    if (field_idx != index || field == NULL_TREE)
      return NULL;

    /* Get the identifier for the field name */
    if (!(field_name = get_name(field)))
      return NULL;

    field_id = get_identifier(field_name);

    /* Search all members of parent and find the one with the same id */
    FOR_EACH_VEC_ELT(ssa_name_info_t, fn->names, i, info)
      if (info->base == parent && info->identifier == field_id)
        return info;

    return NULL;
}


/* Given a variable.  Traverse the points-to graph to find the first version of
 * this variable that is not a compiler inserted/ssa variable. 
 */
static ssa_name_info_t find_first_non_artificial(const ssa_name_info_t info)
{
    unsigned i;
    ssa_name_info_t test;

    if (!DECL_ARTIFICIAL(info->ssaname))
      return info;

    FOR_EACH_VEC_ELT(ssa_name_info_t, info->points_to, i, test)
      if (!DECL_ARTIFICIAL(test->ssaname))
        return test;
      else if (test = find_first_non_artificial(test))
        return test;

    return NULL;
}


/* Returns 'true' if the region is already in the parammap */
static bool region_exists_in_maps_p(
    const region_t          reg,
    VEC(param_to_reg_t,gc) *themap)
{
    unsigned i, j;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *n_to_reg;

    FOR_EACH_VEC_ELT(param_to_reg_t, themap, i, pr)
      FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, j, n_to_reg)
        if (reg == n_to_reg->region)
          return true;

    return false;
}


static bool region_exists_in_map_p(const region_t reg, param_to_reg_t *themap)
{
    VEC(param_to_reg_t,gc) *maps = VEC_alloc(param_to_reg_t, gc, 1);
    VEC_quick_push(param_to_reg_t, maps, themap);
    return region_exists_in_maps_p(reg, maps);
}


/* Return the first region that is shared by the formal params and return */
static region_t find_shared_ret_region(
    const_gimple         call,
    const rbmm_fn_data_t caller,
    const rbmm_fn_data_t callee,
    int                  ret_parammap_idx) /* Entry in the return parammap */
{
    unsigned i, j;
    int caller_param_idx;
    tree arg;
    region_t reg;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *n_to_r, *callee_n_to_r;

    if (!VEC_length(ssa_name_to_reg_t, callee->returned_parammap->regions))
      return NULL;

    /* Get the callee region */
    callee_n_to_r =  VEC_index(
        ssa_name_to_reg_t,callee->returned_parammap->regions,ret_parammap_idx);
    reg = callee_n_to_r->region;
    caller_param_idx = -1;

    /* Search each callee param's lookin for where 'reg' is also used */
    FOR_EACH_VEC_ELT(param_to_reg_t, callee->parammap, i, pr)
    {
        FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, j, n_to_r)
          if (reg == n_to_r->region)
          {
              caller_param_idx = i;
              break;
          }

        if (caller_param_idx != -1)
          break;
    }

    /* Get the variable the caller is passing to callee */
    arg = gimple_call_arg(call, caller_param_idx);

    /* Use the region of 'arg' as an alternative to the one in the ret reg */
    return find_region_by_decl(caller, arg);
}


/* Return the basic block which has the return statement in it */
static inline basic_block get_return_bb(void)
{
    return EDGE_PRED(EXIT_BLOCK_PTR, 0)->src;
}


static tree get_type_from_call(const_gimple stmt, int index)
{
    if (index == -1)
      return TREE_TYPE(TREE_TYPE(gimple_call_fndecl(stmt)));
    else
      return TREE_TYPE(gimple_call_arg(stmt, index));
}


/* Given a function call 'stmt' and a decl determine if we need to create a
 * region, and if so do it.  Note, it is assumed that any 'decl' we pass here is
 * directly related to the calling of 'stmt'.  Decl might be an input argument
 * we are passing to 'stmt' or a return value of 'stmt' that we are setting.
 * Returns the set of regions we need to pass as args to the call. Pass decl_idx
 * as -1 if you are trying to findout if the call needs a region for the return
 * value.
 */
static VEC(region_t,gc) *check_if_call_needs_region(
    const rbmm_fn_data_t fn,
    const_gimple         call,
    const_tree           decl,
    int                  decl_idx)
{
    unsigned i;
    tree fn_decl;
    rbmm_fn_data_t call_data;
    ssa_name_info_t parent, mem;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *callee_reg;
    region_t reg, last_reg;
    VEC(region_t,gc) *regs;

    /* No decl, e.g. when the lhs is not set.  And the fn() needs regions for
     * the lhs/return value.  Such is the case if fn() returns values allocated
     * inside fn().  But the caller does not have a lhs.  Memory will leak
     * otherwise.
     */
    if (!decl && (decl_idx != -1))
      return NULL;

    /* Data about the callee so we can see if we need to pass regions to it */
    if (!(fn_decl = can_handle_fncall(call)))
      return NULL;

    /* Get the regions needed for the parameter 'decl_idx' */
    if (!(call_data = find_fn_data(fn_decl)))
      return NULL;

    /* Parent ssaname (the variable being passed to call) */
    if (decl && TREE_CODE(decl) == MEM_REF)
      decl = get_dereferenced_data(fn, decl, &parent);
    else
      parent = find_ssa_name_info(fn, decl, NULL);
    
    /* Find the parameter data for the 'decl_idx' param of the callee */
    if (decl_idx == -1) /* <--- Return variable regions */
      pr = call_data->returned_parammap;
    else if (call_data->parammap)
      pr = VEC_index(param_to_reg_t, call_data->parammap, decl_idx);
    else
      return NULL;

    /* Build a list of regions needed for param 'decl_idx' into callee */
    last_reg = NULL;
    regs = VEC_alloc(region_t, gc, 0);
    FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, i, callee_reg)
    {
        if (!callee_reg->use_as_formal_param)
          continue;

        /* Check the first entry and use the parent's region if we can... if not
         * we only need to pass a member contained within the parent
         * FIXME: VEC_index assumes that parent->members was created in order,
         * this is a loose assumption based on gccgo frontend ordering of return
         * values.
         */
        if ((decl_idx == -1) && parent && parent->members &&
            (i < VEC_length(ssa_name_info_t, parent->members)) &&
            (TREE_CODE(parent->ssaname) == VAR_DECL))
        {
            if (!(mem = VEC_index(ssa_name_info_t, parent->members, i)))
              mem = parent;
        }
        else if (i == 0 && callee_reg->info && !callee_reg->info->base)
          mem = parent;
        else if (callee_reg->info||(callee_reg->member_index!=-1)) /* Import */
          mem = find_member_at_index(fn, parent, callee_reg);
        else if (!callee_reg->info&&callee_reg->member_index==-1)  /* Import */
          mem = parent;
        else
          mem = NULL;

        /* If its a region for a member of a return value */
        if (!mem && (i != 0) && (decl_idx == -1) && callee_reg->info)
          mem = find_member_from_id_type(fn,parent,callee_reg->info->ssaname);

        /* Last check for member TODO: Is this a safe assumption? */
        if (!mem)
          mem = parent;

        /* Find a region for the variable */
        reg = NULL;
        if (mem)
          reg = find_region_by_id(fn, mem->region_uid);
        
        /* If no region, search lhs, and all params for regions 
         * Still, if no reg, create temp one
         */
        if (!reg && decl_idx == -1)
        {
            reg = find_shared_ret_region(call, fn, call_data, i);

            /* If the region being pased as an argument, and is shared with a
             * return region (if we have a reg instance here), and if there is no
             * lhs to accept the region, remove the region we use, to avoid
             * leaking.
             */
            if (reg && !gimple_get_lhs(call))
              reg->force_region_removal = true;
        }
        
        /* If we do not have a member pass a temporay region to avoid leaks */
        if (!reg && (decl_idx == -1) && !decl)
        {
            if (!last_reg)
            {
                tree type = get_type_from_call(call, decl_idx);
                reg = new_temp_region_info(fn, type);
            }
            else
              reg = last_reg;
            last_reg = reg;
        }
        else if (!reg) /* Normal argument needs temp region */
        {
            /* If the region is for a member of a variable passed in another
             * argument, just allocated from the "parents" region, and avoid
             * creating a temp region.
             */
            if (parent && callee_reg->member_index != -1)
              reg = find_region_by_id(fn, parent->region_uid);
            else if (fn->is_specialized_version)
              reg = global_region;
            if (!reg)
              reg = new_temp_region_info(fn, TREE_TYPE(mem->ssaname));
        }

        /* Need a place to eventually kill off the temp region */
        if (TEMP_REGION_P(reg))
        {
            reg->aliased_state |= 
                callee_reg->region->aliased_state & 
                (ALIASED_BY_GLOBAL | ALIASED_BY_ALLOC | ALIASED_BY_SLICE);
            //add_live_bbs(fn, reg, parent);
            VEC_safe_push(basic_block, heap, reg->live_bbs, get_return_bb());
        }

        VEC_safe_push(region_t, gc, regs, reg);
    }

    return regs;
}


/* Get outer most loop where stmt is inside of.
 * If defbb is also in one of those loops get the loop just after defbb.
 * 
 * Example:  Return loop2 (NOTE: if defbb was not in any of these, loop0 would be
 * returned:
 * loop0
 *   loop1
 *      ... defbb ...
 *     loop2
 *       loop3
 *         ... stmt ...
 *
 * NOTE: This is for inc_dec_ref_count()
 */
static inline loop_p get_outermost_loop(gimple stmt, const_basic_block defbb)
{
    loop_p fruit, prev;
    basic_block bb;
    
    bb = gimple_bb(stmt);

    /* No nested loops (or we dont know where the reg was created) */
    if (!bb->loop_father->superloops || !defbb)
      return bb->loop_father;

    /* Nested loops */
    for (fruit=bb->loop_father; fruit; fruit=loop_outer(fruit))
      if (defbb->loop_father == fruit)
         break;

    if (fruit == NULL)
      fruit = bb->loop_father;

    /* fruit is inside the loop nest: so get the immediatly following loop */
    if (defbb->loop_father == fruit)
    {
        prev = bb->loop_father;
        for (fruit=bb->loop_father; fruit; fruit=loop_outer(fruit))
          if (fruit == defbb->loop_father)
            return prev;
          else
            prev = fruit;
    }

    return fruit;
}


/* Determine if we need to increment or decrement the reference counter.  'stmt'
 * is a function call.   This routine will be called once to determine if we
 * need to increment the reference counter before 'stmt' is called, and once
 * afterwards.  TRUE is returned if we incremented or decremented the counter.
 * 
 * that 'reg' reference counter should be incremented.
 */
static bool inc_dec_ref_count(
    rbmm_fn_data_t fn,
    region_t       reg,
    gimple         stmt,
    bool           dec) /* Decrement if true else increment */
{
    unsigned i;
    edge e;
    edge_iterator ei;
    basic_block bb, defbb, dest;
    VEC(edge,gc) *edges_gc;
    VEC(edge,heap) *edges_h;
    struct loop *fruit;

    if (ALIASED_BY_GLOBAL_P(reg))
      return false;

    /* We need loop data */
    if (!current_loops)
      loop_optimizer_init(LOOPS_NORMAL|LOOPS_HAVE_RECORDED_EXITS);
    bb = gimple_bb(stmt);
   
    /* If we need to protect the region since its live after 
     * this stmt or in a loop...
     */
    if (fn->recursive || bb->loop_depth || dec || TEMP_REGION_P(reg) ||
        ((region_live_after_p(reg,stmt)) && !GLOBAL_REGION_P(reg)))
    {
        /* Simple case... no loops (or region created in same loop) */
        defbb = gimple_bb(SSA_NAME_DEF_STMT(reg->region_var));
        if (!defbb || bb->loop_depth==0 || (bb->loop_depth==defbb->loop_depth))
        {
            incdec_ref_count_bn(reg, stmt, dec);
            return true;
        }

        /* If we reach this point we are in a loop.  Now get the loop info we
         * need:the outermost loop or the loop the reg was created in (defbb)
         */
        fruit = get_outermost_loop(stmt, defbb);
        
        /* In a loop and checking before a call to stmt then return the stmt
         * before the loop.  So we dont repeadely increment/decrement in the
         * loop
         */
        if (dec)
        {
            /* Place decrement as first statement leading FROM this */
            edges_h = get_loop_exit_edges(fruit);
            FOR_EACH_VEC_ELT(edge, edges_h, i, e)
            {
                dest = e->dest;
                while (!bb_seq(dest)->first)
                  dest = EDGE_SUCC(dest, 0)->dest;
                if ((can_reach(defbb, dest) || (defbb->index == dest->index)))
                  incdec_ref_count_bn(reg, gsi_stmt(gsi_start_bb(dest)), true);
            }
        }
        else /* In loop: increment */
        {
            edges_gc = fruit->header->preds;
            FOR_EACH_EDGE(e, ei, edges_gc)
            {
                /* Not at the same loop and the creation can see the inc */
                if (e->src->loop_depth < fruit->header->loop_depth &&
                   (can_reach(defbb,e->src) || (defbb->index == e->src->index)))
                  incdec_ref_count_bn(reg,gsi_stmt(gsi_last_bb(e->src)),false);
            }
        }
        return true;
    }
      
    return false;
}


static inline basic_block get_bb(unsigned index)
{
    return BASIC_BLOCK_FOR_FUNCTION(cfun, index); 
}


/* Move the region creation stmt for 'reg' so that it can reached seen by 'bb' */
static bool move_create(region_t reg, basic_block bb)
{
    edge e;
    edge_iterator ei;
    basic_block created, move_to;
    gimple_stmt_iterator gsi;

    created = gimple_bb(reg->created_stmt);

    if (created == bb)
      return false;

    FOR_EACH_EDGE(e, ei, bb->preds)
    {
        /* Recurse until we can find a node to move to */
        if (!dominated_by_p(CDI_DOMINATORS, created, e->src))
          if (move_create(reg, e->src))
            return true;

        move_to = (e->src->index != 0) ? e->src : get_bb(2);

        /* Remove original statement */
        gsi = gsi_for_stmt(reg->created_stmt);
        gsi_remove(&gsi, true);

        /* Get last statement */
        gsi = gsi_last_bb(move_to);
        
        /* If there are no statements in the block */
        if (!gimple_seq_first(gsi_seq(gsi)))
          gsi = gsi_last_bb(get_bb(2));

        /* Create the original statement, but in a differnt location */
        create_region_bn(gsi_stmt(gsi), reg);
        reg->created_stmt = gsi_stmt(gsi);
        return true;
    }
}


/* Insert region as an argument to the fncall in 'stmt'
 * This also increments/decrements the protection counter, if it hasnt already
 * been inc/dec as listed in 'added_inc/added_dec' bitmaps.
 */
static gimple add_region_arg_and_increments_to_fn(
    rbmm_fn_data_t fn,
    region_t       reg,
    int            reg_idx,
    int            call_idx, /* -1 for return value */
    gimple         stmt,
    bitmap         added_inc,
    bitmap         added_dec)
{
    bool incremented;

    if (!reg)
      return stmt;

    if (!reg->region_var)
      create_region_bn(stmt, reg);
    else if (reg->created_stmt) /* Will be null if created for formal param */
    {
        /* Make sure the bb where reg was created can reach region for stmt */
        if (!can_reach(gimple_bb(reg->created_stmt), gimple_bb(stmt)))
          move_create(reg, gimple_bb(stmt));
    }

    /* If this region will be used later we need to +1 its ref count so that
     * the callee doesnt whack it.  (If we havent already incremented it)
     */
    if (!(incremented = (bool)bitmap_bit_p(added_inc, reg->id)))
      incremented = inc_dec_ref_count(fn, reg, stmt, false);

    /* global_region's region_var has the DECL node and not a SSANAME */
   if (reg == global_region)
     if (!(reg = find_global_region_reference(fn)))
     {
         reg = new_region_info(fn, 0);
         reg->aliased_state = ALIASED_BY_GLOBAL;
         reg->region_var = assign_global_to_temp(stmt);
         fn->global_temp_var = true;
     }

    /* If for return arg */   
    if (call_idx == -1)
      stmt = add_region_arg_to_fncall(
             reg, reg_idx, stmt, gimple_call_lhs(stmt));
    else
      stmt = add_region_arg_to_fncall(
             reg, reg_idx, stmt, gimple_call_arg(stmt, call_idx));

    /* Decrement region reference count if necessary */
    if (incremented && !bitmap_bit_p(added_dec, reg->id))
      inc_dec_ref_count(fn, reg, stmt, true);

    return stmt;
}


/* This is now empty.  All functions remain untouched/un-transformed by this
 * plugin.  We clone functions that are transformed and fix-up the calls to
 * them.  So, we do not need to do anything for fnptr assignments.  Since the
 * fnptr function is left untransformed (global region).
 */
static void process_regions_for_fnptr_args(gimple stmt)
{
    return;
}


/* Check the function call that is being called in 'stmt'
 * If the called function expect regions for any of the original arguments pass
 * thoes regions.  Increment/decrement region calls are also created here,
 * protecting the regions from being removed by the function-to-be-called if we
 * still need thoes regions after the call.
 */
static void process_regions_for_fncall(rbmm_fn_data_t fn, gimple stmt)
{
    int i, j, reg_idx, n_params, n_orig_params;
    region_t reg;
    bitmap added_inc, added_dec;
    rbmm_fn_data_t call_data;
    VEC(region_t, gc) *regs;

    /* First: Process arguments that might be function pointers */
    process_regions_for_fnptr_args(stmt);
        
    /* Do not process functions defined in other modules */
    if (!can_handle_fncall(stmt))
      return;
    
    /* Since this is a GIMPLE_CALL, the trampoline_bn will scour the arguments
     * looking for function pointers and swap them to trampolines
     */
    //trampoline_bn(fn, stmt); /* TODO: Disable all trampoline stuff */
    
    /* Bitmap of region ids, so we dont inc/dec a reg counter more than once */ 
    added_inc = BITMAP_GGC_ALLOC(); /* Bit is the region id */
    added_dec = BITMAP_GGC_ALLOC(); /* Bit is the region id */

    /* Original parameters to the function call */
    n_params = gimple_call_num_args(stmt);
    n_orig_params = gimple_call_num_args(stmt);

    /* If we have already modified this... bail now! */
    call_data = find_fn_data(gimple_call_fndecl(stmt));
    if (call_data && call_data->n_orig_params < n_orig_params)
      return;

    /* Get the function's ret val and set fn to know about this */
    if ((regs = check_if_call_needs_region(fn, stmt, gimple_call_lhs(stmt),-1)))
      FOR_EACH_VEC_ELT(region_t, regs, i, reg)
      {
          reg_idx = n_params;
          stmt = add_region_arg_and_increments_to_fn(
                 fn, reg, reg_idx, -1, stmt, added_inc, added_dec);
          n_params = gimple_call_num_args(stmt);
      }
    
    /* Args */
    for (i=0; i<n_orig_params; ++i)
    {
        /* Just a normal (non-function address) argument */
        if ((regs = check_if_call_needs_region(
             fn, stmt, gimple_call_arg(stmt, i), i)))
        {
            FOR_EACH_VEC_ELT(region_t, regs, j, reg)
            {
                reg_idx = n_params + j;
                stmt = add_region_arg_and_increments_to_fn(
                       fn, reg, reg_idx, i, stmt, added_inc, added_dec);
            }
            n_params = gimple_call_num_args(stmt);
        }
    }
}


/* Check an allocation site and see if we need to create a region or not for the
 * lhs of the allocation call
 */
static void process_regions_for_allocation(rbmm_fn_data_t fn, gimple stmt)
{
    tree id, lhs, size_arg;
    region_t reg;
    basic_block created_bb, alloc_bb;
    ssa_name_info_t info;

    id = get_identifier(get_name(gimple_call_fn(stmt)));

    if (id == get_identifier("__go_map_index"))
      lhs = gimple_call_arg(stmt, 0);
    else
      lhs = gimple_call_lhs(stmt);
    
    /* Return early if we do not have sufficient information */
    if (!(lhs=gimple_call_lhs(stmt))|| !(info=find_ssa_name_info(fn,lhs,NULL)))
      return;

    /* Find the region, if we dont have one abort() */
    if (!(reg = find_region_by_id(fn, info->region_uid)))
      abort();
    
    /* So it's aliased by something... emit code to create the region */
    if (!reg->region_var)
    {
        RBMM_DEBUG(DEBUG_ANAL,
                   "Create local region id(%d) for %s",
                   reg->id, get_name(lhs));
        create_region_bn(stmt, reg);
    }

    /* Move the create if we cannot see it from the alloc point 'stmt' */
    created_bb = reg->created_stmt ? gimple_bb(reg->created_stmt) : NULL;
    alloc_bb = gimple_bb(stmt);
    if (created_bb && !dominated_by_p(CDI_DOMINATORS, alloc_bb, created_bb))
      move_create(reg, gimple_bb(stmt));
    
    /* If its a map, it gets special treatment */
    if (id ==  get_identifier("__go_new_map"))
      create_map_bn(reg, stmt);
    else if (id == get_identifier("__go_map_index"))
      map_index_bn(reg, stmt);

    /* Slices also get special treatment */
    else if (id == get_identifier("__go_make_slice1"))
      slice1_make_bn(fn, reg, stmt);
    else if (id == get_identifier("__go_make_slice2"))
      slice2_make_bn(fn, reg, stmt);

    /* Append gets special treatment too */
    else if (id == get_identifier("__go_append"))
      append_bn(reg, stmt);

    /* This guy gets special handling as well */
    else if (id == get_identifier("__go_construct_map"))
      construct_map_bn(reg, stmt);

    /* __go_new() or __go_new_nopointers() for allocating a slice data region
     * but whose slice header is on the stack or setup automatically
     * This is only useful if we are building RBMM with our GC.
     */
//#ifdef RBMM_GC
//    else if (is_slice_type_p(TREE_TYPE(lhs)) || 
//                 (POINTER_TYPE_P(TREE_TYPE(lhs)) &&
//                  is_slice_type_p(TREE_TYPE(TREE_TYPE(lhs)))))
//      alloc_slice_values_bn(fn, reg, stmt);
//#endif

    /* __go_new() or __go_new_nopointers() */
    else
    {
        size_arg = gimple_call_arg(stmt, 0);
        alloc_from_region_bn(fn, stmt, reg, lhs, size_arg);
    }
        
    RBMM_DEBUG(DEBUG_ANAL, "Allocate memory from region %d for %s",
               reg->id, 
               TREE_CODE(lhs)==SSA_NAME ? 
                   get_name(SSA_NAME_VAR(lhs)) : get_name(lhs));
}


/* Check the name of the tree node to see if it might be an interface method
 * table
 */
static bool inline is_interface_method_table(tree imt)
{
    return false; /* TODO */
    return (get_name(imt) &&
            (strstr(get_name(imt), "__go__imt_") ||
             strstr(get_name(imt), "__go__pimt_")));
}


/* The analysis pass does all the magic for assignments.  And we dont need to do
 * any transformation... UNLESS the assignment has a fnptr.
 */
static void process_regions_for_assignment(rbmm_fn_data_t fn, gimple stmt)
{
    tree base, member;

    /* If its an interface method table */
    if (is_interface_method_table(gimple_assign_rhs1(stmt)))
    {
        /* TODO 
         * specialize_interface_methods()
         * -- If lhs is global... do nothing
         * -- Else specialize
         */
    }

    /* Functioin ptr being assigned.  Let it use the original function. */
    else if (is_fnptr(gimple_assign_lhs(stmt)) ||
             is_function_p(gimple_assign_rhs1(stmt)))
    {
        ; /* Do nothing */
    }

    /* Not an interface method table, maybe its a fnptr */
    else
    {
        base = get_base(gimple_get_lhs(stmt));
        member = get_member(gimple_get_lhs(stmt));
        if (member && is_fnptr(member))
          trampoline_bn(fn, stmt);
        else if (is_fnptr(base))
          trampoline_bn(fn, stmt);
    }
}


/* Fnptr calls all use the global region.  And the specialized version of the
 * call being made does not need transformation.
 */
static void process_regions_for_fnptr_call(rbmm_fn_data_t fn, gimple stmt)
{
    return;
}


/* The points_to member can have cycles.  To avoid processing data in cycles,
 * a function can use the 'mark' member of the ssa_name_info_t type.  This
 * function clears the marks for all ssa_name_info_t names in 'fn'
 */
static inline void add_live_in_bb(
    rbmm_fn_data_t  fn,
    ssa_name_info_t info,
    basic_block     bb)
{
    unsigned i;
    ssa_name_info_t pt;
    VEC(ssa_name_info_t,gc) *pts_to;

    if (bb)
      VEC_safe_push(basic_block, heap, info->live_bbs, bb);

    pts_to = get_who_points_to(fn, info);

    /* Add live bb to points to if the pointed to node is a member who is also
     * in the same data type as 'info' OR if the parent node is 'info'
     */
    FOR_EACH_VEC_ELT(ssa_name_info_t, pts_to, i, pt)
      if (pt->base && (pt->base == info->base || pt->base == info))
        VEC_safe_push(basic_block, heap, pt->live_bbs, bb);
}


/* Create the max num of regions our unification type analysis has produced */
static void names_to_regions(rbmm_fn_data_t fn, VEC(ssa_name_info_t,heap) *names)
{
    unsigned i, j;
    region_t reg;
    ssa_name_info_t info;

    /* Run this twice to make sure we get all data propagated:
     * primarily for _add_live_bbs.
     * FIXME: Update _add_live_bbs and remove this 2x loop
     * FIXME: The _add_live_bbs piece can be fixed if we associate
     *        members and allocations properly in process_fncall()
     */
    for (i=0; i<2; ++i)
      FOR_EACH_VEC_ELT(ssa_name_info_t, names, j, info)
      {
          /* Create region info if it doesn't exist and add ourself */
          if (!(reg = find_region_by_id(fn, info->region_uid)))
            reg = new_region_info(fn, info->region_uid);

          add_to_region(fn, reg, info);
          names_to_regions(fn, info->members);
      }
    
    fixup_duplicate_names(fn);
}


/* If parent is true, return the parent of the merge:
 * parent ---subsumes---> child
 */
#if 0
static region_t get_mergee(region_t reg1, region_t reg2, bool get_parent)
{
    region_t parent, child;

    if (reg1->region_var_idx == reg2->region_var_idx)
    {
        parent = reg1->id < reg2->id ? reg1 : reg2;
        child  = reg1->id > reg2->id ? reg1 : reg2;
    }
    else
    {
        parent = (reg1->region_var_idx < reg2->region_var_idx) ?
                  reg1 : reg2;
        child = (reg1->region_var_idx > reg2->region_var_idx) ?
                 reg1 : reg2;
    }

    return (get_parent) ? parent : child;
}
#endif


/* Create fndata with one region per region argument and with the ssaname for
 * the associated input parameter.
 */
void create_tramp_fndata(tree tramp_fndecl)
{
    tree param;
    int i, n_regions;
    //bool has_return_region;
    rbmm_fn_data_t tramp_fndata;
    ssa_name_info_t name;
    
    tramp_fndata = XCNEW(struct _rbmm_fn_data_d);
    tramp_fndata->func_decl = tramp_fndecl;
    VEC_safe_push(rbmm_fn_data_t, heap, rbmm_analyized_fns, tramp_fndata);

    /* Add return region (strip off fndecl and function_type nodes) */
    if (REGION_CANDIDATE_TYPE_P(TREE_TYPE(TREE_TYPE(tramp_fndecl))))
    {
        name = create_ssa_name_info(create_tmp_var(ptr_type_node, "DUMMY"));
        name->aliased_state = ALIASED_BY_RETURN;
        add_ssa_name_info(tramp_fndata, name);
        //has_return_region = true;
    }

    /* Add in all additional args */
    i = n_regions = 0;
    for (param=DECL_ARGUMENTS(tramp_fndecl); param; param=DECL_CHAIN(param))
    {
        if (TREE_TYPE(param) == __rbmm_region_type)
          ++n_regions;
        else
        {
            ssa_name_info_t name = 
                create_ssa_name_info(gimple_default_def(cfun, param));
            name->aliased_state = ALIASED_BY_CALLPARAM;
            name->param_decl_idx = i;
            add_ssa_name_info(tramp_fndata, name);
            ++i;
        }
    }
    tramp_fndata->n_orig_params = count_fnparams(tramp_fndecl) - n_regions;

    names_to_regions(tramp_fndata, tramp_fndata->names);

    /* Now for each region look at its single ssa name and set its region_var to
     * the default_def from the PARAM_DECL
     */
    for (i=0; i<tramp_fndata->n_orig_params; ++i)
    {
        /* Get the region param (remember this is a tramp, worse case each
         * pointer and return pointer has an associated region
         */
        //reg = find_region_by_param_idx(tramp_fndata, i);
     //   idx = tramp_fndata->n_orig_params + i + has_return_region;
    //    reg->region_var = get_param(tramp_fndata->func_decl, idx);
    }
}


/* This assumes that the fndecl is a function that takes a function pointer
 * argument. This function changes the data type of that function pointer to
 * be of trampoline type.
 */
#if 0
static void replace_fnptr_in_proto(tree fndecl)
{
    VEC(tree,gc) *args;
    function_args_iterator fi;
    tree arg, param, tramptype, def;

    for (param=DECL_ARGUMENTS(fndecl); param; param=DECL_CHAIN(param))
    {
        /* If fnptr parameter, swap it with a trampoline param */
        if (is_fnptr(TREE_TYPE(param)))
        {
            /* Strip off DECL and POINTER types */
            tramptype = build_pointer_type(
                create_trampoline_type(TREE_TYPE(TREE_TYPE(param))));

            /* Change the type of the parameter and its default def */
            TREE_TYPE(param) = tramptype;
            def = gimple_default_def(cfun, param);
            TREE_TYPE(SSA_NAME_VAR(def)) = tramptype;
            TREE_TYPE(def) = tramptype;

            /* Also change the ARG_TYPE */
            args = VEC_alloc(tree, gc, 1);
            FOREACH_FUNCTION_ARGS(TREE_TYPE(cfun->decl), arg, fi)
            {
                if (TREE_TYPE(param) == arg)
                  VEC_safe_push(tree, gc, args, tramptype);
                else
                  VEC_safe_push(tree, gc, args, arg);
            }
            TYPE_ARG_TYPES(TREE_TYPE(cfun->decl)) = build_tree_list_vec(args);
        }
    }

    cleanup_tree_cfg();
    update_ssa(TODO_update_ssa);
}
#endif /* if 0 */


/* Transform the prototype of this function (which has fnptr formal params) */
#if 0
static void make_sexxy_proto_with_fnptr(rbmm_fn_data_t fn)
{
    int i, j;
    unsigned ai;
    tree param;
    region_t a, b, parent, child;
    bool do_merge;
    bitmap_iterator itr;
    bitmap a_idxes, b_idxes;
    ssa_name_info_t name;

    replace_fnptr_in_proto(fn->func_decl);

    /* Toss in regions arguments (worse case: 1 region for every ptr) */
    i = 0;
    for (param=DECL_ARGUMENTS(fn->func_decl); param; param=DECL_CHAIN(param))
    {
        if (!is_fnptr(TREE_TYPE(param)) &&
            REGION_CANDIDATE_TYPE_P(TREE_TYPE(param)))
        {
            name = create_ssa_name_info(param);
            name->aliased_state |= ALIASED_BY_CALLPARAM;
            add_ssa_name_info(fn, name);
        }
        ++i;
    }

    unify_finalize(fn);
    names_to_regions(fn, fn->names);
    
    /* Now we have all the formal-to-regionvar parameters in place.  Check for
     * region's which have different ids (different regions) that share the same
     * param_idx.
     */
    FOR_EACH_VEC_ELT(region_t, fn->regions, i, a)
      FOR_EACH_VEC_ELT(region_t, fn->regions, j, b)
      {
          if (a == b)
            continue;

          /* If region A and B share region for the same param index */
          do_merge = false;
          if (a->param_idx == b->param_idx)
            do_merge = true;
          else
          {
              a_idxes = get_param_indices(fn, a);
              EXECUTE_IF_SET_IN_BITMAP(a_idxes, 0, ai, itr)
              {
                  b_idxes = get_param_indices(fn, b);
                  if (bitmap_bit_p(b_idxes, ai))
                  { 
                      do_merge = true;
                      break;
                  }
              }
          }

          if (do_merge)
          {
              parent = (a->id < b->id) ? a : b;
              child  = (a->id > b->id) ? a : b;
              merge_regions(fn, parent, child);
          }
      }
}
#endif


/* Transform the prototype of this function to take (as needed) regions as input */
static void make_sexxy(rbmm_fn_data_t fn)
{
    unsigned ii, jj;
    tree param;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *n_to_reg;
    
    if (!fn || fn->do_not_region_enable)
      return;

    /* Leave main's prototype as it is */
    if (get_identifier(get_name(fn->func_decl)) == get_identifier("main.main"))
      return;

    /* Detect any function pointer params */
    for (param=DECL_ARGUMENTS(fn->func_decl); param; param=DECL_CHAIN(param))
      if (is_function_p(TREE_TYPE(param)) || is_fnptr(TREE_TYPE(param)))
      {
          //make_sexxy_proto_with_fnptr(fn);
          break;
      }

    /* First, add parameters for regions needed for returned values */
    FOR_EACH_VEC_ELT(ssa_name_to_reg_t, fn->returned_parammap->regions,
                     ii, n_to_reg)
      if (needs_formal_param_p(n_to_reg->region))
        add_region_param(fn, fn->func_decl, n_to_reg->region);

    /* Second, add parameters for formal parameters that need regions */
    FOR_EACH_VEC_ELT(param_to_reg_t, fn->parammap, ii, pr)
      FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, jj, n_to_reg)
        if (needs_formal_param_p(n_to_reg->region))
          add_region_param(fn, fn->func_decl, n_to_reg->region);
    
    RBMM_DEBUG_BLOCK(DEBUG_PARAMS, print_param_to_region_mapping(fn););
}


/* Given a statement in basic block bb, iterate across all of the ops of that
 * statement and if we are analyizing any of them, add 'bb' to that analysis'
 * set of basic blocks.
 */
static void update_ops_last_use(rbmm_fn_data_t fn, gimple stmt)
{
    unsigned i;
    tree op, member, base;
    ssa_name_info_t info;

    /* Update last use */
    for (i=0; i<gimple_num_ops(stmt); ++i)
    {
        if (!(op = gimple_op(stmt, i)))
          continue;

        base = get_base(op);
        member = get_member(op);

        if ((info = find_ssa_name_info(fn, base, NULL)))
          add_live_in_bb(fn, info, gimple_bb(stmt));
        if (member && (info = find_member_from_id_type(fn, info, member)))
          add_live_in_bb(fn, info, gimple_bb(stmt));
    }
}


static inline void set_before_backedge_loop(basic_block bb, bitmap indexes)
{
    bitmap_set_bit(indexes, bb->loop_father->latch->index);
}


static void unset_dominators(bitmap bbs)
{
    unsigned b, c;
    bitmap_iterator bi, ci;

    EXECUTE_IF_SET_IN_BITMAP(bbs, 0, b, bi)
      EXECUTE_IF_SET_IN_BITMAP(bbs, 0, c, ci)
        if (b!=c && dominated_by_p(CDI_DOMINATORS, get_bb(c), get_bb(b)))
          bitmap_clear_bit(bbs, b);
}


/* Remove the former bb if the latter can be reached,
 * even if its not dominated but can only be reached by later blocks which we
 * plan on removing.  The idea here is to remove any potential double frees of
 * regions
 */
static void unset_non_dominators(bitmap bbs)
{
    unsigned b, c;
    bitmap_iterator bi, ci;

    EXECUTE_IF_SET_IN_BITMAP(bbs, 0, b, bi)
      EXECUTE_IF_SET_IN_BITMAP(bbs, 0, c, ci)
        if (b!=c && can_reach(get_bb(b), get_bb(c)))
          bitmap_clear_bit(bbs, b);
}


/* If we have a path from creation to exit and there is no delete in there,
 * toss one in.
 */
static void set_paths_to_exit(
    basic_block def, /* Parent, that which should dominate the child bbs */
    basic_block fin,
    bitmap      last_live) /* Last live points for a region */
{
    edge e;
    unsigned idx;
    basic_block pred;
    bitmap exits;
    edge_iterator ei;
    bitmap_iterator bi;

    if (bitmap_count_bits(last_live) == 0)
      return;

    /* Search all paths leading into fin */
    exits = BITMAP_GGC_ALLOC();
    FOR_EACH_EDGE(e, ei, fin->preds)
    {
        pred = e->src;

        /* If any bb in last_live dominates this bb-to-fin (pred) continue
         * else set bb
         */
        EXECUTE_IF_SET_IN_BITMAP(last_live, 0, idx, bi)
        {
            if (dominated_by_p(CDI_DOMINATORS, pred, get_bb(idx)))
              continue;

            if (pred->loop_depth != 0)
              continue;

            /* Skip all fallthrough nodes */
            while (pred && pred->succs &&
                   (pred != get_return_bb()) && FALLTHRU_EDGE(pred) &&
                   (pred->loop_depth == 0))
              pred = (EDGE_SUCC(pred, 0)) ? EDGE_SUCC(pred, 0)->dest : NULL;

            if (pred && dominated_by_p(CDI_DOMINATORS, pred, def))
              bitmap_set_bit(exits, pred->index);
        }
    }

    bitmap_ior_into(last_live, exits);
}


/* Give a node where a region is created, and all of the points it is live in,
 * determine if we should place the create and removal inside the loop or
 * outside the loop.
 */
static basic_block adjust_create_for_loop(
    region_t    reg,
    basic_block defbb,
    bitmap      last_live)
{
    bool in_same_loop;
    unsigned i, idx;
    edge e;
    basic_block bb;
    bitmap_iterator bi;
    VEC(edge,heap) *exits;
    struct loop *loop;

    /* If create and remove are in the same loop.  Leave things as is. */
    in_same_loop = true;
    EXECUTE_IF_SET_IN_BITMAP(last_live, 0, idx, bi)
      if (defbb->loop_father != get_bb(idx)->loop_father)
      {
          in_same_loop = false;
          break;
      }

    if (in_same_loop) /* Allocation does not escape function */
      return defbb;

    /* If the create is in a loop and the last use is in another.  Move the
     * create to the loop of the last use.
     */
    EXECUTE_IF_SET_IN_BITMAP(last_live, 0, idx, bi)
    {
        bb = get_bb(idx);
        loop = find_common_loop(defbb->loop_father, bb->loop_father);

        /* Move the region create (before the loop) */
        if (loop->header->index == 0)
          move_create(reg, get_bb(2));
        else
        {
            e = BRANCH_EDGE(loop->header);
            move_create(reg, get_bb(e->src->index));
        }
        defbb = gimple_bb(reg->created_stmt);

        /* Move the region remove (after the loop) */
        bitmap_clear_bit(last_live, idx);
        if (loop->latch->index == 1)
          bitmap_set_bit(last_live, get_return_bb()->index);
        else
        {
            exits = get_loop_exit_edges(loop);
            FOR_EACH_VEC_ELT(edge, exits, i, e)
              bitmap_set_bit(last_live, e->dest->index);
            VEC_free(edge, heap, exits);
        }
    }

    return defbb;
}


static void set_outside_loop(
    const basic_block bb,
    bitmap            bbs,
    const basic_block defbb)
{
    unsigned i;
    edge e;
    VEC(edge,heap) *exits;

    /* Unset bb in the bitmap */
    bitmap_clear_bit(bbs, bb->index);

    /* Find all exits outside of the loop (well just place them at the same
     * level as the loop where the region was created)
     * If the loop_father and its exits are the same, then its the end of the
     * function.
     */
    if (defbb->loop_father->exits == defbb->loop_father->exits->next)
      bitmap_set_bit(bbs, get_return_bb()->index);
    else
    {
        exits = get_loop_exit_edges(defbb->loop_father);
        FOR_EACH_VEC_ELT(edge, exits, i, e)
          bitmap_set_bit(bbs, e->dest->index);
        VEC_free(edge, heap, exits);
    }
}


static void kill_regions(rbmm_fn_data_t fn)
{
    unsigned i, j, live_idx;
    region_t reg;
    basic_block bb, defbb;
    bitmap last_live;
    bitmap_iterator bi;

    if (!current_loops)
      loop_optimizer_init(LOOPS_NORMAL | LOOPS_HAVE_RECORDED_EXITS);

    last_live = BITMAP_GGC_ALLOC();
    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
    {
        if (GLOBAL_REGION_P(reg) /*| ALIASED_BY_RETURN_P(reg)*/ | !reg->region_var)
          continue;

        /* If the region is infact never incremented or decremented and is
         * passed as input to another function.  Do not put a kill region here,
         * since the callees will do the job of killing the region.  And the
         * function is not "main.main" (we want our stats to look good).
         * TODO: Optimize furthur, even if we have inc/dec the prot counter we
         * still might not need to try to delete this region.
         */
        if (!ALIASED_BY_PARAM_P(reg)            &&
            reg->is_passed_as_argument          &&
            (reg->n_protection_increments == 0) && 
            get_identifier("main.main") != 
            get_identifier(get_name(fn->func_decl)))
          continue;

        /* Get the bb where the region was created in */
        if (!(defbb = gimple_bb(SSA_NAME_DEF_STMT(reg->region_var))))
          defbb = get_bb(2);

        /* Get all uses of the region */
        FOR_EACH_VEC_ELT(basic_block, reg->live_bbs, j, bb)
          bitmap_set_bit(last_live, bb->index);

        /* Unset dominator nodes in uses, keeping the last used point */
        unset_dominators(last_live);
        unset_non_dominators(last_live);
        set_paths_to_exit(defbb, get_return_bb(), last_live);
        unset_dominators(last_live);

        /* If create is in a loop handle that */
        if (defbb->loop_depth)
          defbb = adjust_create_for_loop(reg, defbb, last_live);

        /* If removal is in a loop, handle them */
        EXECUTE_IF_SET_IN_BITMAP(last_live, 0, live_idx, bi)
        {
            bb = get_bb(live_idx);
            if (defbb->loop_father != bb->loop_father)
              set_outside_loop(bb, last_live, defbb);
        }
        
        /* Check that all removal points can be reached by create */
        EXECUTE_IF_SET_IN_BITMAP(last_live, 0, live_idx, bi)
          if (!dominated_by_p(CDI_DOMINATORS, get_bb(live_idx), defbb))
            bitmap_clear_bit(last_live, live_idx);

        /* Oh shit check */
        if (bitmap_count_bits(last_live) == 0)
        {
            // WTF!?!?! XXX FIXME bitmap_set_bit(last_live, defbb->index);
            bitmap_set_bit(last_live, get_return_bb()->index);
        }
        
        /* Check that all removal points are not error handling crap */
        EXECUTE_IF_SET_IN_BITMAP(last_live, 0, live_idx, bi)
          if (gimple_code(gsi_stmt(gsi_last_bb(get_bb(live_idx))))==GIMPLE_RESX)
          {
              bitmap_clear_bit(last_live, live_idx);
              bitmap_set_bit(last_live, get_return_bb()->index);
          }

        /* Remove */    
        EXECUTE_IF_SET_IN_BITMAP(last_live, 0, live_idx, bi)
          remove_region_bn(reg, get_bb(live_idx));

        /* Prepare to be regurgitated */
        bitmap_zero(last_live);
    }

    loop_optimizer_finalize();
}


/* If the global region is used in this function, create a temp variable to it
 * and assign the temp variable the global value.  We must assign global to
 * temp, since it is only of type tree-node DECL, and the gcc checkers barf if
 * you pass decls, instead of ssaname instances to function calls, as args.
 */
static void insert_global_temp(rbmm_fn_data_t fn)
{
    unsigned ii;
    bool already_assigned;
    region_t reg;
    basic_block bb;
    tree global_temp, have, search;
    gimple stmt;
    gimple_stmt_iterator gsi;

    if (fn->global_temp_var)
      return;
    
    /* Only create the global temp once but set multiple regions to use it */
    already_assigned = false;

    /* For each region, if it is global set the region_var to the tmp */
    FOR_EACH_VEC_ELT(region_t, fn->regions, ii, reg)
    {
        if (!ALIASED_BY_GLOBAL_P(reg))
          continue;

        bb = BASIC_BLOCK_FOR_FUNCTION(cfun, 2);
        gsi = gsi_start_bb(bb);
        if (gsi.ptr == NULL)
          gsi = gsi_start_bb(EDGE_SUCC(bb, 0)->dest);
        stmt = gsi_stmt(gsi);
        
        /* If this is main.init, make sure we insert the TEMP = GLOBAL after
         * GLOBAL has actually be defined.  So get the stmt after that.
         */
        if ((gimple_code(stmt) == GIMPLE_CALL) &&
            !SSA_VAR_P(gimple_call_fn(stmt)))
        {
            search = get_identifier("__rbmm_create_global_region");
            have = get_identifier(get_name(gimple_call_fn(stmt)));
            if (have == search)
            {
                gsi_next(&gsi);
                stmt = gsi_stmt(gsi);
            }
        }

        if (!already_assigned)
        {
            global_temp = assign_global_to_temp(stmt);
            fn->global_temp_var = true;
            already_assigned = true;
        }
        reg->region_var = global_temp;
    }
}


/* Construct analysis object (one per function) */
static rbmm_fn_data_t new_fn_data(tree func_decl)
{
    rbmm_fn_data_t fn_data = XCNEW(struct _rbmm_fn_data_d);
    fn_data->func_decl = func_decl;
    fn_data->func_name = get_identifier(get_name(func_decl));
    fn_data->n_orig_params = count_fnparams(func_decl);
    fn_data->returned_parammap = XCNEW(param_to_reg_t);
    return fn_data;
}


/* Match the decl.  If it is in the list of given ID's then return TRUE. */
static bool is_fn_p(const char *fnname, const_tree fndecl)
{
    return get_identifier(get_name((tree)fndecl)) == get_identifier(fnname);
}


/* First pass
 * Analyize the program, per function, but make no code transformations
 */
static void rbmm_analysis_pass(rbmm_fn_data_t fn)
{
    gimple stmt;
    basic_block bb;
    gimple_stmt_iterator ss;
    
    RBMM_DEBUG(DEBUG_ANAL, "++ Pass 1: Analyizing function: %s ++",
               get_name(cfun->decl));
    
    calculate_dominance_info(CDI_DOMINATORS);
    calculate_dominance_info(CDI_POST_DOMINATORS);

    FOR_EACH_BB(bb)
    {
        /* Iterate across each statement in the basic block */
        for (ss=gsi_start_bb(bb); !gsi_end_p(ss); gsi_next(&ss))
        {
            stmt = gsi_stmt(ss);

            /* Statements that we care about */
            if ((gimple_code(stmt) == GIMPLE_ASSIGN) &&
                 is_fnptr(gimple_get_lhs(stmt)))
            {
                ;
            }
            else if (gimple_code(stmt) == GIMPLE_ASSIGN)
              process_assignment(fn, stmt);
            else if (gimple_code(stmt) == GIMPLE_CALL &&
                     is_allocation_call(stmt))
              process_allocation(fn, stmt);
            else if (gimple_code(stmt) == GIMPLE_RETURN)
              process_retval(fn, stmt);
        }
    }
    RBMM_DEBUG(DEBUG_ANAL, "+++++++++++++++++++++++++++++++++++++++++++++++++");
    
    /* Up to this point we have unified ssanames in each block.  Now we need to
     * associate them to inputs and outputs of the function being analyized.
     */
    unify_finalize(fn);

    /* Convert our ssaname unification set to regions */
    names_to_regions(fn, fn->names);
}


/* 1.5 pass
 * The pass between 1 and 2.  Pass 1 has analyized each function
 * intraprocedurally.  We now look SOLELY at func calls and see if we need to
 * add anymore data to our analysis.  No transformations occur here.  This is
 * just an extension of pass 1's analysis.
 * Returns boolean TRUE if we have modifedour analysis
 */
static void build_param_maps(); /* Forward decl */
static bool rbmm_analysis_fncall_pass(rbmm_fn_data_t fn)
{
    gimple stmt;
    gimple_stmt_iterator gsi;
    basic_block bb;
    bool has_been_modded;

    /* For fixed point analysis */
    has_been_modded = false;

    /* Some analysis need to know about how to handle arguments to functions.
     * This is all in a fixed point, so we need to continusously update the
     * information.
     */
    build_param_maps();

    FOR_EACH_BB(bb)
      for (gsi=gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi))
      {
          stmt = gsi_stmt(gsi);

          if (gimple_code(stmt)==GIMPLE_CALL && !is_allocation_call(stmt))
          {
             if (is_fnptr(gimple_call_fn(stmt)) && process_fnptr_call(fn, stmt))
               has_been_modded = true;
             else if (process_fncall(fn, stmt))
               has_been_modded = true;
          }
          else if ((gimple_code(stmt) == GIMPLE_ASSIGN) &&
                   (is_fnptr(gimple_get_lhs(stmt)) ||
                    is_function_p(gimple_assign_rhs1(stmt))))
          {
              if (process_fnptr_assign(fn, stmt))
                has_been_modded = true;
          }
          else if ((gimple_code(stmt) == GIMPLE_ASSIGN) &&
                   is_interface_method_table(gimple_assign_rhs1(stmt)))
          {
              /* TODO */
          }
            
          /* Liveness analysis: need to know what bb the region is live in */
          update_ops_last_use(fn, stmt);
      }

    unify_finalize(fn);
    names_to_regions(fn, fn->names);
    build_param_maps();

    return has_been_modded;
}


/* Look at the function and if it's main.init insert the global region
 * declaration here.  This should occur during the second pass (code
 * transformaiton pass).
 */
static void rbmm_insert_initilization_code(void)
{
    gimple_stmt_iterator gsi;

    if (get_identifier(get_name(cfun->decl)) != get_identifier("main.init"))
      return;

    /* cfun is main.init */
    gsi = gsi_start_bb(BASIC_BLOCK_FOR_FUNCTION(cfun, 2));
    create_global_region_bn(gsi_stmt(gsi));
}


static void rbmm_transformation_fini(void)
{
#ifdef STATS
    gimple_stmt_iterator gsi;
    basic_block return_bb;

    if (get_identifier(get_name(cfun->decl)) != get_identifier("main.main"))
      return;

    /* cfun is main */
    return_bb = get_return_bb();
    gsi = gsi_last_bb(return_bb);
    print_stats_bn(gsi_stmt(gsi));
#endif /* STATS */
}


/* Insert region merge calls */
static void insert_merges(rbmm_fn_data_t fn)
{
#ifdef ENABLE_RUNTIME_MERGING
    unsigned i;
    merge_t *md;
    gimple stmt;

    FOR_EACH_VEC_ELT(merge_t, fn->merge_data, i, md)
    {
        if (md->use_stmt)
          stmt = md->stmt;
        else
          stmt = gsi_stmt(md->gsi);
        merge_region_bn(md->parent, md->child, stmt);
    }
#endif
}


/* Transform the header for each function that needs region arguments */
static void make_sexxys(void)
{
    tree old_fn_decl;
    struct function *func;
    struct cgraph_node *node;

    for (node=cgraph_nodes; node; node=node->next)
    {
        if (!(func = DECL_STRUCT_FUNCTION(node->decl)))
          continue;
        push_cfun(func);
        old_fn_decl = current_function_decl;
        current_function_decl = node->decl;
        make_sexxy(find_fn_data(cfun->decl));
        current_function_decl = old_fn_decl;
        pop_cfun();
    }
}


/* Return true if the ssaname's region is already listed in the map */
static bool has_param_reg_map_entry_p(
    const param_to_reg_t  *pr,
    const ssa_name_info_t  entry)
{
    unsigned i;
    ssa_name_to_reg_t *n_to_reg;

    FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, i, n_to_reg)
      if (entry->region_uid == n_to_reg->region->id)
        return true;

    return false;
}


/* Return an index into the parent data type which has this guy as a member */
static int get_member_index(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t member)
{
    unsigned i;
    const_tree type;
    const_tree parent = (member->base) ? member->base->ssaname : NULL;

    if (!parent)
      return -1;

    type = TREE_TYPE(parent);
    while (type && (TREE_CODE(type) != RECORD_TYPE))
      type = TREE_TYPE(type);

    if (!type)
      return -1;

    /* Iterate through the type looking for a match to member's name */
    for (i=0, type=TYPE_FIELDS(type); type; type=TREE_CHAIN(type), ++i)
      if (get_identifier(get_name((tree)type)) == member->identifier)
        return i;

    return -1;
}


/* This call is recursive.
 * Add all of the regions for the child nodes of 'info' to the parammap entry.
 *
 * depth: How many levels deep into param's members we are looking.  A depth
 *        of '0' means we are looking at the parent/param passed in.  '1'
 *        means we are looking at the members of 'param', and '2+' means we
 *        are looking at the members of the memebers... you get the idea.
 */
static void _add_param_reg_map_entry(
    rbmm_fn_data_t   fn,
    const_tree       param,
    int              param_idx,
    param_to_reg_t  *parammap,
    ssa_name_info_t  info)
{
    bool need_for_parent, reg_has_already_been_added;
    unsigned i;
    region_t reg;
    ssa_name_info_t member;
    ssa_name_to_reg_t *name_to_reg;
    VEC(ssa_name_info_t,gc) *assoc_members;

    if (info->marked)
      return;
    
    /* Region for the name we /might/ need for out paramter-to-region map */
    reg = find_region_by_id(fn, info->region_uid);

    /* FIXME:
     * We should probably use the need_formal_param_p() instead but that does
     * not take into account ALLOC flags for members of the region.
     */
    if (ALIASED_BY_GLOBAL_P(reg))
      return;

    /* Do not add the region if we alrady have it added */
    reg_has_already_been_added = false;
    if (region_exists_in_maps_p(reg, fn->parammap) ||
        region_exists_in_map_p(reg, fn->returned_parammap))
      reg_has_already_been_added = true;

    /* Do we need to pass a region because of our parent ? */
    need_for_parent = false;
    if (info->base && ALIASED_BY_ALLOC_P(reg) &&
        (ALIASED_BY_PARAM_P(info->base) | ALIASED_BY_RETURN_P(info->base)))
      need_for_parent = true;

    if (needs_formal_param_p(reg) || need_for_parent)
    {
        /* A region can be for multiple parameters, if we have never set this
         * region in the function prototype yet, set it now.
         */
        if (reg->region_var_idx == -1)
        {
            reg->param_idx = param_idx;
            reg->region_var_idx = fn->n_region_var_params + fn->n_orig_params;
            ++fn->n_region_var_params;
        }

        /* Now toss this into the name-to-region map */
        name_to_reg =
            VEC_safe_push(ssa_name_to_reg_t, gc, parammap->regions, NULL);
        memset(name_to_reg, 0, sizeof(ssa_name_to_reg_t));
        name_to_reg->info = info;
        name_to_reg->member_index = get_member_index(fn, info);
        name_to_reg->region = reg;
        name_to_reg->use_as_formal_param = reg_has_already_been_added == false;
    }

    /* Uniquely add all of the members of info */
    info->marked = 1;
    assoc_members = get_associated_members(fn, info);

    /* We have to map identifiers (proper names) so when building the map put
     * the actual instance and not that of a temporary variable.  We just brows
     * through this and remove any temp. variables.
     */
    FOR_EACH_VEC_ELT(ssa_name_info_t, assoc_members, i, member)
      if ((get_name((tree)member->ssaname) == NULL) || /* <-- Lame check   */
          ((TREE_CODE(member->ssaname) == SSA_NAME) && /* <-- Better check */
           is_artificial_p(member->ssaname)))
        VEC_ordered_remove(ssa_name_info_t, assoc_members, i);

    /* Add any members that need a region for input to the function */
    FOR_EACH_VEC_ELT(ssa_name_info_t, assoc_members, i, member)
      if (!has_param_reg_map_entry_p(parammap, member))
        _add_param_reg_map_entry(
          fn, param, param_idx, parammap, member);

    VEC_free(ssa_name_info_t, gc, assoc_members);
}


static void add_param_reg_map_entry(rbmm_fn_data_t fn, const_tree param)
{
    int param_idx;
    param_to_reg_t *entry;
    ssa_name_info_t info;

    entry = VEC_safe_push(param_to_reg_t, gc, fn->parammap, NULL);
    memset(entry, 0, sizeof(param_to_reg_t));
    entry->param_decl = param;
    entry->regions = VEC_alloc(ssa_name_to_reg_t, gc, 0);

    /* Get the ssa for this formal param */
    info = find_ssa_name_info(fn, param, NULL);
    entry->param_info = info;
    param_idx = VEC_length(param_to_reg_t, fn->parammap) - 1;

    /* Update the param and region_var indicies (for convience) */
    clear_marks(fn);
    if (info)
      _add_param_reg_map_entry(fn, param, param_idx, entry, info);
    clear_marks(fn);
}


/* Return all regions this name is found in */
static void find_regions_for_ssa_name(
    const rbmm_fn_data_t    fn,
    const ssa_name_info_t   info,
    VEC(region_t,gc)      **regs)
{
    unsigned i, j;
    region_t reg;
    ssa_name_info_t child;

    if (!info)
      return;

    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
    {
        if (!find_ssa_name_in_region(reg, info->ssaname))
          continue;

        unique_add_to_region_vec(regs, reg);

        /* Add members of info: Strip off ssaname and pointer type nodes */
        FOR_EACH_VEC_ELT(ssa_name_info_t, info->members, j, child)
          find_regions_for_ssa_name(fn, child, regs);
    }
}


static gimple get_return_stmt(const rbmm_fn_data_t fn)
{
    basic_block bb;
    gimple stmt;
    gimple_stmt_iterator gsi;

    /* Find the return statement */
    stmt = NULL;
    FOR_EACH_BB_FN(bb, DECL_STRUCT_FUNCTION(fn->func_decl))
    {
        for (gsi=gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi))
        {
            stmt = gsi_stmt(gsi);
            if (gimple_code(stmt) == GIMPLE_RETURN)
              break;
        }

        if (stmt && gimple_code(stmt) == GIMPLE_RETURN)
          break;
    }

    return stmt;
}


/* Given a return variable, sort the regions that are to be returned from 'fn'
 * in the order they appear in the return structure.
 */
static VEC(region_t,gc) *sort_returned_regions(
    const rbmm_fn_data_t  fn,
    VEC(region_t,gc)     *regs,
    const_tree            ret)
{
    unsigned i;
    tree field;
    region_t reg;
    const_tree r = ret;
    VEC(region_t,gc) *sorted = VEC_alloc(region_t, gc, 0);

    /* Look at the return type */
    if (TREE_CODE(r) == RESULT_DECL || TREE_CODE(r) == VAR_DECL)
      r = TREE_TYPE(r);

    /* Sort if the return is not a record type */
    if (TREE_CODE(r) != RECORD_TYPE)
      return regs;

    /* For each field of the return value data type */
    for (field=TYPE_FIELDS(r); field; field=TREE_CHAIN(field))
      if ((reg = find_region_by_decl(fn, field)))
        unique_add_to_region_vec(&sorted, reg);

    /* Add back in all other regions (unique add does the magic) */
    FOR_EACH_VEC_ELT(region_t, regs, i, reg)
      unique_add_to_region_vec(&sorted, reg);

    return sorted;
}


/* Given a function, return a set of regions 'regs' which contain the region
 * variables that are needed for the variables this function returns.
 */
static void get_returned_regions(rbmm_fn_data_t fn, VEC(region_t,gc) **regs)
{
    unsigned i;
    tree ret;
    gimple stmt;
    region_t reg;
    ssa_name_info_t info;

    /* Get the return statement */
    stmt = get_return_stmt(fn);
    if (!(ret = gimple_return_retval(stmt)))
      return;

    if (!(info = find_ssa_name_info(fn, ret, NULL)))
      return;

    /* Get only regions we are interested in */
    VEC(region_t,gc) *regions = VEC_alloc(region_t, gc, 0);
    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
      if (!ALIASED_BY_GLOBAL_P(reg) && ALIASED_BY_ALLOC_P(reg))
        VEC_safe_push(region_t, gc, regions, reg);

    /* Sort based on order in return variable/struct */
    regions = sort_returned_regions(fn, regions, ret);
    FOR_EACH_VEC_ELT(region_t, regions, i, reg)
      VEC_safe_push(region_t, gc, *regs, reg);

    return;
}


/* Inspect the type at decl and if it is a structure, get the idx member.  We
 * only look at fields that a region would be used for.  So, 'idx' of '2' would
 * be the second region-capable field (probably the second struct or pointer) in
 * 'decl.'
 */
static tree get_region_field(const_tree decl, int idx)
{
    int count;
    tree fld;

    while (POINTER_TYPE_P(decl) || TREE_CODE(decl) == SSA_NAME)
      decl = TREE_TYPE(decl);
       
    count = 0; 
    for (fld=TYPE_FIELDS(decl); fld; fld=DECL_CHAIN(fld))
      if (TREE_TYPE(fld) && REGION_CANDIDATE_TYPE_P(TREE_TYPE(fld)))
        if (++count == idx)
          break;

    return fld;
}


/* Build a param map, which maps the return values of fn to regions */
static void build_ret_reg_map(rbmm_fn_data_t fn)
{
    unsigned i;
    region_t reg;
    tree ret, field;
    VEC(region_t,gc) *returned_regs;
    ssa_name_info_t ret_info;
    ssa_name_to_reg_t *n_to_reg;

    /* Create an empty parammap */
    VEC_free(ssa_name_to_reg_t, gc, fn->returned_parammap->regions);
    memset(fn->returned_parammap, 0, sizeof(param_to_reg_t));
    fn->returned_parammap->regions = VEC_alloc(ssa_name_to_reg_t, gc, 0);
    
    /* Get a list of regions returned */
    returned_regs = VEC_alloc(region_t, gc, 0);
    get_returned_regions(fn, &returned_regs);

    /* Get the variable that is returned */
    ret = gimple_return_retval(get_return_stmt(fn));
    ret_info = (ret) ? find_ssa_name_info(fn, ret, NULL) : NULL;

    /* The return region is converted by the frontend into a structure which
     * contains a member for each return variable
     */
    fn->returned_parammap->param_decl = NULL_TREE;
    fn->returned_parammap->param_info = NULL;
    FOR_EACH_VEC_ELT(region_t, returned_regs, i, reg)
    {
        if (!needs_formal_param_p(reg))
          continue;

        n_to_reg = VEC_safe_push(ssa_name_to_reg_t, gc, 
                                 fn->returned_parammap->regions, NULL);
        memset(n_to_reg, 0, sizeof(ssa_name_to_reg_t));

        /* Get the information about the type */
        if (i == 0)
          n_to_reg->info = find_ssa_name_info(fn, ret, NULL);
        else /* Find the i'th member of "ret" that is in "reg" */
        {
            field = get_region_field(ret, i);
            n_to_reg->info = find_member_from_id_type(fn, ret_info, field);

            if (!n_to_reg->info)
              n_to_reg->info = find_ssa_name_in_region(reg, field);

            if (!n_to_reg->info)
              n_to_reg->info = ret_info;
        }

        n_to_reg->member_index = get_member_index(fn, n_to_reg->info);
        n_to_reg->region = reg;
        n_to_reg->use_as_formal_param = true;
    }

    VEC_free(region_t, gc, returned_regs);
}


/* Build a map (vector) of param located at param_idx into the vector to what
 * regions it needs.  The region-to-return value map is build separately see:
 * build_ret_reg_map()
 */
static void build_param_reg_map(rbmm_fn_data_t fn)
{
    const_tree param;

    /* Create an empty parammap */
    if (fn->parammap)
      VEC_free(param_to_reg_t, gc, fn->parammap);
    fn->parammap = VEC_alloc(param_to_reg_t, gc, 0);

    /* For each original formal param */
    for (param=DECL_ARGUMENTS(fn->func_decl); param; param=DECL_CHAIN(param))
      add_param_reg_map_entry(fn, param);
}


static void build_param_maps(void)
{
    tree old_fn_decl;
    rbmm_fn_data_t fn;
    struct function *func;
    struct cgraph_node *node;

    for (node=cgraph_nodes; node; node=node->next)
    {
        if (!(func = DECL_STRUCT_FUNCTION(node->decl))||
            !(fn = find_fn_data(func->decl)))
          continue;

        /* Don't build maps for funcs we are not transforming */
        if (!fn->is_clone)
          continue;

        push_cfun(func);
        old_fn_decl = current_function_decl;
        current_function_decl = node->decl;
        fn->n_region_var_params = 0;
        build_param_reg_map(fn);  /* Build param map  */
        build_ret_reg_map(fn);    /* Build return map */
        current_function_decl = old_fn_decl;
        pop_cfun();
    }
}


/* Second pass
 * Check to see if we make any calls that MIGHT need regions.  This
 * pass is responsible for creating regions as needed.  This also
 * modifies/transforms the code.  This operates per function.
 */
static void rbmm_transformation_pass(rbmm_fn_data_t fn_data)
{
    gimple stmt;
    basic_block bb;
    gimple_stmt_iterator ss;

    RBMM_DEBUG(DEBUG_ANAL, "++ Pass 2: Transforming function: %s ++",
               get_name(cfun->decl));

    /* Insert global temp variable if this function makes use of it */
    insert_global_temp(fn_data);

    /* Process statements */
    FOR_EACH_BB(bb)
    {
        for (ss=gsi_start_bb(bb); !gsi_end_p(ss); gsi_next(&ss))
        {
            stmt = gsi_stmt(ss);

            if (gimple_code(stmt) == GIMPLE_ASSIGN)
              process_regions_for_assignment(fn_data, stmt);
            else if (!(gimple_code(stmt) == GIMPLE_CALL))
              continue;
            else if (is_allocation_call(stmt))
              process_regions_for_allocation(fn_data, stmt);
            else if((TREE_CODE(gimple_op(stmt, 1)) == SSA_NAME) &&
                    is_fnptr(gimple_op(stmt, 1)))
              process_regions_for_fnptr_call(fn_data, stmt);
            else 
              process_regions_for_fncall(fn_data, stmt);
        }
    }
    
    /* Insert region merges */
    insert_merges(fn_data);

    /* Figure out where we should destroy regions, or if we even need to */
    kill_regions(fn_data);

    /* Check to see if we allocate any memory that escaped */
    RBMM_DEBUG(DEBUG_ANAL, "+++++++++++++++++++++++++++++++++++++++++++++++++");
}


/* Check environmental variable (':' delimited) for function names.  If a name
 * appears, in the list and it matches fndecl, ignore/do-not-analyize fndecl.
 */
static bool ignore_func_env_p(const_tree fndecl)
{
    char *search;
    const_tree id;

    if (!(search = getenv("RBMM_IGNORE_FNS")))
      return false;

    id = get_identifier(get_name((tree)fndecl));
    for (search=strtok(search, ":"); search; search=strtok(NULL, ":"))
      if (get_identifier(search) == id)
        return true;

    return false;
}


/* Uniquly add a name to the higher-order specialize vector */
static void add_function_to_specialize(const char *name)
{
    unsigned i;
    tree node;
    
    FOR_EACH_VEC_ELT(tree, hospecialize, i, node)
      if (get_identifier(name) == node)
        return;

    VEC_safe_push(tree, gc, hospecialize, get_identifier(name));
}


/* Copy the function and then go thorough and pass the global region for all
 * funcalls that need regions.
 */
#if 0
static tree specialize(tree fndecl)
{
    tree dolly, old_fn_decl;
    basic_block bb;
    gimple stmt;
    gimple_stmt_iterator gsi;
    rbmm_fn_data_t fn_data;
    struct function *func;
    struct cgraph_node *cgnode;

    dolly = clone_function(fndecl, "__original");
    cgnode = cgraph_get_node(dolly);
    DECL_PRESERVE_P(dolly) = 1;
    TREE_USED(dolly) = 1;
    cgraph_mark_needed_node(cgnode);

    /* Make the function externally accessible (undo what the function
     * versioning does, as it makes functions local.  We do not want
     * that in this case.
     */
    TREE_PUBLIC(dolly) = 1;
    cgnode = cgraph_get_node(dolly);
    cgnode->local.externally_visible = 1;
    cgnode->local.local = 0;

    /* Transform the function we are specializing... modify the clone */
    func = DECL_STRUCT_FUNCTION(dolly);
    old_fn_decl = current_function_decl;
    push_cfun(func);
    current_function_decl = dolly;
    
    /* Create some metadata about the specialized function */
    fn_data = new_fn_data(func->decl);
    fn_data->is_specialized_version = true;

    /* In the function we are cloning, look at each statment.  When we find a
     * function call, handle that but by passing the global region for all
     * region arguments.
     */
    FOR_EACH_BB_FN(bb, func)
      for (gsi=gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi))
        if (gimple_code(stmt = gsi_stmt(gsi)) == GIMPLE_CALL)
          process_regions_for_fncall(fn_data, stmt);
    
    /* Clean up */
    free(fn_data);
    current_function_decl = old_fn_decl;
    pop_cfun();

    return dolly;
}
#endif

/*
static void remove_unused_fns(void)
{
    unsigned i;
    rbmm_fn_data_t fn;
    cgraph_node *node;

    
    FOR_EACH_VEC_ELT(rbmm_fn_data_t, rbmm_analyized_fns, i, fn)
      if (fn && fn->is_clone          &&
          !TREE_PUBLIC(fn->func_decl) &&
          !cgraph_get_node(fn->func_decl)->callers)
        cgraph_remove_node(cgraph_get_node(fn->func_decl));

    FOR_EACH_VEC_ELT(rbmm_fn_data_t, rbmm_analyized_fns, i, fn)
      if (fn && !fn->is_clone && fn->clone_decl          &&
          !TREE_PUBLIC(fn->func_decl)                    &&
          !cgraph_get_node(fn->func_decl)->address_taken &&
          !cgraph_get_node(fn->func_decl)->callers)
        cgraph_remove_node(cgraph_get_node(fn->func_decl));

    FOR_EACH_VEC_ELT(rbmm_fn_data_t, rbmm_analyized_fns, i, fn)
      if (fn && fn->is_clone && (node = cgraph_get_node(fn->func_decl)))
        node->needed = 1;
}
*/

/* Look at all functions in the program, and create an initial (empty)
 * representation for our analysis to store information.
 */
static void init_fn_data(void)
{
    tree dolly;
    rbmm_fn_data_t fn, clone_fn;
    struct function *func;
    struct cgraph_node *node;

    for (node=cgraph_nodes; node; node=node->next)
    {
        /* If we are explictly ignoring this function */
        if (ignore_func_env_p(node->decl))
          continue;
        else if (!(func = DECL_STRUCT_FUNCTION(node->decl)))
          continue;
        else if (find_fn_data(func->decl))
          continue;
        
        /* Store info we analyize for each function */
        fn = new_fn_data(func->decl);
        VEC_safe_push(rbmm_fn_data_t, heap, rbmm_analyized_fns, fn);

        /* Since we see this function, we will try to transform it later.
         * Clone the function.  The clone will be the one we transform.
         * Do not want to clone the name main.main or main.init.  We will still
         * transform them though.
         */
        if (!is_fn_p("main.main", func->decl) &&
            !is_fn_p("main.init", func->decl))
        {
            /* Update the stack */
            dolly = clone_function(func->decl, RBMM_FN_SUFFIX);
            TREE_PUBLIC(dolly) = TREE_PUBLIC(fn->func_decl);
            cgraph_get_node(dolly)->local.externally_visible = 
                cgraph_get_node(func->decl)->local.externally_visible;
            clone_fn = new_fn_data(dolly);
            VEC_safe_push(rbmm_fn_data_t, heap, rbmm_analyized_fns, clone_fn);
            
            /* Update the analysis data to refer to the cloned data */
            fn->clone_decl = dolly;
            clone_fn->func_decl = dolly;
            clone_fn->is_clone = true;
        }
    }
}


static void debug_gc(void)
{
    unsigned i;
    rbmm_fn_data_t fn;
    FOR_EACH_VEC_ELT(rbmm_fn_data_t, rbmm_analyized_fns, i, fn)
      rbmm_gc_debug(fn);
}


static void set_unique_types(void)
{
    unsigned i;
    rbmm_fn_data_t fn;

    FOR_EACH_VEC_ELT(rbmm_fn_data_t, rbmm_analyized_fns, i, fn)
      rbmm_gc_create_type_info(fn);
}


static void rbmm_exec(void *gcc_data, void *user_data)
{
    unsigned i, j;
    struct function *func;
    tree old_fn_decl;
    rbmm_fn_data_t fn;
    region_t reg;
    bool fixed_point;
    struct cgraph_node *node;

    /* Create the global region data, but not insert it into the code yet */
    init_global_region_data(true);

    /* Generate structures to hold our analysis */
    init_fn_data();
    
    /* Pass 1: Perform intraprocedural analysis */
    for (node=cgraph_nodes; node; node=node->next)
    {
        if (!(func = DECL_STRUCT_FUNCTION(node->decl)))
          continue;

        push_cfun(func);
        old_fn_decl = current_function_decl;
        current_function_decl = node->decl;

        if ((fn = find_fn_data(cfun->decl)))
          rbmm_analysis_pass(fn);
            
        current_function_decl = old_fn_decl;
        pop_cfun();
    }

    /* Look for any functions that should not have their interface (what they
     * take as arguments and return, transformed.  Instead we just transform the
     * body of the function.  And we can do this to any function, even those
     * passed to non-region compiled functions, same for just plain-ole
     * higher-order functions.
     */
    FOR_EACH_VEC_ELT(rbmm_fn_data_t, rbmm_analyized_fns, i, fn)
    {
        if (fn->is_clone)
          continue;

        FOR_EACH_VEC_ELT(region_t, fn->regions, j, reg)
          if (ALIASED_BY_PARAM_P(reg) || ALIASED_BY_RETURN_P(reg))
          {
              reg->aliased_state =
                reg->aliased_state & ~(ALIASED_BY_PARAM | ALIASED_BY_RETURN);
              reg->aliased_state |= ALIASED_BY_GLOBAL;
          }
    }

    /* Pass 1.5: Now that we have data for all functions process them agian and
     * look ONLY for fncall statements
     */
    fixed_point = false;
    while (!fixed_point) /* Probably need just 2 iters max */
    {
        fixed_point = true;
        for (node=cgraph_nodes; node; node=node->next)
        {
            if (!(func = DECL_STRUCT_FUNCTION(node->decl)))
              continue;

            push_cfun(func);
            old_fn_decl = current_function_decl;
            current_function_decl = node->decl;
            
            if (!(fn = find_fn_data(cfun->decl)))
            {
                current_function_decl = old_fn_decl;
                pop_cfun();
                continue;
            }

            if (rbmm_analysis_fncall_pass(fn))
              fixed_point = false;

            current_function_decl = old_fn_decl;
            pop_cfun();
        }
    }
    
    /* Initalize region builtin data */
    init_rbmm_builtins();

    /* Create the type information for the regions in this fn */
    rbmm_gc_init();
    set_unique_types();

    /* Before we transform and add rbmm_create_region... we need more type
     * information from the garbage collector.
     */
    rbmm_gc_insert_type_info_table();

    /* Adjust the function prototypes (First transformation) */
    make_sexxys();

    /* Pass 2: Code generation */
    RBMM_DEBUG_NL();
    RBMM_DEBUG_NL();
    for (node=cgraph_nodes; node; node=node->next)
    {
        if (!(func = DECL_STRUCT_FUNCTION(node->decl)))
          continue;
        
        /* Get our analysis data */ 
        fn = find_fn_data(node->decl);
        if (!fn || fn->do_not_region_enable)
            continue;
    
        /* Set cfun (global current function being analyized) */ 
        push_cfun(func);
        old_fn_decl = current_function_decl;
        current_function_decl = node->decl;
        compute_reachability(cfun);

        rbmm_transformation_pass(fn);
        rbmm_transformation_fini();
        rbmm_insert_initilization_code();

        update_ssa(TODO_update_ssa);
        cleanup_tree_cfg();
        rebuild_cgraph_edges();

        free_dominance_info(CDI_DOMINATORS);
        free_dominance_info(CDI_POST_DOMINATORS);

        current_function_decl = old_fn_decl;
        pop_cfun();
    }

    /* Remove functions that have been transformed, but have no callers */
    //remove_unused_fns();
    
    RBMM_DEBUG_BLOCK(DEBUG_ANAL, print_rbmm_info();)
    RBMM_DEBUG_BLOCK(DEBUG_GIMPLE, print_trampolines(););

    /* Emit type information table */
    rbmm_gc_finalize(rbmm_analyized_fns);
    debug_gc();

    /* Crap the analysis data to teh object file */
    rbmm_emit_analysis(rbmm_analyized_fns);

    /* Dump the final GIMPLE representation */
    RBMM_DEBUG_BLOCK(DEBUG_GIMPLE,
        for (node=cgraph_nodes; node; node=node->next)
          debug_function(node->decl, 0);
    );
}


int plugin_init(struct plugin_name_args *info, struct plugin_gcc_version *version)
{
    int i;
    char *ho;

    /* Parse the args */
    for (i=0; i<info->argc; ++i)
    {
        /* hospecial: Higher Order Specialize
         * Specialize the functions listed as comma-separated-values to this.
         *     ex: -fplugin-arg-rbmm-hospecial=foo,bar,baz
         * The specialization is for each function listed, forcing the compiler
         * to make a version of the function that supports higher order "ho."
         */
        if (strncmp("hospecial", info->argv[i].key, strlen("hospecial"))==0)
        {
            ho = NULL;
            while ((ho = strtok(ho ? NULL:info->argv[i].value, ",")))
              add_function_to_specialize(ho);
        }
    }

    /* Initialize before we start analyzing */
    init_global_region();

    register_callback(TAG, PLUGIN_ALL_IPA_PASSES_END, rbmm_exec, NULL);
    return 0;
}
