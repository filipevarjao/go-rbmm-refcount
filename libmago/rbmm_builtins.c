#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <fcntl.h>
#include <math.h>
#include <assert.h>
#define __USE_GNU
#include <ucontext.h>
#include <unistd.h>
#include <sys/mman.h>
#include "rbmm_runtime.h"
#include "go-assert.h"
#include "map.h"
#include "array.h"
#include "libcrap/crap_runtime.h"


#ifdef DEBUG_RUNTIME
#define RBMM_PR(...) fprintf(stderr, "[RBMM] " __VA_ARGS__)
#else
#define RBMM_PR(...)
#endif


#ifdef DEBUG_GC
#define RBMM_PRGC(...) \
    do{fprintf(stderr, "[RBMM GC] " __VA_ARGS__); fputc('\n', stderr);}while(0)
#else
#define RBMM_PRGC(...)
#endif


#ifdef DEBUG_GC
#define SLICE_PR(...) \
    do{fprintf(stderr, "[Slice] " __VA_ARGS__); fputc('\n', stderr);}while(0)
#else
#define SLICE_PR(...)
#endif


#define MIN(_x, _y) ((_x) < (_y)) ? _x : _y


/* Enable/disable the print_xxxx routines in here for debugging via gdb 
 * Define/set these in the Makefile
 *   #define DEBUG_RUNTIME_UTILS
 *   #define DEBUG_PAGE_HELL
 */


#define DEFAULT_PAGE_SIZE      4096
#define DEFAULT_PAGE_SIZE_BITS 12 /* 4096 == 2^12 */
#define DEFAULT_PAGE_MASK      0xFFFFFFFFFFFFF000


#define GLOBAL_REGION_ID  -1
#define INVALID_REGION_ID -666

#ifdef DEBUG_PAGE_HELL /* So we can check rbmm_builtin.o */
static int using_page_sanity_checking = 1;
#endif


#ifdef RBMM_GC /* Symbol we can check for in rbmm_builtin.o */
static int using_rbmm_gc = 1;
#endif


/* A region has a linked list of these.  This is where objects in the region
 * are kept.
 */
struct __sub_header;
typedef struct __page_header
{
    /* Next page */
    struct __page *next;

    /* For use only by pages which only hold the region headers */
    void *region_hdr_brk;

    /* Data size (this might be two contigious conjoined pages for really big
     * alloc requests).
     */
    size_t data_size;

#ifdef DEBUG_PAGE_HELL
    bool in_use; /* True if the page is not on the free list */
#endif

#ifdef RBMM_GC
    bool to_space;
    unsigned long n_alloc_bytes;
#endif

#ifdef STATS
    struct __page *next_inuse;
    struct __page *prev_inuse;
#endif
} _page_hdr_t;


typedef struct __page
{
    _page_hdr_t hdr;
    unsigned char data[0];
} _page_t;


#ifdef RBMM_GC
/* Additional metadata for slices.
 * Aids garbage collection.
 * [meta][data]
 */
typedef struct __rbmm_array_meta_d
{
    uintptr_t has_been_preserved; /* For now          */
    size_t header_size;      /* metadata + redir bits */ 
    size_t n_elts;
    struct __rbmm_array_meta_d *next, *prev;
    unsigned char redir_elts_map[];
} _rbmm_array_meta_t;

#define FIRST_METADATA(_sub) \
        ((_rbmm_array_meta_t *)(_sub)->bottom_page->data)

#define FIRST_METADATA_ADDR(_addr) \
        ((_rbmm_array_meta_t *)addr_to_page(_addr)->data)

#define LAST_METADATA(_sub) ((_rbmm_array_meta_t*)(FIRST_METADATA(_sub)->prev))

#define SLICE_REDIR_MAP_SIZE(_meta) ((int)(((double)(_meta)->capacity/8.0) + 1.0))

#define SLICE_DATA_START(_meta) \
    ((uintptr_t)(_meta) + ((_meta)->header_size))
#endif /* RBMM_GC */



/* What the transformed program passes around to reason about the memory */
struct __super_header;
typedef struct __sub_header
{
    /* Next free chunk of mem that can be given out.
     * Each time a value is allocated from the region we give the requester brk,
     * and then we advance brk + requested_size bytes, and we are ready to give
     * more out
     */
    void *brk;
 
    /* Where the data for this region is kept (stack discipline, so the head of
     * the list is the page that is currently being allocated from).
     */
    _page_t *pages;
    _page_t *bottom_page; /* Bottom of the page stack (oldest page) */

    /* If we are garbage collecting, all of the new pages and data from the
     * region (the to-space are in 'pages' and 'bottom_page')
     * The older (from-space) are stored here so they can be reclaimed just
     * after the garbage collection is about to complete (after all objects have
     * been collected).
     */
#ifdef RBMM_GC
    _page_t *from_space;        /* Stores old 'pages' member during GC       */
    _page_t *from_space_bottom; /* Stores old 'bottom_page' member during GC */

    /* Basic Object (0), Slice (1), Map (2) */
    unsigned char flags : 2;
    
    /* Type information about objects in this subregion */
    const __crap_tientry_t *type_info;

    /* Back pointer to the super-region containing this subregion */
    struct __super_header *superregion;

    /* Just the size of the redirected map. Number of bits, one bit to represent
     * each object stored on the page.
     * This map being the first X bytes of the data chunk.
     */
    size_t n_redirected_bits;
#endif /* RBMM_GC */

#ifdef STATS
    unsigned long n_allocs;
#endif
} _sub_t;


/* Subregion accessors only for when garbage collection is enabled */
#ifdef RBMM_GC
#define N_REDIR_BITS(_sub)          (_sub)->n_redirected_bits
#define SET_SUPERREGION(_sub, _sup) (_sub)->superregion = (_sup)
#define SET_FLAGS(_sub, _val)       (_sub)->flags = (_val)
#define SET_TYPE_INFO(_sub, _idx) \
    (_sub)->type_info = crap_rt_get_type_info((_idx))
#else
#define N_REDIR_BITS(_sub) 0
#define SET_SUPERREGION(_sub, _sup)
#define SET_TYPE_INFO(_ignore0, _ignore1)
#define SET_FLAGS(_sub, _val)
#endif /* RBMM_GC */


/* A super region contanins multiple sub-regions, where memory comes from */
typedef struct __super_header
{
    /* If 0: we can kill this, else some other routine needs the region */
    unsigned long long ref_count;
    
    /* Next region so we can manage a list of regions */
    struct __super_header *next_region;

    /* How many bytes long this page is (more sub-regions means more bytes) 
     * This includes the size of this header + number of sub-regions.
     */
    size_t size;
    
    /* Identity (flag used for sanity and global detection) */
    int id;
    
    /* True if this region is being preserved */
#ifdef RBMM_GC
    bool has_been_copied;
    unsigned long bytes_before_gc;
    unsigned long bytes_after_gc; 
#endif

     /* Holds the id of the region once it is deleted */
#ifdef DEBUG_RUNTIME
    int old_id;
#endif

    /* Use to count number of times this region header has been reused */
#ifdef STATS
    int n_region_uses;
#endif

    int n_subregions; /* TODO: Remove and derive from this.size member */
    _sub_t *sub_regions[0];
} _super_t;


/* Set flags so that when this super region is removed it will be removed no
 * matter what.
 */
#define SET_FORCE_REMOVAL(_super) ((_super)->ref_count = 0)


/* Quick sub-region access */
#ifdef DEBUG_PAGE_HELL
#define CHECK(_super, _idx) (((_idx) >= (_super)->n_subregions) ? NULL:(_super))
#else
#define CHECK(_super, _idx) ((_super))
#endif
#define SUB(_super, _idx)      ((_super)->sub_regions[(_idx)])
#define SUB_PTR(_super, _idx) &(CHECK(_super,_idx)->sub_regions[(_idx)])


#ifdef STATS
static unsigned long stat_n_pages_created;
static unsigned long stat_n_pages_deleted;
static unsigned long stat_n_pages_recycled;
static unsigned long stat_n_regions_created;
static unsigned long stat_n_regions_deleted;
static unsigned long stat_n_regions_recycled;
static unsigned long stat_n_region_pages;
static unsigned long stat_n_subregions;
static unsigned long stat_n_subregions_used;
static unsigned long stat_n_allocs;
static unsigned long stat_n_alloc_bytes;
static unsigned long stat_n_alloc_mmap;

/* GC stats */
static unsigned long stat_n_gcs;
static unsigned long stat_n_gcs_slices;
static unsigned long stat_n_gc_copies;
static unsigned long stat_n_gc_copied_bytes;
static unsigned long stat_n_gc_redirects;
static unsigned long stat_n_gc_allocs;
static unsigned long stat_n_gc_types;
static long long     stat_n_gc_bytes; /* <--- Signed intentionally */

/* HWM stats */
static unsigned long stat_n_pages_inuse_hwm;
static unsigned long stat_n_pages_inuse_hwm_bytes;
static unsigned long stat_n_pages_free_hwm;
static unsigned long stat_n_pages_free_hwm_bytes;

#define STATINC(x) (++(x))
#define STATINC_VAL(x, a) ((x) += (a))
#else 
#define STATINC(x)
#define STATINC_VAL(x, a)
#endif /* STATS */


/* For garbage collection:
 * This should be set by any function that is called by the mutator and not our
 * runtime.  In other words, and public exposed function should set this to the
 * bp of the caller (the mutator).
 */
#ifdef RBMM_GC
static _Bool __rbmm_gc(void);
uintptr_t __bp_global; /* Stack base pointer */
#define GET_BP()   __bp_global
#define CLEAR_BP() __bp_global = 0
#define SET_BP() \
    do { \
      if (__bp_global == 0) \
        asm ("mov %%rbp, %0\n": "=r"(__bp_global)::"memory");\
    } while (0)

/* Flag for detecting if we are already performing a GC */
static bool __is_gcing;
#define IS_PERFORMING_GC()       __is_gcing
#define SET_IS_PERFORMING_GC()   __is_gcing = true
#define CLEAR_IS_PERFORMING_GC() __is_gcing = false
#else /* Else: Not using our RBMM garbage collector */
#define IS_PERFORMING_GC() false
#define CLEAR_BP()
#define SET_BP()
#endif /* RBMM_GC */


/* Linked-list of free regions (these are all on a page (maybe a few pages)) */
static _super_t *freeregions;
static _page_t *region_pages;


/* Linked-list of free pages
 * This is treated as a stack.  'freepages' points to the top of the stack.
 * This should reduce cache misses since the most recently returned page is on
 * the top of the stack.  
 */
static _page_t *freepages;


/* Linked-list of in-use pages (non-free pages) */
#ifdef STATS
static _page_t *inusepages;
static void stats_update_hwmarks(_Bool write_to_log);
#endif


/* We redefine the "__go_open_array" type, maintaining its original structure,
 * but tacking on additional fields for our garbage collector sexxyness.
 * Original structure ripped from: gcc-4.7.1 libgo/runtime/array.h
 */
typedef struct __rbmm_open_array_d
{
    void *__values;
    int __count;
    int __capacity;
} _rbmm_open_array_t;

 
/* For linker sanity */
extern void *__go_new(size_t);
extern void runtime_panicstring(const char *);


/* So maps don't remove memory we allocate for them */
void __go_free(void *p) { }


/* Constants for GC */
#ifdef RBMM_GC
static uintptr_t _total_mmap_alloc;
#define _next_gc_increment (DEFAULT_PAGE_SIZE)
static uintptr_t _next_gc = _next_gc_increment * 2;
#endif


/* Using mmap instead of sbrk */
static uintptr_t _my_mmap_base;
static void *my_sbrk(size_t size)
{
    static int fd = -1;
#define MMAP_ARGS NULL, size,                         \
                  PROT_READ | PROT_WRITE | PROT_EXEC, \
                  MAP_ANON | MAP_PRIVATE, fd, 0
    if (fd < 0)
    {
        if ((fd = open("/dev/zero", O_RDONLY)) < 1)
          abort();
    }
#ifdef RBMM_GC
    _total_mmap_alloc += size;
#endif
#ifdef STATS
    STATINC_VAL(stat_n_alloc_mmap, size);
#endif
    return mmap(MMAP_ARGS);
}


#ifdef RBMM_GC
/* In the cases where the page is > DEFAULT_PAGE_SIZE, we call this a jumbo
 * contigious page.  The first entry signifying the page header has an
 * unmodified data, e.g. the subregion pointer has a low bit of 0.  If the page
 * is a jumbo page, all DEFAULT_PAGE_SIZE blocks making up the jumbo page have
 * their subregion data bit set.
 */
#define IS_PART_OF_JUMBO_PAGE 1
#define PG_TO_SUB_MAX 4095*1024
static _sub_t *_pg_to_sub[PG_TO_SUB_MAX+1];


#define PG_TO_SUB_IDX(_addr) \
    ((_my_mmap_base - (uintptr_t)(_addr)) >> DEFAULT_PAGE_SIZE_BITS)


#define PG_SUB_IDX_TO_ADDR(_idx) \
        (((uintptr_t)_my_mmap_base - \
          ((uintptr_t)_idx<<DEFAULT_PAGE_SIZE_BITS)) & DEFAULT_PAGE_MASK);
#endif /* RBMM_GC */


#ifdef STATS
/* Update stats for high water marks */
static void stats_update_hwmarks(_Bool write_to_log)
{
    unsigned long i;
    static FILE *fp;
    const _page_t *pg;
    unsigned long n_free, n_inuse, n_free_bytes, n_inuse_bytes;

    if (write_to_log && !fp)
    {
        fp = fopen(".rbmm_stats_hwm", "w");
        fprintf(fp, "# Page HWM data\n"
                    "# FreePagesHWM, FreePagesBytesHWM, "
                    "InUsePagesHWM, InUsePagesBytesHWM\n");
    }

    n_free = n_inuse = n_free_bytes = n_inuse_bytes = 0;
    
    /* Free pages and their bytes */
    for (pg=freepages; pg; pg=pg->hdr.next)
    {
        ++n_free;
        n_free_bytes += pg->hdr.data_size + sizeof(_page_hdr_t);
    }
    if (n_free > stat_n_pages_free_hwm)
      stat_n_pages_free_hwm = n_free;
    if (n_free_bytes > stat_n_pages_free_hwm_bytes)
      stat_n_pages_free_hwm_bytes = n_free_bytes;

    /* In use pages and their bytes */
    for (pg=inusepages; pg; pg=pg->hdr.next_inuse)
    {
        ++n_inuse;
        n_inuse_bytes += pg->hdr.data_size + sizeof(_page_hdr_t);
    }
    if (n_inuse > stat_n_pages_inuse_hwm)
      stat_n_pages_inuse_hwm = n_inuse;
    if (n_inuse_bytes > stat_n_pages_inuse_hwm_bytes)
      stat_n_pages_inuse_hwm_bytes = n_inuse_bytes;

    if (write_to_log)
      fprintf(fp, "%lu, %lu, %lu, %lu\n",
              stat_n_pages_free_hwm, stat_n_pages_free_hwm_bytes,
              stat_n_pages_inuse_hwm, stat_n_pages_inuse_hwm_bytes);
}
#endif /* STATS */


/* Given a (possibly) list of pages, or a single page, add it back to the free
 * list.  Treat this as a stack.  Top is the most recently used page and bottom,
 * is also from the same list, but the oldest page.
 * Most recently deleted pages get plucked ontop of the freelist stack.
 */
static void add_to_page_subregion_map(_sub_t*, _page_t*); /* Forward */
static inline void delete_pages(_page_t *top, _page_t *bottom)
{
    _page_t *tmp;

    /* Stack discipline */
    if (!freepages)
      freepages = top;
    else
    {
        tmp = freepages;
        freepages = top;
        bottom->hdr.next = tmp;
    }

#ifdef RBMM_GC
    {
        _page_t *_pg = top;
        while (_pg)
        {
            add_to_page_subregion_map(NULL, _pg);
            _pg->hdr.to_space = false;
            if (_pg == bottom)
              break;
            _pg = _pg->hdr.next;
        }
    }
#endif

#ifdef DEBUG_PAGE_HELL /* Mark the pages as being unused...free...liberated */
    {
        _page_t *_pg = top;
        while (_pg)
        {
            __builtin_memset(_pg->data, 0xFF, _pg->hdr.data_size);
            _pg->hdr.in_use = false;
            if (_pg == bottom)
              break;
            _pg = _pg->hdr.next;
        }
    }
#endif

#ifdef STATS
    {
        _page_t *_pg = top;
        while (_pg)
        {
            _pg->hdr.n_alloc_bytes = 0;
            if (_pg == bottom)
              break;
            _pg = _pg->hdr.next;
        }
        if (!IS_PERFORMING_GC())
        {
            static unsigned long n_dels;
            const char *c = getenv("RBMM_STATS_SAMPLE");
            if (c && ((atoi(c) % (++n_dels)) == 0))
              stats_update_hwmarks(true);
        }
    }
#endif

#ifdef STATS
    {
        _page_t *_pg = top;
        while (_pg)
        {
            if (_pg == inusepages)
              inusepages = NULL;
            if (_pg->hdr.prev_inuse)
              _pg->hdr.prev_inuse->hdr.next_inuse = _pg->hdr.next_inuse;
            if (_pg->hdr.next_inuse)
                _pg->hdr.next_inuse->hdr.prev_inuse = _pg->hdr.prev_inuse;;
            _pg->hdr.next_inuse = _pg->hdr.prev_inuse = NULL;

            STATINC(stat_n_pages_deleted);
            if (_pg == bottom)
              break;
            _pg = _pg->hdr.next;
        }
    }
#endif
}


#ifdef STATS
static void print_inuse(void)
{
    const _page_t *pg;

    for (pg=inusepages; pg; pg=pg->hdr.next_inuse)
      printf("%p\n", pg);
}
#endif


/* Allocate a single page the size of n_pages */
static _page_t *new_page(int n_pages)
{
    _page_t *pg, *prev;
    size_t sz, data_size;
    unsigned max_checks;

    /* Size of data we need (might span multiple page structs) */
    sz = DEFAULT_PAGE_SIZE * n_pages;
    data_size = sz - sizeof(_page_hdr_t);// - 1;

    /* Scan freepages, if a pg with enough space is found rm from freelist */
    max_checks = 0;
    for (prev=NULL, pg=freepages; pg; pg=pg->hdr.next)
    {
        if (pg->hdr.data_size >= data_size) // Was '==' constraint
        {
            if (prev)
              prev->hdr.next = pg->hdr.next;
            break;
        }

        if (++max_checks == 10)
        {
            pg = NULL;
            break;
        }
        prev = pg;
    }

#if STATS
    if (pg)
      STATINC(stat_n_pages_recycled);
#endif
    
    /* No free pages so make some */
    if (!pg)
    {
        pg = (_page_t *)my_sbrk(sz);
        STATINC(stat_n_pages_created);
    }
    
    /* Make the freepages head/tail pretty */
    if (freepages == pg)
      freepages = freepages->hdr.next;

    /* Useable size (dont count region header) */
    __builtin_memset(pg->data, 0, data_size);
    pg->hdr.next = NULL;
    pg->hdr.data_size = data_size;

#ifdef DEBUG_PAGE_HELL
    /* Created and giving out page... it is in use */
    pg->hdr.in_use = true;
#endif

#ifdef STATS
    pg->hdr.prev_inuse = NULL;
    pg->hdr.next_inuse = inusepages;
    if (inusepages)
      inusepages->hdr.prev_inuse = pg;
    inusepages = pg;
#endif

    return pg;
}


static _super_t *alloc_region(size_t size)
{
    _page_t *p, *tmp;
    _super_t *prev = NULL, *r = NULL;

    /* Look for regions on the free list */
    for (r=freeregions; r; r=r->next_region)
    {
        if (r->size < size)
        {
            prev = r;
            continue;
        }

        /* Unlink 'r' */
        if (!prev)
          freeregions = NULL;
        else
          prev->next_region = r->next_region;
        break;
    }

    /* None on the free region list, create a region from the available space on
     * a region page
     */
    if (!r)
    {
        p = region_pages;
      //for (p=region_pages; p; p=p->hdr.next)
        if (p &&
            (char *)p->hdr.region_hdr_brk + size <=
            (char *)p->data + p->hdr.data_size)
        {
            r = (_super_t *)p->hdr.region_hdr_brk;
            p->hdr.region_hdr_brk += size;
    //        break;
        }
    }

    /* Still no region found... get a new page and use that */
    if (!r)
    {
        p = new_page(ceil((double)size / DEFAULT_PAGE_SIZE));
        tmp = region_pages;
        region_pages = p;
        p->hdr.region_hdr_brk = p->data;
        p->hdr.next = tmp;
        p->hdr.region_hdr_brk += size;
        r = (_super_t *)p->data;
        STATINC(stat_n_region_pages);
    }

    return r;
}


/* Locate a new region that has enough room to contain the given subregions */
static _super_t *new_region(int n_subs)
{
    _super_t *r;
    _page_t *pg, *r_pg;
    size_t size;
    static int counter;

    ++counter;

    size = sizeof(_super_t) + (sizeof(_sub_t *) * n_subs);
    r = alloc_region(size);
    memset(r, 0, size);

#ifdef STATS
    STATINC(stat_n_regions_created);
    STATINC_VAL(stat_n_subregions, n_subs);
    if (r->n_region_uses) /* Number of times we have reused this region */
      STATINC(stat_n_regions_recycled);
    STATINC(r->n_region_uses);
#endif
    r->size = size;
    r->n_subregions = n_subs;
    return r;
}


/* Create the region and the data for it to pimp out:
 * We lazily create sub regions.  When asked for a subregion, the size requested
 * will be passed and the sub region will be created for objects of 'size'  This
 * prevents us to have to pass a string to this function which states how big
 * each subregion is.
 */
_super_t *__rbmm_create_region(int highest_type_index, bool is_global)
{
    int i;
    static int id = 123;
    _sub_t *sub;
    _super_t *sup = new_region(highest_type_index + 1);
    
    /* Setup super-region */
#ifdef RBMM_GC // FIXME
    sup->id = is_global ? GLOBAL_REGION_ID : ++id;
#else 
    sup->id = ++id;
#endif
    sup->ref_count = 0;

    /* Setup sub-region
    for (i=0; i<=highest_type_index; ++i)
    {
        sub = SUB_PTR(sup,i);
        SET_SUPERREGION(sub, sup);
        sub->pages = NULL;
        sub->bottom_page = sub->pages;
        SET_TYPE_INFO(sub, i);
    }
    */
    RBMM_PR("Super-region created at %p for %d with %d sub-regions\n", 
            sup, sup->id, highest_type_index);
#ifdef STATS
    stat_n_gc_types = highest_type_index;
#endif
    return sup;
}


/* Only for rbmm-no-gc: The GC variant will use a normal region as a global */
_super_t *__rbmm_create_global_region(void)
{
    _super_t *r = new_region(1);
    r->id = GLOBAL_REGION_ID;
    RBMM_PR("Global region created at %p\n", r);
    return r;
}


/* Return number of bytes needed to contain a bitmap, where each bit represents
 * an object on the page.
 * 'n_pages' is the page-size multiplier.  This is >1 if the page being
 * allocated is larger than DEFAULT_PAGE_SIZE.  In that case there only ever
 * needs to be 1 redirected-bit.
 */
#ifdef RBMM_GC
static inline size_t calc_n_redir_bits(size_t obj_size, int n_pages)
{
    const size_t n_page_data_bits =
        ((DEFAULT_PAGE_SIZE*n_pages) - sizeof(_page_hdr_t)) * 8;
    const size_t n_obj_bits = 1 + (obj_size * 8);
    assert(floor((double)n_page_data_bits/(double)n_obj_bits));
    return ceil((double)n_page_data_bits / (double)n_obj_bits);
}


/* Given 'n' number of objects that the redirected map can hold for a page, this
 * calculates the offset where the data actually begins.
 * Returns number of bytes the page header + redirmap size is.
 */
static inline size_t calc_header_and_redir_size(int n_redir_objects)
{
    const int hdr_bytes = ceil((double)n_redir_objects/8)+sizeof(_page_hdr_t);
    const int alignment = sizeof(void *);
    return ceil((double)hdr_bytes / (double)alignment) * alignment;
}
#endif


#if defined(RBMM_GC) && defined(DEBUG_GC)
static void print_type_info_table(void)
{
    int i;
    const __crap_tientry_t *ti;
        
    RBMM_PRGC("Type info table:");

    i=0;
    for (ti=crap_rt_get_type_info(i); ti; ti=crap_rt_get_type_info(++i))
      RBMM_PRGC("[%d] <%d, %d, %d, %d, %s, %s>",
                i,
                ti->parent_offset,
                ti->size,
                ti->type_info_idx,
                ti->n_members,
                ti->is_pointer ? "pointer" : "not pointer",
                ti->name);
}
#endif


#ifdef RBMM_GC
static void add_to_page_subregion_map(_sub_t *sub, _page_t *page)
{
    int i, n_pages;
    unsigned long idx;
    static bool initialized;

    if (!initialized)
    {
        _my_mmap_base = (uintptr_t)page + DEFAULT_PAGE_SIZE - 1;
        initialized = true;
    }

    n_pages = ceil((float)page->hdr.data_size / DEFAULT_PAGE_SIZE);
    for (i=0; i<n_pages; ++i)
    {
        /* If we have a jumbo page, use the low bit of the data in the array
         * (the subregion pointer) to flag this.
         */
        idx = PG_TO_SUB_IDX((uintptr_t)page + (i * DEFAULT_PAGE_SIZE));
        if (idx > PG_TO_SUB_MAX)
          return;
        else if (i > 0 && sub)
          _pg_to_sub[idx] = (_sub_t *)((uintptr_t)sub | IS_PART_OF_JUMBO_PAGE);
        else
          _pg_to_sub[idx] = sub;
    }
}


static inline _sub_t *addr_to_subregion(uintptr_t addr)
{
    /* Remove the "jumbo-page flag/bit" */
    int idx = PG_TO_SUB_IDX(addr);
    return (idx < 0 || idx > PG_TO_SUB_MAX) ?
        NULL : (_sub_t *)((uintptr_t)_pg_to_sub[idx] & ~IS_PART_OF_JUMBO_PAGE);
}


static inline const __crap_tientry_t *addr_to_typeinfo(uintptr_t addr)
{
    const _sub_t *sub = addr_to_subregion(addr);
    return sub ? sub->type_info : NULL;
}


static _page_t *addr_to_page(uintptr_t addr)
{
    long long idx = PG_TO_SUB_IDX(addr);

    if (idx < 0 || idx > PG_TO_SUB_MAX)
      return NULL;

    while ((uintptr_t)_pg_to_sub[idx] & IS_PART_OF_JUMBO_PAGE)
      ++idx; /* Jumbo pages are stored as [last_page][next_to_last]...[first]*/

    return (_page_t *)PG_SUB_IDX_TO_ADDR(idx);
}


static uintptr_t gc_stack_base;
static inline _Bool addr_is_on_stack(uintptr_t addr)
{
    /* Just use this stack frame bp as the lower bound on stack.
     * It is an apporximation, but should suffice
     */
    register uintptr_t rbp1 asm("rbp");
    return (addr<gc_stack_base) && (addr>rbp1); /* Grows down */
}


static const __crap_tientry_t *addr_to_type_info(uintptr_t addr)
{
    _sub_t *sub = addr_to_subregion(addr);
    return (sub) ? sub->type_info : NULL;
}
#endif /* RBMM_GC */


/* If the subregion hasn't been initalized, the size field will be
 * used to initialize the subregion.
 * Sorry for the ifdefs ugly as all hell.
 */
static void *alloc_from_subregion(_sub_t *sub, size_t size)
{
    void *addr;
    int n_redir_bytes;
    size_t total_size;
    _page_t *tmp, *added_page;
#ifdef RBMM_GC
      /* GC ... hopefully we have more room now */
        if (_total_mmap_alloc > _next_gc)
        {
            if (__rbmm_gc())
              _next_gc = _next_gc * 2;//+ _total_mmap_alloc; 
        }
#endif

#ifdef STUPID_DEBUG
    static int x;
    if (++x % STUPID_DEBUG == 0) { __rbmm_gc(); }
#endif

    /* If we have run out of room ... */
    added_page = NULL;
    /* Still no room? Shit... allocate more */
    if (!sub->pages ||
        (((char *)sub->brk) + size) >
        (((char *)sub->pages->data) + sub->pages->hdr.data_size))
    {
        /* Calculate the size needed (we add extra book keeping data) */
        n_redir_bytes = ceil((double)N_REDIR_BITS(sub) / 8.0);
        total_size = size + n_redir_bytes;

        /* Add page(s) as stack discipline */
        tmp = sub->pages;
        sub->pages =
            new_page(ceil((double)total_size / (double)DEFAULT_PAGE_SIZE));
        sub->brk = sub->pages->data + n_redir_bytes;
        sub->pages->hdr.next = tmp;
        added_page = sub->pages;
        
        if (!tmp) /* Never had a page to begin with */
          sub->bottom_page = sub->pages;
    }

    /* If we had to request for a new page ... map it */
#ifdef RBMM_GC
    if (added_page)
    {
        add_to_page_subregion_map(sub, added_page);
        if (IS_PERFORMING_GC())
          added_page->hdr.to_space = true;
    }
#endif

    addr = sub->brk;
    sub->brk = (char *)sub->brk + size;

#ifdef DEBUG_PAGE_HELL
    /* If a region can alloc from it, the page is in use */
    assert(sub->pages->hdr.in_use);
#endif

#ifdef STATS
    if (IS_PERFORMING_GC())
    {
        STATINC(stat_n_gc_allocs);
        const char *c = getenv("RBMM_STATS_SAMPLE");
        if (c && ((atoi(c) % (stat_n_allocs+1)) == 0))
          stats_update_hwmarks(true);
    }
#if defined(STATS) && defined(RBMM_GC)
    {
       _page_t *pg;
       if ((pg = addr_to_page((uintptr_t)addr)))
         pg->hdr.n_alloc_bytes += size;
    }
#endif

    if (!sub->n_allocs)
      STATINC(stat_n_subregions_used);
#endif /* STATS */
    STATINC(stat_n_allocs);
    STATINC_VAL(stat_n_alloc_bytes, size);
    STATINC(sub->n_allocs);
    CLEAR_BP();
    return addr;
}


static inline void remove_subregion(_sub_t *sub)
{
    if (sub->pages)
    {
        delete_pages(sub->pages, sub->bottom_page);
        sub->pages = sub->bottom_page = NULL;
    }
}


#ifdef RBMM_GC
static _rbmm_array_meta_t *get_slice_metadata(
    const _sub_t *sub,
    uintptr_t     addr,
    size_t        capacity)
{
    uintptr_t end;
    _rbmm_array_meta_t *md = FIRST_METADATA_ADDR(addr);
    const _rbmm_array_meta_t *stop = md;

    do
    {
        if (!md)
          return NULL;
        end = SLICE_DATA_START(md) + (md->n_elts * sub->type_info->size);
        //if (addr >= (uintptr_t)md && addr <= end && (md->n_elts == capacity))
        if (addr >= (uintptr_t)md && addr <= end)
          return md;
        md = md->next;
    } while (md != stop);

    return NULL;
}
#endif /* RBMM_GC */


#ifdef RBMM_GC
static inline has_been_redirected(
    const _sub_t  *sub,
    const _page_t *page,
    uintptr_t      addr)
{
    const uintptr_t st =
        (uintptr_t)page->data + ceil((double)N_REDIR_BITS(sub)/8.0);
    const int idx = (addr - st) / sub->type_info->size;
    const int byte = (idx / 8);
    const int bit = idx % 8;
    return page->data[byte] & 1<<bit;
}


static void set_redirected_bit(_page_t *page, _sub_t *sub, uintptr_t addr)
{
    const uintptr_t st = 
      (uintptr_t)page->data + ceil((double)N_REDIR_BITS(sub)/8.0);
    const int idx = (addr - st) / sub->type_info->size;
    const int byte = (idx / 8);
    const int bit = idx % 8;
    page->data[byte] |= 1<<bit;
}
#endif /* RBMM_GC */


#ifdef RBMM_GC
static inline void clone_region(_super_t *super)
{
    int i;
    _sub_t *sub;
    _rbmm_array_meta_t *md;
    _page_t *pg;
    const _rbmm_array_meta_t *stop;
    
    /* Have all of the pages (from space) be stashed away so all allocations are
     * from new memory
     */
    for (i=0; i<super->n_subregions; ++i)
    {
        if (!(sub = SUB(super,i)))
          continue;
        
        /* If a slice subregion, then reset all the has been collected flags */
        stop = md = FIRST_METADATA(sub);
        if ((sub->flags & PRESERVE_SLICE) && sub->bottom_page)/* Where md is */
          do {
              md->has_been_preserved = 0;
          } while (md != stop);

        /* Move all pages to the 'from' space (read-only) and force our
         * allocator to create new to-space pages by NULL'ing pages member
         */
        sub->from_space = sub->pages;
        sub->from_space_bottom = sub->bottom_page;
        sub->pages = sub->bottom_page = NULL;

        for (pg=sub->from_space; pg; pg=pg->hdr.next)
          pg->hdr.to_space = false;

#ifdef DEBUG_PAGE_HELL
        _page_t *_pg = sub->from_space;
        while (_pg)
        {
            _pg->hdr.in_use = false;
            if (_pg == sub->from_space_bottom)
              break;
            _pg = _pg->hdr.next;
        }
#endif
    }
}
#endif /* RBMM_GC */


/* Return the number of alloc'd bytes in the list of pages */
#ifdef RBMM_GC
static unsigned long allocd_bytes(const _page_t *pg)
{
    unsigned long sum = 0;
    for (pg; pg; pg=pg->hdr.next)
      sum += pg->hdr.n_alloc_bytes;
    return sum;
}
#endif /* RBMM_GC */


/* If we are just garbage collecting.  We want to keep the newly allocated
 * (to-space) pages which are the 'pages' and 'bottom_page' members.
 * The (old, can be removed, from-space) pages should be removed.
 */
#ifdef RBMM_GC
static inline void remove_subregion_for_gc(_sub_t *sub)
{
    unsigned long to_space_alloc_bytes = allocd_bytes(sub->pages);
    unsigned long from_space_alloc_bytes = allocd_bytes(sub->from_space);

    sub->superregion->bytes_before_gc += from_space_alloc_bytes;
    sub->superregion->bytes_after_gc  += to_space_alloc_bytes;

#ifdef STATS
STATINC_VAL(stat_n_gc_bytes, from_space_alloc_bytes-to_space_alloc_bytes);
#endif
    if (sub->from_space)
    {
        delete_pages(sub->from_space, sub->from_space_bottom);
        sub->from_space = sub->from_space_bottom = NULL;
    }
}
#endif /* RBMM_GC */


#ifdef RBMM_GC
static _super_t *removed_list;
static void add_to_remove_list(_super_t *super)
{
    const _super_t *sup;
    
    for (sup=removed_list; sup; sup=sup->next_region)
      if (sup == super)
        return;

    if (removed_list)
      super->next_region = removed_list;
    else
      removed_list = super;
}
#endif /* RBMM_GC */


#ifdef RBMM_GC
static void discard_remove_list(void)
{
    int i;
    _super_t *sup, *die;
    _sub_t *sub;

    sup = removed_list;
    while (sup)
    {
        sup->has_been_copied = false;
        sup->bytes_before_gc = sup->bytes_after_gc = 0;
        die = sup;
        sup = sup->next_region;
        for (i=0; i<die->n_subregions; ++i)
          if ((sub = SUB(die, i)))
            remove_subregion_for_gc(sub);
    }
    removed_list = NULL;
}
#endif /* RBMM_GC */


#ifdef RBMM_GC
static inline void add_meta_data(_sub_t *sub, _rbmm_array_meta_t *md)
{
    _rbmm_array_meta_t *last;

    if (!FIRST_METADATA(sub) || !LAST_METADATA(sub))
    {
        *FIRST_METADATA(sub) = *md;
        md->prev = md;
    }
    else
    {
        last = LAST_METADATA(sub);
        last->next = md;
        md->prev = last;
        md->next = FIRST_METADATA(sub);
        FIRST_METADATA(sub)->prev = md; /* First points to end via prev */
    }
}
#endif /* RBMM_GC */


#ifdef RBMM_GC
static void update_register(int regno, uintptr_t addr)
{
    /* Based on ucontext register numbers (/usr/include/sys/ucontext.h)
     * Only consider callee-saved registers (see glibc's 86_64/jmpbuf-offsets.h)
     */
    switch (regno)
    {
        case REG_RBX:
            __asm__ __volatile__("mov %0, %%rbx\n" : : "r"(addr));
            break;
        case REG_R12:
            __asm__ __volatile__("mov %0, %%r12\n" : : "r"(addr));
            break;
        case REG_R13:
            __asm__ __volatile__("mov %0, %%r13\n" : : "r"(addr));
            break;
        case REG_R14:
            __asm__ __volatile__("mov %0, %%r14\n" : : "r"(addr));
            break;
        case REG_R15:
            __asm__ __volatile__("mov %0, %%r15\n" : : "r"(addr));
            break;
        default:
            abort();
    }
}
#endif /* RBMM_GC */


#ifdef RBMM_GC
static uintptr_t preserve(
    uintptr_t               addr,
    const __crap_tientry_t *ti,
    _page_t                *page,
    _sub_t                 *sub)
{
    size_t size;
    uintptr_t newaddr;

    if (!sub->superregion->has_been_copied)
    {
        clone_region(sub->superregion); /* Flip to and from spaces */
        sub->superregion->has_been_copied = true;
        add_to_remove_list(sub->superregion);
    }

    /* If addr is already in to-space sacrifice a goat */
    if (page->hdr.to_space)/* XXX Seriously?! We better not get true here */
      return 0;

    size = ti->size; 
    newaddr = (uintptr_t)alloc_from_subregion(sub, size);
    __builtin_memcpy((void *)newaddr, (void *)addr, size);
    set_redirected_bit(page, sub, addr);
    *(uintptr_t *)addr = newaddr;
    STATINC_VAL(stat_n_gc_copied_bytes, size);
    return newaddr;
}


/* Given an address into an object, find the object start */
static inline uintptr_t obj_head(
    uintptr_t      addr,
    const _page_t *page,
    size_t         redir_map_size,
    size_t         obj_size)
{
    int idx;
    uintptr_t obj_data_start;

    obj_data_start = (uintptr_t)page->data + redir_map_size;
    idx = floor((double)(addr - obj_data_start) / (double)obj_size);
    if (idx < 0)
      return 0;
    return obj_data_start /*+ redir_map_size*/ + (obj_size * idx);
}


/* Preserve and trace function pointer type.  We need one for slices and one for
 * basic objects.
 * The indexes into the jump table 'pres_and_trace_fnps' should match the
 * PRESERVE_XXX macro values.
 */
typedef void (*pres_and_trace_fnp)(
    uintptr_t               addrptr,
    int                     regnum,
    const __crap_tientry_t *ti,
    bool                    toplevel);


/* Preserve and trace jump-table */
static pres_and_trace_fnp pres_and_trace_fnps[];


static inline bool addr_is_on_heap(uintptr_t addr)
{
    return addr_to_subregion(addr);
}


static inline bool is_valid_addr(uintptr_t addr)
{
    return (addr && (addr_is_on_heap(addr) || addr_is_on_stack(addr)));
}


/* Calls proper preserve and trace routine (either basic obj, or slices) */
static void preserve_and_trace(
    uintptr_t               addrptr,
    const __crap_tientry_t *ti,
    bool                    is_pointer,
    int                     regnum,
    bool                    toplevel)
{
    /* We must pass a pointer to a pointer.  If we just have a stack or whole
     * object, we need a pointer to it... therefore pass '&addrptr'
     */
    int idx;
    uintptr_t ptr;
    //_sub_t *sub; /* TODO Fix analysis so we dont need this */

    if (!ti)
      return; 
    else if (ti->flags == PRESERVE_SLICE && !regnum && is_pointer)
      ptr = *(uintptr_t *)addrptr;
    else if (ti->flags == PRESERVE_SLICE && !regnum)
      ptr = addrptr;
    else
      ptr = is_pointer ? addrptr : (uintptr_t)&addrptr;

    /* Make sure we dont defref junk (e.g. parsing uninitilized members */
    if (!ptr || !is_valid_addr(*(uintptr_t *)ptr))
      return;

    //sub = addr_to_subregion(*(uintptr_t *)ptr);
    idx = ti->flags;
    //idx = sub ? sub->flags | ti->flags : ti->flags;
   
    /* Figure out what type of data we have */
    pres_and_trace_fnps[idx](ptr, regnum, ti, toplevel);
}


static void preserve_and_trace_basic_object(
    uintptr_t               addrptr,
    int                     regnum,
    const __crap_tientry_t *ti,
    bool                    toplevel)
{
    int i;
    uintptr_t addr, newbase, base, offset;
    bool needs_tracing;
    _sub_t *sub;
    _page_t *pg;
    const __crap_tientry_t *mem_ti, *type;

    /* If 'addr' is in heap, than it must be in a subregion */
    addr = *(uintptr_t *)addrptr;
    pg = addr_to_page(addr);
    sub = addr_to_subregion(addr);
    if (sub && pg)
    {
        type = sub->type_info;
        base = obj_head(addr,pg,ceil((double)N_REDIR_BITS(sub)/8.0),type->size);
        if (base == 0)
          return;
        offset = addr - base;

#ifdef PAGE_HELL /* Saftey XXX TODO remove this check */
        if (!addr_to_page(base)){abort();}
#endif
        if (!has_been_redirected(sub, pg, base))
        {
            if ((newbase = preserve(base, type, pg, sub)))
            {
                *(uintptr_t *)addrptr = newbase + offset;
                needs_tracing = true;
                if (regnum)
                  update_register(regnum, *(uintptr_t *)addrptr);
                STATINC(stat_n_gc_copies);
            }
            else
              return;
        }
        else
        {
            newbase = *(uintptr_t *)base;             /* Get redir pointer */
            *(uintptr_t *)addrptr = newbase + offset; /* Set redir pointer */
            needs_tracing = false;
            if (regnum)
              update_register(regnum, *(uintptr_t *)addrptr);
            STATINC(stat_n_gc_redirects);
        }
    }
    else if (addr) /* Addrptr is not in the heap, it must be a root */
    {
        if (toplevel)
        {
            newbase = addr;
            type = ti;
            needs_tracing = true;
        }
        else
          needs_tracing = false;
    }

    type = !sub ? ti : sub->type_info;

    if (needs_tracing)
      for (i=0; i<type->n_members; ++i)
      {
          mem_ti = crap_rt_get_type_info(type->type_info_idx + i + 1);
          preserve_and_trace(newbase+mem_ti->parent_offset, mem_ti,
                             mem_ti->is_pointer, 0, false);
      }
}


/* Preserve an array */
static void preserve_and_trace_array(
    uintptr_t               addrptr,
    int                     regnum,
    const __crap_tientry_t *ti,
    bool                    toplevel)
{
    int i;
    uintptr_t addr, newbase, base, offset, elt;
    bool needs_tracing;
    _sub_t *sub;
    _page_t *pg;
    const __crap_tientry_t *elt_ti, *type;

    /* If 'addr' is in heap, than it must be in a subregion */
    pg = addr_to_page(addr);
    sub = addr_to_subregion(addr);
    if (sub && pg)
    {
        type = sub->type_info;
        base = obj_head(addr,pg,ceil((double)N_REDIR_BITS(sub)/8.0),type->size);
        if (base == 0)
          return;
        offset = addr - base;

#ifdef PAGE_HELL /* Saftey XXX TODO remove this check */
        if (!addr_to_page(base)){abort();}
#endif
        if (!has_been_redirected(sub, pg, base))
        {
            if ((newbase = preserve(base, type, pg, sub)))
            {
                *(uintptr_t *)addrptr = newbase + offset;
                needs_tracing = true;
                if (regnum)
                  update_register(regnum, *(uintptr_t *)addrptr);
                STATINC(stat_n_gc_copies);
            }
            else
              return;
        }
        else
        {
            newbase = *(uintptr_t *)base;             /* Get redir pointer */
            *(uintptr_t *)addrptr = newbase + offset; /* Set redir pointer */
            needs_tracing = false;
            if (regnum)
              update_register(regnum, *(uintptr_t *)addrptr);
            STATINC(stat_n_gc_redirects);
        }
    }
    else if (addr) /* Addrptr is not in the heap, it must be a root */
    {
        if (toplevel)
        {
            newbase = addr;
            type = ti;
            needs_tracing = true;
        }
        else
          needs_tracing = false;
    }

    type = !sub ? ti : sub->type_info;

    if (needs_tracing)
    {
        elt_ti = crap_rt_get_type_info(type->type_info_idx);
        for (i=0; i<type->n_members; ++i)
        {
            elt = newbase + (i * elt_ti->size);
            preserve_and_trace(elt, elt_ti, elt_ti->is_pointer, 0, false);
        }
    }
}


static bool in_mid_of_slice(uintptr_t addr)
{
    const _sub_t *sub = addr_to_subregion(addr);
    const _rbmm_array_meta_t *md = get_slice_metadata(sub, addr, 0);
    return SLICE_DATA_START(md) != addr;
}


static _rbmm_array_meta_t *preserve_entire_slice(uintptr_t addrptr)
{
    int i;
    uintptr_t copybase, elt_base, addr;
    _sub_t *sub;
    _rbmm_array_meta_t *md;
    _rbmm_open_array_t *hdr;
    size_t elt_size, copybytes;
    const __crap_tientry_t *elt_ti, *slice_hdr_ti, *vals_ti;

    addr = *(uintptr_t *)addrptr;

    /* Get the start of the slice (data not header) */
    if (!(sub=addr_to_subregion(addr)) || !(md=get_slice_metadata(sub,addr,0)))
      return;

    /* Safety
     * FIX UP THIS sub->flags mess
     */
    if (sub->flags != PRESERVE_SLICE && sub->type_info->flags != PRESERVE_SLICE)
      return NULL;
    
    /* Get the type of the __values in the slice (first member) */
    slice_hdr_ti = sub->type_info;
    vals_ti = crap_rt_get_type_info(slice_hdr_ti->type_info_idx + 1);
    elt_ti = crap_rt_get_type_info(vals_ti->type_info_idx);
    elt_size = elt_ti->size;
    if (!md->has_been_preserved)
    {
        RBMM_PRGC("Preserving %d elements of a slice", hdr->__count);
        if (!sub->superregion->has_been_copied)
        {
            clone_region(sub->superregion); /* Flip to and from spaces */
            sub->superregion->has_been_copied = true;
            add_to_remove_list(sub->superregion);
        }

        hdr = (_rbmm_open_array_t *)addrptr;
        copybytes = md->header_size + (hdr->__capacity * elt_size);
        copybase = (uintptr_t)alloc_from_subregion(sub, copybytes);

        /* Copy offset fill in md */
        __builtin_memcpy((void *)copybase + md->header_size ,
                         (void *)SLICE_DATA_START(md),
                         hdr->__capacity * elt_size);
        _rbmm_array_meta_t *new_md = (_rbmm_array_meta_t *)copybase;
        new_md->header_size = md->header_size;
        new_md->n_elts = hdr->__capacity;
        add_meta_data(sub, new_md);

        /* Set redirect in the now "old" header */
        md->has_been_preserved = copybase;

        /* Preserve each of the elements (in from-space) */
        elt_base = copybase + md->header_size;

        /* Trace the elements */
        for (i=0; i<hdr->__count; ++i)
          preserve_and_trace(elt_base + i * elt_size,
                             elt_ti, elt_ti->is_pointer, 0, false);
    }
    else /* (md->has_been_preserved) */
      copybase = md->has_been_preserved;

    return (_rbmm_array_meta_t *)copybase;
}


/* This sucks: Check to see if the header is a valid slice header
 * XXX: This is only temporary
 */
#if 0
static bool is_header_ok(const _rbmm_open_array_t *hdr)
{
    if (hdr->__count <= 0 || hdr->__capacity <= 0 || 
        (hdr->__count > hdr->__capacity))
      return false;

    /* This might fail for slices of slices */
    const __crap_tientry_t *ti = addr_to_typeinfo((uintptr_t)hdr->__values);
    if (!ti || ti->flags != PRESERVE_SLICE)
      return false;

    return true;
}
#endif


static void preserve_and_trace_slice(
    uintptr_t               addrptr,
    int                     regnum,
    const __crap_tientry_t *ti,
    bool                    toplevel)
{
    int idx;
    size_t elt_size;
    uintptr_t slicestart, addr, redir_value;
    _sub_t *sub;
    const __crap_tientry_t *elt_ti, *slice_hdr_ti, *vals_ti;
    _rbmm_array_meta_t *md, *new_md;
    _rbmm_open_array_t *hdr;

    addr = *(uintptr_t *)addrptr;
    if (!(addr = *(uintptr_t *)addrptr))
      return;

    if (!(new_md = preserve_entire_slice(addrptr)))
      return;

    /* If in the middle of the slice and a register... this means we do not have
     * a slice-heaer from the Go-frontend.  So we should not update or redirect
     * anything
     */
    hdr = (_rbmm_open_array_t *)addrptr;
    if (/*!is_header_ok(hdr) || */in_mid_of_slice(addr))
    {
        if (!(sub = addr_to_subregion(addr)) ||
            !(md = get_slice_metadata(sub,addr,0)))
          return;
        slicestart = SLICE_DATA_START(md);
        elt_ti = sub->type_info;
        elt_size = sizeof(void*); /* This should always be an address */
        idx = (*(uintptr_t *)addrptr - slicestart) / elt_size;
        redir_value = 
            md->has_been_preserved + md->header_size + (idx * elt_size);
        if (regnum)
          update_register(regnum, redir_value);
    }
    else
    {
        hdr->__values = (void *)SLICE_DATA_START(new_md);
        if (regnum)
          update_register(regnum, (uintptr_t)hdr->__values);
    }
    //hdr->__values = (void *)SLICE_DATA_START(new_md);
    STATINC(stat_n_gcs_slices);
}


/* Define the jump table for preserve_and_trace */
static pres_and_trace_fnp pres_and_trace_fnps[] =
{
    preserve_and_trace_basic_object, /* CRAP_FLAGS_OBJECT */
    preserve_and_trace_array,        /* CRAP_FLAGS_ARRAY  */
    preserve_and_trace_slice,        /* CRAP_USER_FLAG_2  */
};
#endif /* RBMM_GC */


#ifdef RBMM_GC
/* Callee-saved table */
static uintptr_t callee_saved[NGREG] = {0};
#endif


#ifdef RBMM_GC
static _Bool __rbmm_gc(void)
{
    int i, n_globals;
    uintptr_t *call_site_addr;
    uintptr_t stack_addr, prev_caller_bp, caller_bp, rootptr;
    const __crap_tientry_t *tientry;
    const __crap_csentry_t *csentry;
    const __crap_rientry_t *rientry;
    const __crap_vientry_t *vientry, **globals;
    const __crap_llentry_t *llentry, *llentry_guess;
    ucontext_t ctx;
    uintptr_t bp1 = GET_BP();

    if (IS_PERFORMING_GC() || !bp1) /* XXX SHOULD NEVER HAVE !bp1 */
      return false;
    SET_IS_PERFORMING_GC();
    RBMM_PRGC("**** Garbage Collecting ****");
    
    /* Gimmie some registers.  These registers were saved by the caller.  We are
     * the callee.  So we must not f..screw them up.
     */
    getcontext(&ctx);

#if defined(STUPID_DEBUG) || defined(DEBUG_GC)
    static bool has_printed;
    if (!has_printed)
    {
        crap_rt_print_type_info_table();
        has_printed = true;
    }
#endif

    /* Get the callsite of the function calling this... */
    call_site_addr = (uintptr_t *)(bp1 + sizeof(uintptr_t)); /* rip */

    /* Trace the stack */
    llentry_guess = NULL;
    prev_caller_bp = bp1;
    while ((llentry = crap_rt_get_llentry(*call_site_addr)) || 
           (llentry_guess = crap_rt_get_llentry_guess(*call_site_addr)))
    {
        if (llentry_guess)
          llentry = llentry_guess;

        caller_bp = *(uintptr_t *)prev_caller_bp;
        RBMM_PRGC("Callsite is %p with frame pointer %p in %s %s", 
                  *call_site_addr, caller_bp,
                  llentry->caller_name, llentry_guess?"(Guess)":"");
       
        /* Just for address-is-on-stack check */ 
        gc_stack_base = caller_bp;

        /* This loop looks at all variables defined in a function (local and
         * formal parameters).
         */
        for (i=0; i<llentry->func_info->n_var_info; ++i)
        {
            vientry = llentry->func_info->var_info[i];
            tientry = crap_rt_get_type_info(vientry->type_info_idx);
            rootptr = caller_bp + vientry->u.fp_offset;
            RBMM_PRGC("Visiting variable %s type index %d",
                      vientry->name, vientry->type_info_idx);
            preserve_and_trace(rootptr, tientry, vientry->is_pointer, 0, true);
        }
        
        /* Trace callee-save stack values */
        for (i=0; i<llentry->func_info->n_callee_save_info; ++i)
        {
            csentry = llentry->func_info->callee_save_info[i];
            stack_addr = caller_bp + csentry->offset_from_fp + sizeof(void *);
            if ((tientry=addr_to_type_info(*(uintptr_t *)stack_addr)))
               preserve_and_trace(stack_addr, tientry, true, 0, true);
        }

        /* Trace registers */
        for (i=0; i<llentry->n_registers; ++i)
        {
            rientry = llentry->reg_info[i];
            rootptr = ctx.uc_mcontext.gregs[rientry->register_number];
            tientry = addr_to_type_info(rootptr);
            RBMM_PRGC("Visiting register %d", rientry->register_number);
            preserve_and_trace(rootptr, tientry, false,
                               rientry->register_number, true);
        }

        /* Next stack frame */
        call_site_addr = (uintptr_t *)(caller_bp + 8);
        prev_caller_bp = caller_bp;
        llentry_guess = NULL;
    }
   
    /* Reset */    
    gc_stack_base = 0;

    /* Trace globals */
    globals = crap_rt_get_globals(&n_globals);
    for (i=0; i<n_globals; ++i)
    {
        vientry = globals[i];
        tientry = crap_rt_get_type_info(vientry->type_info_idx);
        RBMM_PRGC("Visiting global %d", vientry->name);
        preserve_and_trace(vientry->u.global_addr, tientry, tientry->is_pointer, 0, true);
    }

    /* Reset callee saved list */
    __builtin_memset(callee_saved, 0, sizeof(callee_saved));

    /* Update any metadata */

    /* Last: remove the regions we cleaned up */ 
    discard_remove_list();
    CLEAR_BP();
    CLEAR_IS_PERFORMING_GC();
    STATINC(stat_n_gcs);
    RBMM_PRGC("**** Garbage Collecting Complete ****");
    return true;
}
#endif /* RBMM_GC */


void inline *__rbmm_alloc_from_region(
    _super_t    *reg,
    size_t      size, 
    const void *type)
{
    int idx;
    size_t total_size, n_redir_bytes, n_redir_bits;
    _page_t *pg;
    _sub_t *sub = NULL;

#ifdef RBMM_GC
    const char *base = (const char *)__crap_plug_type_info_table[0];
#endif
    SET_BP();
    RBMM_PR("Allocate memory from region at %p with id %d for %d bytes "
            "ref count is %d\n", reg, reg->id, size, reg->ref_count);

    /* Special case: global region (garbage collected) */
#ifndef RBMM_GC /* If we are not using our GC use the Go GC */
    if (reg->id == GLOBAL_REGION_ID)
      return __go_new(size);
    idx = 0;
#else
    idx = ((char *)type - base) /  sizeof(__crap_tientry_t);
#endif

    /* Prevent allocating from a removed region */
    if (reg->id == INVALID_REGION_ID)
    {
        RBMM_PR("Trying to allocate from a removed region: %d\n", reg->id);
        abort();
    }

    if (!SUB(reg, idx)) /* Lazy Initilization */
    {
        /* Calculate the size needed (we add extra book keeping data)
         * Add '1' just encase the object is an exact
         * size or exact multiple of a page.
         * We need at least 1 bit (1 byte is the * smallest we can request)
         * For the redirected bit on a really large alloc.
         */
        total_size = size + 1 + sizeof(_sub_t);
        n_redir_bytes = 0;
        n_redir_bits = 0;
#ifdef RBMM_GC
        const __crap_tientry_t *ti =  crap_rt_get_type_info(idx);
        if (!(ti->flags & PRESERVE_SLICE))
        {
            n_redir_bits = calc_n_redir_bits(
                size,
                ceil((double)total_size / (double)DEFAULT_PAGE_SIZE));
            total_size += calc_header_and_redir_size(n_redir_bits);
            n_redir_bytes = ceil((double)n_redir_bits / 8.0);
        }
#endif
        /* Setup the pages */
        pg = new_page(ceil((double)total_size / (double)DEFAULT_PAGE_SIZE));
        sub = (_sub_t *)pg->data;
        sub->pages = NULL;
        sub->bottom_page = sub->pages;
        sub->pages = pg;
        sub->bottom_page = sub->pages;
        reg->sub_regions[idx] = sub;
#ifdef RBMM_GC
        SET_SUPERREGION(sub, reg);
        N_REDIR_BITS(sub) = n_redir_bits;
        SET_TYPE_INFO(sub, idx);
#endif

        /* Adjust data break so we do not overwrite the redirected_map */
        sub->brk = (char *)(sub->pages->data + n_redir_bytes + sizeof(_sub_t));
    }

    return alloc_from_subregion(SUB(reg,idx), size);
}


/* Allocate the "__value" member and prefix metadata onto the head.
 * The only thing modified for the 'array_hdr' (array header) is the pointer to
 * this metadata.
 */
#ifdef RBMM_GC
static void *alloc_slice_values_gc(
    _super_t           *reg,
    uintptr_t           elt_size, 
    uintptr_t           capacity,
    const void         *type)
{
    void *data;
    int sub_idx;
    size_t meta_size, redir_size, total_size;
    _sub_t *sub;
    _rbmm_array_meta_t *meta;
    const char *base = (const char *)__crap_plug_type_info_table[0];

    /* Get the subregion and set some flags */
    sub_idx = ((char *)type - base) /  sizeof(__crap_tientry_t);
    sub = SUB(reg, sub_idx);
    SET_FLAGS(sub, PRESERVE_SLICE);

    /* 2 bit per element in the array/slice */
    // TODO redir_size = ceil((2.0 * (double)capacity) / 8.0);

    // TODO
    redir_size = 0;

    /* Array data follows the metadata: [meta_hdr[redirbits]][data] */
    meta_size = sizeof(_rbmm_array_meta_t) + redir_size;
    total_size = meta_size + (elt_size * capacity);
    data = __rbmm_alloc_from_region(reg, total_size, type);
    meta = (_rbmm_array_meta_t *)data;
    meta->has_been_preserved = 0;
    meta->n_elts = capacity;
    meta->header_size = meta_size;

    /* Chain-in the metadata */
    add_meta_data(sub, meta);
    
    SLICE_PR("Allocating (%p): "
             "Cap(%d) EltSz(%d) HdrSz(%d) RedirSz(%d) TotalSz(%d)",
              meta, capacity, elt_size,
              sizeof(_rbmm_array_meta_t), redir_size, total_size);

    return (void *)SLICE_DATA_START(meta);
}
#endif /* RBMM_GC */


#ifdef RBMM_GC
// TODO: Make a call to this in preserve_and_trace_slice
static const __crap_tientry_t *get_slice_element_type(
   const __crap_tientry_t *slice_ti)
{
    const __crap_tientry_t *vals_ti = 
        crap_rt_get_type_info(slice_ti->type_info_idx + 1);
    const __crap_tientry_t *elt_ptr_ti = 
        crap_rt_get_type_info(vals_ti->type_info_idx);
    return crap_rt_get_type_info(elt_ptr_ti->type_info_idx);
}

#endif /* RBMM_GC */


#ifdef RBMM_GC
/* Allocates a slice header which is what the Go Frontend creates.
 * i.e. the '__go_open_array' or as we call it '__rbmm_open_array_t'
 */
void inline *__rbmm_alloc_slice_values(
    _super_t    *reg,
    size_t      size, 
    const void *type)
{
    /* Prevent allocating from a removed region */
    if (reg->id == INVALID_REGION_ID)
    {
        RBMM_PR("Trying to allocate from a removed region: %d\n", reg->id);
        abort();
    }

    /* Size should probably just be sizeof(_rbmm_open_array_t) */
    return alloc_slice_values_gc(reg, size, 1, type);
}
#endif


void __rbmm_remove_region(_super_t *reg)
{
    int i;
    _super_t *tmp;

    /* We can only remove non-global regions with refcount of 0 */
    if ((reg->ref_count != 0) || reg->id == GLOBAL_REGION_ID)
      return;
    
    if (reg->id == INVALID_REGION_ID)
    {
        RBMM_PR("Warning: Trying to remove region already removed\n");
        return;
    }

    RBMM_PR("Trying to remove region %d\n", reg->id);

    /* Remove pages */
    for (i=0; i<reg->n_subregions; ++i)
      remove_subregion(SUB(reg,i));
    
    /* Put region back onto free region list */
    tmp = freeregions;
    freeregions = reg;
    freeregions->next_region = tmp;

#ifdef DEBUG_RUNTIME
    reg->old_id = reg->id;
#endif
    reg->id = INVALID_REGION_ID;
    STATINC(stat_n_regions_deleted);
}


#ifdef STATS
void __rbmm_print_stats(void)
{
    unsigned long avg_n_objs_per_region = 0, avg_n_subregions = 0;
    unsigned long avg_n_bytes_per_used_subregion = 0;

    /* Subtract 1 to ignore the global region */
    if ((stat_n_regions_created - 1) != 0)
    {
        avg_n_objs_per_region = stat_n_allocs / (stat_n_regions_created - 1);
        avg_n_subregions = stat_n_subregions / (stat_n_regions_created - 1);
        avg_n_bytes_per_used_subregion =
            stat_n_alloc_bytes / stat_n_subregions_used;
    }

    printf("----- Region Statistics -----\n"
           "Number of Regions Created:                 %10lu\n"
           "Number of Regions Removed:                 %10lu\n"
           "Number of Regions Recycled:                %10lu\n"
           "Region Header Size:                        %10lu\n"
           "Number of Zone Headers Created:            %10lu\n"
           "Number of Zone Headers Used:               %10lu\n"
           "Number of Zone Headers Not Used:           %10lu\n"
           "Number of Region Header Pages:             %10lu\n"
           "Number of Pages Created:                   %10lu\n"
           "Number of Pages Removed:                   %10lu\n"
           "Number of Pages Recycled:                  %10lu\n"
           "Number of Total Garbage Collections:       %10lu\n"
           "Number of Garbage Collection (slices):     %10lu\n"
           "Number of Garbage Collected Bytes:         %10lu\n"
           "Number of Garbage Collection Copies:       %10lu\n"
           "Number of Garbage Collection Copied Bytes: %10lu\n"
           "Number of Garbage Collection Redirs:       %10lu\n"
           "Number of Total Local Region Allocs:       %10lu\n"
           "Number of Garbage Collection Allocs:       %10lu\n"
           "Total Bytes Allocated From Zones:          %10lu\n"
           "Total Bytes Allocated From System (mmap):  %10lu\n"
           "Avg. Number of Objects Per Region:         %10lu\n"
           "Avg. Number of Zones Per Region:           %10lu\n"
           "Avg. Number of Alloc Bytes Per Used Zone:  %10lu\n",
           stat_n_regions_created,
           stat_n_regions_deleted,
           stat_n_regions_recycled,
           sizeof(_sub_t) * stat_n_gc_types,
           stat_n_subregions,
           stat_n_subregions_used,
           stat_n_subregions - stat_n_subregions_used,
           stat_n_region_pages,
           stat_n_pages_created,
           stat_n_pages_deleted,
           stat_n_pages_recycled,
           stat_n_gcs,
           stat_n_gcs_slices,
           stat_n_gc_bytes,
           stat_n_gc_copies,
           stat_n_gc_copied_bytes,
           stat_n_gc_redirects,
           stat_n_allocs,
           stat_n_gc_allocs,
           stat_n_alloc_bytes,
           stat_n_alloc_mmap,
           avg_n_objs_per_region,
           avg_n_subregions,
           avg_n_bytes_per_used_subregion);

    printf("----- Page HWM Stats -----\n"
           "In Use HWM:       %10lu\n"
           "In Use HWM Bytes: %10lu\n"
           "Free HWM:         %10lu\n"
           "Free HWM Bytes:   %10lu\n"
           "Default Page Size:                         %10lu\n"
           "In Use HWM If Pages Were All Default Size: %10lu\n"
           "Free HWM If Pages Were All Default Size:   %10lu\n",
           stat_n_pages_inuse_hwm,
           stat_n_pages_inuse_hwm_bytes,
           stat_n_pages_free_hwm,
           stat_n_pages_free_hwm_bytes,
           DEFAULT_PAGE_SIZE,
           stat_n_pages_inuse_hwm_bytes / DEFAULT_PAGE_SIZE,
           stat_n_pages_free_hwm_bytes / DEFAULT_PAGE_SIZE);

}
#endif /* STATS */
          

#ifdef DEBUG_RUNTIME_UTILS
/* For debugging */
static const _page_t *get_page(const _super_t *reg, int num)
{
    int id;
    const _page_t *pg;

    id = 0;
    for (pg=SUB(reg, 0)->pages; pg; pg=pg->hdr.next)
      if (id == num)
        return pg;
      else
         ++id;

    return NULL;
}


/* For debugging
 * Return the page number (starting at 1 to n) where the address lies on.  Zero
 * is retuned if we can't find the page
 */
static int get_page_num(const _super_t *reg, const void *addr)
{
    int id;
    const _page_t *pg;

    id = 0;
    for (pg=SUB(reg, 0)->pages; pg; pg=pg->hdr.next)
    {
        ++id;
        if (addr && ((void *)addr >= (void *)pg->data) &&
          ((void *)addr <= (void *)(pg->data+pg->hdr.data_size)))
          return id;
    }

    return 0;
}


/* For debugging
 * If addr is non-NULL report what page this addr is in
 */
static void print_pages(const _super_t *reg, const void *addr)
{
    int n_pages, pg_num;
    const _page_t *pg;

    n_pages = 0;

    if (addr)
      pg_num = get_page_num(reg, addr);

    printf("Region %d [\n", reg->id);
    for (pg=SUB(reg, 0)->pages; pg; pg=pg->hdr.next)
    {
        ++n_pages;
        printf("\t[%d] %p-%p", n_pages, pg->data, pg->data+pg->hdr.data_size);

        if (pg_num == n_pages)
          printf(" <--- (%p)\n", addr);
        else
          printf("\n");
    }
    printf("]");
    fflush(NULL);
}


#ifdef STATS
static void print_page_chain(const _page_t *pg)
{
    int i;
    const _page_t *p;
    for (p=pg; p; p=p->hdr.next)
      printf(">> [%d] Page %p\n", ++i, p);
}
#endif


/* For debugging */
static void is_in_free_page(void *addr)
{
    const _page_t *pg;
    const unsigned char *a = (unsigned char *)addr;

    for (pg=freepages; pg; pg=pg->hdr.next)
    {
        printf("Page [%p to %p]", pg->data, pg->data + pg->hdr.data_size);
        if ((a >= pg->data) && (a <= (pg->data+pg->hdr.data_size)))
          printf("<--- (%p)", a);
        putc('\n', stdout);
    }
}
#endif /* DEBUG_RUNTIME_UTILS    */


#if 0
void __rbmm_inc(_super_t *reg)
{
    if (reg->id == INVALID_REGION_ID)
      return;
    ++reg->ref_count;
}
void __rbmm_dec(_super_t *reg)
{
    if (reg->id == INVALID_REGION_ID)
      return;
    --reg->ref_count;
}
#endif


/*********************************************************************
** ALL FUNCTIONS BELOW ARE FROM GCC 4.7.0 and modified for regions  **
** Our analysis replaces the go version with this version           **
*********************************************************************/
/* Rippin' off __go_new_map (gcc 4.7.0) */
void *__rbmm_new_map(
    _super_t                        *reg,
    const struct __go_map_descriptor *descriptor,
    size_t                            entries)
{
  int ientries;
  struct __go_map *ret;
  SET_BP();

  ientries = (int) entries;
  if (ientries < 0 || (uintptr_t) ientries != entries)
    runtime_panicstring ("map size out of range");

  if (entries == 0)
    entries = 5;
  else
    entries = __go_map_next_prime (entries);
  ret = (struct __go_map *) __rbmm_alloc_from_region(reg, sizeof(struct __go_map), 0); //TODO
  ret->__descriptor = descriptor;
  ret->__element_count = 0;
  ret->__bucket_count = entries;
  ret->__buckets = 
      (void **) __rbmm_alloc_from_region(reg,  (entries * sizeof(void *)), 0);   //TODO
  __builtin_memset (ret->__buckets, 0, entries * sizeof (void *));
  return ret;
}
    

/* Rippin' off __go_map_rehash (4.7.0) */
static void __rbmm_map_rehash(
    _super_t        *reg,
    struct __go_map *map)
{
  const struct __go_map_descriptor *descriptor;
  const struct __go_type_descriptor *key_descriptor;
  uintptr_t key_offset;
  size_t key_size;
  uintptr_t (*hashfn) (const void *, uintptr_t);
  uintptr_t old_bucket_count;
  void **old_buckets;
  uintptr_t new_bucket_count;
  void **new_buckets;
  uintptr_t i;

  descriptor = map->__descriptor;

  key_descriptor = descriptor->__map_descriptor->__key_type;
  key_offset = descriptor->__key_offset;
  key_size = key_descriptor->__size;
  hashfn = key_descriptor->__hashfn;

  old_bucket_count = map->__bucket_count;
  old_buckets = map->__buckets;

  new_bucket_count = __go_map_next_prime (old_bucket_count * 2);
  new_buckets =
      (void **) __rbmm_alloc_from_region(reg, new_bucket_count*sizeof(void *), 0); // TODO
  __builtin_memset (new_buckets, 0, new_bucket_count * sizeof (void *));

  for (i = 0; i < old_bucket_count; ++i)
    {
      char* entry;
      char* next;

      for (entry = (char *)old_buckets[i]; entry != NULL; entry = next)
    {
      size_t key_hash;
      size_t new_bucket_index;

      /* We could speed up rehashing at the cost of memory space
         by caching the hash code.  */
      key_hash = hashfn (entry + key_offset, key_size);
      new_bucket_index = key_hash % new_bucket_count;

      next = *(char **) entry;
      *(char **) entry = (char *)new_buckets[new_bucket_index];
      new_buckets[new_bucket_index] = entry;
    }
    }

  /* __go_free (old_buckets); <--- Using regions, so dont call this */

  map->__bucket_count = new_bucket_count;
  map->__buckets = new_buckets;

}


/* Rippin' off __go_map_index (4.7.0) */
void *__rbmm_map_index (
    _super_t       *reg,
    struct __go_map *map,
    const void      *key,
    bool             insert)
{
  const struct __go_map_descriptor *descriptor;
  const struct __go_type_descriptor *key_descriptor;
  uintptr_t key_offset;
  bool (*equalfn) (const void*, const void*, uintptr_t);
  size_t key_hash;
  size_t key_size;
  size_t bucket_index;
  char *entry;

  if (map == NULL)
    {
      if (insert)
    runtime_panicstring ("assignment to entry in nil map");
      return NULL;
    }

  descriptor = map->__descriptor;

  key_descriptor = descriptor->__map_descriptor->__key_type;
  key_offset = descriptor->__key_offset;
  key_size = key_descriptor->__size;
  __go_assert (key_size != 0 && key_size != -1UL);
  equalfn = key_descriptor->__equalfn;

  key_hash = key_descriptor->__hashfn (key, key_size);
  bucket_index = key_hash % map->__bucket_count;

  entry = (char *) map->__buckets[bucket_index];
  while (entry != NULL)
    {
      if (equalfn (key, entry + key_offset, key_size))
    return entry + descriptor->__val_offset;
      entry = *(char **) entry;
    }

  if (!insert)
    return NULL;

  if (map->__element_count >= map->__bucket_count)
    {
      __rbmm_map_rehash (reg, map);
      bucket_index = key_hash % map->__bucket_count;
    }

  entry = (char *) __rbmm_alloc_from_region(reg, descriptor->__entry_size, 0); // TODO
  __builtin_memset (entry, 0, descriptor->__entry_size);

  __builtin_memcpy (entry + key_offset, key, key_size);

  *(char **) entry = (char *)map->__buckets[bucket_index];
  map->__buckets[bucket_index] = entry;

  map->__element_count += 1;

  return entry + descriptor->__val_offset;

}

#ifndef RBMM_GC
static inline void *alloc_slice_values_nongc(
    _super_t  *reg,
    size_t    elt_size,
    uintptr_t capacity)
{
    return __rbmm_alloc_from_region(reg, elt_size * capacity, NULL);
}
#endif


/* Rippin' off __go_make_slice2 (4.7.0) */
_rbmm_open_array_t
__rbmm_make_slice2 (
    _super_t                          *reg,
    const void                        *type,
    const struct __go_type_descriptor *td,
    uintptr_t                          len,
    uintptr_t                          cap)
{
  const struct __go_slice_type* std;
  int ilen;
  int icap;
  //uintptr_t size;
  _rbmm_open_array_t ret;
  SET_BP();

  __go_assert (td->__code == GO_SLICE);
  std = (const struct __go_slice_type *) td;

  ilen = (int) len;
  if (ilen < 0 || (uintptr_t) ilen != len)
    runtime_panicstring ("makeslice: len out of range");

  icap = (int) cap;
  if (cap < len
      || (uintptr_t) icap != cap
      || (std->__element_type->__size > 0
      && cap > (uintptr_t) -1U / std->__element_type->__size))
    runtime_panicstring ("makeslice: cap out of range");

  ret.__count = ilen;
  ret.__capacity = icap;

  //size = cap * std->__element_type->__size;
#ifdef RBMM_GC
  ret.__values=alloc_slice_values_gc(reg,std->__element_type->__size,icap,type);
#else
  ret.__values=alloc_slice_values_nongc(reg,std->__element_type->__size,icap);
#endif

  return ret;
}


/* Rippin' off __go_make_slice1 (4.7.0) */
_rbmm_open_array_t
__rbmm_make_slice1 (
    _super_t                          *reg,
    const void                        *type,
    const struct __go_type_descriptor *td,
    uintptr_t                          len)
{
  SET_BP();
  return __rbmm_make_slice2 (reg, type, td, len, len);
}


/* Rippin' off __go_make_slice2_big (4.7.0) */
_rbmm_open_array_t
__rbmm_make_slice2_big (
    _super_t                          *reg,
    const struct __go_type_descriptor *td,
    uint64_t                           len,
    uint64_t                           cap)
{
  uintptr_t slen;
  uintptr_t scap;
  SET_BP();

  abort(); // TODO

  slen = (uintptr_t) len;
  if ((uint64_t) slen != len)
    runtime_panicstring ("makeslice: len out of range");

  scap = (uintptr_t) cap;
  if ((uint64_t) scap != cap)
    runtime_panicstring ("makeslice: cap out of range");

  return __rbmm_make_slice2 (reg, 0, td, slen, scap);
}


/* Rippin' off __go_make_slice1_big (4.7.0) */
_rbmm_open_array_t
__go_make_slice1_big (
    _super_t                          *reg,
    const struct __go_type_descriptor *td,
    uint64_t                           len)
{
  SET_BP();
  abort(); // TODO
  return __rbmm_make_slice2_big (reg, td, len, len);
}


/* Rippin' off __go_append (gcc 4.7.1) */
_rbmm_open_array_t
__rbmm_append (
    _super_t           *reg,
    const void         *type,
    _rbmm_open_array_t  a,
    void               *bvalues, 
    uintptr_t           bcount,
    uintptr_t           element_size)
{
  uintptr_t ucount;
  int count;

  if (bvalues == NULL || bcount == 0)
    return a;

  ucount = (uintptr_t) a.__count + bcount;
  count = (int) ucount;
  if ((uintptr_t) count != ucount || count <= a.__count)
    runtime_panicstring ("append: slice overflow");

  if (count > a.__capacity)
    {
      int m;
      void *n;

    m = a.__capacity;
    if (m == 0)
      m = (int) bcount;
    else
    {
      do
        {
          if (a.__count < 1024)
        m += m;
          else
        m += m / 4;
        }
      while (m < count);
    }

      /* Replacing: n = __go_alloc (m * element_size); */
#ifdef RBMM_GC
    n = alloc_slice_values_gc(reg, element_size, m, type);
#else
    n = alloc_slice_values_nongc(reg, element_size, m);
#endif
      __builtin_memcpy (n, a.__values, a.__count * element_size);
      a.__values = n;
      a.__capacity = m;
    }

  __builtin_memmove ((char *) a.__values + a.__count * element_size,
             bvalues, bcount * element_size);
  a.__count = count;
  return a;
}
	
	
/* Rippin' off __go_construct_map (gcc 4.7.1) */
struct __go_map *
__rbmm_construct_map(
	_super_t *reg,
	const struct __go_map_descriptor *descriptor,
	uintptr_t count, uintptr_t entry_size,
	uintptr_t val_offset, uintptr_t val_size,
	const void *ventries)
{
  struct __go_map *ret;
  const unsigned char *entries;
  uintptr_t i;
  SET_BP();

  ret = __rbmm_new_map (reg, descriptor, count);

  entries = (const unsigned char *) ventries;
  for (i = 0; i < count; ++i)
    {
      void *val = __rbmm_map_index (reg, ret, entries, 1);
      __builtin_memcpy (val, entries + val_offset, val_size);
      entries += entry_size;
    }

  return ret;
}
