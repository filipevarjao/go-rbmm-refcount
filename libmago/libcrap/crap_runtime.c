#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <sys/mman.h>
#include "crap_runtime.h"


#define TAG "crap runtime"


/* Console output macros */
#define _P(_fp, _label, ...)                         \
do                                                   \
{                                                    \
    fprintf(stderr, "["TAG"] " _label" " __VA_ARGS__); \
    fputc('\n', stderr);                             \
} while (0)
    

/* Normal and Error console output macros */
#define P(...) _P(stdout, "", __VA_ARGS__)
#define E(...) \
do                                      \
{                                       \
    _P(stderr, "[Error]", __VA_ARGS__); \
    abort();                            \
} while (0)


/* Debugging console output mcros */
#ifdef DEBUG
#define D(...) _P(stderr,"", __VA_ARGS__)
#else
#define D(...) /* Ignore */
#endif


/* Ignore this member (it is for debugging only) (from crap_plug.c) */
#define IGNORE_ME_TYPE_INDEX_P(_index) ((_index) < 0)


/* Memory regions */
struct _space_d
{
    void *start;
    void *brk;
    size_t size;
    int n_allocs;
    void *(*alloc)(struct _space_d *, size_t, int);
};
typedef struct _space_d space_t;


/* Data we prefix each allocation with */
struct _crap_data_hdr_d
{
    /* Size of allocation including initial crap_data_hdr header */
    ssize_t size;
}; 
typedef struct _crap_data_hdr_d crap_data_hdr_t;


/* Accessors for the data header */
#define DATA(_x) ((void *)((char *)(_x) + sizeof(crap_data_hdr_t)))
#define DATA_HDR(_x) \
    (((crap_data_hdr_t *)((char *)(_x) - sizeof(crap_data_hdr_t))))
#define DATA_HDR_SIZE(_x) (DATA_HDR((_x))->size)

 /* HAS_DATA_BEEN_COPIED:  This uses the sign bit of the size field to signal to
 * the garbage collector that the data has already been copied to the to-space
 * during the current collection cycle.
 */
#define DATA_HDR_HAS_BEEN_COPIED(_x)     (DATA_HDR_SIZE((_x)) < 0)
#define DATA_HDR_SET_HAS_BEEN_COPIED(_x) (DATA_HDR_SIZE((_x)) *= -1)


/* Global memory spaces */
static space_t *to_space;
static space_t *from_space;


/* Global space memory offset */
static unsigned long long to_space_offset;


/* Data type to reprent which algorithm we are using for collecting */
struct _collector_d
{
    void (*initialize)(void);
    void (*finalize)(void);
};
typedef struct _collector_d collector_t;


/* Garbage Collector Algorithm: Cheney's */
static void initialize_cheney(void);
static void finalize_cheney(void);
static collector_t collector = {initialize_cheney, finalize_cheney};


/* Public function(s) */
void __crap_collect(void);


/* Table sizes:
 * Extern since data is in the binary, the linker resolves this for us.
 */
extern const int __crap_plug_type_info_table_n_entries;
extern const int __crap_plug_func_info_table_n_entries;
extern const int __crap_plug_global_info_table_n_entries;
extern const int __crap_plug_label_layout_table_n_entries;


#ifdef DEBUG
static void __crap_print_label_layout_table(void)
{
    int i;
    const __crap_llentry_t *llentry;

    D("Layout table located at %p", __crap_plug_label_layout_table);
    for (i=0; i<__crap_plug_label_layout_table_n_entries; ++i)
    {
        llentry = __crap_plug_label_layout_table[i];
        D("    [%d] Callsite at %p in '%s' Function Info at %p",
          i,
          llentry->return_callsite_addr,
          llentry->caller_name,
          llentry->func_info);
    }
}
#endif /* DEBUG */


#ifdef DEBUG
static void __crap_print_func_info_table(void)
{
    int i, j;
    const __crap_vientry_t *vientry;
    const __crap_fientry_t *fientry;

    D("Function info table located at %p", __crap_plug_func_info_table);
    for (i=0; i<__crap_plug_func_info_table_n_entries; ++i)
    {
        fientry = __crap_plug_func_info_table[i];
        vientry = *fientry->var_info;
        D("    [%d] %s Frame Size: %lu Number of Vars: %d Var Info: %p",
          i,
          fientry->name,
          fientry->stack_frame_size,
          fientry->n_var_info,
          fientry->var_info);

        for (j=0; j<fientry->n_var_info; ++j)
        {
            vientry = fientry->var_info[j];
            D("    ----> Contains variable %s at offset %d type info at %d",
              vientry->name,
              vientry->u.fp_offset,
              vientry->type_info_idx);
        }
    }
}
#endif /* DEBUG */


static void __crap_print_type_info_table(void)
{
    int i, n_members;
    const __crap_tientry_t *tientry;

    P("Type info table located at %p", __crap_plug_type_info_table);
    P("Format: <typename:fieldname, offset, size, num members, "
      "is_pointer, type_table_index, flags>");

    n_members = 0;
    for (i=0; i<__crap_plug_type_info_table_n_entries; ++i)
    {
        tientry = __crap_plug_type_info_table[i];
        P("    %s[%d] <%s, %d, %d, %d, %d, %d, %d>",
          n_members && (tientry->flags & ~CRAP_FLAGS_ARRAY) ? "     " : "",
          i,
          tientry->name,
          tientry->parent_offset,
          tientry->size,
          tientry->n_members,
          tientry->is_pointer,
          tientry->type_info_idx,
          tientry->flags);

        if (!n_members)
          n_members = tientry->n_members;
        else
          --n_members;
    }
}


#ifdef __cplusplus
extern "C"
#endif
const __crap_llentry_t *crap_rt_get_llentry(uintptr_t return_ptr)
{
    int i;

    for (i=0; i<__crap_plug_label_layout_table_n_entries; ++i)
      if (return_ptr==__crap_plug_label_layout_table[i]->return_callsite_addr)
        return __crap_plug_label_layout_table[i];

    return NULL;
}


/* Like 'get_llentry()' but this takes into account an additional 'jmp'
 * instruction after the callsite that GCC inserted between the 'label' and the
 * callsite.  In this case our return pointer is 2 bytes from the table we built
 * at compile time.  This occurs becuse we move the lable to the next basic
 * block in certain cases (error handling stuff in gcc).
 */
#ifdef __cplusplus
extern "C"
#endif
const __crap_llentry_t *crap_rt_get_llentry_guess(uintptr_t return_ptr)
{
    int i;
    uintptr_t callsite;

    for (i=0; i<__crap_plug_label_layout_table_n_entries; ++i)
    {
        callsite = __crap_plug_label_layout_table[i]->return_callsite_addr;
        if (return_ptr + 2 == callsite || return_ptr + 4 == callsite)
          return __crap_plug_label_layout_table[i];
    }

    return NULL;
}


#ifdef __cplusplus
extern "C"
#endif
const __crap_vientry_t **crap_rt_get_globals(int *n_globals)
{
    *n_globals = __crap_plug_global_info_table_n_entries;
    return __crap_plug_global_info_table;
}


#ifdef __cplusplus
extern "C"
#endif
const __crap_tientry_t *crap_rt_get_type_info(int type_info_table_idx)
{
    return __crap_plug_type_info_table[type_info_table_idx];
}


#ifdef __cplusplus
extern "C"
#endif
void crap_rt_print_type_info_table(void)
{
    __crap_print_type_info_table();
}


#ifdef DEBUG
static void where(uintptr_t addr)
{
    int space = -1;
    char str[64];

    if ((addr >= (uintptr_t)to_space->start) &&
        (addr < (uintptr_t)to_space->start + to_space->size))
      space = 0;
    else if ((addr >= (uintptr_t)from_space->start) &&
             (addr < (uintptr_t)from_space->start + from_space->size))
      space = 1;
    else
      D("Address %p is not in any space", addr);

    sprintf(str, "<--(%p)", addr);
      
    D("Space Ranges:");
    D("    To-Space   [%p - %p] %s",
       to_space->start, 
       (uintptr_t)to_space->start + to_space->size,
       space == 0 ? str : "");
    D("    From-Space [%p - %p] %s",
       from_space->start,
       (uintptr_t)from_space->start + from_space->size,
       space == 1 ? str : "");
}
#endif /* DEBUG */


static uintptr_t copy_data_into_to_space(uintptr_t data, size_t size)
{
    uintptr_t new_addr, data_and_hdr;
   
    /* Copy data in from-space into to-space */ 
    new_addr = (uintptr_t)((char *)to_space->start + to_space_offset);
    data_and_hdr = (uintptr_t)DATA_HDR(data);

    memcpy((void *)new_addr, (void *)data_and_hdr, size);
    to_space->brk = (char *)to_space->start + to_space_offset + size;
    to_space_offset += size;
    ++to_space->n_allocs;

    /* Set flag in data header */
    DATA_HDR_SET_HAS_BEEN_COPIED(data);

    D("    Copied %d bytes of data into to-space(%p) at %p", 
      size, to_space, new_addr);

    return (uintptr_t)DATA(new_addr);
}


static void update_from_space_reference(uintptr_t data, uintptr_t new_addr)
{
    *((void **)data) = (void *)new_addr;
    D("    Updating reference in from-space %p to refer to %p",data, new_addr);
}


/* Look at a variable's data starting at the given address and of type ti */
static void inspect_data(
    uintptr_t               data, 
    const __crap_tientry_t *ti,
    _Bool                   is_member,
    _Bool                   is_pointer)
{
    int i, idx;
    uintptr_t var, val, new_addr;
    const __crap_tientry_t *next_ti, *member_ti;

    /* Figure out what data we are to manipulate */
    if (ti->parent_offset)
       var = *(uintptr_t *)data + ti->parent_offset;
    else
      var = data;

    val = *(uintptr_t *)var;

    /* Not allocated/used */
    if (val == 0)
      return;

    D("    Inspecting variable %s (%p) %s %s",
      ti->name,
      data,
      ti->is_pointer ? "[pointer]" : "",
      is_member ? "[member]" : "");

    /* If it is a pointer copy it from from-space to to-space
     * THIS IS THE COLLECTOR ALGORITHM MAGIC:
     * http://en.wikipedia.org/wiki/Cheney's_algorithm
     */
    if (is_pointer)
    {
        /* Check header (from-space) if it has already been copied
         * if not, copy data to-space.
         */
        if (!DATA_HDR_HAS_BEEN_COPIED(val))
        {
            /* Copy */
            new_addr = copy_data_into_to_space(val, DATA_HDR_SIZE(val));

            /* Update from-space object to have the new_addr instead of data */
            **(uintptr_t **)var = new_addr;
        }
        else
          new_addr = **(uintptr_t **)var;

        /* Update from-space to point to to-space instead */
        update_from_space_reference(var, new_addr);
    }

    /* Inspect each member */
    if (ti->n_members)
    {
        member_ti = __crap_plug_type_info_table[ti->type_info_idx];
        for (i=0; i<member_ti->n_members; ++i)
        {
            idx = 1 + i + ti->type_info_idx; 
            next_ti = __crap_plug_type_info_table[idx];
            if (next_ti->is_pointer)
              inspect_data(data, next_ti, true, next_ti->is_pointer);
        }
    }
}


static void sig_handler(int signum)
{
    switch (signum)
    {
        case SIGUSR1:
            __crap_collect();
            break;

        default: 
            break;
    }
}


/* Alloc memory from a space (to-space or from-space).  If the space argument is
 * nospace enumerated value then a space is created of the specified size.
 */
static void *alloc(space_t *space, size_t size, int no_gc)
{
    void *new_data;

    /* No garbage collector (use heap) */
    if (no_gc)
      return malloc(size);

    /* Check space size and bump the brk */ 
    if (((char *)space->brk + size) > ((char *)space->start + space->size))
      E("Not enough room in From-Space");

    new_data = space->brk;
    space->brk = (char *)space->brk + size;
    return new_data;
}


static void *create_space(size_t size)
{
    int fd;
    space_t *space;

    /* Alloc data that is not garbage collected for the space pointer */
    if (!(space = (space_t *)alloc(NULL, sizeof(space_t), 1)))
      E("Not enough system memory to create a data space container");
    memset(space, 0, sizeof(space_t));
    space->alloc = alloc;
    
    /* File descriptor for the memory map */
    if ((fd = open("/dev/zero", O_RDONLY)) < 1)
      E("Could not open /dev/zero for memory to use in memory map");
    
    /* Now create the region for storing data (garbage collected goodies)
     * Adding one byte for a saftey buffer
     */
    if (!(space->start = mmap(NULL, size + 1, PROT_READ|PROT_WRITE|PROT_EXEC,
                              MAP_ANON|MAP_PRIVATE, fd, 0)))
      E("Not enough system memory to create initial data space");

    space->size = size;
    space->brk = space->start;
}


/* Initalize the crap garbage collector.
 * This call is inserted by the plugin crap_plug and we need to avoid mangling
 * it.
 */
#ifdef __cplusplus
extern "C"
#endif
void __crap_init(void)
{
    static int has_initted = false;

    if (has_initted == true)
      return;
    has_initted = true;

    /* Set up the memory spaces */
    to_space = (space_t *)create_space(4096);
    from_space = (space_t *)create_space(4096);

#ifdef DEBUG
    /* Register some signals */
    signal(SIGUSR1, sig_handler);
    signal(SIGUSR2, sig_handler);

    __crap_print_label_layout_table();
    __crap_print_func_info_table();
    __crap_print_type_info_table();
#endif /* DEBUG */
}


/* Allocate data
 * Returns pointer to the beginning of the header to the data.
 * [header][data]
 */
void *__crap_alloc(size_t size)
{
    int i;
    crap_data_hdr_t *head, *hdr;
    size_t size_with_header, offset;

    /* Total allocation size and word align (each member gets a header) */
    size_with_header = size  + sizeof(crap_data_hdr_t);
    if (size_with_header % sizeof(void *))
      size_with_header += sizeof(void *) - (size_with_header % sizeof(void *));

    /* Get the blob of data */
    head = (crap_data_hdr_t *)from_space->alloc(from_space, size_with_header, 0);
    ++from_space->n_allocs;

    /* Set up initial header for the whole allocation */
    head->size = size_with_header;

    return DATA(head);
}


static void debug_spaces(void)
{
    uintptr_t alloc, freed;

    alloc = (uintptr_t)from_space->brk - (uintptr_t)from_space->start;
    freed = from_space->size - alloc;
    D("From-Space (%p) [%p-%p]:",
      from_space,
      from_space->start,
      (char *)from_space->start + from_space->size);
    D("  %d bytes with %d allocations [%d alloc %d free]",
      from_space->size, from_space->n_allocs, alloc, freed);
            
    alloc = (uintptr_t)to_space->brk - (uintptr_t)to_space->start;
    freed = to_space->size - alloc;
    D("To-Space (%p)   [%p-%p]:",
      to_space,
      to_space->start,
      (char *)to_space->start + to_space->size);
    D("  %d bytes with %d allocations [%d alloc %d free]",
      to_space->size, to_space->n_allocs, alloc, freed);
}


/* Initalize data for the collector algorithm */
static void initialize_cheney(void)
{
    to_space_offset = 0;
}
    

static void finalize_cheney(void)
{
    space_t *tmp;

    /* Swap the to-space and from-space pointers */
    tmp = to_space;
    to_space = from_space;
    from_space = tmp;
    to_space->n_allocs = 0;
    to_space->brk = to_space->start;
    
    /* For detecting use of data in the free/should-not-be-used space
     * Call this after swap spaces has occured
     */
#ifdef DEBUG
    memset(to_space->start, 0xA5, to_space->size);
#endif
}


/* Trace a garbage collector root node */
static void process_root(
    uintptr_t               caller_bp, 
    const __crap_vientry_t *vientry,
    _Bool                   is_global) /* TODO: Remove and pass data instead */
{
    int idx;
    uintptr_t data;
    const __crap_tientry_t *tientry;

    if (is_global)
      data = vientry->u.global_addr;
    else
      data = caller_bp + vientry->u.fp_offset;

    /* Get the type info */
    idx = vientry->type_info_idx;

    /* Since this is not a member type it might have the IGNORE_ME
     * index.  If that is the case.  Get the next index (first member).
     */
    if (IGNORE_ME_TYPE_INDEX_P(idx))
      idx = (idx + 1) * -1;

    /* Sanity check */
    if (idx >= __crap_plug_type_info_table_n_entries)
      return;

    tientry = __crap_plug_type_info_table[idx];
    inspect_data(data, tientry, 0, vientry->is_pointer);
}


/* Run the collector */
void __crap_collect(void)
{
    int i, n_vars;
    uintptr_t *call_site_addr, prev_caller_bp, caller_bp, ptr;
    const __crap_vientry_t *vientry;
    const __crap_llentry_t *llentry, *llentry_guess;
    static int has_initted = false;
    register uintptr_t bp1 asm("rbp");

    if (has_initted == false)
      __crap_init();
    has_initted = true;

    D("");
    D("Shoveling the Crap...");
    debug_spaces();

    collector.initialize();

    /* Get the callsite of the function calling this... */
    call_site_addr = (uintptr_t *)(bp1 + sizeof(uintptr_t)); /* rip */

    /* Trace the stack */
    llentry_guess = NULL;
    prev_caller_bp = bp1;
    while ((llentry = crap_rt_get_llentry(*call_site_addr)) || 
           (llentry_guess = crap_rt_get_llentry_guess(*call_site_addr)))
    {
        if (llentry_guess)
          llentry = llentry_guess;

        caller_bp = *(uintptr_t *)prev_caller_bp;

        D("Previous callsite was %p with frame pointer %p in %s %s", 
          *call_site_addr,
          caller_bp,
          llentry->caller_name,
          llentry_guess?"(Guess)":"");

        /* This loop looks at all variables defined in a function (local and
         * formal parameters).  If we have an instance of a struct type. Such
         * as: struct Foo bar = {1, 2.0, "foo"}; Then the "Foo" type in the
         * type-table would have an index of -666 which means its the start of a
         * new type to parse, or to STOP parsing since you just complete the
         * previous type.  So this if case below avoids never processig the type
         * in the first place.  That 'if' statement is the one labeled ***
         */
        for (i=0; i<llentry->func_info->n_var_info; ++i)
        {
            vientry = llentry->func_info->var_info[i];
            D("Processing root variable: %s", vientry->name);
            process_root(caller_bp, vientry, false);
        }
        
        /* Next stack frame */
        call_site_addr += 2 + (STACK_FRAME_SIZE(llentry) / 8);
        prev_caller_bp = caller_bp;
        llentry_guess = NULL;
    }
   
    /* Trace the global variables (maybe do this before we trace the stack?) */ 
    for (i=0; i<__crap_plug_global_info_table_n_entries; ++i)
    {
        vientry = __crap_plug_global_info_table[i];
        D("Processing global root variable: %s", vientry->name);
        process_root(caller_bp, vientry, true);
    }

    collector.finalize();
    D("Done collecting");
    debug_spaces();
}
