#include <stdio.h>
#include <gcc-plugin.h>
#include <basic-block.h>
#include <bitmap.h>
#include <output.h>
#include <simple-object.h>
#include <tree.h>
#include <cgraph.h>
#include "rbmm.h"


/* Local structure used to store packages we import */
struct _rbmm_pkg_d
{
    /* Identifier node */
    const_tree name;

    /* ALl of the functions stored/imported in/from this package */
    VEC(rbmm_fn_data_t,heap) *fns;
};
typedef struct _rbmm_pkg_d *rbmm_pkg_t;


/* Local vector of imported pkgs */
DEF_VEC_P(rbmm_pkg_t);
DEF_VEC_ALLOC_P(rbmm_pkg_t, heap);
VEC(rbmm_pkg_t,heap) *rbmm_imported_pkgs;


/* Returns the file descriptor for the module (close it!)
 * or -1 on error.
 */
static int find_package_file(const char *name)
{
    int fd;
    char *fname;

    RBMM_DEBUG(DEBUG_IMPORT, "Looking for package '%s'...", name);

    /* Couldn't find the module, so load it...Scan for filetypes */
    fname = (char *)xmalloc(strlen(name) + 5);
    snprintf(fname, strlen(name) + 5, "%s.o", name); /* name.o */
    fd = open(fname, O_RDONLY);

    free(fname);
    return fd;
}


/* Parse the region data out from a string
 * 'st' is the start character representing the beginning of a region and its
 * associated data, that character being '['
 * 'st' is advanced to the end of the region data
 *
 * FORMAT: [param_index reg_id type_id aliased_state region_index param_index]
 */
static region_t import_next_region(rbmm_fn_data_t fn, char **st)
{
    char *buf, *en, *s;
    unsigned type_member_idx;
    int id, aliased, region_var_idx, param_idx, p_idx;
    region_t reg;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *n_to_reg;

    /* Fast forward 'st' to the start of the region data */
    while (**st && **st!='[' && **st!='>' && **st!='\n')
      ++*st;

    /* Bail if we have hit the end */
    if (**st == '>' || **st=='\n')
      return NULL;

    /* Skip the leading '[' */
    ++*st;

    /* Get just the region data */
    if (!(en = strchr(*st, ']')))
      return NULL;
    buf = (char *)xcalloc(1, en - *st + 2);
    strncpy(buf, *st, en - *st + 1);
    *st = en;

    /* Get the data about the region */
    p_idx = atoi(strtok_r(buf, " ", &s));
    id = atoi(strtok_r(NULL, " ", &s));
    type_member_idx  = atoi(strtok_r(NULL, " ", &s));
    aliased = atoi(strtok_r(NULL, " ", &s));
    region_var_idx = atoi(strtok_r(NULL, " ", &s));
    param_idx = atoi(strtok_r(NULL, " ", &s));

    /* Create the region and add it to fn */
    reg = new_region_info(fn, id);
    reg->aliased_state = aliased;
    reg->region_var_idx = region_var_idx;
    reg->param_idx = param_idx;

    /* Allocate empty maps */
    if (!fn->returned_parammap)
    {
        fn->returned_parammap = XCNEW(param_to_reg_t);
        fn->returned_parammap->regions = VEC_alloc(ssa_name_to_reg_t, gc, 0);
    }

    if (!fn->returned_parammap)
      fn->parammap = VEC_alloc(param_to_reg_t, gc, 0);

    /* For returned region vars */
    if (p_idx < 0)
      pr = fn->returned_parammap;
    else /* For a formal param (alloc space if we need it) */
    {
        if (!fn->parammap)
        {
            fn->parammap = VEC_alloc(param_to_reg_t, gc, fn->n_orig_params);
            pr = VEC_safe_push(param_to_reg_t, gc, fn->parammap, NULL);
        }
        else
          pr = VEC_index(param_to_reg_t, fn->parammap, p_idx);
    }

    /* Create the entry for the map */
    n_to_reg = VEC_safe_push(ssa_name_to_reg_t, gc, pr->regions, NULL);
    n_to_reg->info = NULL; /* TODO <---- */
    n_to_reg->member_index = type_member_idx;
    n_to_reg->region = reg;
    n_to_reg->use_as_formal_param = (id == -1);

    free(buf);
    return reg;
}


/* Parse function data (type rbmm_fn_data_t) from a string
 * 'st' is the start character representing the begining of a function and its
 * list of regions, that character being '<'
 * FORMAT: <funcname,n_orig_params,[REGION],...>
 */
static rbmm_fn_data_t import_next_fn(char **st)
{
    char *c, *buf, *en;
    region_t reg;
    rbmm_fn_data_t fn;
    struct cgraph_node *node;

    fn = XCNEW(struct _rbmm_fn_data_d);
    
    /* Fast forward 'st' to the start of the region data */
    while (**st && **st!='<')
      ++*st;
    if (!**st)
      return NULL;
    ++*st;
    
    /* Extract just the data for this function */
    if (!(en = strchr(*st, '>')))
      return NULL;
    buf = (char *)xcalloc(1, en - *st + 2);
    strncpy(buf, *st, en - *st + 1);

    /* Get the first two elements... 'name, n_orig_params' */
    fn->func_name = get_identifier(strtok(buf, ","));
    fn->n_orig_params = atoi(strtok(NULL, ","));
    fn->is_imported = true;

    /* Obtain the function decl (based on names and identifiers, this might
     * flake in the case of name mangling
     */
    for (node=cgraph_nodes; node; node=node->next)
    {
        if (fn->func_name == get_identifier(get_name(node->decl)))
        {
            fn->func_decl = node->decl;
            break;
        }
    }

    /* For each region */
    c = *st + strlen(buf) + 1;
    while ((reg = import_next_region(fn, &c)))
      ; /* <-- Iterate */

    /* Advance to next function definition in st */
    *st = en;
    free(buf);
    return fn;
}


/* Given the identifier name for the package, look through all packages we have
 * imported and return that information, or NULL if we do not find anything
 * already loaded.
 */
static rbmm_pkg_t find_pkg_data(const_tree name)
{
    unsigned i;
    rbmm_pkg_t pkg;

    FOR_EACH_VEC_ELT(rbmm_pkg_t, rbmm_imported_pkgs, i, pkg)
      if (pkg->name == name)
        return pkg;

    return NULL;
}


static rbmm_fn_data_t find_fn_data_from_pkg(
    const rbmm_pkg_t  pkg,
    const char       *fn_name)
{
    unsigned i;
    const_tree id;
    rbmm_fn_data_t fn;

    /* Look for given name (probably does not have __rbmm suffix) */
    id = get_identifier(fn_name);
    FOR_EACH_VEC_ELT(rbmm_fn_data_t, pkg->fns, i, fn)
      if (fn->func_name == id)
        return fn;

    return NULL;
}


/* Returns the identifier node for the package name from the decl.  It is of the
 * from: "pkg_name.func_name()"
 */
static tree pkg_name_from_fn_name(const char *fn_name)
{
    char *pkg_name;
    tree node;

    /* Strip off up to the first '.' */
    pkg_name = xstrdup(fn_name);
    if (!strchr(pkg_name, '.'))
      return NULL_TREE;
    *strchr(pkg_name, '.') = '\0';

    node = get_identifier(pkg_name);
    free(pkg_name);

    return node;
}


/* Return the function data from an imported file.  'decl' is the function
 * declaration for the function information we need to retrieve.
 */
rbmm_fn_data_t find_imported_fn_data(const char *fn_name)
{
    tree pkg_name;
    rbmm_pkg_t pkg;

    pkg_name = pkg_name_from_fn_name(fn_name);
    if (!(pkg = find_pkg_data(pkg_name)))
      return NULL;

    return find_fn_data_from_pkg(pkg, fn_name);
}


/* Look at the function declaration and try to figure out which module this decl is from.
 * If we can find the module, try to import data from it.
 * Retuns 'true' on successful import or 'false' otherwise.
 */
bool import_module_used_in_decl(const_tree decl)
{
    int fd, err;
    char *buf, *st;
    off_t xset, len;
    ssize_t bytes;
    const char *errmsg;
    tree name;
    rbmm_pkg_t pkg;
    rbmm_fn_data_t fn;
    simple_object_read *sor;

    if (!(name = pkg_name_from_fn_name(get_name((tree)decl))))
      return false;
        
    /* If we already have it don't reload it */
    if (find_pkg_data(name))
    {
        RBMM_DEBUG(DEBUG_IMPORT, "Located previously loaded module")
        return true;
    }
    
    /* Allocate a package */
    pkg = (rbmm_pkg_t)xcalloc(1, sizeof(struct _rbmm_pkg_d));

    /* Find and set the package file name */
    if ((fd = find_package_file(IDENTIFIER_POINTER(name))) == -1)
      return false;
    pkg->name = name;

    /* Get the data from the module */
    if (!(sor = simple_object_start_read(fd, 0, "__GNU_GO", &errmsg, &err)))
    {
        close(fd);
        return false;
    }

    /* Get the section */
    if (!(simple_object_find_section(
          sor, ".rbmm_export", &xset, &len, &errmsg, &err)))
    {
        close(fd);
        return false;
    }
    simple_object_release_read(sor);

    /* Read in the section */
    buf = (char *)xcalloc(1, len + 1);
    lseek(fd, xset, SEEK_SET);
    if ((bytes = read(fd, buf, len)) == -1)
      abort();

    RBMM_DEBUG(DEBUG_IMPORT,"Read %d bytes from the .go_export section",bytes);
    RBMM_DEBUG(DEBUG_IMPORT,"Data:\n%s", buf);

    /* Parse */
    st = buf;
    while ((fn = import_next_fn(&st)))
      VEC_safe_push(rbmm_fn_data_t, heap, pkg->fns, fn);

    /* Add the package */
    VEC_safe_push(rbmm_pkg_t, heap, rbmm_imported_pkgs, pkg);

    close(fd);
    return true;
}


/* Append to 'str' the region data:
 * [pram_idx reg_id type_idx aliased_state reg_idx param_idx]
 * NOTE: If the reg_id is -1, the data for this region can be ignored.
 */
static char *create_region_param_string(
    const param_to_reg_t *pr, 
    int                   pr_idx) /* neg if return param */
{
    int len, reg_id;
    unsigned i, type_member_idx;
    size_t siz;
    region_t reg;
    ssa_name_to_reg_t *n_to_reg;
    char *str;
    char temp[128] = {0};

    siz = 1024;
    str = (char *)xcalloc(1, siz);
    len = 0;

    /* If we do not need this region for a parameter, just pass an id of -1 */
    FOR_EACH_VEC_ELT(ssa_name_to_reg_t, pr->regions, i, n_to_reg)
    {
        reg = n_to_reg->region;
        type_member_idx = n_to_reg->member_index;
        reg_id = n_to_reg->use_as_formal_param ? -1 : reg->id;
        len += snprintf(temp, 128, "[%d %d %d %d %d %d]",
                        pr_idx, reg_id, 
                        type_member_idx, reg->aliased_state,
                        reg->region_var_idx, reg->param_idx);
        if (len > siz)
        {
            siz += 1024;
            str = (char *)xrealloc(str, siz);
        }
        strncat(str, temp, siz);
    }

    return str;
}


/* Return true if the function reguires at least one region for input parameters
 * of return value.
 */
static bool exports_regions_p(const rbmm_fn_data_t fn)
{
    unsigned i;
    param_to_reg_t *pr;

    /* Check if the function has regions for returned values */
      if (fn->returned_parammap &&
          VEC_length(ssa_name_to_reg_t, fn->returned_parammap->regions) != 0)
        return true;
    
    /* Check if the function has regions for input parameters */
    FOR_EACH_VEC_ELT(param_to_reg_t, fn->parammap, i, pr)
      if (VEC_length(ssa_name_to_reg_t, pr->regions) != 0)
        return true;

    return false;
}


/* Create a demand file whose name is unique */
static FILE *create_empty_demand_file(const char *prefix)
{
    char name[1024] = {0};
    snprintf(name, sizeof(name), ".%s.demand", prefix);
    return fopen(name, "w"); /* TODO: Only modify if necessary */
}


static void emit_demand_file(VEC(rbmm_fn_data_t,heap) *analyized_fns)
{
    tree fnptr;
    FILE *fp;
    unsigned i, j;
    rbmm_fn_data_t fn;

    /* For each function in this file */
    fp = NULL;
    FOR_EACH_VEC_ELT(rbmm_fn_data_t, analyized_fns, i, fn)
      FOR_EACH_VEC_ELT(tree, fn->needed_extern_fnptrs, j, fnptr)
      {
          if (VEC_length(tree, fn->needed_extern_fnptrs) == 0)
            return;

          /* We need a demand entry, create a file if necessary */
          if (!fp)
          {
              RBMM_DEBUG(DEBUG_OBJ,"Creating demand file for %s",
                         main_input_filename);
              if (!(fp = create_empty_demand_file(main_input_filename)))
                abort();
          }

          /* Output the function needed by this input file */
          RBMM_DEBUG(DEBUG_OBJ, "Adding demand for %s", get_name(fnptr));
          fprintf(fp, "%s\n", get_name(fnptr));
      }

    if (fp)
      fclose(fp);
}


/* Emit analysis, per function in this translation unit, for the resulting
 * object file.
 * FORMAT: <func_name, n_orig_params,
 *          [param_idx reg_id type_idx aliased_state reg_idx param_idx] ...>
 * NOTE: If the reg_id is -1, the data for this region can be ignored, and the
 *       import routine can ignore it to by setting the ssa_name_to_reg_d
 *       'use_as_formal_param' flag to 'false'.
 */
void rbmm_emit_analysis(VEC(rbmm_fn_data_t,heap) *analyized_fns)
{
    int i, j;
    rbmm_pkg_t pkg;
    rbmm_fn_data_t fn;
    section *sec;
    param_to_reg_t *pr;
    char *temp, str[8192] = {0};

    /* Register all imported packages with GC data */
    FOR_EACH_VEC_ELT(rbmm_pkg_t, rbmm_imported_pkgs, i, pkg)
      rbmm_gc_import_info(IDENTIFIER_POINTER(pkg->name));

    /* Output demand file */
    emit_demand_file(analyized_fns);

    /* Add analysis data to .rbmm_export section in the resulting .o file */
    sec = get_section(".rbmm_export", SECTION_DEBUG, NULL);
    switch_to_section(sec);
    RBMM_DEBUG(DEBUG_OBJ, "Output data for object file section: .rbmm_export");

    /* For each function we have analyized (This adds analysis data */
    FOR_EACH_VEC_ELT(rbmm_fn_data_t, analyized_fns, i, fn)
    {
        /* Dont dump data for private functions (dont use TREE_PRIVATE) */
        if (!TREE_PUBLIC(fn->func_decl))
          continue;

        /* If the func needs no region arguments, or is higher order,
         * don't emit analysis data.
         */
        if (!exports_regions_p(fn) || fn->used_by_fnptr)
          continue;

        /* Function name and number of original parameters (before regions) */
        memset(str, 0, sizeof(str));
        snprintf(str, sizeof(str), "<%s, %d, ",
                 get_name((tree)fn->func_decl), fn->n_orig_params);

        /* Add in the return regions */
        {
            pr = fn->returned_parammap;
            temp = create_region_param_string(pr, -1);
            if (strlen(temp) + strlen(str) >= sizeof(str))
              abort();
            strncat(str, temp, sizeof(str));
            free(temp);
        }
    
        /* Add in the regions for the formal parameters to fn */
        FOR_EACH_VEC_ELT(param_to_reg_t, fn->parammap, j, pr)
        {
            temp = create_region_param_string(pr, j);
            if (strlen(temp) + strlen(str) >= sizeof(str))
              abort();
            strncat(str, temp, sizeof(str));
            free(temp);
        }

        if (strlen(str) + 1 >= sizeof(str))
          abort();
        strncat(str, ">", sizeof(str));
        assemble_string(str, strlen(str));
        RBMM_DEBUG(DEBUG_OBJ, "\t\t%s\b", str);
    }
}
