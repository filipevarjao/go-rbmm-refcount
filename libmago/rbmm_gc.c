#include <gcc-plugin.h>
#include <coretypes.h>
#include <cgraph.h>
#include <tree.h>
#include <tree-flow.h>
#include <tree-flow-inline.h>
#include <vec.h>
#include "libcrap/crap.h"
#include "rbmm.h"
#include "rbmm_runtime.h"


#ifdef RBMM_GC
/* Vector of all types that belong to regions */
static crap_t *all_gc_context;
#endif
         

static inline void add_unique_type(
    const rbmm_fn_data_t  fn,
    const ssa_name_info_t info,
    region_t               reg)
{
    rbmm_gc_add_unique_type(reg, find_non_void_ptr(fn, info));
}


/* Add unique types to vec of tree nodes representing type per region */
static void add_unique_types(rbmm_fn_data_t fn, region_t reg)
{
    unsigned i;
    ssa_name_info_t info;
    FOR_EACH_VEC_ELT(ssa_name_info_t, reg->names, i, info)
      add_unique_type(fn, info, reg);
}


/* Add unique types to the global region */
static void add_unique_types_global(rbmm_fn_data_t fn)
{
    unsigned i, j;
    region_t reg;
    ssa_name_info_t info;

    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
    {
        if (!ALIASED_BY_GLOBAL_P(reg))
          continue;

        FOR_EACH_VEC_ELT(ssa_name_info_t, reg->names, j, info)
          if (info && info->ssaname)
            add_unique_type(fn, info, global_region);
    }
}


void rbmm_gc_create_type_info_nogc(rbmm_fn_data_t fn)
{
    unsigned i;
    region_t reg;

    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
      add_unique_types(fn, reg);

    /* Also add unique types to the global region */
    add_unique_types_global(fn);
}


/* 
 * EVERYTHING BELOW HERE IS ONLY FOR GC
 */
#ifdef RBMM_GC


/* Returns true of the type has already been added to the list of all types */
static bool type_exists(VEC(tree,gc) *types, const_tree type)
{
    unsigned i;
    tree ti;

    FOR_EACH_VEC_ELT(tree, types, i, ti)
    {
//#ifdef RBMM_GC
//        if (crap_is_same_type_p(ti, type))
//          return true;
//#else
        if (ti == type)
          return true;
//#endif
    }

    return false;
}


/* Interface to add unique types without having to have an ssaname */
void rbmm_gc_add_unique_type(region_t reg, tree type)
{
    if (type_exists(reg->unique_types, type))
      return;

    /* Make sure the gc knows about the type (this will not dup data) */
    crap_new_type_info(all_gc_context, type);

    VEC_safe_push(tree, gc, reg->unique_types, type);

    if (POINTER_TYPE_P(type))
      rbmm_gc_add_unique_type(reg, TREE_TYPE(type));
}


/* Add types to the list of all types if they have not yet been added.
 * Returns a list of type infos so that a superregion can be created with the
 * proper number of subregions.
 */
static void create_type_info(const rbmm_fn_data_t fn, region_t reg)
{
    unsigned i;
    tree type;

    gcc_assert(all_gc_context);

    add_unique_types(fn, reg);

    FOR_EACH_VEC_ELT(tree, reg->unique_types, i, type)
      crap_new_type_info(all_gc_context, type);
}


/* This should go in the rbmm plugin init */
void rbmm_gc_init(void)
{
    all_gc_context = crap_new_context();
    crap_plug_set_context(all_gc_context);

    /* Register what function calls we need register for, since we only gc at
     * allocations, we register thoes calls now.
     */
    crap_get_registers_at_calls_to(
        all_gc_context, get_alloc_from_region_fndecl());

    /* Register what types we want to ignore (peel-off the pointer type) */
    crap_register_type_to_ignore(all_gc_context,TREE_TYPE(__rbmm_region_type));

    /* Do not insert gc lables for the following fndecls */
    crap_register_fn_to_ignore(
        all_gc_context, get_identifier("__rbmm_remove_region"));
    crap_register_fn_to_ignore(
        all_gc_context, get_identifier("__go_runtime_error"));
    crap_register_fn_to_ignore(
        all_gc_context, get_identifier("__go_print_nl"));
    crap_register_fn_to_ignore(
        all_gc_context, get_identifier("__go_print_int64"));
    crap_register_fn_to_ignore(
        all_gc_context, get_identifier("__rbmm_create_region"));
 
    /* Setup the frame size pass.  This is an RTL pass and happens way late once
     * sizes of things have been determined.  This is called after all of the
     * rbmm passes (rbmm analysis and transformation passes).
     */
    crap_plug_init_frame_size_pass();
    crap_plug_init_insert_tables_pass();
}


void rbmm_gc_import_info(const char *package_name)
{
    char *tmp;
    gcc_assert(all_gc_context);
    asprintf(&tmp, "%s.o", package_name);
    crap_import_info(all_gc_context, ggc_strdup(tmp));
    free(tmp);
}


/* Create type information for the regions in this function */
void rbmm_gc_create_type_info(rbmm_fn_data_t fn)
{
    unsigned i;
    region_t reg;
    static bool has_collected_globals;

    if (!has_collected_globals)
    {
        gcc_assert(all_gc_context);
        crap_create_global_type_info(all_gc_context);
        has_collected_globals = true;
    }

    /* For each non-global region */
    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
      create_type_info(fn, reg);

    /* For the global region */
    add_unique_types_global(fn);
}


/* Insert the type information table */
void rbmm_gc_insert_type_info_table(void)
{
    unsigned i;
    type_info_t *ti;

    gcc_assert(all_gc_context);

    /* Update the type info flags */
    FOR_EACH_VEC_ELT(type_info_t, all_gc_context->type_infos, i, ti)
    {
        if (is_slice_type_p(ti->type))
        {
            /* Update slice header and member '__values' */
            ti->flags |= PRESERVE_SLICE; /* header */
            ti = VEC_index(type_info_t, all_gc_context->type_infos, ++i);
            ti->flags |= PRESERVE_SLICE; /* values */
        } 
    }

    crap_insert_type_info_table(all_gc_context);
}


int rbmm_gc_n_type_infos(void)
{
    gcc_assert(all_gc_context);
    return VEC_length(type_info_t, all_gc_context->type_infos);
}


/* Create a object that can be emited into a program/binary...
 * For simplicity just create an array of void *.
 * Each void* element of this array points to a __crap_type_info_t
 */
tree rbmm_gc_create_type_info_array(const region_t reg, gimple stmt)
{
    unsigned i, n_elts;
    const type_info_t *ti;
    tree type, unique, decl;
    VEC(constructor_elt,gc) *entries = NULL;

    gcc_assert(all_gc_context);

    /* Create the type for the array */
    n_elts = VEC_length(tree, reg->unique_types);
    type = build_array_type_nelts(ptr_type_node, n_elts);
    TYPE_READONLY(type) = 1;
    crap_register_type_to_ignore(all_gc_context, type);

    /* Build the constructor of (const void *) */
    FOR_EACH_VEC_ELT(tree, reg->unique_types, i, unique)
    {
        if (!(ti = crap_get_type_info(all_gc_context, unique)))
          continue;

        CONSTRUCTOR_APPEND_ELT(
            entries, 
            build_int_cst(integer_ptr_type_node, i),
            build1(ADDR_EXPR, 
                   build_pointer_type(TREE_TYPE(ti->decl)),
                   (tree)ti->decl));
    }

    /* Create the VAR_DECL for the array and set the constructor and force it to
     * be generated at local scope.
     */
    decl = create_tmp_var(type, "__rbmm_gc_ti");
    DECL_INITIAL(decl) = build_constructor(type, entries);
    TREE_ADDRESSABLE(decl) = 1;
    TREE_READONLY(decl) = 1;

    return build1_loc(gimple_location(stmt), DECL_EXPR, type, decl);
}


static int find_max_type_index_slice(const region_t reg, const_tree slice)
{
    const type_info_t *ti;
    int idx = -1, max = -1;

    while (is_slice_type_p(slice))
    {
        ti = crap_get_type_info(all_gc_context, get_slice_type(slice));
        idx = crap_get_type_info_index(all_gc_context, ti);
        slice = ti->type;
        if (idx > max)
          max = idx;
    }

    return max;
}


int rbmm_gc_find_max_type_index(const region_t reg)
{
    int max, idx;
    unsigned i;
    tree type;
    const type_info_t *ti;
    
    gcc_assert(all_gc_context);

    max = -1;
    FOR_EACH_VEC_ELT(tree, reg->unique_types, i, type)
    {
        if (is_slice_type_p(type))
          idx = find_max_type_index_slice(reg, type);
        else
        {
            ti = crap_get_type_info(all_gc_context, type);
            idx = crap_get_type_info_index(all_gc_context, ti);
        }

        if (idx > max)
          max = idx;
    }

    // TODO: FIXME
    return crap_get_num_type_infos(all_gc_context);

    //return max;
}


const_tree rbmm_gc_find_subregion_type(const_tree type)
{
    const type_info_t *ti;
    gcc_assert(all_gc_context);

    ti = crap_get_type_info(all_gc_context, type); /* Normal case */

    return ti ? ti->id_symbol : NULL_TREE;
}


static void disp_pts_to(const region_t reg, const ssa_name_info_t info)
{
    unsigned i;
    ssa_name_info_t ptsto;

    if (info->marked)
      return;

    FOR_EACH_VEC_ELT(ssa_name_info_t, info->points_to, i, ptsto)
    {
        ptsto->marked = true;
        if (reg->id != info->region_uid)
          printf("Region %d can reach region %d\n", reg->id, ptsto->region_uid);
        disp_pts_to(reg, ptsto);
    }
}


static void display_region_table(const analyized_fn_vec_t fns)
{
    unsigned i, j, k;
    rbmm_fn_data_t fn;
    region_t reg;
    ssa_name_info_t info;


    FOR_EACH_VEC_ELT(rbmm_fn_data_t, fns, i, fn)
    {
        clear_marks(fn);
        FOR_EACH_VEC_ELT(region_t, fn->regions, j, reg)
          FOR_EACH_VEC_ELT(ssa_name_info_t, reg->names, k, info)
            disp_pts_to(reg, info);
        clear_marks(fn);
    }
}


/* All analysis is over.  Time to squirt our GC and type information data
 * into the binary.
 */
void rbmm_gc_finalize(const analyized_fn_vec_t fns)
{
    gcc_assert(all_gc_context);

    /* Insert GC tables */
    crap_insert_labels(all_gc_context);

    /* Insert regions table (which regions have data which point to other
     * regions.  An int table mapping region id to other region id it points to.
     */
    display_region_table(fns);
}


void rbmm_gc_debug(const rbmm_fn_data_t fn)
{
    unsigned i, j;
    tree ti;
    region_t reg;

    RBMM_DEBUG(DEBUG_GC, "GC Information for Function %s:",
               get_name(fn->func_decl));

    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
    {
        RBMM_DEBUG(DEBUG_GC, "  Region %d has %d unique types:",
                   reg->id, VEC_length(tree, reg->unique_types));

        FOR_EACH_VEC_ELT(tree, reg->unique_types, j, ti)
        {
            RBMM_DEBUG(DEBUG_GC, "    Type %d: %s (%p)",
                       j+1, crap_get_type_name(ti), ti);
        }
    }

    RBMM_DEBUG(DEBUG_GC, "GC Information for the Global Region:");
    FOR_EACH_VEC_ELT(tree, global_region->unique_types, i, ti)
    {
        RBMM_DEBUG(DEBUG_GC, "    Type %d: %s (%p)",
                   i+1, crap_get_type_name(ti), ti);
    }
}
#endif /* RBMM_GC */
