#ifndef _CRAP_RUNTIME_H 
#define _CRAP_RUNTIME_H
#include <stdbool.h>


/* Flags set by libcrap, user can add their own after this */
#define CRAP_FLAGS_OBJECT 0 /* Basic object to collect     */ 
#define CRAP_FLAGS_ARRAY  1 /* An array to collect         */
#define CRAP_FLAGS_USER_2 2 /* For the user                */
#define CRAP_FLAGS_USER_3 3 /* For the user                */
#define CRAP_FLAGS_USER_4 4 /* For the user                */
#define CRAP_FLAGS_USER_5 5 /* For the user                */
#define CRAP_FLAGS_USER_6 6 /* For the user                */
#define CRAP_FLAGS_USER_7 7 /* For the user                */


/* Types for easy manipulation/accessing the data generated by the compiler.
 * These should match what the compiler emits in crap_plug.so
 */


/* Information about data types (-1 parent offset means this is the parent)
 * see: crap_plug.c: build_type_info_type()
 */
struct __crap_type_info_d
{
    int parent_offset;
    int size;
    int type_info_idx; /* For members only      */
    int n_members;
    _Bool is_pointer;  /* For members only      */
    char flags;        /* User added            */
    const char *name;  /* Type name:member name */
};
typedef struct __crap_type_info_d __crap_tientry_t;


/* Information about variables for each function.
 * see: crap_plug.c: build_var_info_type()
 */
struct __crap_var_info_d
{
    /* If its a global the frame pointer address is pointless, since all
     * frames can reach the global data.
     */
    union
    {
        int fp_offset;         /* Frame pointer offset */
        uintptr_t global_addr; /* Or a global          */
    } u;
    int type_info_idx;
    _Bool is_pointer;
    const char *name;
};
typedef struct __crap_var_info_d __crap_vientry_t;


/* The caller which calls "alloc" and begins a garbage collection, needs to have
 * its registers scanned.  These registers, per callsite to "alloc", are thoes
 * that should be scanned.
 * see: crap.c: build_reg_info_type()
 */
struct __crap_reg_info_entry_d
{
    unsigned char register_number;
    int type_info_idx;    
};
typedef struct __crap_reg_info_entry_d __crap_rientry_t;


struct __crap_callee_save_info_entry_d
{
    int offset_from_fp;    
    unsigned char register_number;
};
typedef struct __crap_callee_save_info_entry_d __crap_csentry_t;



/* Function information */
struct __crap_func_info_entry_d
{
    const char *name;
    unsigned long stack_frame_size;
    int n_var_info;
    __crap_vientry_t **var_info;
    int n_callee_save_info;
    __crap_csentry_t **callee_save_info;
};
typedef struct __crap_func_info_entry_d __crap_fientry_t;


/* Information about each callsite */
struct __crap_label_layout_entry_d
{
    uintptr_t return_callsite_addr;
    const char *caller_name;
    __crap_fientry_t *func_info;
    int n_registers;
    __crap_rientry_t **reg_info;
};
typedef struct __crap_label_layout_entry_d __crap_llentry_t;


/* Accessors */
#define STACK_FRAME_SIZE(_llentry) ((llentry)->func_info->stack_frame_size)


/* Runtime: Given a callsite (rip) address.  Get the function information. */
#ifdef __cplusplus
extern "C" {
#endif
extern const __crap_llentry_t *crap_rt_get_llentry(uintptr_t return_ptr);
extern const __crap_llentry_t *crap_rt_get_llentry_guess(uintptr_t return_ptr);
extern const __crap_tientry_t *crap_rt_get_type_info(int type_info_table_idx);
extern const __crap_vientry_t **crap_rt_get_globals(int *n_globals);
extern void crap_rt_print_type_info_table(void);

/* Pointers to the global tables:
 * Extern since data is in the binary, the linker resolves this for us.
 */
extern const __crap_tientry_t *__crap_plug_type_info_table[];
extern const __crap_fientry_t *__crap_plug_func_info_table[];
extern const __crap_vientry_t *__crap_plug_global_info_table[];
extern const __crap_llentry_t *__crap_plug_label_layout_table[];
#ifdef __cplusplus
}
#endif


#endif /* _CRAP_RUNTIME_H */
