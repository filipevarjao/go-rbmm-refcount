SHELL=/bin/bash
%.o:
	@echo "Make: $(basename $@).go"
	@$(BUILDOBJ)
	@let x=`grep -c -e "package.*main" $(basename $@).go`; \
            if [ $$x == 1 ]; then \
                $(BUILDEXEC); \
                $(TEST) ./$(basename $@); \
            fi
