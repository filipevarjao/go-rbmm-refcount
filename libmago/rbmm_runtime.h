#ifndef _RBMM_RUNTIME_GCC_PLUGIN_H
#define _RBMM_RUNTIME_GCC_PLUGIN_H
#include "libcrap/crap_runtime.h"

#ifdef RBMM_GC
/* Index for the preserve jump table (only useful for performing GC) */
#define PRESERVE_OBJECT CRAP_FLAGS_OBJECT
#define PRESERVE_ARRAY  CRAP_FLAGS_ARRAY
#define PRESERVE_SLICE  CRAP_FLAGS_USER_2
#define PRESERVE_MAP    CRAP_FLAGS_USER_3
#endif

#endif /* _RBMM_RUNTIME_GCC_PLUGIN_H */
