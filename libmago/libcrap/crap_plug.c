#include <gcc-plugin.h>
#include <cgraph.h>
#include <tree.h>
#include <langhooks.h>
#include <tree-dump.h>
#include <tree-flow.h>
#include <gimple.h>
#include <rtl.h>
#include <emit-rtl.h>
#include <vec.h>
#include "crap.h"
#include "crap_local.h"


int plugin_is_GPL_compatible = 0; /* Hehehe */


/* Set the current function that is being modified */
static tree _old_fndecl;
#define SET_CFUN(_fn)                    \
do                                       \
{                                        \
    push_cfun(_fn);                      \
    _old_fndecl = current_function_decl; \
    current_function_decl = _fn->decl;   \
} while (0)


/* Restore the current function that is being modified */
#define UNSET_CFUN()                     \
do                                       \
{                                        \
    current_function_decl = _old_fndecl; \
    pop_cfun();                          \
} while (0)


/* Initialize global (declared extern by crap_local.h) */
crap_t *all_infos = NULL;


void crap_plug_set_context(crap_t *context)
{
    all_infos = context;
}


void crap_get_registers_at_calls_to(crap_t *context, tree fndecl)
{
    unsigned i;
    tree match;
    
    if (TREE_CODE(fndecl) != FUNCTION_DECL)
      return;

    FOR_EACH_VEC_ELT(tree, context->get_registers_at_calls_to, i, match)
      if (fndecl == match)
        return;

    D("Look for registers at function calls to %s", get_name(fndecl));
    VEC_safe_push(tree, heap, context->get_registers_at_calls_to, fndecl);
}


void crap_register_type_to_ignore(crap_t *context, tree type)
{
    unsigned i;
    tree match;

    FOR_EACH_VEC_ELT(tree, context->types_to_ignore, i, match)
      if (type == match)
        return;

    VEC_safe_push(tree, heap, context->types_to_ignore, type);
}


void crap_register_fn_to_ignore(crap_t *context, tree id)
{
    unsigned i;
    tree match;
    
    FOR_EACH_VEC_ELT(tree, context->ignore_fn_ids, i, match)
      if (id == match)
        return;

    VEC_safe_push(tree, heap, context->ignore_fn_ids, id);
}


/* Returns instruction of the last use in a RTX set */
static rtx find_last_storage(unsigned regno, gimple stmt)
{
    rtx insn, set, reg;
    basic_block bb;

    if (regno > FIRST_PSEUDO_REGISTER)
      return NULL;
    
    bb = gimple_bb(stmt);
    
    if (!bb->preds && !bb->succs)
      bb = BASIC_BLOCK_FOR_FUNCTION(cfun, bb->index);

    if (!bb)
      return NULL;

    /* Go forward until we find a call to stmt */
    FOR_EACH_BB(bb)
    {
        FOR_BB_INSNS(bb, insn)
        {
            if (!CALL_P(insn))
              continue;

            if (RTL_LOCATION(insn) && RTL_LOCATION(insn) == gimple_location(stmt))
            {
                D("Found last storage for reg %s (%d)", reg_names[regno], regno);
                break;
            }
            
            /* This is gross.  Use symbol and identifier to check if the call
             * matches.  If a call occurs twice in a basic block, this will
             * always get the last call.
            else
            {
                call = PATTERN(insn);
                sym = XEXP(call, 0);
                if ((GET_CODE(sym) == SYMBOL_REF) &&
                    (get_identifier(XSTR(sym, 0)) == id))
                {
                    D("Found last storage for reg %s (%d)",
                      reg_names[regno], regno);
                    break;
                }
            }
             */
        }
    }

    /* Starting at the insn before the call... */
    for (insn=PREV_INSN(insn); insn; insn=PREV_INSN(insn))
    {
        set = PATTERN(insn);
        if (!set || (GET_CODE(set) != SET))
          continue;

        reg = XEXP(set, 0); /* (set (op0 op1))... op0 = op1 */
        if (!REG_P(reg))
          continue;

        if (regno == REGNO(reg))
          return insn;
    }

    return NULL;
}


/* Return true if this is a callee saved register.
 * TODO: This is HIGHLY CPU SPECIFIC.  Make a portable version.
 */
static inline bool is_caller_saved_register_p(int regno)
{
    return call_used_regs[regno];
}


/* Generate the register information for this call site */
static void create_reg_info(call_info_t *ci)
{
    unsigned i, j, regno;
    rtx rxp, last;
    var_info_t *vi;
    rtx last_use[FIRST_PSEUDO_REGISTER] = {NULL};

    gcc_assert(all_infos);

    /* Look for any bad offset variables/temps they are probably register
     * alloted instead.
     */
    FOR_EACH_VEC_ELT(var_info_t, ci->caller_func->var_info, i, vi)
    {
        if (vi->u.offset_from_fp != BAD_OFFSET) /* Skip good offsets */
          continue;

        if (!DECL_RTL_SET_P(vi->decl))
          continue;

        rxp = DECL_RTL(vi->decl);
        if (!REG_P(rxp))
          continue;

        regno = REGNO(rxp);
        if (last_use[regno])
          continue;

        if (!(last = find_last_storage(regno, ci->stmt)))
          continue;

        /* Ignore caller-save registers.  We only need callee-save regs. */
        if (is_caller_saved_register_p(regno))
          continue;

        last_use[regno] = last;

        D("Variable %s is assigned to register %s (%d)",
          vi->name, reg_names[regno], regno);
    }

    /* Now add each last use */
    for (i=0; i<FIRST_PSEUDO_REGISTER; ++i)
    {
        if (!last_use[i])
          continue;

        /* Find the decl for this register: This just copies the BAD_OFFSET
         * instance but gets an offset.
         * TODO: Do not copy but just move the vi to the ri list.
         */
        FOR_EACH_VEC_ELT(var_info_t, ci->caller_func->var_info, j, vi)
          if (vi->u.offset_from_fp==BAD_OFFSET && i==REGNO(DECL_RTL(vi->decl)))
          {
              create_var_info(
                  vi->decl, (vec_var_info_t *)&ci->register_info,true);
              break;
          }
    }
}


/* At the callsite, scan up to function PROLOGUE and see if the function stores
 * any callee saves registers (look for 'push' <callee save>).
 * If we find them, then this function uses a callee save register.
 * 1) Find the offset the callee save register is pushed
 * XXX: x86 i386.md specific
 */
static void create_callee_save_info(func_info_t *fi)
{
    rtx insn, set, reg, loc;
    int offset;
    callee_save_info_t *cs;

    /* Scan starting in function prologue and stopping in prologue end */
    offset = 0;
    for (insn=get_insns(); insn; insn=NEXT_INSN(insn))
    {
        if (NOTE_P(insn) && NOTE_KIND(insn) == NOTE_INSN_PROLOGUE_END)
          break;

        set = PATTERN(insn);
        if (!set || (GET_CODE(set) != SET))
          continue;

        loc = XEXP(set, 0); /* Mem */
        if (!MEM_P(loc))
          continue;
        loc = XEXP(loc, 0); /* Decrement */
        loc = XEXP(loc, 0); /* Location  */
        reg = XEXP(set, 1); /* Register  */

        if (!REG_P(reg) || !REG_P(loc))
          continue;
       
        /* Should be sp for a push */ 
        if (REGNO(loc) != SP_REG)
          continue;
        
        offset -= GET_MODE_SIZE(GET_MODE(reg));

        if (is_caller_saved_register_p(REGNO(reg))) /* Need callee save */
          continue;

        /* Add it */
        cs = VEC_safe_push(callee_save_info_t,heap,fi->callee_save_info,NULL);
        cs->offset_from_fp = offset;
        cs->gcc_register_number = REGNO(reg);

        D("Found push on callee-save regno %d in %s at offset %d",
          REGNO(reg), get_name(current_function_decl), offset);
    }
}


/* For the call specified by 'ci' look at both the parameters and the local
 * arguments and generate type information about them.
 */
static void get_function_params_locals_regs_info(
    crap_t      *ctx,
    call_info_t *ci)
{
    unsigned i, vec_idx;
    tree decl;
    const VEC(tree,heap) *vecs[] = {ci->caller_func->formal_param_decls,
                                    ci->caller_func->local_decls};

    gcc_assert(all_infos);

    /* Get the register data for the callsite */
    FOR_EACH_VEC_ELT(tree, ctx->get_registers_at_calls_to, i, decl)
      if (decl == gimple_call_fndecl(ci->stmt))
      {
          create_reg_info(ci);
          break;
      }

    /* Check to see if we have already added data for this */
    if (ci->caller_func->var_info)
      return;

    /* Get locals and params */
    ci->caller_func->var_info = new_vec_var_info_n(
       VEC_length(tree, ci->caller_func->local_decls) + 
       VEC_length(tree, ci->caller_func->formal_param_decls));

    /* First check the function's formal params */
    D("Inspecting types used in %s", ci->caller_func->name);
    for (vec_idx=0; vec_idx<2; ++vec_idx)
      FOR_EACH_VEC_ELT(tree, vecs[vec_idx], i, decl)
        create_var_info(decl, &ci->caller_func->var_info, false);
}


/* Other programs which use this library might set the frame size after the
 * func_info_entry tree node has been created.
 */
static void set_frame_size(func_info_t *fi, unsigned long frame_size)
{
    constructor_elt *frame_size_elt;
    double_int val;

    if (fi->func_info_entry == NULL_TREE)
      return;

    /* The gcc entry has been created, update it */
    frame_size_elt = CONSTRUCTOR_ELT(fi->func_info_entry,STACK_FRAME_SIZE_IDX);
    val = shwi_to_double_int(frame_size);
    TREE_INT_CST_LOW(frame_size_elt->value) = val.low;
    TREE_INT_CST_HIGH(frame_size_elt->value) = val.high;
}


/* Insert a label for a call to 'ci' in this function */
tree crap_create_callsite_label(
    call_info_t *ci,
    const_tree   the_call_fndecl,
    rtx         *insn_p)
{
    static int idpool;
    tree sym_fndecl, lbl_decl;
    rtx lbl, sym, call, insn = *insn_p;
    
    D("Found call to function %s in function %s at line %d",
      gimple_call_fndecl(ci->stmt) ?
          get_name(gimple_call_fndecl(ci->stmt)) :
          "(function pointer)",
      ci->caller_func->name,
      gimple_lineno(ci->stmt));

    if (!insn)
      insn = get_insns();

    /* Find this instance of the call (starting at rxp) */
    for ( ; insn; insn=NEXT_INSN(insn))
    {
        *insn_p = insn;
        if (!CALL_P(insn))
          continue;

        /* (insn(call(mem(sym_ref */
        call = PATTERN(insn);
        if (GET_CODE(call) == SET)
          call = XEXP(call, 1);
        sym = XEXP(call, 0); /* Mem */
        sym = XEXP(sym, 0);
        if (GET_CODE(sym) != SYMBOL_REF)
          continue;

        sym_fndecl = SYMBOL_REF_DECL(sym);
        if (sym_fndecl != the_call_fndecl)
          continue;

        /* Found the call: Create and insert label */
        lbl = gen_label_rtx();
        LABEL_PRESERVE_P(lbl) = 1;
        emit_label_after(lbl, insn);
        insn = NEXT_INSN(insn);
        *insn_p = insn;

        lbl_decl= create_artificial_label(UNKNOWN_LOCATION);
        DECL_NAME(lbl_decl) = get_identifier(uniq_name("label_", idpool++));
        DECL_EXTERNAL(lbl_decl) = 1;
        
        SET_DECL_RTL(lbl_decl, lbl);
        return build1(ADDR_EXPR, ptr_type_node, lbl_decl);
    }
        
    return NULL_TREE;
}


static bool crap_plug_frame_size_gate(void)
{
    return true;
}


/* This is called once for each function.
 * Obtain the stack frame size and update all 
 * label layout entries with this value.
 */
static unsigned crap_plug_frame_size_pass(void)
{
    unsigned i;
    call_info_t *ci;
    func_info_t *fi;
    rtx lastrtx;
    tree call_fndecl;
    unsigned long frame_size = get_frame_size();
    
    gcc_assert(all_infos);

    /* Since this is an RTL pass insert a label (at RTL time)... which is way
     * late and we want to avoid GCC moving our label.
     * Backup directory has a version that puts this NOT in frame_size
     * FIXME: resurrect the backup directory version and get it to work
     */
    lastrtx = NULL;
    FOR_EACH_VEC_ELT(call_info_t, all_infos->call_infos, i, ci)
    {
        if (ci->caller_func->func != cfun)
          continue;

        /* We are in the caller and 'ci' is a call with in the caller */
        call_fndecl = gimple_call_fndecl(ci->stmt);

        /* Create the label and update the info */
        ci->return_callsite_addr = 
           crap_create_callsite_label(ci, call_fndecl, &lastrtx);
    }

    /* Add in additional information only available once the prologue has been
     * generated:
     * Locate the function's callee save stack data
     */
    FOR_EACH_VEC_ELT(func_info_t, all_infos->func_infos, i, fi)
      if (fi->func == cfun)
      {
          create_callee_save_info(fi);
          break;
      }

    /* Locate all caller frames with same fndecl */
    FOR_EACH_VEC_ELT(call_info_t, all_infos->call_infos, i, ci)
      if (ci->caller_func->func == cfun)
      {
          SET_CFUN(ci->caller_func->func);
          ci->caller_func->stack_frame_size = frame_size;
          set_frame_size(ci->caller_func, frame_size);
          D("Setting frame size of %lu bytes for %s",
            frame_size, get_name(cfun->decl));
          UNSET_CFUN();
      }

    /* Now get the offset-from-frame-pointer, specified in the RTL for vars */
    FOR_EACH_VEC_ELT(call_info_t, all_infos->call_infos, i, ci)
      if (ci->caller_func->func == cfun)
      {
          SET_CFUN(ci->caller_func->func);
          get_function_params_locals_regs_info(all_infos, ci);
          UNSET_CFUN();
      }

    return 0;
}


static bool crap_plug_insert_tables_gate(void)
{
    const struct function *func;
    const struct cgraph_node *node;
    static int n_seen, n_functions;

    /* If we have not initialized this value... do so now */
    if (n_functions == 0)
      for (node=cgraph_nodes; node; node=node->next)
        if ((func = DECL_STRUCT_FUNCTION(node->decl)) && func->cfg)
          ++n_functions;

    return (++n_seen == n_functions);
}


/* Returns false we cannot find a 'main' in the callgraph.
 * Using 'main_identifier_node' in gcc/tree.h does not seem to work
 * at the time this routine is called.  This is language specific, for 'go' look
 * for "main.main" since main is not in the call graph
 */
bool crap_this_translation_unit_is_a_library(void)
{
    const struct cgraph_node *node;

    if (main_identifier_node)
      return false;
    else if (get_identifier(lang_hooks.name) == get_identifier("GNU Go"))
    {
        /* For Go programs */
        for (node=cgraph_nodes; node; node=node->next)
          if(get_identifier(get_name(node->decl))==get_identifier("main.main"))
            return false;
    }

    return true; /* Didn't see "main" so this must be a library */
}


/* This is called once for each function.  Ultimately, we only need to call this
 * once after all of the 'crap_plug_frame_size_pass' callbacks have been called.
 * Once crap_plug_frame_size_pass() has been called 'n_functions' time, we can
 * emit the label layout table and its entries.
 */
unsigned crap_plug_insert_tables_pass(void)
{
    gcc_assert(all_infos);

    /* If an obj file and not main/application emit type info to elf section */
    if (crap_this_translation_unit_is_a_library())
      crap_export_info(all_infos);
    else
    {
        crap_insert_type_info_table(all_infos);
        crap_create_function_info_table(all_infos);
        crap_create_global_info_table(all_infos);
        crap_create_label_layout_table(all_infos);
    }

    return 0;
}


static void insert_init_runtime_call(void)
{
    tree fndecl;
    gimple fncall;
    gimple_stmt_iterator gsi;

    /* Build the fndecl and call to the function */
    fndecl =
        build_fn_decl("__crap_init",
        build_function_type_list(void_type_node, NULL_TREE));
    fncall = gimple_build_call(fndecl, 0);

    /* Insert the function call */
    gsi = gsi_start_bb(BASIC_BLOCK(2));
    gsi_insert_before(&gsi, fncall, GSI_NEW_STMT);
}


/* Returns 'true' if the function is in list of function decls that we are to
 * ignore inserting callsite information for.
 */
static bool ignore_fn(const crap_t *ctx, const_tree fndecl)
{
    unsigned i;
    tree ignoreme;

    FOR_EACH_VEC_ELT(tree, ctx->ignore_fn_ids, i, ignoreme)
      if (fndecl == ignoreme)
        return true;

    return false;
}


/* Insert lablels at all fncall sites for use in identifying call sites */
void crap_plug_insert_label_pass(void *gcc_data, void *user_data)
{
    struct function *func;
    struct cgraph_node *node;
    crap_t *ctx;
    basic_block bb;
    gimple_stmt_iterator gsi;

    ctx = (crap_t *)user_data;
    gcc_assert(ctx);

    /* Look at all statements in the program (function by function)
     * Then, as we find all CALL statements, add their data to a vector, which
     * we will then later process.
     */
    for (node=cgraph_nodes; node; node=node->next)
    {
        if (!(func = DECL_STRUCT_FUNCTION(node->decl)) || !func->cfg)
          continue;

        D("Processing function: %s", get_name(func->decl));

        /* Set cfun (global current function being analyized) */
        SET_CFUN(func);

        /* For each basic block, look at each statement */
        FOR_EACH_BB(bb)
        for (gsi=gsi_start_bb(bb); !gsi_end_p(gsi); gsi_next(&gsi))
          if (gimple_code(gsi_stmt(gsi)) == GIMPLE_CALL)
          {
              /* Ignore certain user-requested functions */
              if (ignore_fn(ctx, gimple_call_fndecl(gsi_stmt(gsi))))
                continue;
              crap_new_call_info(ctx, func, gsi_stmt(gsi));
          }

        /* If this is main add an initalizer to the runtime.  Also, this should
         * occur AFTER we have located all function calls main makes.
         */
        if (get_identifier(get_name(func->decl)) == main_identifier_node)
          insert_init_runtime_call();
       
        /* Restore the cfun */
        UNSET_CFUN();
    }
}


/* Find all global variables in the program */
void crap_plug_collect_globals_pass(void *gcc_data, void *user_data)
{
    crap_t *ctx;
    struct varpool_node *node;

    ctx = (crap_t *)user_data;
    gcc_assert(ctx);

    FOR_EACH_STATIC_VARIABLE(node)
      if (!DECL_ARTIFICIAL(node->decl)) /* Ignore compiler inserted globals */
        VEC_safe_push(tree, heap, ctx->global_vars, node->decl);
}


/* Public interface */
void crap_plug_init_frame_size_pass(void)
{
    static struct rtl_opt_pass get_frame_size_pass;
    static struct register_pass_info frame_size_pass_info;

    get_frame_size_pass.pass.type = RTL_PASS;
    get_frame_size_pass.pass.name = TAG"_frame_size";
    get_frame_size_pass.pass.gate = crap_plug_frame_size_gate;
    get_frame_size_pass.pass.execute = crap_plug_frame_size_pass;
    get_frame_size_pass.pass.todo_flags_finish = TODO_dump_func;
    
    frame_size_pass_info.pass = &get_frame_size_pass.pass;
    frame_size_pass_info.reference_pass_name = "pro_and_epilogue";
    frame_size_pass_info.ref_pass_instance_number = 0;
    frame_size_pass_info.pos_op = PASS_POS_INSERT_AFTER;
    
    register_callback(TAG,PLUGIN_PASS_MANAGER_SETUP,NULL,&frame_size_pass_info);
}


/* Public interface */
void crap_plug_init_insert_tables_pass(void)
{
    static struct rtl_opt_pass insert_tables_pass;
    static struct register_pass_info table_pass_info;

    insert_tables_pass.pass.type = RTL_PASS;
    insert_tables_pass.pass.name = TAG"_insert_tables";
    insert_tables_pass.pass.gate = crap_plug_insert_tables_gate;
    insert_tables_pass.pass.execute = crap_plug_insert_tables_pass;
    insert_tables_pass.pass.todo_flags_finish = TODO_dump_func;

    table_pass_info.pass = &insert_tables_pass.pass;
    table_pass_info.reference_pass_name = TAG"_frame_size";
    table_pass_info.ref_pass_instance_number = 0;
    table_pass_info.pos_op = PASS_POS_INSERT_AFTER;
    
    register_callback(TAG, PLUGIN_PASS_MANAGER_SETUP, NULL, &table_pass_info);
}


/* Public interface */
void crap_plug_init_collect_globals_pass(void)
{
    register_callback(
        TAG,PLUGIN_ALL_IPA_PASSES_END,crap_plug_collect_globals_pass,all_infos);
}


int plugin_init(struct plugin_name_args *info, struct plugin_gcc_version *version)
{
    all_infos = crap_new_context();

    /* Pass1: info for RTL pass which we use to gather stack frame sizes 
     *        info telling gcc when to callback for the stack frame size pass 
     */
    crap_plug_init_frame_size_pass();

    /* Pass2: info for RTL pass which we insert the lable layout table
     *        info telling gcc when to callback for the lable layout insert
     */
    crap_plug_init_insert_tables_pass();

    /* Get called back after the IPA passes end so we can find all callsites
     * The data gathered from this pass will be used to build the label layout
     * table which occurs very late, after stack frame data is available.
     */
    register_callback(
        TAG, PLUGIN_ALL_IPA_PASSES_END, crap_plug_insert_label_pass,all_infos);

    crap_plug_init_collect_globals_pass();

    return 0;
}
