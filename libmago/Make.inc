TOPDIR=/usr/local
TOPLIBDIR=$(TOPDIR)/lib64
GOTOPDIR=/usr/local/libexec/gcc/x86_64-unknown-linux-gnu/4.6.4
GCCSRCDIR=/usr/src/gcc/
GO_RUNTIME_DIR=$(GCCSRCDIR)/libgo/runtime/
WORKINGDIR=/usr/libmago/

GCC = $(TOPDIR)/bin/gcc
GXX = $(TOPDIR)/bin/g++
GCCGO = $(TOPDIR)/bin/gccgo
GOCC = $(GOTOPDIR)/go1
EXTRALIBS = -L$(TOPDIR)/lib64/ -lgo
EKAM = $(WORKINGDIR)/ekam.py
GDB = gdb
OPTIMIZERS = -O0 -g3
OTHEROBJS = $(WORKINGDIR)/rbmm_builtins.o \
            $(WORKINGDIR)/libcrap/crap_runtime.o

CC = $(GXX)
PLUGIN_OBJ_NAME = rbmm.so
PLUGIN_SOURCE_FILES = rbmm.c rbmm_region.c rbmm_import_export.c rbmm_gc.c
PLUGIN_OBJECT_FILES = rbmm.o rbmm_region.o rbmm_import_export.o rbmm_gc.o
GCCPLUGINS_DIR = $(shell $(GCC) -print-file-name=plugin)
CFLAGS += -g3 -O0 -fPIC -Wunused $(EXTRA_CFLAGS) $(TARGET_SPECIFIC) \
          -I$(GCCPLUGINS_DIR)/include -I$(GCCSRCDIR)include

BUILDOBJ = LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):$(TOPDIR)/lib64/ \
           $(GCCGO) -c $(basename $@).go \
           -fplugin=$(WORKINGDIR)/$(PLUGIN_OBJ_NAME) $(OPTIMIZERS)

BUILDEXEC = LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):$(TOPDIR)/lib64/ \
            $(GCCGO) $(basename $@).go -o $(basename $@) \
            -fplugin=$(WORKINGDIR)/$(PLUGIN_OBJ_NAME) \
            $(OTHEROBJS) $(OPTIMIZERS) $(RBMM_ARGS)

# Like BUILDOBJ but pass the filenames and gcc args (-c or -o) yourself
BUILDPLAIN = LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):$(TOPDIR)/lib64/ \
             $(GCCGO) -fplugin=$(WORKINGDIR)/$(PLUGIN_OBJ_NAME) \
             $(OTHEROBJS) $(OPTIMIZERS) $(RBMM_ARGS) \


# Like BUILDOBJ but pass the filenames and gcc args (-c or -o) yourself
BUILDOBJPLAIN = LD_LIBRARY_PATH=$(LD_LIBRARY_PATH):$(TOPDIR)/lib64/ \
                $(GCCGO) -fplugin=$(WORKINGDIR)/$(PLUGIN_OBJ_NAME) \
                $(OPTIMIZERS) $(RBMM_ARGS)

# Pass the filenames and extra gcc arguments (-c or -o) yourself
DEBUGGER = \
  LD_LIBRARY_PATH=$(GOTOPDIR)/x86_64-unknown-linux-gnu/libgo/.libs/:$(LD_LIBRARY_PATH):$(TOPDIR)/lib64/ \
  $(GDB) --args $(GOCC) \
  -fplugin=$(WORKINGDIR)/$(PLUGIN_OBJ_NAME) $(OPTIMIZERS) $(RBMM_ARGS) \
  -L$(TOPDIR)/lib64/

TEST = $(WORKINGDIR)/test_tools/exectest.py






