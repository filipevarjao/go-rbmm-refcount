#!/usr/bin/env python

import os, subprocess, sys

def P(message):
    print("[ekam] " + message)
    sys.stdout.flush()


# One Demand class instance per translation unit.  Data in the demand file for
# this translation unit is represented in this class.
class Demand(object):
    def __init__(self, fname):
        self.fname = fname
        f = open(fname)
        self.needed_fns = f.readlines()
        self.needed_fns = [x.strip() for x in self.needed_fns]
        f.close()
        P('Found demand file for ' + fname + \
          ': (' + ','.join(self.needed_fns) + ')')

    def __str__(self):
        return self.fname + ': ' + \
               ' '.join([str(fn) + '' for fn in self.needed_fns])


# Given a list of Demand instances generate a string to pass to gcc
def gen_demand_args(demands):
    return 'RBMM_ARGS="-fplugin-arg-rbmm-hospecial=' + \
           ','.join([','.join(x.needed_fns) for x in demands]) + '"'

def is_demand_file(name):
    pieces = name.split(".")
    return pieces[0] == '' and pieces[-1] == 'demand'

def get_demands():
    demand_files =  [x for x in os.listdir('.') if is_demand_file(x)]
    return [Demand(x) for x in demand_files]

# Build demands and run make
def run_make(demands):
    cmd = ["make"]
    cmd.extend(sys.argv[1:])
    cmd.append(gen_demand_args(demands))
    P("Executing GCC with the following arguments: " + ' '.join(cmd))
    subprocess.call(cmd)

def touch(fname):
    P("Touching " + fname)
    os.utime(fname, None)

# Check previous and current demands and figure out which files have a demand on
# them, touch so they will be updated next compile phase with 'make'
# TODO: Touch specific files and not every damn one
def need_to_update(prev, curr):
    prev_names = [x.fname for x in prev]
    curr_names = [x.fname for x in curr]
#   P("prev_names: " + str(prev_names))
#   P("currv_names: " + str(curr_names))
    touchus = set(curr_names) - set(prev_names) # In current and not in prev
    if len(touchus):
        P("Rebuilding to incorporate demands")
        fs = [f for f in os.listdir('.') if f.split('.')[-1]=='go']
        print(fs)
        [touch(f) for f in fs]
        return True
    else:
        return False

def test():
    P("Testing...")
    d1 = Demand("test_demands1.txt")
    d2 = Demand("test_demands2.txt")
    s1 = set(d1.needed_fns)
    s2 = set(d2.needed_fns)
    diff = s2 - s1
    print("Difference:")
    [print(str(d)) for d in diff]

# Call make twice and use demand files if available
if __name__ == "__main__":
    if sys.argv[-1] == "TEST":
        test()
    else:
        before = get_demands()
        run_make(before)
        after = get_demands()
        if need_to_update(before, after):
            run_make(after)
