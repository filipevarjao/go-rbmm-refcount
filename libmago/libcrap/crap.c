#include <gcc-plugin.h>
#include <cgraph.h>
#include <tree.h>
#include <tree-dump.h>
#include <tree-flow.h>
#include <gimple.h>
#include <rtl.h>
#include <emit-rtl.h>
#include <vec.h>
#include "crap.h"
#include "crap_local.h"
#include "crap_runtime.h"


/* For creating the identifier names that should be in the final binary.  
 * These have to be distinct per module to avoid duplicate name errors at link
 * time.
 */
const char *uniq_name(const char *name, int id)
{
    char *str;
    const char *tmp;
    asprintf(&str, "__"TAG"_%s%d", name, id);
    tmp = ggc_strdup(str);
    free(str);
    return tmp;
}


static tree create_global_var(tree type, const char *name)
{
    tree decl;

    decl = build_decl(
        BUILTINS_LOCATION, VAR_DECL, get_identifier(name), type);
    DECL_ARTIFICIAL(decl) = 1;
    DECL_EXTERNAL(decl) = 0;
    DECL_PRESERVE_P(decl) = 1;
    TREE_PUBLIC(decl) = 1;
    TREE_STATIC(decl) = 1;
    TREE_USED(decl) = 1;

    return decl;
}


/* For building data types.  
 * This macro is for the first field.  Additional fields
 * should be specified by BUILD_ADD_FIELD, and the type is completed using the
 * BUILD_TYPE_DONE macro.
 */
#define BUILD_FIRST_FIELD(_field, _prev_field, _name, _type)          \
    _field = build_decl(                                              \
        BUILTINS_LOCATION, FIELD_DECL, get_identifier(_name), _type); \
    DECL_CHAIN(_field) = NULL_TREE;                                   \
    _prev_field = _field;


/* Add another field to the data type being created */
#define BUILD_ADD_FIELD(_field, _prev_field, _name, _type)             \
     _field = build_decl(                                              \
         BUILTINS_LOCATION, FIELD_DECL, get_identifier(_name), _type); \
     DECL_CHAIN(_field) = _prev_field;                                 \
     _prev_field = _field;


/* Generate the gcc representation of the data type */
#define BUILD_TYPE_DONE(_node, _name, _field)  \
    _node = make_node(RECORD_TYPE);            \
    finish_builtin_struct(_node, _name, _field, NULL_TREE);


/* Build type information data type.
 *
 * struct type_info_t
 * {
 *     int offset_from_parent;
 *     int type_info_table_index; // Fields of struct which might be other type
 *     int n_members; // Number of members
 *     bool is_pointer;
 *     char *name; // Debugging
 * };
 */
static tree build_type_info_type(void)
{
    tree field, prev_field;
    static tree type_info_type;

    if (type_info_type)
      return type_info_type;

    BUILD_FIRST_FIELD(
        field, prev_field, "__parent_offset", integer_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__size", integer_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__type_info_idx", integer_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__n_members", integer_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__is_pointer", boolean_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__flags", char_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__name", ptr_type_node);
    BUILD_TYPE_DONE(type_info_type, "__type_info_t", field);

    return type_info_type;
}


/* Build var_info type.  Each function has an array of these which tell
 * about the data (local vars and formal params) used in this function.
 *
 * stuct __var_info_t
 * {
 *     int __fp_offset; // Offset from frame pointer
 *     int __type_info_table_index; // Info about the type (and members)
 *     bool __is_pointer;
 *     char * __name; // Debugging
 */
static tree build_local_var_info_type(void)
{
    tree field, prev_field;
    static tree var_info_entry_type;

    if (var_info_entry_type)
      return var_info_entry_type;

    BUILD_FIRST_FIELD(field, prev_field, "__fp_offset", integer_ptr_type_node);
    BUILD_ADD_FIELD(field,prev_field,"__type_info_idx", integer_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__is_pointer", boolean_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__name", ptr_type_node);
    BUILD_TYPE_DONE(var_info_entry_type, "__var_info_t", field);

    return var_info_entry_type;
}


/* An array of pointers */
static tree build_local_var_info_table_type(int n_elts)
{
    return build_array_type_nelts(
        build_pointer_type(build_local_var_info_type()), n_elts);
}


/* Build reg_info type.  Each callsite has a list of registers and the type of
 * data stored in their registers.
 *
 * struct __reg_info_t
 * {
 *     unsigned char register_number;
 *     int type_info_table_index;
 * };
 */
static tree build_reg_info_type(void)
{
    tree field, prev_field;
    static tree reg_info_entry_type;

    if (reg_info_entry_type)
      return reg_info_entry_type;

    BUILD_FIRST_FIELD(field, prev_field, "__reg_num", unsigned_char_type_node);
    BUILD_ADD_FIELD(field,prev_field,"__type_info_idx", integer_type_node);
    BUILD_TYPE_DONE(reg_info_entry_type, "__reg_info_t", field);

    return reg_info_entry_type;
}


/* Build reg_info type.  Each callsite has a list of registers and the type of
 * data stored in their registers.
 *
 * struct __reg_info_t
 * {
 *     int offset_from_fp;
 *     unsigned char register_number;
 * };
 */
static tree build_callee_save_info_type(void)
{
    tree field, prev_field;
    static tree callee_save_info_entry_type;

    if (callee_save_info_entry_type)
      return callee_save_info_entry_type;

    BUILD_FIRST_FIELD(field, prev_field, "__offset_fp", integer_type_node);
    BUILD_ADD_FIELD(field, prev_field,"__reg_num", unsigned_char_type_node);
    BUILD_TYPE_DONE(callee_save_info_entry_type, "__callee_save_info_t", field);

    return callee_save_info_entry_type;
}


static tree build_callee_save_info_table_type(int n_elts)
{
    static tree callee_save_info_table_type;
    if (!callee_save_info_table_type)
    {
        callee_save_info_table_type = 
            build_array_type_nelts(build_pointer_type(
                                   build_callee_save_info_type()), n_elts);
    }
    return callee_save_info_table_type;
}


/* An array of pointers to register infos */
static tree build_reg_info_table_type(int n_elts)
{
    return build_array_type_nelts(
        build_pointer_type(build_reg_info_type()), n_elts);
}


static tree build_global_var_info_type(void)
{
    tree field, prev_field;
    static tree var_info_entry_type;

    if (var_info_entry_type)
      return var_info_entry_type;

    BUILD_FIRST_FIELD(field, prev_field, "__global_addr", ptr_type_node);
    BUILD_ADD_FIELD(field,prev_field,"__type_info_idx", integer_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__is_pointer", boolean_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__name", ptr_type_node);
    BUILD_TYPE_DONE(var_info_entry_type, "__var_info_t", field);

    return var_info_entry_type;
}


/* An array of pointers */
static tree build_global_var_info_table_type(int n_elts)
{
    return build_array_type_nelts(
        build_pointer_type(build_global_var_info_type()), n_elts);
}


/* Return the register number used by the runtime:
 * Currently the runtime uses getcontext()
 * See gcc registers in:     gcc/config/i386/i386.md
 * See runtime registers in: /usr/include/sys/ucontext.h
 */
static int get_reg_num_for_runtime(int regno)
{
    /* Avoid using designated initilizers since g++ might compile this */
    static bool has_init;
    static int gcc_reg_to_ucontext[FIRST_PSEUDO_REGISTER];

    if (!has_init)
    {
        gcc_reg_to_ucontext[AX_REG] = REG_RAX;
        gcc_reg_to_ucontext[DX_REG] = REG_RDX;
        gcc_reg_to_ucontext[CX_REG] = REG_RCX;
        gcc_reg_to_ucontext[BX_REG] = REG_RBX;
        gcc_reg_to_ucontext[SI_REG] = REG_RSI;
        gcc_reg_to_ucontext[DI_REG] = REG_RDI;
        gcc_reg_to_ucontext[R8_REG] = REG_R8;
        gcc_reg_to_ucontext[R9_REG] = REG_R9;
        gcc_reg_to_ucontext[R10_REG] = REG_R10;
        gcc_reg_to_ucontext[R11_REG] = REG_R11;
        gcc_reg_to_ucontext[R12_REG] = REG_R12;
        gcc_reg_to_ucontext[R13_REG] = REG_R13;
        has_init = true;
    }

    D("GCC Register %d(%s) maps to ucontext register %d %s",
      regno, reg_names[regno], gcc_reg_to_ucontext[regno], "");

    return gcc_reg_to_ucontext[regno];
}


/* Create an entry for the reg_info_table */
static tree create_reg_info_entry(const reg_info_t *ri)
{
    int regno;
    tree field, type;
    VEC(constructor_elt,gc) *entry = NULL;

    type = build_reg_info_type();
    field = TYPE_FIELDS(type);

    /* Register number */
    regno = get_reg_num_for_runtime(REGNO(DECL_RTL(ri->decl)));
    CONSTRUCTOR_APPEND_ELT(
        entry,
        field,
        build_int_cst(unsigned_char_type_node, regno));
    field = DECL_CHAIN(field);

    /* Type info table index (type that is in the register) */
    CONSTRUCTOR_APPEND_ELT(
        entry,
        field,
        build_int_cst(integer_type_node, ri->ti->type_info_table_index));
    field = DECL_CHAIN(field);

    D("Adding register info entry: <%d (%s %s), %d>",
      REGNO(DECL_RTL(ri->decl)),
      get_name(ri->decl),
      reg_names[REGNO(DECL_RTL(ri->decl))],
      ri->ti->type_info_table_index);

    return build_constructor(type, entry);
}


/* Create register info which is to be put into a table */
static tree create_reg_info_table(call_info_t *ci)
{    
    unsigned i, n_registers;
    tree var;
    reg_info_t *ri;
    VEC(constructor_elt,gc) *entries = NULL;
    static unsigned entry_id;
        
    n_registers = VEC_length(reg_info_t, ci->register_info);

    if (n_registers == 0)
      return NULL_TREE;

    FOR_EACH_VEC_ELT(reg_info_t, ci->register_info, i, ri)
    {
        var = create_global_var(
            build_reg_info_type(), uniq_name("reg_info", entry_id++));
        DECL_INITIAL(var) = create_reg_info_entry(ri);
        CONSTRUCTOR_APPEND_ELT(
            entries,
            NULL,
            build1(ADDR_EXPR, 
                   build_pointer_type(build_reg_info_type()), var));

        varpool_finalize_decl(var);
    }

    return build_constructor(build_reg_info_table_type(n_registers), entries);
}


static tree create_label_layout_entry(tree type, call_info_t *ci)
{
    int n_registers;
    tree field, addr, decl, str, ri_table;
    VEC(constructor_elt,gc) *ll_entry;
    static int reg_table_id;

    if (ci->return_callsite_addr == NULL)
      return NULL_TREE;

    if (ci->caller_func->func_decl == NULL)
      return NULL_TREE;

    if (cgraph_get_node(ci->caller_func->func_decl) == NULL)
      abort();

    ll_entry = NULL;
    field = TYPE_FIELDS(type);

    /* Return address member/field */
    addr = ci->return_callsite_addr;
    CONSTRUCTOR_APPEND_ELT(ll_entry, field, addr);
    field = DECL_CHAIN(field);

    /* Caller name member/field */
    str = build_string_literal(
        strlen(ci->caller_func->name) + 1, ci->caller_func->name);
    CONSTRUCTOR_APPEND_ELT(ll_entry, field, str);
    field = DECL_CHAIN(field);

    /* Function info entry pointer */
    CONSTRUCTOR_APPEND_ELT(
        ll_entry,
        field,
        build1(ADDR_EXPR, ptr_type_node, ci->caller_func->func_info_entry));
    field = DECL_CHAIN(field);

    /* Number of elements in register info array */
    n_registers = VEC_length(reg_info_t, ci->register_info);
    CONSTRUCTOR_APPEND_ELT(
        ll_entry,
        field,
        build_int_cst(integer_type_node, n_registers));
    field = DECL_CHAIN(field);

    /* Create a pointer to an array of pointers */
    ri_table = create_reg_info_table(ci);
    if (ri_table == NULL_TREE)
      decl = null_pointer_node;
    else
    {
        decl = create_global_var(
            build_reg_info_table_type(n_registers), 
            uniq_name("reg_info_table", reg_table_id++));
        DECL_INITIAL(decl) = ri_table;
        varpool_finalize_decl(decl);
    }

    /* Array of register info */
    CONSTRUCTOR_APPEND_ELT(
        ll_entry,
        field,
        build1(ADDR_EXPR, 
               build_pointer_type(build_reg_info_table_type(n_registers)),
               decl));

   return build_constructor(type, ll_entry);
}


/* Given a type return its name:
 * Strips off the array or pointer-to types.
 */
const char *crap_get_type_name(const_tree type)
{
    const_tree tp = type;

    while (tp && (POINTER_TYPE_P(tp) || (TREE_CODE(tp) == ARRAY_TYPE)))
      tp = TREE_TYPE(tp);

    if (TYPE_NAME(tp) && TREE_CODE(TYPE_NAME(tp)) == IDENTIFIER_NODE)
      return IDENTIFIER_POINTER(TYPE_NAME(tp));
    else if (TYPE_NAME(tp) && TREE_CODE(TYPE_NAME(tp)) == TYPE_DECL)
      return IDENTIFIER_POINTER(DECL_NAME(TYPE_NAME(tp)));
    else
      return "unknown";
}


static tree create_type_info_entry(const type_info_t *ti)
{
    char buf[64];
    tree field;
    VEC(constructor_elt,gc) *ti_entry;

    /* If this is the parent and has members, we do not need to output the
     * parent/name of the struct into the binary.
     * In other words if this is the base of the struct, we need the members.
     * Our finding algroithm should locate the first member for this struct in
     * the table.
     */
    if (TREE_CODE(ti->type) == COMPONENT_REF)
      return NULL_TREE;

    ti_entry = NULL;
    field = TYPE_FIELDS(build_type_info_type());

    /* Parent offset */
    CONSTRUCTOR_APPEND_ELT(
        ti_entry,
        field,
        build_int_cst(integer_type_node, ti->offset_from_parent));
    field = DECL_CHAIN(field);

    /* Size */
    CONSTRUCTOR_APPEND_ELT(
        ti_entry,
        field,
        build_int_cst(integer_type_node, ti->size));
    field = DECL_CHAIN(field);

    /* Type info table index */
    CONSTRUCTOR_APPEND_ELT(
        ti_entry,
        field,
        build_int_cst(integer_type_node, ti->type_info_table_index));
    field = DECL_CHAIN(field);
    
    /* Number of members in type (if a struct type) */
    CONSTRUCTOR_APPEND_ELT(
        ti_entry,
        field,
        build_int_cst(integer_type_node, ti->n_members));
    field = DECL_CHAIN(field);
    
    /* Is pointer */
    CONSTRUCTOR_APPEND_ELT(
        ti_entry,
        field,
        build_int_cst(boolean_type_node, ti->is_pointer));
    field = DECL_CHAIN(field);

    /* Flags: User added */
    CONSTRUCTOR_APPEND_ELT(
        ti_entry,
        field,
        build_int_cst(char_type_node, ti->flags));
    field = DECL_CHAIN(field);

    /* Type:member name */
    snprintf(buf, sizeof(buf), "%s:%s",
             ti->name,
             ti->offset_from_parent==-1 ? "[no members]" : ti->field_name);
    CONSTRUCTOR_APPEND_ELT(
        ti_entry,
        field, 
        build_string_literal(strlen(buf) + 1, buf));

    return build_constructor(build_type_info_type(), ti_entry);
}


/* GCC Adds an identical (distinct copy) type but flagged with a different
 * symtable data and as a "pointer-to-type".  This is a comparison which takes
 * this nugget of information into consideration.  If they are themself and a
 * pointer to itself then return TRUE.  'a' and 'b' MUST be type nodes.
 */
bool crap_is_same_type_p(const_tree a, const_tree b)
{
    if (a == NULL_TREE || b == NULL_TREE)
      return false;

    if (DECL_P(a))
      a = TREE_TYPE(a);
    if (DECL_P(b))
      b = TREE_TYPE(b);
    return a == b;

    return ((a == b) || types_compatible_p((tree)a, (tree)b) ||
             (TYPE_CANONICAL(a) && TYPE_CANONICAL(b) &&
                (TYPE_CANONICAL(a) == b || TYPE_CANONICAL(b) == a)));
}


/* Locate the index in the table for type 'type'  We make sure that we do not
 * return the type if it is a member.  We need the actual type.
 */
static int get_type_table_index(crap_t *ctx, const_tree type)
{
    unsigned i;
    type_info_t *ti;

    FOR_EACH_VEC_ELT(type_info_t, ctx->type_infos, i, ti)
      if (crap_is_same_type_p(type, ti->type) && (ti->is_member == false))
      {
          /* If this is a struct, we never need to reference it at runtime, we
           * just need the data of it's first member (which should be index + 1)
           */
          //if (ti->n_members) TODO: REMOVE THIS */
          //  ++i;
          return (int)i;
      }

    return UNKNOWN_TYPE_INDEX;
}


static inline bool is_struct_p(const_tree type)
{
    return TREE_CODE(type) == RECORD_TYPE;
}


static int get_type_size(const_tree type)
{
    while (POINTER_TYPE_P(type))
      type = TREE_TYPE(type);

    if (TYPE_SIZE(type))
      return size_low_cst(TYPE_SIZE(type)) / 8;
    else
      return sizeof(void *);
}


/* Look across all data types we know about, and return this types info.
 * We add the data type to the global list of data types if it does not exist.
 * If 'force_add' is true, we can add the specified data type
 */
type_info_t *get_type_info(crap_t *ctx, const_tree node, bool force_add)
{
    unsigned i;
    bool is_pointer;
    tree ignore;
    const_tree type, field, field_type;
    type_info_t *ti, *field_ti, *elt_ti, *ptr_to_ti;
    static int idpool;

    type = node;
    is_pointer = false;

    if (DECL_P(type))
      type = TREE_TYPE(type);

    /* We only collect pointer things not primitives */
    if (!force_add && 
        !POINTER_TYPE_P(type) && !AGGREGATE_TYPE_P(type) &&
        (type != void_type_node))
      return NULL;

    /* If we have already added it... bail (we have to look for the type and not
     * a member)
     */ 
    FOR_EACH_VEC_ELT(type_info_t, ctx->type_infos, i, ti)
      if (crap_is_same_type_p(ti->type, type) && (ti->is_member == false))
        return ti;

    /* Check types to ignore (presumably types created for memory mgmt) */
    FOR_EACH_VEC_ELT(tree, ctx->types_to_ignore, i, ignore)
      if (!force_add && crap_is_same_type_p(type, ignore))
        return NULL;
    
    D("Adding type %s (%s)",
      tree_code_name[TREE_CODE(type)],
      crap_get_type_name(type));

    /* Guard against adding more types after the table has been generated */
    if (ctx->has_built_type_info_table)
    {
        // TODO Should place a warning here or abort();
        return NULL;
    }

    /* Add the base */
    ti = VEC_safe_push(type_info_t, heap, ctx->type_infos, NULL);
    memset(ti, 0, sizeof(type_info_t));
    ti->type = type;
    ti->id_symbol = get_identifier(uniq_name("type_info_", idpool++));
    ti->flags = 0;
    ti->is_member = false;
    ti->offset_from_parent = 0;
    ti->name = ggc_strdup(crap_get_type_name(type));
    ti->is_pointer = POINTER_TYPE_P(type);
    ti->size = POINTER_TYPE_P(type) ? sizeof(void *) : get_type_size(type);

    /* If we have a pointer, get the actual type that is being pointed to.
     * Special case: If the pointer is to an array, get the array data and not
     * the element data.
     */
    if (ti->is_pointer)
    {
        ptr_to_ti = get_type_info(ctx, TREE_TYPE(type), true);
        if (TREE_CODE(TREE_TYPE(type)) == ARRAY_TYPE)
          ti->type_info_table_index = get_type_table_index(ctx, ptr_to_ti->type);
        else
          ti->type_info_table_index = ptr_to_ti->type_info_table_index;
    }
    else
      ti->type_info_table_index = VEC_length(type_info_t, ctx->type_infos) - 1;

    /* Handle arrays specially */
    if (TREE_CODE(type) == ARRAY_TYPE)
    {
        ti->flags |= CRAP_FLAGS_ARRAY;

        /* Number of elements in this array (have to add 1) */
        ti->n_members = int_cst_value(array_type_nelts(ti->type)) + 1;

        /* Tell the array type what type its elements are */
        elt_ti = get_type_info(ctx, TREE_TYPE(type), true);
        ti->type_info_table_index = crap_get_type_info_index(ctx, elt_ti);
    }
    D("    Located a type table index %d", ti->type_info_table_index);

    /* Add each member... members can be pointers */
    if (TREE_CODE(type) == RECORD_TYPE)
    {
        D("Adding fields");
        i = 0;
        for (field=TYPE_FIELDS(type); field; field=DECL_CHAIN(field))
        {
            if (TREE_CODE(field) != FIELD_DECL)
              continue;

            field_type = TREE_TYPE(field);

            /* We only collect pointer things not primitives */
            if (!POINTER_TYPE_P(field_type) && !AGGREGATE_TYPE_P(field_type))
              continue;

            // TODO ADD IGNORE OTHER TYPE HERE TOO!

            ++ti->n_members;
            is_pointer = false;
            if (POINTER_TYPE_P(field_type))
            {
                field_type = TREE_TYPE(field_type);
                is_pointer = true;
            }

            D("    [%d] Adding field %s (%s) (%s) %s",
              i++,
              tree_code_name[TREE_CODE(field_type)],
              crap_get_type_name(field_type),
              get_name((tree)field),
              is_pointer ? "[pointer]" : "");

            field_ti = VEC_safe_push(type_info_t, heap, ctx->type_infos, NULL);
            field_ti->type = field_type;
            field_ti->n_members = 0;
            field_ti->flags = 0;
            field_ti->name = ggc_strdup(crap_get_type_name(field_type));
            field_ti->field_name = ggc_strdup(get_name((tree)field));
            field_ti->is_pointer = is_pointer;
            field_ti->size = get_type_size(field_type);
            field_ti->is_member = true;
            field_ti->id_symbol = 
                get_identifier(uniq_name("type_info_", idpool++));
            field_ti->type_info_table_index = 
                get_type_table_index(ctx, field_ti->type);
            field_ti->offset_from_parent = 
                (int)size_low_cst(DECL_FIELD_OFFSET(field));
            field_ti->offset_from_parent += 
                (int)(size_low_cst(DECL_FIELD_BIT_OFFSET(field)) / 8);

            D("        Located at type table index %d",
              field_ti->type_info_table_index);
        }

        /* Now that we have added all members, add their types if necessary */
        for (field=TYPE_FIELDS(type); field; field=DECL_CHAIN(field))
          get_type_info(ctx, field, false);
    }

    return ti;
}


static tree create_local_var_info_table_entry(const var_info_t *vi)
{
    int idx;
    tree field;
    VEC(constructor_elt,gc) *vi_entry;

    vi_entry = NULL;
    field = TYPE_FIELDS(build_local_var_info_type());

    /* Offset from frame pointer */
    CONSTRUCTOR_APPEND_ELT(
        vi_entry,
        field,
        build_int_cst(integer_ptr_type_node, vi->u.offset_from_fp));
    field = DECL_CHAIN(field);
    
    /* Type information pointer (arrays are special) their typeinfo must be the
     * array itself and not the first element
     */
    idx = (vi->ti->flags & CRAP_FLAGS_ARRAY) ? 
       crap_get_type_info_index(all_infos, vi->ti) : vi->ti->type_info_table_index;
    CONSTRUCTOR_APPEND_ELT(
        vi_entry,
        field,
        build_int_cst(integer_type_node, idx));
    field = DECL_CHAIN(field);
   
    /* Is pointer */ 
    CONSTRUCTOR_APPEND_ELT(
        vi_entry,
        field,
        build_int_cst(boolean_type_node, vi->is_pointer));
    field = DECL_CHAIN(field);

    /* Variable name */
    CONSTRUCTOR_APPEND_ELT(
        vi_entry,
        field,
        build_string_literal(strlen(vi->name) + 1, vi->name));

    return build_constructor(build_local_var_info_type(), vi_entry);
}


static tree create_global_var_info_table_entry(const var_info_t *vi)
{
    tree field;
    VEC(constructor_elt,gc) *vi_entry;

    vi_entry = NULL;
    field = TYPE_FIELDS(build_global_var_info_type());

    /* Global address/symbol */
    CONSTRUCTOR_APPEND_ELT(
        vi_entry,
        field,
        vi->u.global_addr);
    field = DECL_CHAIN(field);
 
    /* Type information pointer */
    CONSTRUCTOR_APPEND_ELT(
        vi_entry,
        field,
        build_int_cst(integer_type_node, vi->ti->type_info_table_index));
    field = DECL_CHAIN(field);
   
    /* Is pointer */ 
    CONSTRUCTOR_APPEND_ELT(
        vi_entry,
        field,
        build_int_cst(boolean_type_node, vi->is_pointer));
    field = DECL_CHAIN(field);

    /* Variable name */
    CONSTRUCTOR_APPEND_ELT(
        vi_entry,
        field,
        build_string_literal(strlen(vi->name) + 1, vi->name));

    return build_constructor(build_local_var_info_type(), vi_entry);
}


static tree create_callee_save_info_table_entry(const callee_save_info_t *cs)
{
    int regno;
    tree field;
    VEC(constructor_elt,gc) *entry;

    entry = NULL;
    field = TYPE_FIELDS(build_callee_save_info_type());

    /* Offset from frame pointer */
    CONSTRUCTOR_APPEND_ELT(
        entry,
        field,
        build_int_cst(integer_type_node, cs->offset_from_fp));
    field = DECL_CHAIN(field);

    /* Register number (converted to ucontext value for runtime) */
    regno = get_reg_num_for_runtime(cs->gcc_register_number);
    CONSTRUCTOR_APPEND_ELT(
        entry,
        field,
        build_int_cst(unsigned_char_type_node, regno));
    field = DECL_CHAIN(field);

    return build_constructor(build_callee_save_info_type(), entry);
}


/* Create array of callee-save info used by function 'fi' */
static tree create_callee_save_info_table(
    const func_info_t *fi,
    int               *n_entries) /* Returned */
{
    unsigned i;
    callee_save_info_t *cs;
    tree decl, cs_entry;
    VEC(constructor_elt,gc) *cs_entries;
    static int pool_id;

    *n_entries = 0;
    cs_entries = NULL;

    /* An array of pointers */
    FOR_EACH_VEC_ELT(callee_save_info_t, fi->callee_save_info, i, cs)
    {
        cs_entry = create_callee_save_info_table_entry(cs);
        decl = create_global_var(
            build_callee_save_info_type(),
            uniq_name("callee_save_entry", pool_id++));
        DECL_INITIAL(decl) = cs_entry;

        CONSTRUCTOR_APPEND_ELT(
            cs_entries,
            NULL, 
            build1(ADDR_EXPR, 
                   build_pointer_type(build_callee_save_info_type()), decl));

        varpool_finalize_decl(decl);
        ++(*n_entries);
    }

    if (*n_entries == 0)
      return null_pointer_node;
    else
      return build_constructor(
          build_callee_save_info_table_type(*n_entries), cs_entries);
}


/* Create array of type info used by function 'fi'  this array is returned:
 * Actually this is a string encoded (eventually binary) string:
 * FORMAT: <n_entries, [<offset,var_name> ...]>
 * n_entries is returned
 */
static tree create_func_var_info_table(const func_info_t *fi, int *n_entries)
{
    unsigned i;
    var_info_t *vi;
    tree decl, vi_entry;
    VEC(constructor_elt,gc) *vi_entries;
    static int pool_id;

    *n_entries = 0;
    vi_entries = NULL;

    /* An array of pointers */
    FOR_EACH_VEC_ELT(var_info_t, fi->var_info, i, vi)
    {
        if (vi->u.offset_from_fp == BAD_OFFSET)
          continue;

        if (vi->ti == NULL)
          continue;

        vi_entry = create_local_var_info_table_entry(vi);
        decl = create_global_var(
            build_local_var_info_type(), uniq_name("var_entry", pool_id++));
        DECL_INITIAL(decl) = vi_entry;

        CONSTRUCTOR_APPEND_ELT(
            vi_entries,
            NULL, 
            build1(ADDR_EXPR, 
                   build_pointer_type(build_local_var_info_type()), decl));

        varpool_finalize_decl(decl);
        ++(*n_entries);
    }

    return build_constructor(
        build_local_var_info_table_type(*n_entries),vi_entries);
}


static tree create_func_info_table_entry(tree entry_type, const func_info_t *fi)
{
    int n_var_info_table_entries, n_callee_save_info_table_entries;
    tree field, str, var_info_table, callee_save_info_table, decl;
    VEC(constructor_elt,gc) *fi_entry;
    static int pool_id;

    fi_entry = NULL;
    field = TYPE_FIELDS(entry_type);

    /* Func name */
    str = build_string_literal(strlen(fi->name) + 1, fi->name);
    CONSTRUCTOR_APPEND_ELT(fi_entry, field, str);
    field = DECL_CHAIN(field);

    /* Stack frame size entry */
    CONSTRUCTOR_APPEND_ELT(
        fi_entry,
        field,
        build_int_cst(long_unsigned_type_node, fi->stack_frame_size));
    field = DECL_CHAIN(field);

    /* Number of var info elements */
    var_info_table = create_func_var_info_table(fi, &n_var_info_table_entries);
    CONSTRUCTOR_APPEND_ELT(
        fi_entry,
        field,
        build_int_cst(integer_type_node, n_var_info_table_entries));
    field = DECL_CHAIN(field);

    /* Var info table (a pointer to a table) */
    decl = create_global_var(
        build_local_var_info_table_type(n_var_info_table_entries),
        uniq_name("var_table", pool_id++));
    DECL_INITIAL(decl) = var_info_table;
    varpool_finalize_decl(decl);
    
    CONSTRUCTOR_APPEND_ELT(
        fi_entry,
        field, 
        build1(ADDR_EXPR, build_pointer_type(
            build_local_var_info_table_type(n_var_info_table_entries)), decl));
    field = DECL_CHAIN(field);

    /* Number of callee save info elements */
    callee_save_info_table = create_callee_save_info_table(
        fi, &n_callee_save_info_table_entries);
    CONSTRUCTOR_APPEND_ELT(
        fi_entry,
        field,
        build_int_cst(integer_type_node, n_callee_save_info_table_entries));
    field = DECL_CHAIN(field);

    /* Callee save info table (a pointer to a table) */
    decl = create_global_var(
        build_callee_save_info_table_type(n_callee_save_info_table_entries),
        uniq_name("callee_save_table", pool_id));
    DECL_INITIAL(decl) = callee_save_info_table;
    varpool_finalize_decl(decl);
    
    CONSTRUCTOR_APPEND_ELT(
        fi_entry,
        field, 
        build1(ADDR_EXPR, build_pointer_type(
               build_callee_save_info_table_type(
                   n_callee_save_info_table_entries)), decl));
    
    return build_constructor(entry_type, fi_entry);
}


/* Create a global table (array) "label_layout_table", where each entry is a
 * tuple, and keyed on the return label address.  The data at that key is a
 * pointer to the type information of the function its (local arguments and
 * local variables).
 */
void crap_create_label_layout_table(crap_t *context)
{
    int n_entries;
    unsigned i;
    call_info_t *ci;
    tree decl, field, prev_field, label_entry_type, label_layout_table_type;
    tree ll_entry, var;
    VEC(constructor_elt,gc) *ll_entries;

    /* Build label_layout_entry type:
     *
     * struct _label_layout_entry_type_t
     * {
     *     void *__return_callsite_addr;
     *     char *__caller_name; <-- Name of the function where the callsite is 
     *     func_info_entry_t *__func_info_ptr;
     *     int __n_registers;
     *     reg_info_entry_t *registers[];
     * };
     */
    BUILD_FIRST_FIELD(field, prev_field,"__return_callsite_addr",ptr_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__caller_name", ptr_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__func_info_ptr", ptr_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__n_registers", integer_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__registers", ptr_type_node);
    BUILD_TYPE_DONE(label_entry_type, "__label_layout_entry_t", field);

    /* Create actual instances of label layout entries */
    n_entries = 0;
    ll_entries = NULL;
    D("Adding %d label layout entries", n_entries);
    FOR_EACH_VEC_ELT(call_info_t, context->call_infos, i, ci)
    {
        if (!(ll_entry = create_label_layout_entry(label_entry_type, ci)))
          continue;

        var = create_global_var(label_entry_type,
            get_name(ci->return_callsite_addr));
        DECL_INITIAL(var) = ll_entry;
        CONSTRUCTOR_APPEND_ELT(
            ll_entries,
            NULL,
            build1(ADDR_EXPR, build_pointer_type(label_entry_type), var));

        varpool_finalize_decl(var);
        ++n_entries;
    }
    
    /* Create the array of pointers */
    label_layout_table_type = build_array_type_nelts(
        build_pointer_type(label_entry_type), n_entries);
    
    /* Create a global array instance of label layout table */
    decl = create_global_var(
        label_layout_table_type, "__"TAG"_label_layout_table");
        
    /* Populate the array */
    DECL_INITIAL(decl) = build_constructor(
        label_layout_table_type, ll_entries);
    varpool_finalize_decl(decl);

    /* Create a global variable containing the num of entries of the table */
    decl = create_global_var(
        integer_type_node, "__"TAG"_label_layout_table_n_entries");
    DECL_INITIAL(decl) = build_int_cst(integer_type_node, n_entries);
    varpool_finalize_decl(decl);
}


/* Function info table (each entry is a function with a corresponding for
 * type data for that function.
 */
void crap_create_function_info_table(crap_t *context)
{
    int n_entries;
    unsigned i;
    func_info_t *fi;
    tree decl, var, field, prev_field;
    tree fi_entry, func_info_entry_type, func_info_table_type;
    VEC(constructor_elt,gc) *fi_entries;

    /* Build func_info_table_entry type:
     *
     * (NOTE: __stack_frame_size is the STACK_FRAME_SIZE_IDX field
     *        in this struct)
     *
     * struct __func_info_t
     * {
     *     char *__name; // Debugging
     *     unsigned long __stack_frame_size;
     *     int __n_var_info;
     *     void *__var_info; // Actually a ptr to a table of ptrs :-)
     *     int __n_callee_save_info;
     *     void *__callee_save_info;
     * };
     */
    BUILD_FIRST_FIELD(field, prev_field, "__name", ptr_type_node);
    BUILD_ADD_FIELD(
        field, prev_field, "__stack_frame_size", long_unsigned_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__n_var_info", integer_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__var_info", ptr_type_node);
    BUILD_ADD_FIELD(field, prev_field,"__n_callee_save_info",integer_type_node);
    BUILD_ADD_FIELD(field, prev_field, "__callee_save_info", ptr_type_node);
    BUILD_TYPE_DONE(func_info_entry_type, "__func_info_entry_t", field);

    /* Create the array type */
    n_entries = VEC_length(func_info_t, context->func_infos);
    func_info_table_type = build_array_type_nelts(
        build_pointer_type(func_info_entry_type), n_entries);

    /* Create a global array instance of the function info table */
    decl = create_global_var(func_info_table_type, "__"TAG"_func_info_table");

    /* Now that the type and function info type-info nodes are built, for each
     * function:
     * Build table of type info
     * Build function info entry
     */
    fi_entries = NULL;
    FOR_EACH_VEC_ELT(func_info_t, context->func_infos, i, fi)
    {
        fi_entry = create_func_info_table_entry(func_info_entry_type, fi);
        fi->func_info_entry = fi_entry;

        /* Create instance of the entry and append it to our array */
        var = create_global_var(
            func_info_entry_type, uniq_name("func_info_entry", fi->id));
        DECL_INITIAL(var) = fi_entry;
        CONSTRUCTOR_APPEND_ELT(
           fi_entries,
           NULL,
           build1(ADDR_EXPR, build_pointer_type(func_info_entry_type), var));
        varpool_finalize_decl(var);
    }

    /* Populate the array */
    DECL_INITIAL(decl) = build_constructor(func_info_table_type, fi_entries);
    varpool_finalize_decl(decl);

    /* Create a global variable containing the num of entries of the table */
    decl = create_global_var(
        integer_type_node, "__"TAG"_func_info_table_n_entries");
    DECL_INITIAL(decl) = build_int_cst(integer_type_node, n_entries);
    varpool_finalize_decl(decl);
}


static func_info_t *new_func_info(crap_t *ctx, struct function *func)
{
    unsigned i;
    tree decl;
    func_info_t *fi;

    /* Check to see if we have already created this func info */
    FOR_EACH_VEC_ELT(func_info_t, ctx->func_infos, i, fi)
      if (fi->func == func)
        return fi;

    /* Init the func info vector */
    if (!ctx->func_infos)
      ctx->func_infos = VEC_alloc(func_info_t, heap, cgraph_n_nodes);

    /* Never been created, so instantiate the info now */
    fi = VEC_safe_push(func_info_t, heap, ctx->func_infos, NULL);
    memset(fi, 0, sizeof(func_info_t));
    fi->id = i;
    fi->name = ggc_strdup(get_name(func->decl));
    fi->func = func;
    fi->func_decl = func->decl;
    fi->local_decls = VEC_alloc(tree, heap, VEC_length(tree, func->local_decls));
    FOR_EACH_VEC_ELT(tree, func->local_decls, i, decl)
      VEC_safe_push(tree, heap, fi->local_decls, decl);

    /* These will be initted when get_function_params_locals_regs() is called */
    fi->var_info = NULL;
    fi->callee_save_info = NULL;
   
    /* Copy the formal parameter declaration nodes */ 
    fi->formal_param_decls = 
        VEC_alloc(tree, heap, list_length(DECL_ARGUMENTS(func->decl)));
    for (decl=DECL_ARGUMENTS(func->decl); decl; decl=DECL_CHAIN(decl))
      VEC_safe_push(tree, heap, fi->formal_param_decls, decl);

    return fi;
}


/* Create a new caller info instance and add it to 'calls' */
call_info_t *crap_new_call_info(
    crap_t          *context,
    struct function *caller_func,
    gimple           stmt)
{
    static unsigned id_pool;
    call_info_t *ci = VEC_safe_push(call_info_t, heap, context->call_infos,NULL);
    memset(ci, 0, sizeof(call_info_t));
    ci->caller_func = new_func_info(context, caller_func);
    ci->stmt = stmt;
    ci->id = id_pool++;
    D("Adding call data to %s in %s", 
      gimple_call_fndecl(stmt) ? 
          get_name(gimple_call_fndecl(stmt)) : 
          "(function pointer)",
      ci->caller_func->name);
    return ci;
}


static long get_local_offset_var_decl(tree decl)
{
    rtx rxp;

    if (!DECL_RTL_SET_P(decl))
      return BAD_OFFSET;
            
    rxp = DECL_RTL(decl);
    if (!MEM_P(rxp))
      return BAD_OFFSET;

    /* Get to the 'plus' expression */
    rxp = XEXP(rxp, 0);

    /* If this is not a plus expression, then the data is stored in a
     * register and not as an offset of the stack.  We ignore that now.
     * TODO: Add register support
     */
    if (REG_P(rxp))
      return BAD_OFFSET;

    /* Get the second field of plus */
    rxp = XEXP(rxp, 1);

    /* Get offset from stack */
    return  XWINT(rxp, 0);
}


/* Get the offset of a local variable parsing the RTL.  This is much more
 * accurate than the fallback routine: "get_local_offset_var_decl()"
 * This routine parses the RTL looking for the MEM_P where the
 * offset of the variable from the hard_frame_pointer is a PLUS expression.
 * e.g. 
 * (set (mem/f/c:DI (plus:DI (reg/f:DI 6 bp)
 *               (const_int -24 [0xffffffffffffffe8])) [0 matt+0 S8 A64])
 *                       (const_int 0 [0])) foo.go:4 62 {*movdi_internal_rex64}
 *
 *
 * FIXME: GO ONLY
 */
static long get_local_offset(tree decl)
{
    tree expr;
    rtx insn, var, mem, set, frame_ptr;
    struct mem_attrs *var_attrs;

    /* Get the attributes for the declaration */
    if (!DECL_RTL_SET_P(decl))
      return get_local_offset_var_decl(decl);;

    var = DECL_RTL(decl);
    if (!MEM_P(var))
      return get_local_offset_var_decl(decl);

    var_attrs = MEM_ATTRS(var);

    /* Look for all MEM_P in the first basic block */
    for (insn=get_insns();insn; insn=NEXT_INSN(insn))
    {
        if (NOTE_P(insn) || !INSN_P(insn))
          continue;
        if (!(mem = PATTERN(insn)))
          continue;
        if (GET_CODE(mem) != SET)
          continue;

        set = mem;
        mem = XEXP(set, 0);
        if (!MEM_P(mem))
        {
            mem = XEXP(set, 1);
            if (!MEM_P(mem))
              continue;
        }

        /* Check mem attributes to see if it matches attributes of decl... in
         * other words, is the MEM reference for the variable in decl
         */
        if (!MEM_ATTRS(mem))
          continue;
        else if (var_attrs->expr != MEM_ATTRS(mem)->expr)
        {
            expr = MEM_ATTRS(mem)->expr;
            if (!expr                                         ||
                (TREE_CODE(TREE_TYPE(decl)) != RECORD_TYPE)   ||
                (TREE_CODE(expr)            != COMPONENT_REF) ||
                (TREE_OPERAND(expr, 0)      != decl))
              continue;
        }

        mem = XEXP(mem, 0); /* PLUS */
        if (GET_CODE(mem) != PLUS)
          continue;

        frame_ptr = XEXP(mem, 0);
        if (frame_ptr != hard_frame_pointer_rtx)
          continue;

        mem = XEXP(mem, 1); /* Second operand to PLUS (first is frame ptr) */
        if (!CONST_INT_P(mem))
          continue;

        return XINT(mem, 0);
    }

    return get_local_offset_var_decl(decl);
}


var_info_t *create_var_info(tree decl, vec_var_info_t *vars, bool is_register)
{
    var_info_t *vi;
    type_info_t *ti;

    gcc_assert(all_infos);

    /* First get the type for the variable */
    if (!(ti = get_type_info(all_infos, decl, false)))
      return NULL;
    
    /* Add this type information */
    vi = VEC_safe_push(var_info_t, heap, *vars, NULL);
    memset(vi, 0, sizeof(var_info_t));

    /* If the variable is local to the function, get its offset info */
    if (is_register)
      vi->u.reg = REGNO(DECL_RTL(decl));
    else if (is_global_var(decl))
      vi->u.global_addr = build1(ADDR_EXPR, TREE_TYPE(decl), decl);
    else /* Else it's on the stack... */
      vi->u.offset_from_fp = get_local_offset(decl);

    vi->ti = ti;
    vi->decl = decl;
    vi->is_pointer = POINTER_TYPE_P(TREE_TYPE(decl));

    if (get_name(decl))
      vi->name = ggc_strdup(get_name(decl));
    else
      vi->name = ggc_strdup("Unknown");

    if (is_global_var(decl))
      D("-- Found global variable %s", get_name(decl));
    else if (vi->u.offset_from_fp <= 0)
      D("-- Found variable or formal param %s at offset: %ld and type %d",
        get_name(decl), vi->u.offset_from_fp, vi->ti->type_info_table_index);
    else if (vi->u.reg != BAD_OFFSET)
      D("-- Found register or formal param %s Located at register: %s (%d) "
        "with type %d", get_name(decl), reg_names[vi->u.reg], vi->u.reg,
        vi->ti->type_info_table_index);

    return vi;
}


/* Probably should be called 'indicies' but indexes makes sense too */
static inline void fixup_indexes(crap_t *ctx)
{
    unsigned i;
    type_info_t *ti;

    FOR_EACH_VEC_ELT(type_info_t, ctx->type_infos, i, ti)
      if (ti->type_info_table_index == UNKNOWN_TYPE_INDEX)
        ti->type_info_table_index = get_type_table_index(ctx, ti->type);

    /* This is separate... just to keep things clean.  Find all types that have
     * members (struct types).  Make their index negative, just flip the unused
     * sign-bit.  That way the runtime will see the negative bit and use that as
     * an indicator to not actually process that table entry.  For instance:
     * struct Foo{int a; double d;}; the Foo entry in the table would have three
     * entries: Foo, member:a, member:d.  The runtime does not need to do
     * anything with the first entry, Foo, and should instead look at 'a'.  We
     * can remove Foo. But for debugging it makes sense to leave it there, it
     * makes the table easier to read for us humans.
     * TODO: REMOVE THIS
     */
    //FOR_EACH_VEC_ELT(type_info_t, types, i, ti)
    //  if (ti->n_members) /* If it is a struct type */
    //    ti->type_info_table_index *= -1;
}


#ifdef DEBUG
static void print_type_info(const crap_t *ctx)
{
    unsigned i, j;
    const type_info_t *ti, *mem;

    D("***** Type Information Table *****");

    for (i=0; i<VEC_length(type_info_t, ctx->type_infos); ++i)
    {
        ti = VEC_index(type_info_t, ctx->type_infos, i);
        D("[%d] Type %s with index %d and %d members %s %s",
          i, ti->name, ti->type_info_table_index, ti->n_members,
          ti->is_pointer ? "(pointer)" : "",
          ti->flags & CRAP_FLAGS_ARRAY ? "[array]" : "");

        if (ti->flags & ~CRAP_FLAGS_ARRAY)
          for (j=0; j<(unsigned)ti->n_members; ++j)
          {
              ++i;
              mem = VEC_index(type_info_t, ctx->type_infos, i);
              D("     [%d] Member %s with index %d",
                i, mem->name, mem->type_info_table_index);
          }
    }
    D("**********************************");
}
#endif /* DEBUG */


/* Inserts the type info data into the binary */
void crap_insert_type_info_table(crap_t *ctx)
{
	unsigned i, n_entries;
	type_info_t *ti;
    tree decl, ti_entry, type_info_table_type;
    VEC(constructor_elt,gc) *ti_entries;

    /* Avoid inserting the type info table into a .o file... only the main
     * binary needs this table.
     */
    if (crap_this_translation_unit_is_a_library())
      return;

    /* Avoid producing duplicate symbols in the symbol table */
    if (ctx->has_built_type_info_table)
      return;
    ctx->has_built_type_info_table = true;

    /* Now all types have just been added.  Make sure we have valid indexes */
    fixup_indexes(ctx);

#ifdef DEBUG
    print_type_info(ctx);
#endif

    /* Now that we have a constructed type vector.  Build the table of types. */
    ti_entries = NULL;
    FOR_EACH_VEC_ELT(type_info_t, ctx->type_infos, i, ti)
    {
        /* Emit type info into the binary:
         * Each entry is a tuple:
         * <type_name, offset_from_parent, is_pointer>
         * Consider:
         * struct Foo { int id; float f; };
         * struct Bar { Foo *f; int i;   };
         * The resulting table would look like:
         * Foo: <int,   0, 0>
         *      <float, 4, 0>
         * Bar: <Foo,   0, 0>
         *      <int,   8, 0>
         */
        if ((ti_entry = create_type_info_entry(ti)))
        {
            /* Make this a globally accessable value */
            decl = create_global_var(
                build_type_info_type(), IDENTIFIER_POINTER(ti->id_symbol));
            ti->decl = decl;
            DECL_INITIAL(decl) = ti_entry;

            CONSTRUCTOR_APPEND_ELT(
                ti_entries,
                NULL,
                build1(ADDR_EXPR,
                       build_pointer_type(build_type_info_type()),
                       decl));
            varpool_finalize_decl(decl);
        }
    }
    
    /* Array to hold all entries */
    n_entries = VEC_length(constructor_elt, ti_entries);
    type_info_table_type = build_array_type_nelts(
         build_pointer_type(build_type_info_type()), n_entries);

    /* Build the table and put it into the varpool so it will get emitted */
    decl = create_global_var(type_info_table_type, "__"TAG"_type_info_table");
    DECL_INITIAL(decl) = build_constructor(type_info_table_type, ti_entries);
    varpool_finalize_decl(decl);

    /* Create a global variable containing the num of entries of the table */
    decl = create_global_var(
        integer_type_node, "__"TAG"_type_info_table_n_entries");
    DECL_INITIAL(decl) = build_int_cst(integer_type_node, n_entries);
    varpool_finalize_decl(decl);
}


void crap_create_global_info_table(crap_t *context)
{
    int i, n_entries, entries;
    char buf[64];
    tree decl, vi_entry, global_var;
    var_info_t *vi;
    vec_var_info_t vars;
    VEC(constructor_elt,gc) *vi_entries;
    static int pool_id;

    /* Create the instances/entries for the global table */
    n_entries = 0;
    vi_entries = NULL;
    vars = new_vec_var_info_n(VEC_length(tree, context->global_vars));
    FOR_EACH_VEC_ELT(tree, context->global_vars, i, global_var)
    {
        if (!(vi = create_var_info(global_var, &vars, false)))
          continue;
        ++n_entries;
        vi_entry = create_global_var_info_table_entry(vi);
        snprintf(buf, sizeof(buf), "__global_var_entry_%d", ++pool_id);
        decl = create_global_var(build_global_var_info_type(), buf);
        DECL_INITIAL(decl) = vi_entry;
        CONSTRUCTOR_APPEND_ELT(
            vi_entries,
            NULL,
            build1(ADDR_EXPR,
                   build_pointer_type(build_global_var_info_type()),
                   decl));

        varpool_finalize_decl(decl);
    }

    /* Populate the array: Set entries to 1 if we have none to create an empty
     * table.  So the runtime will link properly.
     */
    entries = (n_entries == 0) ? 1 : n_entries;
    decl = create_global_var(
        build_global_var_info_table_type(entries), "__"TAG"_global_info_table");
    DECL_INITIAL(decl) =
        build_constructor(build_global_var_info_table_type(entries),vi_entries);
    varpool_finalize_decl(decl);

    /* Add a counter (use n_entries even if 0) */
    decl = create_global_var(
        integer_type_node, "__"TAG"_global_info_table_n_entries");
    DECL_INITIAL(decl) = build_int_cst(integer_type_node, n_entries);
    varpool_finalize_decl(decl);
}


crap_t *crap_new_context(void)
{
    crap_t *ctx = XCNEW(crap_t);
    ctx->type_infos = new_vec_type_info_n(96); /*TODO remove 96*/
    ctx->func_infos = new_vec_func_info();
    ctx->call_infos = new_vec_call_info();
    ctx->get_registers_at_calls_to = VEC_alloc(tree,heap,16);/*TODO remove 16*/
    ctx->types_to_ignore = VEC_alloc(tree,heap,16); /* TODO: remove 16 */
    return ctx;
}


/* Remove information regarding function 'fndecl' */
void crap_remove_info(crap_t *context, const_tree fndecl)
{
    unsigned i;
    call_info_t *ci;
    func_info_t *fi;

    FOR_EACH_VEC_ELT(call_info_t, context->call_infos, i, ci)
      if (ci->caller_func->func->decl == fndecl)
        VEC_ordered_remove(call_info_t, context->call_infos, i);
    
    FOR_EACH_VEC_ELT(func_info_t, context->func_infos, i, fi)
      if (fi->func->decl == fndecl)
        VEC_ordered_remove(func_info_t, context->func_infos, i);
}


/* Public interface */
type_info_t *crap_new_type_info(
	crap_t      *context,
	const_tree   node)
{
    return get_type_info(context, node, false);
}


/* Public interface */
unsigned crap_get_num_type_infos(const crap_t *context)
{
    return VEC_length(type_info_t, context->type_infos);
}


/* Public interface */
type_info_t *crap_get_type_info(const crap_t *context, const_tree type)
{
    unsigned i;
    type_info_t *ti;

    FOR_EACH_VEC_ELT(type_info_t, context->type_infos, i, ti)
      if (ti->type == type)
        return ti;

    return NULL;
}


/* Public interface */
int crap_get_type_info_index(crap_t *context, const type_info_t *ti)
{
    unsigned i;
    type_info_t *match;
    FOR_EACH_VEC_ELT(type_info_t, context->type_infos, i, match)
      if (match == ti)
        return i;

    return -1;
}


/* Public interface */
void crap_insert_labels(crap_t *context)
{
    crap_plug_insert_label_pass(NULL, (void *)context);
}


/* Public interface */
void crap_create_global_type_info(crap_t *context)
{
    crap_plug_collect_globals_pass(NULL, (void *)context);
}
