#include <stdio.h>
#include "libcrap.h"

struct TypeA {int i; struct TypeA *next;};

static void bar(void)
{
    __crap_collect();
}

static void foo(void)
{
    struct TypeA *a1 = (struct TypeA *)__crap_alloc(sizeof(struct TypeA));
    struct TypeA *a2 = (struct TypeA *)__crap_alloc(sizeof(struct TypeA));

    a1->i = 1;
    a2->i = 2;

    bar();

    printf("%d %d\n", a1->i, a2->i);

    __crap_collect();
    printf("%d %d\n", a1->i, a2->i);

    __crap_collect();
    printf("%d %d\n", a1->i, a2->i);

    __crap_collect();
    printf("%d %d\n", a1->i, a2->i);
}

int main(void)
{
    foo();
    return 0;
}
