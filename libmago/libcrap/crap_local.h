#ifndef _CRAP_LOCAL_H 
#define _CRAP_LOCAL_H
#include "crap.h"


#undef TAG
#define TAG "crap_plug"


/* Global context which either we create or is initialized by a call to
 * "crap_set_context()"
 */
extern crap_t *all_infos;


/* Console output macros */
#define _P(_fp, _label, ...)                         \
do                                                   \
{                                                    \
    fprintf(stderr, "["TAG"] " _label  __VA_ARGS__); \
    fputc('\n', stderr);                             \
} while (0)
    

/* Normal and Error console output macros */
#define P(...) _P(stdout, "", __VA_ARGS__)
#define E(...) _P(stderr, "[Error]", __VA_ARGS__)


/* Debugging console output mcros */
#ifdef DEBUG
#define D(...) _P(stderr,"", __VA_ARGS__)
#else
#define D(...) /* Ignore */
#endif


/* Undefined/not-found type table index */
#define UNKNOWN_TYPE_INDEX -1


/* This is the index into the CONSTRUCTOR (gcc constructor tree type)
 * This is the second member in the struct that is created at compile time, the
 * first member (0) being the name of the function.
 * Other places in the plugin code want to know where this field is located.
 * The type is created in crap.c: crap_create_function_info_table()
 */
#define STACK_FRAME_SIZE_IDX 1


/* A good offset from the frame pointer is always negative on a stack-growing
 * downward x86 machine.
 */
#define BAD_OFFSET 666


/* Functions that should not be in the public (crap.h) header (crap.c) */
extern type_info_t *get_type_info(crap_t *ctx, const_tree node, bool force_add);
extern var_info_t *create_var_info(
    tree            decl,
    vec_var_info_t *vars,
    bool            is_register);
extern void create_type_info_table(crap_t *ctx);
extern void crap_plug_insert_label_pass(void *gcc_data, void *user_data);
extern void crap_plug_collect_globals_pass(void *gcc_data, void *user_data);


/* Export/Import Functionality (crap_import_export.c) */
extern void crap_export_info(crap_t *ctx);


/* Create a callsite label, which is returned.
 * 'insn_p' is also returned and must be passed even if it points to NULL and 
 * is never used afterwards.
 */
extern tree crap_create_callsite_label(
    call_info_t *ci,
    const_tree   the_call_fndecl,
    rtx         *insn_p);


/* Returns 'true' if the compilation unit is a library/object file and not a
 * application (with main).
 */
extern bool crap_this_translation_unit_is_a_library(void);


/* Create a unique symbols name based on 'name' and number 'id'
 * The result is managed by gcc garbage collector, so no need to 'free' it.
 */
extern const char *uniq_name(const char *name, int id);


#endif /* _CRAP_LOCAL_H */
