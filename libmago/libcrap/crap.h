#ifndef _CRAP_H
#define _CRAP_H

#include <gcc-plugin.h>
#include <tree.h>


/* Information about data types */
typedef struct GTY(()) type_info_t
{
    int offset_from_parent;     /* 0 bytes if parent or >0 if member    */
    int size;                   /* Bytes                                */
    bool is_array;              /* Set by libcrap                       */
    bool is_pointer;
    bool is_member;
    const char *name;
    const char *field_name;    /* For members of structs                */
    int type_info_table_index; /* For members which are other structs   */
    int n_members;
    uint8_t flags;             /* Flags added by libcrap srr CRAP_FLAGS */
    const_tree type;
    const_tree decl;
    tree id_symbol;
} type_info_t;
DEF_VEC_O(type_info_t);
DEF_VEC_ALLOC_O(type_info_t, heap);
typedef VEC(type_info_t,heap) *vec_type_info_t;
#define new_vec_type_info() VEC_alloc(type_info_t, heap, 0)
#define new_vec_type_info_n(_num) VEC_alloc(type_info_t, heap, (_num))


/* Information about variables in functions */
typedef struct GTY(()) var_info_t
{
    union /* Either this represented a local or global variables */
    {
        long offset_from_fp; /* Local var: Offset from frame ptr (bytes)*/
        tree global_addr;    /* Global var: Symbol name                 */
        unsigned reg;        /* Register: Value is in register 'reg'    */
    } u;
    bool is_pointer;
    const char *name;
    type_info_t *ti;
    tree decl;
} var_info_t;
DEF_VEC_O(var_info_t);
DEF_VEC_ALLOC_O(var_info_t, heap);
typedef VEC(var_info_t,heap) *vec_var_info_t;
#define new_vec_var_info() VEC_alloc(var_info_t, heap, 0)
#define new_vec_var_info_n(_num) VEC_alloc(var_info_t, heap, (_num));


/* Register info (shares the same struct as var info) */
typedef var_info_t reg_info_t;
DEF_VEC_O(reg_info_t);
DEF_VEC_ALLOC_O(reg_info_t, heap);
typedef VEC(reg_info_t,heap) *vec_reg_info_t;
#define new_vec_reg_info() VEC_alloc(reg_info_t, heap, 0)
#define new_vec_reg_info_n(_num) VEC_alloc(reg_info_t, heap, (_num));


/* Callee save information
 * Each function might push data for callee save registers onto the stack.
 * The GC needs to know which callee save registers are being used so it can
 * trace them from the stack.
 */
typedef struct GTY(()) callee_save_info_t
{
    long offset_from_fp;     /* Offset from frame ptr (bytes)      */
    int gcc_register_number; /* Register being pushed on the stack */
    type_info_t *ti;
} callee_safe_info_t;
DEF_VEC_O(callee_save_info_t);
DEF_VEC_ALLOC_O(callee_save_info_t, heap);
typedef VEC(callee_save_info_t,heap) *vec_callee_save_info_t;
#define new_vec_callee_save_info() VEC_alloc(callee_save_info_t, heap, 0)
#define new_vec_callee_save_info_n(_num) \
    VEC_alloc(callee_save_info_t, heap, (_num));


/* Information about functions.  One instance of this per function. */
typedef struct GTY(()) func_info_t
{
    int id;
    const char *name;
    struct function *func;          /* Only use before rtl passes */
    tree func_decl;
    unsigned long stack_frame_size; /* Bytes                      */
    tree func_info_entry;
    VEC(tree,heap) *local_decls;
    VEC(tree,heap) *formal_param_decls;
    vec_var_info_t var_info;
    vec_callee_save_info_t callee_save_info;
} func_info_t;
DEF_VEC_O(func_info_t);
DEF_VEC_ALLOC_O(func_info_t, heap);
typedef VEC(func_info_t,heap) *vec_func_info_t;
#define new_vec_func_info() VEC_alloc(func_info_t, heap, 0)
#define new_vec_func_info_n(_num) VEC_alloc(func_info_t, heap, (_num))


/* Information about each callsite of a function */
typedef struct GTY(()) call_info_t
{
    unsigned id;
    tree return_callsite_addr;
    gimple stmt;
    func_info_t *caller_func;
    vec_reg_info_t register_info;
} call_info_t;
DEF_VEC_O(call_info_t);
DEF_VEC_ALLOC_O(call_info_t, heap);
typedef VEC(call_info_t,heap) *vec_call_info_t;
#define new_vec_call_info() VEC_alloc(call_info_t, heap, 0)
#define new_vec_call_info_n(_num) VEC_alloc(call_info_t, heap, (_num))


/* Main container for all public crap operations */
struct _crap_d
{
    vec_type_info_t type_infos;
    vec_func_info_t func_infos;
    vec_call_info_t call_infos;
    VEC(tree,heap) *global_vars;

    /* Vector of function decls that we want to get register information for.
     * This means, at each call in the program to any funcdecl in this vector,
     * we obtain register information.  This information is specific to each
     * call site.
     */
    VEC(tree,heap) *get_registers_at_calls_to;

    /* Vector of types to ignore.  These are types that are not to
     * be garbage collected, such as types inserted by the compiler for memory
     * management.
     */
    VEC(tree,heap) *types_to_ignore;

    /* Vector of function decls to ignore callsite of.  Labels at call sites to
     * any function in this list will not be inserted.
     */
    VEC(tree,heap) *ignore_fn_ids;

    /* True: the type info table has been built & inserted into the program */
    bool has_built_type_info_table;
};
typedef struct _crap_d crap_t;


/* Create a context which holds type information function info */
extern crap_t *crap_new_context(void);


/* Set the context for what the crap passes will use.  Some of the passes need
 * access to global data, including a crap_t context.
 */
extern void crap_plug_set_context(crap_t *context);


/* Remove garbage collection data for the specified function */
extern void crap_remove_info(crap_t *context, const_tree fndecl);


/* Load garbage collection information from an object file */
extern void crap_import_info(crap_t *context, const char *fname); 


/* Given a tree node generate type information */
extern type_info_t *crap_new_type_info(crap_t *context, const_tree node);


/* Get the number of types represented in the type information table */
extern unsigned crap_get_num_type_infos(const crap_t *context);


/* Get the type information object associated to the type */
extern type_info_t *crap_get_type_info(
    const crap_t *context,
    const_tree    type);


/* Get the type info table index for the passed type info object */
extern int crap_get_type_info_index(crap_t *context, const type_info_t *ti);


/* Add a function declaration to our get-registers list.  If a function
 * declaration is in this list, at each call to this function (each callsite)
 * the register information is obtained.  This should be done just after the
 * context is created.
 */
extern void crap_get_registers_at_calls_to(crap_t *context, tree fndecl);


/* Register a data type for the garbage collector to ignore */
extern void crap_register_type_to_ignore(crap_t *context, tree type);


/* Ignore function calls to the specified function IDENTIFIER.  Garbage
 * collection labels will not be inserted for calls to fndecl.  This can also be
 * an identifier node.
 */
extern void crap_register_fn_to_ignore(crap_t *context, tree id);


/* Useful for debugging */
extern const char *crap_get_type_name(const_tree type);


/* Determine if two types are the same/compatible */
extern bool crap_is_same_type_p(const_tree type_a, const_tree type_b);


/* Given a function node generate function call information */
extern call_info_t *crap_new_call_info(
    crap_t          *context, 
    struct function *caller_func,
    gimple           stmt);


/* Generate type information regarding the global variables in the program */
extern void crap_create_global_type_info(crap_t *context);


/* Generate the func info table (contains root variables for the fn's stack) */
extern void crap_create_function_info_table(crap_t *context);


/* Generate a table with all global variable information */
extern void crap_create_global_info_table(crap_t *context);


/* Generate a table with the label and their function information data */
extern void crap_create_label_layout_table(crap_t *context);


/* GCC plugin routines for inserting the goto labels */
extern void crap_insert_labels(crap_t *context);


/* Insert the type information table */
extern void crap_insert_type_info_table(crap_t *context);


/* Plugin pass for obtaining size information.  This must be called once the
 * function information has been obtained.  This will fill in stack-frame size
 * information.  'crap_plug_set_context()' must be called before so that the
 * pass initialized in this routine will use the proper context (which contains
 * function data that need their stack frame size information populated.
 * Returns '0' on success.
 */
extern void crap_plug_init_frame_size_pass(void);


/* Plugin pass for obtaining and inserting the type information, call site, and
 * function information into the binary.  This pass is an RTL pass and needs to
 * be done after type expansion (basically just before gcc converts the code to
 * assembly, since size information for data types is needed.
 */
extern void crap_plug_init_insert_tables_pass(void);


/* Generate type information needed for collecting global variables */
extern void crap_plug_init_collect_globals_pass(void);


#endif /* _CRAP_H */
