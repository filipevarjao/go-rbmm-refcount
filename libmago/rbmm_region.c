#include <gcc-plugin.h>
#include <coretypes.h>
#include <ipa-prop.h>
#include <cgraph.h>

#include <tree-dump.h>
#include <tree-flow.h>
#include <tree-flow-inline.h>
#include <tree-iterator.h>
#include "rbmm.h"


/* global region variable: One per program */
static tree global_region_var;


/* Types used in the compiler and emitted into code */
tree __rbmm_region_type = NULL_TREE;
tree __rbmm_open_array_type = NULL_TREE;


static void init_open_array_type(void)
{
    tree field, prev_field;
    
    if (__rbmm_open_array_type)
      return;

    /* From libgo runtime 4.7.0 "array.h"
     * struct {
     * void *__values;
     * int   __count;
     * int   __capacity;
     * };
     */

    field = build_decl(BUILTINS_LOCATION, FIELD_DECL,
                       get_identifier("__values"), ptr_type_node);
    DECL_CHAIN(field) = NULL_TREE;
    prev_field = field;
    
    field = build_decl(BUILTINS_LOCATION, FIELD_DECL,
                       get_identifier("__count"), integer_type_node);
    DECL_CHAIN(field) = prev_field;
    prev_field = field;

    field = build_decl(BUILTINS_LOCATION, FIELD_DECL,
                       get_identifier("__capacity"), integer_type_node);
    DECL_CHAIN(field) = prev_field;
    prev_field = field;

    __rbmm_open_array_type = make_node(RECORD_TYPE);
    finish_builtin_struct(__rbmm_open_array_type,
                          "__rbmm_open_array_type", field, NULL_TREE);
}


static void init_region_type(void)
{
    tree field, prev_field;
    
    if (__rbmm_region_type)
      return;

    /* Based on type we pass around at runtime '_region_t' in rbmm_builtins.c
     * NOTE: Since we only pass a pointer, we only really need to represent the
     * member fields of '_region_t' up to the increment.  If we try to access
     * any other fields after that we will need to add them here.
     */
    field = build_decl(BUILTINS_LOCATION, FIELD_DECL,
                       get_identifier("ref_count"), long_long_unsigned_type_node);
    DECL_CHAIN(field) = NULL_TREE;
    prev_field = field;
    
    field = build_decl(BUILTINS_LOCATION, FIELD_DECL,
                       get_identifier("id"), integer_type_node);
    DECL_CHAIN(field) = prev_field;
    prev_field = field;

    field = build_decl(BUILTINS_LOCATION, FIELD_DECL,
                       get_identifier("old_id"), integer_type_node);
    DECL_CHAIN(field) = prev_field;
    prev_field = field;
    
    __rbmm_region_type = make_node(RECORD_TYPE);
    finish_builtin_struct(__rbmm_region_type, "__rbmm_region_type", field, NULL_TREE);

    /* Make a pointer to a rbmm_region_type treat this data type as a pointer.
     * at runtime __rbmm_create_region is used to allocate and return a pointer
     * to the region data type.
     */
    __rbmm_region_type = build_pointer_type(__rbmm_region_type);
}


static void init_rbmm_types(void)
{
    init_region_type();
    init_open_array_type();
}


/* Initialize builtins by creating their fn decls (this can be automated) */
static tree create_region_fndecl;
static tree create_global_region_fndecl;
static tree create_map_fndecl;
static tree map_index_fndecl;
static tree slice1_make_fndecl;
static tree slice2_make_fndecl;
static tree append_fndecl;
static tree alloc_from_region_fndecl;
static tree alloc_slice_values_fndecl;
static tree remove_region_fndecl;
static tree merge_regions_fndecl;
static tree print_stats_fndecl;
static tree trampoline_fndecl;
static tree construct_map_fndecl;
static tree inc_counter_fndecl;
static tree dec_counter_fndecl;
static tree builtins[13]; /* Number of fndecls */
void init_rbmm_builtins(void)
{
    tree p_i_b_fn_type, p_noargs_fn_type, p_p_size_p_fn_type, void_p_fn_type;
    tree void_void_fn_type, p_p_p_size_fn_type;
    tree p_p_p_p_bool_fn_type, void_p_p_fn_type;
    tree oa_p_p_p_ip_fn_type, oa_p_p_p_ip_ip_fn_type, oa_p_oa_p_ip_ip_fn_type;
    tree p_p_p_ip_ip_ip_ip_p_fn_type;
    static bool initted = false;

    if (initted)
      return;

    /* Initalize the region type */
    init_rbmm_types();

    /* Function prototypes */
	p_noargs_fn_type = 
		build_function_type_list(__rbmm_region_type, NULL_TREE);
    p_i_b_fn_type =
        build_function_type_list(
		__rbmm_region_type, integer_type_node, boolean_type_node, NULL_TREE);
    void_p_fn_type = 
        build_function_type_list(void_type_node, ptr_type_node, NULL_TREE);
    void_void_fn_type = 
        build_function_type_list(void_type_node, NULL_TREE);
    p_p_size_p_fn_type = build_function_type_list(
        ptr_type_node, ptr_type_node, size_type_node, ptr_type_node,NULL_TREE);
    p_p_p_size_fn_type = build_function_type_list(
       ptr_type_node, ptr_type_node, ptr_type_node, size_type_node, NULL_TREE);
    p_p_p_p_bool_fn_type = build_function_type_list(
       ptr_type_node, ptr_type_node, ptr_type_node, ptr_type_node,
       boolean_type_node, NULL_TREE);
    void_p_p_fn_type = build_function_type_list(
       void_type_node, ptr_type_node, ptr_type_node, NULL_TREE);
    oa_p_p_p_ip_fn_type = build_function_type_list(
       __rbmm_open_array_type, ptr_type_node, ptr_type_node, ptr_type_node,
       integer_ptr_type_node, NULL_TREE);
    oa_p_p_p_ip_ip_fn_type = build_function_type_list(
       __rbmm_open_array_type, ptr_type_node, ptr_type_node, ptr_type_node,
       integer_ptr_type_node, integer_ptr_type_node, NULL_TREE);
    oa_p_oa_p_ip_ip_fn_type = build_function_type_list(
        __rbmm_open_array_type, ptr_type_node, __rbmm_open_array_type,
        ptr_type_node, integer_ptr_type_node, integer_ptr_type_node, NULL_TREE);
    p_p_p_ip_ip_ip_ip_p_fn_type = build_function_type_list(
        ptr_type_node, ptr_type_node, ptr_type_node,
        integer_ptr_type_node, integer_ptr_type_node,
        integer_ptr_type_node, integer_ptr_type_node,
        ptr_type_node, NULL_TREE);

    //* Function declarations (using prototypes) */
    create_region_fndecl =
        build_fn_decl("__rbmm_create_region", p_i_b_fn_type);
    create_global_region_fndecl =
        build_fn_decl("__rbmm_create_global_region", p_noargs_fn_type);
    create_map_fndecl =
        build_fn_decl("__rbmm_new_map", p_p_p_size_fn_type);
    map_index_fndecl =
        build_fn_decl("__rbmm_map_index", p_p_p_p_bool_fn_type);
    slice1_make_fndecl =
        build_fn_decl("__rbmm_make_slice1", oa_p_p_p_ip_fn_type);
    slice2_make_fndecl =
        build_fn_decl("__rbmm_make_slice2", oa_p_p_p_ip_ip_fn_type);
    append_fndecl = 
        build_fn_decl("__rbmm_append", oa_p_oa_p_ip_ip_fn_type);
    alloc_from_region_fndecl =
        build_fn_decl("__rbmm_alloc_from_region", p_p_size_p_fn_type);
    alloc_slice_values_fndecl =
        build_fn_decl("__rbmm_alloc_slice_values", p_p_size_p_fn_type);
    remove_region_fndecl =
        build_fn_decl("__rbmm_remove_region", void_p_fn_type);
    merge_regions_fndecl = 
        build_fn_decl("__rbmm_merge_regions", void_p_p_fn_type);
    print_stats_fndecl =
        build_fn_decl("__rbmm_print_stats", void_void_fn_type);
    trampoline_fndecl =
        build_fn_decl("__rbmm_trampoline", void_p_fn_type);
    construct_map_fndecl =
        build_fn_decl("__rbmm_construct_map", p_p_p_ip_ip_ip_ip_p_fn_type);
    inc_counter_fndecl = 
        build_fn_decl("__rbmm_inc", void_p_fn_type);
    dec_counter_fndecl = 
        build_fn_decl("__rbmm_dec", void_p_fn_type);
    
    /* Build a list of builtins, just for convience */
    builtins[0] = create_region_fndecl;
    builtins[1] = create_global_region_fndecl;
    builtins[2] = create_map_fndecl;
    builtins[3] = map_index_fndecl;
    builtins[4] = slice1_make_fndecl;
    builtins[5] = slice2_make_fndecl;
    builtins[6] = append_fndecl;
    builtins[7] = alloc_from_region_fndecl;
    builtins[8] = remove_region_fndecl;
    builtins[9] = merge_regions_fndecl;
    builtins[10] = print_stats_fndecl;
    builtins[11] = trampoline_fndecl;
    builtins[12] = alloc_slice_values_fndecl;
    initted = true;
}


tree get_alloc_from_region_fndecl(void)
{
    return alloc_from_region_fndecl;
}


bool is_rbmm_builtin(const_tree fndecl)
{
    int i;

    for (i=0; i<sizeof(builtins)/sizeof(builtins[0]); ++i)
      if (fndecl == builtins[i])
        return true;

    return false;
}


static tree create_global_var(const char *name, bool is_extern)
{
    tree node, void_node;
    init_rbmm_types();

    /* Create a global variable, thanks to "init_ic_make_global_vars()" */
    void_node = build_pointer_type(void_type_node);
    node = build_decl(BUILTINS_LOCATION, VAR_DECL, NULL, void_node);
    DECL_NAME(node) = get_identifier(name);
    TREE_STATIC(node) = 1;
    TREE_PUBLIC(node) = 1;
    DECL_ARTIFICIAL(node) = 1;
    DECL_EXTERNAL(node) = is_extern;

    if (!is_extern)
    {
        varpool_finalize_decl(node);
        varpool_mark_needed_node(varpool_node(node));
    }
   
    return node;
}


static inline tree create_local_var(const char *name, gimple stmt)
{
    tree tmp;
    init_rbmm_types();
    tmp = create_tmp_var(__rbmm_region_type, name);
    return make_ssa_name(tmp, stmt);
}


void init_global_region_data(bool is_extern)
{
    global_region_var = create_global_var("MATT_GLOBAL", is_extern);
    global_region->region_var = global_region_var;
}


/* Locate the type of '__values' member */
const_tree get_slice_type(const_tree slice)
{ 
    /* Strip field decl and initial ptr type */
    return TREE_TYPE(TREE_TYPE(TYPE_FIELDS(slice)));
}


#if 0
static int find_subregion_idx(const region_t reg, const_tree type)
{
    unsigned i;
    tree ti;

    FOR_EACH_VEC_ELT(tree, reg->unique_types, i, ti)
      if (ti == type)
        return i;

    return -1; /* Possibly abort/assert here */
}
#endif


void create_global_region_bn(gimple stmt)
{
#ifndef RBMM_GC
    gimple call;
    gimple_stmt_iterator gsi;
#endif

    init_global_region_data(false);

#ifdef RBMM_GC
    return create_region_bn(stmt, global_region);

#else /* Normal non-rbmm-gc below... */

    /* Build the lhs (var) and the call (rhs) */
    call = gimple_build_call(create_global_region_fndecl, 0);
    gimple_call_set_lhs(call, global_region_var);

    /* Insert the assignment statement */
    gsi = gsi_for_stmt(stmt);
    gsi_insert_before(&gsi, call, GSI_NEW_STMT);
#endif  /* RBMM_GC */
}


/* Insert a call to our builtin function before stmt */
void create_region_bn(gimple stmt, region_t reg)
{
    int highest_type_idx;
    tree var, max_type_var, is_global;
    char name[32];
    gimple call;
    gimple_stmt_iterator gsi;

#ifndef RBMM_GC /* If normal rbmm-no-gc mode */
    if (ALIASED_BY_GLOBAL_P(reg))
      return;
#endif
    /* Build the lhs (var) if we do not have one already (we might be moving a
     * create statement and in that case we would alreay have created a var)
     */
    if (!reg->region_var)
    {
        snprintf(name, sizeof(name), "MATT_RBMM_%d", reg->id);
        var = create_local_var(name, stmt);
    }
    else
      var = reg->region_var;

    /* Calculate the highest type index */
    highest_type_idx = rbmm_gc_find_max_type_index(reg);
    if (highest_type_idx == -1 && GLOBAL_REGION_P(reg))
      highest_type_idx = rbmm_gc_n_type_infos();

    /* is_global arg (only for garbage collection) */
    if (ALIASED_BY_GLOBAL_P(reg))
      is_global = boolean_true_node;
    else
      is_global = boolean_false_node;

    /* Build the call */
    max_type_var = build_int_cst(integer_type_node, highest_type_idx);
    call = gimple_build_call(create_region_fndecl, 2, max_type_var, is_global);
    gimple_call_set_lhs(call, var);

    reg->created_stmt = call;
    reg->region_var = var;

    /* Insert the assignment statement */
    gsi = gsi_for_stmt(stmt);
    gsi_insert_before(&gsi, call, GSI_NEW_STMT);
}


/* Insert a call to our builtin function responsible for creating a go map */
void create_map_bn(region_t reg, gimple stmt)
{
    gimple call;
    gimple_stmt_iterator gsi;
    tree descriptor_arg, entries_arg;

    descriptor_arg = gimple_call_arg(stmt, 0);
    entries_arg = gimple_call_arg(stmt, 1);

    call = gimple_build_call(
        create_map_fndecl, 3, reg->region_var, descriptor_arg, entries_arg);

    gsi = gsi_for_stmt(stmt);
    gimple_call_set_lhs(call, gimple_call_lhs(stmt));
    gsi_replace(&gsi, call, true);
}


/* Insert a call to our builtin function responsible for indexing a map */
void map_index_bn(region_t reg, gimple stmt)
{
    gimple call;
    gimple_stmt_iterator gsi;
    tree map_arg, key_arg, insert_arg;

    map_arg = gimple_call_arg(stmt, 0);
    key_arg = gimple_call_arg(stmt, 1);
    insert_arg = gimple_call_arg(stmt, 2);

    call = gimple_build_call(
        map_index_fndecl, 4, reg->region_var, map_arg, key_arg, insert_arg);

    gsi = gsi_for_stmt(stmt);
    gimple_call_set_lhs(call, gimple_call_lhs(stmt));
    gsi_replace(&gsi, call, true);
}

#ifdef RBMM_GC
static const_tree strip_pointer_type(const_tree type)
{
    while (POINTER_TYPE_P(type))
      type = TREE_TYPE(type);
    return type;
}
#endif


/* Find the type stored in the slice */
#ifdef RBMM_GC
static const_tree get_non_voidptr_slice_type(rbmm_fn_data_t fn, const_tree slice)
{
    unsigned i;
    ssa_name_info_t info;
    const_tree vals_member, type;
    VEC(ssa_name_info_t,gc) *pts_to;

    if (!is_slice_type_p(slice))
      return NULL_TREE;

    /* Get values member */
    vals_member = get_slice_type(TREE_TYPE(slice));
    type = strip_pointer_type(slice);
    if (vals_member != void_type_node)
      return slice;//vals_member;

    /* Get points to and other relations */
    pts_to = get_who_points_to(fn, find_ssa_name_info(fn, slice, NULL_TREE));
    FOR_EACH_VEC_ELT(ssa_name_info_t, pts_to, i, info)
    {
        type = strip_pointer_type(TREE_TYPE(info->ssaname));
        if (!is_slice_type_p(type))
          continue;
        type = get_slice_type(type);
        if (type != void_type_node)
        {
            VEC_free(ssa_name_info_t, gc, pts_to);
            return TREE_TYPE(info->ssaname);//type;
        }
    }

    VEC_free(ssa_name_info_t, gc, pts_to);
    return NULL_TREE;
}
#endif /* RBMM_GC */


/* Insert a call to our builtin function responsible for indexing a map */
void slice1_make_bn(rbmm_fn_data_t fn, region_t reg, gimple stmt)
{
    gimple call;
    gimple_stmt_iterator gsi;
    tree arg1, arg2, sub;
    const_tree sub_type;

    /* Get lhs type (sub region index)
     * The Go front end will usually treat the lhs as a slice for anytype, e.g.
     * void *.  Look through the actual points-to of lhs and see if we can find
     * a slice without that void *.
     */
#ifdef RBMM_GC
    const_tree lhs_type = get_non_voidptr_slice_type(fn, gimple_get_lhs(stmt));
    //const_tree lhs_type = TREE_TYPE(gimple_get_lhs(stmt));
    sub_type = rbmm_gc_find_subregion_type(lhs_type);
    sub = build_decl(BUILTINS_LOCATION,VAR_DECL,(tree)sub_type,ptr_type_node);
    DECL_EXTERNAL(sub) = 1;
    sub = build1(ADDR_EXPR, ptr_type_node, sub);
#else
    sub = null_pointer_node;
#endif
    
    arg1 = gimple_call_arg(stmt, 0);
    arg2 = gimple_call_arg(stmt, 1);
    call = gimple_build_call(
        slice1_make_fndecl, 4, reg->region_var, sub, arg1, arg2);

    gsi = gsi_for_stmt(stmt);
    gimple_call_set_lhs(call, gimple_call_lhs(stmt));
    gsi_replace(&gsi, call, true);
}


/* Insert a call to our builtin function responsible for indexing a map */
void slice2_make_bn(rbmm_fn_data_t fn, region_t reg, gimple stmt)
{
    gimple call;
    gimple_stmt_iterator gsi;
    tree arg1, arg2, arg3, sub;
    const_tree sub_type;

    /* Get lhs type (sub region index) */
#ifdef RBMM_GC
    const_tree lhs_type = get_non_voidptr_slice_type(fn, gimple_get_lhs(stmt));
    //const_tree lhs_type = TREE_TYPE(gimple_get_lhs(stmt));
    sub_type = rbmm_gc_find_subregion_type(lhs_type);
    sub = build_decl(BUILTINS_LOCATION,VAR_DECL,(tree)sub_type,ptr_type_node);
    DECL_EXTERNAL(sub) = 1;
    sub = build1(ADDR_EXPR, ptr_type_node, sub);
#else
    sub = null_pointer_node;
#endif

    arg1 = gimple_call_arg(stmt, 0);
    arg2 = gimple_call_arg(stmt, 1);
    arg3 = gimple_call_arg(stmt, 2);
    call = gimple_build_call(
        slice2_make_fndecl, 5, reg->region_var, sub, arg1, arg2, arg3);

    gsi = gsi_for_stmt(stmt);
    gimple_call_set_lhs(call, gimple_call_lhs(stmt));
    gsi_replace(&gsi, call, true);
}


/* Insert a call to our builtin function responsible for appending slices */
void append_bn(region_t reg, gimple stmt)
{
    gimple call;
    gimple_stmt_iterator gsi;
    tree arg1, arg2, arg3, arg4, sub;
    const_tree sub_type;
    
    arg1 = gimple_call_arg(stmt, 0);
    arg2 = gimple_call_arg(stmt, 1);
    arg3 = gimple_call_arg(stmt, 2);
    arg4 = gimple_call_arg(stmt, 3);

    /* Get lhs type (sub region index) */
#ifdef RBMM_GC
    tree lhs = gimple_get_lhs(stmt);
    sub_type = rbmm_gc_find_subregion_type(TREE_TYPE(lhs));
    sub = build_decl(BUILTINS_LOCATION,VAR_DECL,(tree)sub_type,ptr_type_node);
    DECL_EXTERNAL(sub) = 1;
    sub = build1(ADDR_EXPR, ptr_type_node, sub);
#else
    sub = null_pointer_node;
#endif

    call = gimple_build_call(
        append_fndecl, 6, reg->region_var, sub, arg1, arg2, arg3, arg4);

    gimple_call_set_lhs(call, gimple_call_lhs(stmt));
    gsi = gsi_for_stmt(stmt);
    gsi_replace(&gsi, call, true);
}


/* If we alloc from the global region we have to pass a temp to it,
 * The temporary local being assigned to is returned
 */
tree assign_global_to_temp(gimple stmt)
{
    tree var;
    gimple ass;
    gimple_stmt_iterator gsi;

    var = create_local_var("MATT_TMP", stmt);
    ass = gimple_build_assign_stat(var, global_region->region_var);
    gsi = gsi_for_stmt(stmt);
    gsi_insert_before(&gsi, ass, GSI_NEW_STMT);
    return var;
}


/* Insert a call to our builtin function before stmt */
void alloc_from_region_bn(
    const rbmm_fn_data_t fn,
    gimple               stmt,
    region_t             reg,
    tree                 lhs,
    tree                 size)
{
    tree var;
    const_tree type, sub;
    ssa_name_info_t info;
    gimple call;
    gimple_stmt_iterator gsi;

    var = reg->region_var;
    if ((info = find_ssa_name_in_region(reg, lhs)))
      type = find_non_void_ptr(fn, info);
    else
      type = TREE_TYPE(lhs);

    while (POINTER_TYPE_P(type))
      type = TREE_TYPE(type);

    /* Find the sub region and insert call */
#ifdef RBMM_GC
    sub = rbmm_gc_find_subregion_type(type);
    sub = build_decl(BUILTINS_LOCATION,	VAR_DECL, (tree)sub, ptr_type_node);
    DECL_EXTERNAL((tree)sub) = 1;
    sub = build1(ADDR_EXPR, ptr_type_node, (tree)sub);

    if (ALIASED_BY_SLICE_P(info))
      printf("ALIASED BY SLICE: %s\n", get_ssaname(info));

#else
    sub = null_pointer_node;
#endif
    gsi = gsi_for_stmt(stmt);
    call = gimple_build_call(alloc_from_region_fndecl,3,var,size,sub);
    gimple_call_set_lhs(call, lhs);
    gsi_replace(&gsi, call, true);
}


#ifdef RBMM_GC
void alloc_slice_values_bn(rbmm_fn_data_t fn, region_t reg, gimple stmt)
{
    tree var, size, lhs;
    const_tree type, sub;
    ssa_name_info_t info;
    gimple call;
    gimple_stmt_iterator gsi;

    lhs = gimple_get_lhs(stmt);
    var = reg->region_var;
    size = gimple_call_arg(stmt, 0);

    if ((info = find_ssa_name_in_region(reg, lhs)))
      type = find_non_void_ptr(fn, info);
    else
      type = TREE_TYPE(lhs);

    while (POINTER_TYPE_P(type))
      type = TREE_TYPE(type);

    /* Find the sub region and insert call */
    sub = rbmm_gc_find_subregion_type(type);
    sub = build_decl(BUILTINS_LOCATION,	VAR_DECL, (tree)sub, ptr_type_node);
    DECL_EXTERNAL((tree)sub) = 1;
    sub = build1(ADDR_EXPR, ptr_type_node, (tree)sub);
    gsi = gsi_for_stmt(stmt);
    call = gimple_build_call(alloc_slice_values_fndecl, 3, var, size, sub);
    gimple_call_set_lhs(call, lhs);
    gsi_replace(&gsi, call, true);
}
#endif /* RBMM_GC */


/* Insert a call to our builtin function responsible for region destruction */
void remove_region_bn(region_t reg, basic_block bb)
{
    tree var;
    gimple call, stmt;
    gimple_stmt_iterator gsi;

    /* This occurs sometimes and I'm not entirely sure why */
    if (!reg->region_var)
      return;

    /* Emit builtin as the last call to bb or before the last if its a goto */
    gsi = gsi_last_bb(bb);
    stmt = gsi_stmt(gsi);

    if (GLOBAL_REGION_P(reg))
      return;

    var = reg->region_var;
    call = gimple_build_call(remove_region_fndecl, 1, var);

    if (lookup_stmt_eh_lp(stmt) > 0 && gsi_one_before_end_p(gsi))
      remove_stmt_from_eh_lp(stmt);

    /* If its "__go_runtime_error" kill region before we kill the program
     * Sure, this seems bad, but we need it so the CFG verifier remains happy
     */
    if (gimple_code(stmt) == GIMPLE_GOTO   ||
        gimple_code(stmt) == GIMPLE_RETURN ||
        gimple_code(stmt) == GIMPLE_COND   ||
        gimple_code(stmt) == GIMPLE_RESX   ||
        (gimple_code(stmt) == GIMPLE_CALL && is_ctrl_altering_stmt(stmt)))
      gsi_insert_before(&gsi, call, GSI_NEW_STMT);
    else
      gsi_insert_after(&gsi, call, GSI_NEW_STMT);
}


/* Insert call to our builtin func for inc/decrementing the regions ref count
 * This is inline'd:
 *     region->ref_count = region->ref_count + 1;
 * If "decrement" is "true" then a subtraction operator would replace the plus
 */
void incdec_ref_count_bn(region_t reg, gimple stmt, bool decrement)
{
    enum tree_code op;
    tree field, member, tmp0, tmp1;
    gimple call0, call1, call2;
    gimple_stmt_iterator gsi;

    /* The following is for perfomring inline insertions */
    if (GLOBAL_REGION_P(reg))
      return;

    op = (decrement) ? MINUS_EXPR: PLUS_EXPR;
    field = TYPE_FIELDS(TREE_TYPE(TREE_TYPE(reg->region_var)));

    for ( ; field; field = DECL_CHAIN(field))
      if (get_name(field) && (get_identifier(get_name(field)) ==
                  get_identifier("ref_count")))
        break;

    /* tmp = region_var->ref_count; */
    tmp0 = create_tmp_var(long_long_unsigned_type_node, "MATT_TMP");
    tmp0 = make_ssa_name(tmp0, gimple_build_nop());
    member = build3(COMPONENT_REF, long_long_unsigned_type_node,
            build_simple_mem_ref(reg->region_var), field, NULL_TREE);
    call0 = gimple_build_assign(tmp0, member);
   
    /* tmp1 = tmp0 + 1 */
    tmp1 = create_tmp_var(long_long_unsigned_type_node, "MATT_TMP");
    tmp1 = make_ssa_name(tmp1, gimple_build_nop());
    call1 = gimple_build_assign_with_ops(
        op, tmp1, tmp0, build_one_cst(long_long_unsigned_type_node));

    /* region_var->ref_count = tmp1 */
    call2 = gimple_build_assign(unshare_expr(member), tmp1);

    /* If we are 'decrementing' make sure we do not place the decrements AFTER a
     * 'return' In such a case we just say we are 'incrementing' even though the
     * PLUS/MINUS op has already been decided by this point.
     */
    if (decrement && (gimple_code(stmt) == GIMPLE_RETURN))
      decrement = false;

    gsi = gsi_for_stmt(stmt);
    if (decrement)
    {
        ++reg->n_protection_decrements;

        /* Remove error handling landing pad (if not gcc will be angry that we
         * insert one before the end on an landing pad edge
         */
        if (lookup_stmt_eh_lp(stmt) > 0 && gsi_one_before_end_p(gsi))
          remove_stmt_from_eh_lp(stmt);

        //call0 = gimple_build_call(dec_counter_fndecl, 1, reg->region_var);
        //gsi_insert_after(&gsi, call0, GSI_NEW_STMT);

        gsi_insert_after(&gsi, call0, GSI_NEW_STMT);
        gsi_insert_after(&gsi, call1, GSI_NEW_STMT);
        gsi_insert_after(&gsi, call2, GSI_NEW_STMT);
    }
    else /* Increment */
    {
        ++reg->n_protection_increments;
        //call0 = gimple_build_call(inc_counter_fndecl, 1, reg->region_var);
        //gsi_insert_before(&gsi, call0, GSI_NEW_STMT);

        gsi_insert_before(&gsi, call2, GSI_NEW_STMT);
        gsi_insert_before(&gsi, call1, GSI_NEW_STMT);
        gsi_insert_before(&gsi, call0, GSI_NEW_STMT);
    }
    update_stmt(stmt);
}


#ifdef ENABLE_RUNTIME_MERGING
/* Insert call to our builtin func for merging regions */
void merge_region_bn(region_t parent, region_t child, gimple stmt)
{
    gimple call;
    gimple_stmt_iterator gsi;

    if (!parent->region_var || !child->region_var)
      return;
    call = gimple_build_call(merge_regions_fndecl, 2,
                             parent->region_var, child->region_var);
    gsi = gsi_for_stmt(stmt);
    gsi_insert_before(&gsi, call, GSI_NEW_STMT);
}
#endif


/* Insert call to print region statistics */
void print_stats_bn(gimple stmt)
{
    gimple call;
    gimple_stmt_iterator gsi;

    call = gimple_build_call(print_stats_fndecl, 0);
    gsi = gsi_for_stmt(stmt);
    gsi_insert_before(&gsi, call, GSI_NEW_STMT);
}


/* Create and add region to func.  This does not add a region to the code that
 * is being analyized.  That is what 'create_region_bn()' is for.
 */
region_t new_region_info(rbmm_fn_data_t fn, unsigned region_id)
{
    region_t reg;

    reg = XCNEW(struct _region_d);
    reg->id = region_id;
    reg->param_idx = -1;
    reg->region_var_idx = -1;

    /* Add region to func (always true unless called from init_global_reg */
    if (fn)
      VEC_safe_push(region_t, heap, fn->regions, reg);

    return reg;
}


/* Create a temporary region (usually to pass to functions that allocat memory
 * for a variable that they return, but the caller does not actually need the
 * return value.  This should be more efficient than using the
 * global-garbage-collected region.
 * The 'type' parameter represents the data type the region is for.  This helps
 * us associate garbage collection information to the temp region.
 */
region_t new_temp_region_info(rbmm_fn_data_t fn, tree type)
{
    static int dec = -42;
    region_t reg = new_region_info(fn, dec--);
    rbmm_gc_add_unique_type(reg, type);
    return reg;
}


/* Given a region and a name, report TRUE if the name is in the region */
static bool ssa_name_exists(const region_t reg, const ssa_name_info_t info)
{
    unsigned i;
    ssa_name_info_t test;

    FOR_EACH_VEC_ELT(ssa_name_info_t, reg->names, i, test)
      if (test == info)
        return true;

    return false;
}


/* Given a region and a basic block, return TRUE if that block is in the
 * region's set of live blocs
 */
static bool live_block_exists(const region_t reg, const basic_block bb)
{
    unsigned i;
    basic_block test;

    FOR_EACH_VEC_ELT(basic_block, reg->live_bbs, i, test)
      if (test == bb)
        return true;

    return false;
}


/* Remove a region from our analysis */
static void remove_region(rbmm_fn_data_t fn, region_t reg)
{
    unsigned i;
    region_t r;

    FOR_EACH_VEC_ELT(region_t, fn->regions, i, r)
      if (r == reg)
        VEC_ordered_remove(region_t, fn->regions, i);
}


/* Merge the two regions, put food into mouth */
void merge_regions(rbmm_fn_data_t fn, region_t mouth, region_t food)
{
    unsigned i;
    basic_block bb;
    ssa_name_info_t info;

    if (mouth == food)
      return;

    mouth->aliased_state |= food->aliased_state;

    /* Variable and param index info */
    if (!mouth->region_var)
      mouth->region_var = food->region_var;
    if (mouth->param_idx == -1)
      mouth->param_idx = food->param_idx;
    if (mouth->region_var_idx == -1)
      mouth->region_var_idx = food->region_var_idx;

    /* Unique (add name from food to mouth and change its id) */ 
    FOR_EACH_VEC_ELT(ssa_name_info_t, food->names, i, info)
    {
        if (!(ssa_name_exists(mouth, info)))
        {
            info->region_uid = mouth->id;
            VEC_safe_push(ssa_name_info_t, heap, mouth->names, info);
        }

        /* If we find a name that exists in the region we are merging, update it
         * to use the parent (the consuming) region's id.  Add it if necessary.
         */
        if (info->region_uid == food->id)
        {
            info->region_uid = mouth->id;
            if (!ssa_name_exists(mouth, info))
              VEC_safe_push(ssa_name_info_t, heap, mouth->names, info);
        }
    }
    
    FOR_EACH_VEC_ELT(basic_block, food->live_bbs, i, bb)
      if (!(live_block_exists(mouth, bb)))
        VEC_safe_push(basic_block, heap, mouth->live_bbs, bb);

    remove_region(fn, food);
}


/* Uniquely add a region to a vector of regions */
void unique_add_to_region_vec(VEC(region_t,gc) **regions, region_t addme)
{
    unsigned i;
    region_t reg;

    FOR_EACH_VEC_ELT(region_t, *regions, i, reg)
      if (reg == addme)
        return;

    VEC_safe_push(region_t, gc, *regions, addme);
}


/* Unqiuely add the basic blocks from info, which info is live in, to reg's list
 * of bbs it is live in.
 */
void add_live_bbs(
    const rbmm_fn_data_t  fn,
    region_t              reg,
    const ssa_name_info_t info)
{
    unsigned i, j;
    bool is_unique;
    VEC(basic_block,gc) *live_bbs;
    region_t parent_reg;
    basic_block bb, test;

    /* Collect basic blocks associated with info */
    live_bbs = VEC_alloc(basic_block, gc, 0);
    FOR_EACH_VEC_ELT(basic_block, info->live_bbs, i, bb)
      VEC_safe_push(basic_block, gc, live_bbs, bb);

    /* Collect more blocks from the parent if 'info' is a member... if parent is
     * alive, then 'info' is reachable.
     * FIXME: 
     * The first version which uses UNIFIED_ID is bad we currently use the
     * version below the if 0 which uses the bbs from the regions of the parent
     */
#if 0 
    if (info->base)
    {
        parents = find_names_by_unified_id(fn, info->base->unified_id);
        FOR_EACH_VEC_ELT(ssa_name_info_t, parents, i, parent)
        {
            FOR_EACH_VEC_ELT(basic_block, parent->live_bbs, j, bb)
              VEC_safe_push(basic_block, gc, live_bbs, bb);
        }
    }
#endif
    if (info->base &&
        (parent_reg = find_region_by_id(fn, info->base->region_uid)))
    {
        FOR_EACH_VEC_ELT(basic_block, parent_reg->live_bbs, i, bb)
          VEC_safe_push(basic_block, gc, live_bbs, bb);
    }

    /* Uniquely add */
    FOR_EACH_VEC_ELT(basic_block, live_bbs, i, bb)
    {
        is_unique = true;
        FOR_EACH_VEC_ELT(basic_block, reg->live_bbs, j, test)
        {
            if (test == bb)
            {
                is_unique = false;
                break;
            }
        }

        if (is_unique)
          VEC_safe_push(basic_block, heap, reg->live_bbs, bb);
    }
}


/* Add the variable to the region */
void add_to_region(
    const rbmm_fn_data_t  fn,
    region_t              reg,
    const ssa_name_info_t info)
{
    unsigned i;
    ssa_name_info_t match;

    /* Share alias state */
    reg->aliased_state |= info->aliased_state;

    /* Add each (unique) bb that info is live in, which makes reg live */
    add_live_bbs(fn, reg, info);

    /* Reject if this name has already been added */
    FOR_EACH_VEC_ELT(ssa_name_info_t, reg->names, i, match)
      if (info == match)
        return;

    VEC_safe_push(ssa_name_info_t, heap, reg->names, info);
}


/* A variable cannot live in 2 regions, if its in two, merge the regions */
static void _fixup_duplicate_names(
    rbmm_fn_data_t  fn,
    region_t        reg,
    ssa_name_info_t info)
{
    unsigned i, j;
    merge_t *md;
    bool is_member, to_be_merged_at_runtime;
    region_t dup, temp;

    /* For each region that is not 'reg' see if info is in there */
    FOR_EACH_VEC_ELT(region_t, fn->regions, i, dup)
    {
        if (dup == reg)
          continue;
        
        /* Check to see if 'dup' has the info in it */
        if (!find_ssa_name_in_region(dup, info->ssaname))
          continue;

        if (dup->id != info->region_uid)
            return;

        /* If this is a nested type we will get here.  Nested types can have 2
         * separate regions. (Remember, multiple regions per var)
         */
        if (info->base && (dup == find_region_by_decl(fn, info->base->ssaname)))
          continue;
        
        /* Check to see if we are to merge these at runtime */
        to_be_merged_at_runtime = false;
        FOR_EACH_VEC_ELT(merge_t, fn->merge_data, j, md)
        {
            if ((md->parent == reg && md->child == dup) ||
                (md->parent == dup && md->child == reg))
             {
                 /* Make sure we dont try to runtime merge with global reg */
                 if (GLOBAL_REGION_P(reg) || GLOBAL_REGION_P(dup))
                 {
                     remove_merge_data(fn, md);
                     continue;
                 }
                 to_be_merged_at_runtime = true;
                 break;
             }
        }

        if (to_be_merged_at_runtime)
          continue;

        /* If the decl is a member/field then YES it can be in two regions, but have
         * two different ssaname representations.  The same GCC tree node but one is
         * a member of parentA and the other is a member of parentB.
         */
        if ((is_member = TREE_CODE(info->ssaname) == FIELD_DECL))
        {
            ssa_name_info_t s1 = find_ssa_name_in_region(reg, info->ssaname);
            ssa_name_info_t s2 = find_ssa_name_in_region(dup, info->ssaname);
            if (s1->base != s2->base)
              continue;
        }
        
        /* We have a duplicate region, so merge it at compile time (now) */
        if (reg->id > dup->id)
        {
            temp = reg;
            reg = dup;
            dup = temp;
        }

        merge_regions(fn, reg, dup);
    }
}


/* If a name is in more than one region, the regions need to be to be merged */
void fixup_duplicate_names(rbmm_fn_data_t fn)
{
    unsigned i, j;
    region_t reg;
    ssa_name_info_t info;

    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
      FOR_EACH_VEC_ELT(ssa_name_info_t, reg->names, j, info)
        _fixup_duplicate_names(fn, reg, info);
}


/* Given the region ID, get the region */
region_t find_region_by_id(const rbmm_fn_data_t fn, unsigned region_id)
{
    unsigned ii;
    region_t reg;

    for (ii=0; VEC_iterate(region_t, fn->regions, ii, reg); ++ii)
      if (reg->id == region_id)
        return reg;

    return NULL;
}


/* Given the decl in node, scan the function to see what region this puppy
 * belongs to.
 */
region_t find_region_by_decl(const rbmm_fn_data_t fn, const_tree decl)
{
    unsigned ii, vv;
    region_t reg;
    ssa_name_info_t var;

    if (!decl)
      return NULL;

    /* Scan the regions in fn */
    for (ii=0; VEC_iterate(region_t, fn->regions, ii, reg); ++ii)
      for (vv=0; VEC_iterate(ssa_name_info_t, reg->names, vv, var); ++vv)
        if (decl == var->ssaname)
          return reg;
        else if (decl == get_ssaname_decl(var))
	  return reg;
        else if (SSA_VAR_P(decl) && SSA_NAME_VAR(decl) == get_ssaname_decl(var))
	  return reg;
    
    return NULL;
}


/* Find first global region reference in this function */
region_t find_global_region_reference(const rbmm_fn_data_t fn)
{
    unsigned i;
    region_t reg;

    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
      if (ALIASED_BY_GLOBAL_P(reg))
        return reg;

    return NULL;
}


/* Given a function find the regions who is associated to the param_index */
VEC(region_t,gc) *find_regions_by_param_idx(
    const rbmm_fn_data_t fn, 
    int                  param_index)
{
    unsigned i;
    region_t reg;
    param_to_reg_t *pr;
    ssa_name_to_reg_t *n_to_reg;
    VEC(region_t,gc) *regs;

    regs = VEC_alloc(region_t, gc, 0);

    /* Regions for return value */
    if (param_index == -1)
    {
        if (fn->returned_parammap->regions)
          FOR_EACH_VEC_ELT(ssa_name_to_reg_t, fn->returned_parammap->regions,
                           i, n_to_reg)
            VEC_safe_push(region_t, gc, regs, n_to_reg->region);
    }
    else if (fn->parammap) /* Regions for parameters */
    {
        if (!(pr = VEC_index(param_to_reg_t, fn->parammap, param_index)))
          return regs;

        /* Get the regions for the param */
        FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
          if (find_ssa_name_in_region(reg, pr->param_decl))
            VEC_safe_push(region_t, gc, regs, reg);
    }

    return regs;
}


/* Given a function and a index for the signature of that function, return the
 * region.
 * Example: fn(a, b, c, RegionA, RegionB, RegionC)
 * find_region_by_fncall_index(fn, 4) would return RegionB
 * This is to be used with "find_region_index"
 */
region_t find_region_by_fncall_idx(const rbmm_fn_data_t fn, int region_index)
{
    unsigned i;
    region_t reg;

    FOR_EACH_VEC_ELT(region_t, fn->regions, i, reg)
      if (reg->region_var_idx == region_index)
        return reg;
}


static void print_aliased_state(const region_t reg)
{
    int i;

    RBMM_DEBUG_NO_NL(DEBUG_ANAL, "  Aliases: ");
    for (i=0; i<N_ALIASES; ++i)
      if (reg->aliased_state & aliases[i].value)
        RBMM_DEBUG_PLAIN(DEBUG_ANAL, "%s ", aliases[i].name);
    RBMM_DEBUG_NL();
}


static void print_region(const region_t reg)
{
    unsigned ii;
    basic_block bb;
    ssa_name_info_t info;

    RBMM_DEBUG(DEBUG_ANAL, 
               "--Region %d ParamIdx(%d) RegIdx(%d) AliasState(%d)--",
               reg->id,
               reg->param_idx,
               reg->region_var_idx,
               reg->aliased_state);
    
    print_aliased_state(reg);
                    
    RBMM_DEBUG_NO_NL(DEBUG_ANAL, "  Alive in BB: ");
    FOR_EACH_VEC_ELT(basic_block, reg->live_bbs, ii, bb)
      RBMM_DEBUG_PLAIN(DEBUG_ANAL, "%d ", bb->index);
    RBMM_DEBUG_NL();

    FOR_EACH_VEC_ELT(ssa_name_info_t, reg->names, ii, info)
      RBMM_DEBUG(DEBUG_ANAL,
                 "  Name: %s.%s ID(%d) "
                 "Aliases[Global(%d) Param(%d) Ret(%d) Alloc(%d)]",
                 (info->base) ? get_ssaname(info->base) : "",
                 get_ssaname(info),
                 info->unified_id,
                 ALIASED_BY_GLOBAL_P(info),
                 ALIASED_BY_PARAM_P(info),
                 ALIASED_BY_RETURN_P(info),
                 ALIASED_BY_ALLOC_P(info));
}


void print_region_info(VEC(region_t,heap) *regions)
{
    unsigned ii;
    region_t reg;

    for (ii=0; VEC_iterate(region_t, regions, ii, reg); ++ii)
      print_region(reg);
    
}


/* Returns the added (new) param decl
 * NOTE: Most of this was lifted from gcc/ipa-prop.c
 * ipa_modify_formal_parameters
 * Returns the region_var (SSA_NAME)
 */
static tree add_initial_arg(tree fn_decl)
{
    tree new_param, new_types, new_fntype, variant, reg_var;

    new_param = build_decl(
        UNKNOWN_LOCATION, PARM_DECL, NULL_TREE, __rbmm_region_type);

    DECL_NAME(new_param) = create_tmp_var_name("MATT");
    DECL_ARTIFICIAL(new_param) = 1;
    DECL_ARG_TYPE(new_param) = __rbmm_region_type;
    DECL_CONTEXT(new_param) = fn_decl;
    TREE_USED(new_param) = 1;
    DECL_IGNORED_P(new_param) = 1;

    /* Give er a default def */
    reg_var = make_ssa_name(new_param, gimple_build_nop());
    set_default_def(new_param, reg_var);

    layout_decl(new_param, 0);
    add_referenced_var(new_param);
    mark_sym_for_renaming(new_param);

    tree *lnk = &DECL_ARGUMENTS(fn_decl);
    *lnk = new_param;
    lnk = &DECL_CHAIN(new_param);

    new_types = NULL;
    new_types = tree_cons(NULL_TREE, __rbmm_region_type, new_types);
    tree new_types_rev = nreverse(new_types);

    if (new_types_rev)
      TREE_CHAIN(new_types) = void_list_node;
    else
      new_types_rev = void_list_node;

    new_fntype = build_distinct_type_copy(TREE_TYPE(fn_decl));
    TYPE_ARG_TYPES(new_fntype) = new_types_rev;

    variant = TYPE_MAIN_VARIANT(TREE_TYPE(fn_decl));
    if (TREE_TYPE(fn_decl) != variant)
    {
        TYPE_MAIN_VARIANT(new_fntype) = variant;
        TYPE_NEXT_VARIANT(new_fntype) = TYPE_NEXT_VARIANT(variant);
        TYPE_NEXT_VARIANT(variant) = new_fntype;
    }
    else
    {
        TYPE_MAIN_VARIANT(new_fntype) = variant;
        TYPE_NEXT_VARIANT(new_fntype) = NULL;
    }

    TREE_TYPE(fn_decl) = new_fntype;
    DECL_VIRTUAL_P(fn_decl) = 0;

    return reg_var;
}


/* NOTE: It seems some functions originially constructed by the Go-frontend have
 * void_type_nodes terminating their TYPE_ARG_TYPES list.  I am sure there is
 * very good reason for this.  We ignore it.  Otherwise, list_length() would be
 * nicer to use.
 */
int count_fnparams(const_tree fndecl)
{
    int n;
    const_tree arg, fntype;
    function_args_iterator fi;

    /* We need the type node, not decl node */
    if (TREE_CODE(fndecl) == FUNCTION_DECL) 
      fntype = TREE_TYPE(fndecl);
    else
      fntype = fndecl;

    n = 0;
    FOREACH_FUNCTION_ARGS(fntype, arg, fi)
      if (arg != void_type_node)
        ++n;

    return n;
}


static unsigned count_return_regions(const rbmm_fn_data_t fn)
{
    unsigned i, n_returned;
    ssa_name_to_reg_t *n_to_r;

    if (!fn->returned_parammap)
      return 0;

    n_returned = 0;
    FOR_EACH_VEC_ELT(ssa_name_to_reg_t,fn->returned_parammap->regions,i,n_to_r)
      if (n_to_r->use_as_formal_param)
        ++n_returned;

    return n_returned;
}


/* Returns true if the 'param' is the region variable for region 'reg' */
static bool is_region_var_for_region_p(const_tree param, const region_t reg)
{
    const char *postfix;

    if (reg->region_var)
      return param == SSA_NAME_VAR(reg->region_var);

    /* Ok, this blows, but we encode the region id in the variable name */
    postfix = get_name((tree)param);
    if (!(postfix = strrchr(postfix, '_')))
      return false;

    /* Compare 'RBMM_<ID>.<GCC ID>' since atoi rounds down we get 'ID' */
    return reg->id == atoi(postfix + 1);
}


/* Add a region parameter to the passed function */
void add_region_param(rbmm_fn_data_t fn, tree fn_decl, region_t reg)
{
    unsigned i, param_count;
    char name[32];
    tree param;
    ipa_parm_adjustment_vec adj;
    struct ipa_parm_adjustment *new_param;

    /* Only add if we havent already added */
    param_count = 0;
    for (param=DECL_ARGUMENTS(fn_decl); param; param=DECL_CHAIN(param))
    {
        /* Only look at params we have added */
        ++param_count;
        if (param_count > fn->n_orig_params)
          if (is_region_var_for_region_p(param, reg))
            return;
    }
    
    /* Count formal parameters */
    param_count = 0;
    for (param=DECL_ARGUMENTS(fn_decl); param; param=DECL_CHAIN(param))
      ++param_count;
    
    if (param_count == 0)
    {
        reg->region_var = add_initial_arg(fn_decl);
        return;
    }
    
    /* Prepare the param */
    adj = VEC_alloc(ipa_parm_adjustment_t, heap, param_count + 1);

    /* Preserve existing parameters */
    for (i=0, param=DECL_ARGUMENTS(fn_decl);
         param;
         ++i, param=DECL_CHAIN(param))
    {
        new_param = VEC_quick_push(ipa_parm_adjustment_t, adj, NULL);
        memset(new_param, 0, sizeof(*new_param));
        new_param->base_index = i;
        new_param->base = param;
        new_param->copy_param = 1;
    }

    /* Add a new param */
    new_param = VEC_quick_push(ipa_parm_adjustment_t, adj, NULL);
    memset(new_param, 0, sizeof(*new_param));
    new_param->type = __rbmm_region_type;
    new_param->base_index = param_count - 1;

    /* Magic */
    snprintf(name, sizeof(name), "RBMM_%d", reg->id); 
    ipa_modify_formal_parameters(fn_decl, adj, name);
    reg->region_var = new_param->reduction;
    reg->region_var = make_ssa_name(reg->region_var, gimple_build_nop());
    set_default_def(new_param->reduction, reg->region_var);
}


/* Add the region argument to the call, returning the modified call:
 * This routine is called by one of the two routines used in finding the region
 * index.
 */
static gimple add_region_arg(
    tree     fn_decl,
    gimple   call,
    region_t reg,
    int      reg_idx)
{
    int i, diff, n_to_add;
    tree var;
    gimple mod_call;

    if (reg_idx < 0)
      return NULL;

    /* Figure out how many extra region variables we need (might not have added
     * them in yet).  In cases where we are told to add region argument at
     * index 3 but we only have 2 arguments.  So we have to fill the gap with 1
     * dummy, and we can plop the known region arg into index three.
     */
    diff = (reg_idx + 1) - (gimple_call_num_args(call) + 1);
    n_to_add = diff + 1;

    /* Build the call with an additional arg and any fillers */
    if (reg_idx+1 > gimple_call_num_args(call))
    {
       mod_call = gimple_build_call(
           fn_decl, n_to_add + gimple_call_num_args(call), NULL_TREE);
    }
    else /* Else, we have enough slots, toss our puppy in and return */
    {
        /* Already set this, don't overwrite (possibly we should warn here) */
        if (gimple_call_arg(call, reg_idx) != null_pointer_node)
          return call;

        gimple_call_set_arg(call, reg_idx, reg->region_var);
        update_stmt(call);
        return call;
    }

    /* Copy args and insert the region as the last arg */
    for (i=0; i<gimple_call_num_args(call); ++i)
      gimple_call_set_arg(mod_call, i, gimple_call_arg(call, i));
   
    /* Copy dummy values to any slots we added but have no data for yet */
    for ( ; i<reg_idx; ++i)
      gimple_call_set_arg(mod_call, i, null_pointer_node);

    /* If we are setting the arg as a global, we need to create a temp, set the
     * temp variable to that of the global and then pass the temp.
     */
    //if (GLOBAL_REGION_P(reg))
    //  var = assign_global_to_temp(call);
    //else
      var = reg->region_var;

    /* And now toss in the dude we were originally trying to add */
    gimple_call_set_arg(mod_call, reg_idx, var);
    
    return mod_call;
}


/* Add a argument to an existing call: fn(a,b) becomes fn(a,b,c)
 * 'param_decl' is the parameter in the function call that this region is
 * associated to.  1 parameter, 1 associated region.
 */
gimple add_region_arg_to_fncall(region_t reg, int reg_idx, gimple call, tree param)
{
    tree fn_decl;
    gimple mod_call;
    rbmm_fn_data_t call_data;
    gimple_stmt_iterator gsi;

    /* Set a flag */
    reg->is_passed_as_argument = true;

    /* Figure out where we place this region in the func call argument list */
    fn_decl = TREE_OPERAND(gimple_call_fn(call), 0);
    call_data = find_fn_data(fn_decl);
    gcc_assert(call_data);

    if (!(mod_call = add_region_arg(fn_decl, call, reg, reg_idx)))
      return call;
    
    /* Restore lhs */
    gimple_call_set_lhs(mod_call, gimple_get_lhs(call));

    /* Replace old with new */
    gsi = gsi_for_stmt(call);
    gsi_replace(&gsi, mod_call, true);

    return mod_call;
}


/* Basic block identifier, and what other blocks it can access */
struct _reach_bb_d {unsigned bb_id; bitmap reaches;};
typedef struct _reach_bb_d *reach_bb_t;
DEF_VEC_P(reach_bb_t);
DEF_VEC_ALLOC_P(reach_bb_t, gc);


/* Function and each basic block reachability map */
struct _reach_d {unsigned fn_id; VEC(reach_bb_t,gc) *bb_reaches;};
typedef struct _reach_d *reach_t;
DEF_VEC_P(reach_t);
DEF_VEC_ALLOC_P(reach_t, gc);
VEC(reach_t, gc) *_all_reaches;


/* Given a basic block, generate a map (reaches) and set each basic block index
 * on the map as 1: reachable or 0: non-reachable
 */
static void calc_reachability(basic_block bb, bitmap reaches)
{
    edge e;
    edge_iterator ei;

    FOR_EACH_EDGE(e, ei, bb->succs)
    {
        if (bitmap_bit_p(reaches, e->dest->index))
          continue;
        bitmap_set_bit(reaches, e->dest->index);
        calc_reachability(e->dest, reaches);
    }
}


/* Compute the reachablity of each basic block in fn */
void compute_reachability(const struct function *fn)
{
    basic_block bb;
    reach_t fn_bb_reaches;
    reach_bb_t bb_map;
        
    fn_bb_reaches = XCNEW(struct _reach_d);
    fn_bb_reaches->fn_id = DECL_UID(fn->decl);
    VEC_safe_push(reach_t, gc, _all_reaches, fn_bb_reaches);

    FOR_EACH_BB_FN(bb, fn)
    {
        bb_map = XCNEW(struct _reach_bb_d);
        bb_map->bb_id = bb->index;
        bb_map->reaches = BITMAP_GGC_ALLOC();
        calc_reachability(bb, bb_map->reaches);
        VEC_safe_push(reach_bb_t, gc, fn_bb_reaches->bb_reaches, bb_map);
    }
}


const_bitmap get_reaches(const struct function *fn, const_basic_block bb)
{
    unsigned i, j;
    reach_t reach;
    reach_bb_t bb_map;

    FOR_EACH_VEC_ELT(reach_t, _all_reaches, i, reach)
      if (DECL_UID(fn->decl) == reach->fn_id)
        FOR_EACH_VEC_ELT(reach_bb_t, reach->bb_reaches, j, bb_map)
          if (bb_map->bb_id == bb->index)
            return bb_map->reaches;

    return NULL;
}


/* Test if we can get from src to dest */
bool can_reach(const_basic_block src, const_basic_block dest)
{
    unsigned i, j;
    reach_t reach;
    reach_bb_t bb_map;
    
    if (!src || !dest)
      return false;

    FOR_EACH_VEC_ELT(reach_t, _all_reaches, i, reach)
    {
        if (reach->fn_id != DECL_UID(cfun->decl))
          continue;

        FOR_EACH_VEC_ELT(reach_bb_t, reach->bb_reaches, j, bb_map)
        {
            if (bb_map->bb_id != src->index)
              continue;
            return bitmap_bit_p(bb_map->reaches, dest->index);
        }
    }

    return false;
}


/* Returns true if an operand in 'stmt' is also in the region 'reg' */
static bool operands_in_region_p(const_gimple stmt, const region_t reg)
{
    unsigned i, j;
    tree base, member;
    ssa_name_info_t info;

    for (i=0; i<gimple_num_ops(stmt); ++i)
      FOR_EACH_VEC_ELT(ssa_name_info_t, reg->names, j, info)
      {
          base = get_base(gimple_op(stmt, i));
          member = get_member(gimple_op(stmt, i));
          if (base == info->ssaname || member == info->ssaname)
            return true;
      }

    return false;
}


/* Starting at 'bb' return TRUE if bb is in the last_live vector of reg */
bool region_live_after_p(const region_t reg, gimple stmt)
{
    unsigned i;
    basic_block bb, test;
    gimple_stmt_iterator gsi;

    bb = gimple_bb(stmt);

    FOR_EACH_VEC_ELT(basic_block, reg->live_bbs, i, test)
    {
        /* If we find ourself, see if we are used later in the same block */
        if (bb == test)
        {
            /* Each stmt after stmt in */
            gsi = gsi_for_stmt(stmt);
            gsi_next(&gsi);
            for ( ; !gsi_end_p(gsi); gsi_next(&gsi))
              if (operands_in_region_p(gsi_stmt(gsi), reg))
                return true;
        }
        else if (can_reach(bb, test))
          return true;
    }

    return false;
}


/* Move the creation point for region 'reg' to basic block 'bb' */
void move_region_creation(region_t reg, basic_block bb)
{
    gimple stmt;
    gimple_stmt_iterator gsi;

    /* Remove region from its initial creation point */
    stmt = SSA_NAME_DEF_STMT(reg->region_var);
    gsi = gsi_for_stmt(stmt);

    /* Plop her in bb */
    gsi_move_to_bb_end(&gsi, bb);
}


void init_global_region(void)
{
    /* Only one global region should exist for the entire program */
    if (global_region)
      return;

    global_region = new_region_info(NULL, -666);
    global_region->aliased_state = ALIASED_BY_GLOBAL;
}


/* Given a function type (not decl) check to see if any args can be regions */
static bool has_potential_region_args_and_ret(const_tree fntype)
{
    const_tree param;
    function_args_iterator fi;
    const_tree fn = fntype;

    if (POINTER_TYPE_P(fn))
      fn = TREE_TYPE(fn);

    if (REGION_CANDIDATE_TYPE_P(TREE_TYPE(fn)))
      return true;

    FOREACH_FUNCTION_ARGS(fn, param, fi)
      if (REGION_CANDIDATE_TYPE_P(param))
        return true;

    return false;
}


/* Global vector of trampoline function decl nodes */
typedef struct _tramp_to_fn_d
{
    tree decl;  /* Original fndecl                                      */
    tree tramp; /* New fndecl (trampoline which wraps and calls fndecl) */
} tramp_to_fn_t;
DEF_VEC_O(tramp_to_fn_t);
DEF_VEC_ALLOC_O(tramp_to_fn_t, gc);
VEC(tramp_to_fn_t,gc) *tramp_fns;


/* Given the trampoline type find the original fn declaration type */
tree find_tramp_orig_fndecl(const_tree orig_fndecl)
{
    unsigned i;
    tramp_to_fn_t *tramp_to_fn;

    FOR_EACH_VEC_ELT(tramp_to_fn_t, tramp_fns, i, tramp_to_fn)
      if (orig_fndecl == TREE_TYPE(tramp_to_fn->tramp))
        return tramp_to_fn->decl;

    return NULL_TREE;
}


/* Given a fndecl find it's associated trampoline (if it has one) */
tree find_tramp_fndecl(const_tree fndecl)
{
    unsigned i;
    tramp_to_fn_t *tramp_to_fn;

    FOR_EACH_VEC_ELT(tramp_to_fn_t, tramp_fns, i, tramp_to_fn)
      if (fndecl == tramp_to_fn->decl)
        return tramp_to_fn->tramp;

    return NULL_TREE;
}


/* Given a fndecl type, look for the corresponding trampoline */
tree find_tramp_fntype(const_tree fntype)
{
    unsigned i;
    tramp_to_fn_t *tramp_to_fn;

    FOR_EACH_VEC_ELT(tramp_to_fn_t, tramp_fns, i, tramp_to_fn)
      if (fntype == TREE_TYPE(tramp_to_fn->decl))
        return tramp_to_fn->tramp;

    return NULL_TREE;
}


/* Given a fndecl and index (starting at zero) return the param decl */
tree get_param(tree fndecl, int param_idx)
{
    int i;
    tree param;

    i = 0;
    for (param=DECL_ARGUMENTS(fndecl); param; param=DECL_CHAIN(param))
      if (i == param_idx)
        return param;
      else
        ++i;

    return NULL_TREE;
}


/* Get the index for the parameter at index 'param_idx'
 * We then assume worse case, where fn has 1 arg per ptr argument and
 * return.  We just go through each of the original (non-region) params to 'fn'
 * and once we hit the requested index, we pass the worse case index for its
 * corresponding region parameter.
 * Returns -1 on error
 */
static int worse_case_idx(rbmm_fn_data_t fn, int param_idx)
{
    int i, has_return_region, n_regions;
    tree param;
    function_args_iterator fi;

    /* If fn returns a ptr we assume a region for that */
    has_return_region = 
        REGION_CANDIDATE_TYPE_P(TREE_TYPE(TREE_TYPE(fn->func_decl)));

    /* Count the number of possible region params up to 'param_idx' */
    i = n_regions = 0;
    FOREACH_FUNCTION_ARGS(TREE_TYPE(fn->func_decl), param, fi)
    {
        if (i > (fn->n_orig_params - 1))
          break;

        n_regions += REGION_CANDIDATE_TYPE_P(param);

        if (i == param_idx)
          break;
        ++i;
    }

    return (fn->n_orig_params - 1) + has_return_region + n_regions;
}


/* Looks something like:
 *
 * __rbmm_tramp_N(tramp_arg_t arg)
 * {
 *      return original_fnptr(args, region_args);
 * }
 */
static tree create_trampoline_body(tree fndecl, tree orig_fndecl)
{
    int i, idx;
    bool needs_region_for_return;
    rbmm_fn_data_t call_data;
    tree stmts, arg, orig_call, ret_var, ret_stmt, type;
    VEC(tree,gc) *args;
  
    /* Information about the function about to be called (the fn that is wrapped
     * by this trampoline)
     */ 
    call_data = find_fn_data(orig_fndecl);
    gcc_assert(call_data);

    /* Create space for worst case number of args (we will remove all NULL_TREE
     * nodes after we add the args)
     */
    args = VEC_alloc(tree, gc, 0);
    for (i=0; i<count_fnparams(fndecl); ++i)
      VEC_safe_push(tree, gc, args, NULL_TREE);

    /* Add in the original (non region) arguments */
    for (i=0; i<call_data->n_orig_params; ++i)
    {
        arg = get_param(fndecl, i);
        VEC_replace(tree, args, i, arg);
    }

    /* If the function being called needs a region for its return value */
    needs_region_for_return = (count_return_regions(call_data) > 0);
    /*
    if (needs_region_for_return)
      VEC_safe_push(tree, gc, args, NULL_TREE);
      */

    /* Now toss in region arguments: (these come after original args and
     * optional arg for return region).  The 'else' branch considers the case
     * where a parameter is removed from the actual function, such is a case
     * when we pass in a pointer to the function, but it doesnt need a region.
     * Since this function trampoline (fndecl) assumes worse case every pointer
     * parameter gets a region, it (fndecl) has mis-matched arguments from
     * (call_data).
     */
    for (i=0; i<call_data->n_orig_params; ++i)
    {
        idx = worse_case_idx(call_data, i);
        arg = get_param(fndecl, idx);
        VEC_replace(tree, args, idx, arg);
    }

    /* Toss in the argument for the return region(s) */
    if (needs_region_for_return)
    {
        /* Return region arg is alwas the FIRST arg after the original args */
        arg = get_param(fndecl, call_data->n_orig_params);
        VEC_replace(tree, args, call_data->n_orig_params, arg);
    }

    /* Remove all NULL_TREE args */
    FOR_EACH_VEC_ELT(tree, args, i, arg)
      if (arg == NULL_TREE)
        VEC_ordered_remove(tree, args, i);
    
    stmts = NULL_TREE;
    orig_call = build_call_expr_loc_vec(UNKNOWN_LOCATION, orig_fndecl, args);
    
    /* If we have a non-void return type, wire-in the return of the original
     * function with the return of the wrapping trampoline.
     */
    ret_var = ret_stmt = NULL_TREE;
    if (TREE_TYPE(TREE_TYPE(fndecl)) != void_type_node)
    {
        ret_var = build2(MODIFY_EXPR, type, DECL_RESULT(fndecl), orig_call);
        ret_stmt = build1(RETURN_EXPR, type, ret_var);
    }

    /* Call the original function */
    if (ret_stmt == NULL_TREE)
      append_to_statement_list(orig_call, &stmts);

    /* Add in the return statement */
    append_to_statement_list(ret_stmt, &stmts);
    return stmts;
}


/* Create a tree TYPE node for a trampoline to the passed function decl.
 * Basically, we preserve the original arguments and then add an additional
 * argument for each pointer and if the return-type is a pointer.
 * The 'fndecl' arg is optional.  If that is passed we can make a better
 * assessment for the number of region formal parameter to add to the type being
 * created.
 */
tree create_trampoline_type(tree fndecl)
{
    int i, n_params, n_region_params;
    tree param, fntype, param_list, rettype;
    VEC(tree,gc) *params;
    rbmm_fn_data_t call_data;
    function_args_iterator fi;

    /* Get type and decl nodes */
    if (TREE_CODE(fndecl) == FUNCTION_DECL)
      fntype = TREE_TYPE(fndecl);
    else if (TREE_CODE(fndecl) == FUNCTION_TYPE)
    {
        fntype = fndecl;
        fndecl = NULL_TREE; /* DECL not given we cant query its analysis data */
    }

    /* Generate arguments that we use as formal params to this tramp
     * We must use TREE_ARG_TYPES not DECL_ARGUMENTS as this might be external.
     */
    n_params = count_fnparams(fntype);
    params = VEC_alloc(tree, gc, 0);
    FOREACH_FUNCTION_ARGS(fntype, param, fi)
      if (param != void_type_node)
        VEC_safe_push(tree, gc, params, param);

    /* Figure out how many region args this will need (we might have already
     * transformed this function (fndecl) so only count the original params)
     */
    call_data = fndecl ? find_fn_data(fndecl) : NULL;
    i = n_region_params = 0;
    FOREACH_FUNCTION_ARGS(fntype, param, fi)
      if (call_data && (i++ >= call_data->n_orig_params))
        break;
      else if (REGION_CANDIDATE_TYPE_P(param) && param != __rbmm_region_type)
        ++n_region_params;

    /* Count the return type too */
    if (REGION_CANDIDATE_TYPE_P(TREE_TYPE(fntype)))
      ++n_region_params;

    /* Since a trampoline has the worst case (all pointers and return pointer
     * must have an associated region) see how many more region args we need.
     */
    if (call_data)
      n_params = (call_data->n_orig_params + n_region_params) - n_params;
    else
      n_params = n_region_params;

    for (i=0; i<n_params; ++i)
      VEC_safe_push(tree, gc, params, __rbmm_region_type);

    /* Return type */
    rettype = TREE_TYPE(fntype);

    param_list = build_tree_list_vec(params);
    return build_function_type(rettype, param_list);
}


/* Create a rbmm trampoline for fndecl */
static tree create_trampoline(tree fndecl)
{
    int i;
    tramp_to_fn_t *tramp_to_fn;
    tree ssaname, body, proto, decl, resdecl, old_fn_decl, rettype;
    tree arg, param, params;
    function_args_iterator fi;
    struct function *tramp_func;
    char name[32];
    static int id;

    /* See if we already have one created for this guy */
    if ((decl = find_tramp_fndecl(fndecl)))
      return decl;

    /* Create the trampoline func, and remember it so we can reuse it */
    ++id;
    snprintf(name, 31, "__rbmm_tramp_%u", id);
    proto = create_trampoline_type(fndecl);
    decl = build_fn_decl(name, proto);
    SET_DECL_ASSEMBLER_NAME(decl, get_identifier(name));
    
    /* Add formal params associated to these args */
    i = 0;
    params = param = NULL_TREE;
    FOREACH_FUNCTION_ARGS(TREE_TYPE(decl), arg, fi)
    {
        if (arg == void_type_node)
          continue;
        snprintf(name, 31, "rbmm_arg_%d", i++);
        param = build_decl(
            BUILTINS_LOCATION, PARM_DECL, get_identifier(name), arg);
        DECL_ARTIFICIAL(param) = 1;
        DECL_ARG_TYPE(param) = arg;
        DECL_CONTEXT(param) = decl;
        TREE_USED(param) = 1;
        DECL_CHAIN(param) = params;
        create_var_ann(param);
        ssaname = make_ssa_name(param, gimple_build_nop());
        set_default_def(param, ssaname);
        params = param;
    }
    DECL_ARGUMENTS(decl) = nreverse(params);
    
    /* Create the rbmm_fn_data_t struct, as if we analyized this trampoline */
    create_tramp_fndata(decl);

    /* Return value */
    rettype = TREE_TYPE(TREE_TYPE(fndecl));
    resdecl = build_decl(BUILTINS_LOCATION, RESULT_DECL, NULL_TREE, rettype);
    DECL_ARTIFICIAL(resdecl) = 1;
    DECL_CONTEXT(resdecl) = decl;
    DECL_RESULT(decl) = resdecl;
   
    /* Func decl */
    TREE_USED(decl) = 1;
    TREE_PUBLIC(decl) = 1;
    TREE_STATIC(decl) = 1;
    DECL_ARTIFICIAL(decl) = 1;
    DECL_UNINLINABLE(decl) = 1;
    DECL_EXTERNAL(decl) = 0;
    DECL_PRESERVE_P(decl) = 1;

    /* Initial body */
    body = make_node(BLOCK);
    TREE_USED(body) = 1;
    DECL_INITIAL(decl) = body;
    DECL_SAVED_TREE(decl) = create_trampoline_body(decl, fndecl);

    /* Make the function */
    push_struct_function(decl);
    tramp_func = cfun;
    old_fn_decl = current_function_decl;
    current_function_decl = decl;
    
    /* Now that we have an empty trampoline function, gimplify a body */
    cfun->function_end_locus = BUILTINS_LOCATION;
    gimplify_function_tree(decl);
    cgraph_add_new_function(decl, false);
    cgraph_mark_needed_node(cgraph_get_node(decl));
    pop_cfun();
    cgraph_process_new_functions();
    current_function_decl = old_fn_decl;

    /* Update the cgraph edges for the internal representation */
    push_cfun(tramp_func);
    old_fn_decl = current_function_decl;
    current_function_decl = decl;
    rebuild_cgraph_edges();
    pop_cfun();
    current_function_decl = old_fn_decl;

    /* Add to a vec of all junk funs we maintain */
    tramp_to_fn = VEC_safe_push(tramp_to_fn_t, gc, tramp_fns, NULL);
    tramp_to_fn->decl = fndecl; /* Original   */
    tramp_to_fn->tramp = decl;  /* Trampoline */

    return decl;
}


/* Work trampoline functionality for a fnptr GIMPLE_ASSIGN */
static void _trampoline_assign_bn(rbmm_fn_data_t fn, gimple stmt)
{
    tree addr, lhs, tramp;
    ssa_name_info_t info;

    lhs = gimple_get_lhs(stmt);
    
    if (!is_fnptr(get_base(lhs)))
      return;

    if (!has_potential_region_args_and_ret(TREE_TYPE(gimple_call_fn(stmt))))
      return;

    /* If the LHS is global do not create a trampoline */
    info = find_ssa_name_info(fn, get_base(lhs), NULL);
    if (info && GLOBAL_REGION_P(info))
    {
        /* If the call has been compiled with regions.  And we are setting LHS
         * to it and the LHS is global (and it is global since we are here), the
         * call might be passed to an EXTERNAL library which was not compiled
         * with regions.  Therefore clone the function (specialze) one for
         * regions and one without regions
         */
        return;
    }

    /* If we do not have data, it must be a call to external lib */
    if (!find_fn_data(TREE_OPERAND(gimple_call_fn(stmt), 0)))
    {
        /* Specialize gimple_call_fn() to use all global region
         * assign that in the statement and return
         */
        return;
    }

    /* Create a trampoline for this */
    tramp = create_trampoline(TREE_OPERAND(gimple_assign_rhs1(stmt), 0));

    /* s/lhs/trampoline_type/ */
    lhs = gimple_get_lhs(stmt);
    TREE_TYPE(TREE_TYPE(lhs)) = TREE_TYPE(tramp);
    gimple_replace_lhs(stmt, lhs);

    /* s/rhs/trampoline/ */
    addr = build_addr(tramp, NULL_TREE);
    gimple_assign_set_rhs1(stmt, addr);
}


/* Work trampoline functionality for a GIMPLE_CALL (args are fnptrs) */
static void _trampoline_call_bn(gimple stmt)
{
    unsigned i;
    bool updated;
    tree arg, addr, tramp;

    /* Sooooo.... scour the args looking for fnptrs */
    updated = false;
    for (i=0; i<gimple_call_num_args(stmt); ++i)
      if (is_fnptr((arg=gimple_call_arg(stmt, i))))
      {
          /* If we do not know or have call data for the function, it must be
           * external from a shared library, and we cant modify that code
           */
          if (SSA_VAR_P(arg) || !find_fn_data(TREE_OPERAND(arg, 0)))
          {
              /* TODO:Copy 'arg' function, make it all global */
              /* gimple_set_arg(non-specialized fnptr) */
              continue;
          }
          tramp = create_trampoline(TREE_OPERAND(arg, 0));
          addr = build_fold_addr_expr(tramp);
          gimple_call_set_arg(stmt, i, addr);
          updated = true;
      }

    if (updated)
      update_stmt(stmt);
}

 
/* Given a statement determine if its a call or assignment.  And for each
 * function pointer replace it with a trampoline.
 */
void trampoline_bn(rbmm_fn_data_t fn, gimple stmt)
{
    /* If assigning and the RHS is a addr_expr , it must be an actual fn */
    if (gimple_code(stmt) == GIMPLE_ASSIGN &&
       (TREE_CODE(gimple_op(stmt, 1)) == ADDR_EXPR))
      _trampoline_assign_bn(fn, stmt);
    else if (gimple_code(stmt) == GIMPLE_CALL)
      _trampoline_call_bn(stmt);
}


/* Construct a map */
void construct_map_bn(region_t reg, gimple stmt)
{
    int i;
    gimple call;
    gimple_stmt_iterator gsi;
	VEC(tree,heap) *args;

    args = VEC_alloc(tree, heap, 7);
    VEC_quick_push(tree, args, reg->region_var);
    for (i=0; i<6; ++i)
      VEC_quick_push(tree, args, gimple_call_arg(stmt, i));

    call = gimple_build_call_vec(construct_map_fndecl, args);
    gsi = gsi_for_stmt(stmt);
    gimple_call_set_lhs(call, gimple_call_lhs(stmt));
    gsi_replace(&gsi, call, true);
    VEC_free(tree, heap, args);
}


void print_trampolines(void)
{
    unsigned i;
    tramp_to_fn_t *tramp;

    FOR_EACH_VEC_ELT(tramp_to_fn_t, tramp_fns, i, tramp)
      debug_function(tramp->tramp, 0);
}
